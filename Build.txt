1. 
https://drive.google.com/drive/folders/0B9pvz6IuOetOVGp3c2NyX2dZWmM から 
plr2_lib_generic-<version>.jar をダウンロードして 
libs/ に置いてください。

2. ./gradlew shadowJar を実行してください。

3. java -jar build/libs/SemGraph-*-all.jar を実行してください。


下記の環境でビルドを確認しました。

------------------------------------------------------------
Gradle 4.10.1
------------------------------------------------------------

Build time:   2018-09-12 11:33:27 UTC
Revision:     76c9179ea9bddc32810f9125ad97c3315c544919

Kotlin DSL:   1.0-rc-6
Kotlin:       1.2.61
Groovy:       2.4.15
Ant:          Apache Ant(TM) version 1.9.11 compiled on March 23 2018
JVM:          11.0.1 (Oracle Corporation 11.0.1+13-Ubuntu-3ubuntu3.18.10.1)
OS:           Linux 4.18.0-16-generic amd64

openjdk version "11.0.1" 2018-10-16
OpenJDK Runtime Environment (build 11.0.1+13-Ubuntu-3ubuntu3.18.10.1)
OpenJDK 64-Bit Server VM (build 11.0.1+13-Ubuntu-3ubuntu3.18.10.1, mixed mode, sharing)

