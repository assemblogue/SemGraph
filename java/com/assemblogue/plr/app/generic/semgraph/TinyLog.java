package com.assemblogue.plr.app.generic.semgraph;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class TinyLog {

	/** ログレベル */
	private static final int NONE_LEVEL = 4;
	private static final int ERROR_LEVEL = 3;
	private static final int INFO_LEVEL  = 2;
	private static final int DEBUG_LEVEL = 1;
	private static final int TRACE_LEVEL = 0;

	/** 日時フォーマット */
	private static final String DATETIME_FORMAT = "yyyy/MM/dd HH:mm:ss.SSS";

	//FIXME
	/** ログレベル指定 */
	private static final int logLevel = NONE_LEVEL;

	/**
	 * Errorレベルログ出力
	 * @param thSTE StackTraceElement 大体は new Throwable().getStackTrace()[0] を指定
	 * @param flg Exception
	 * @param str 表示文字列
	 */
	public static void error(StackTraceElement thSTE, boolean flg, String str) {

		if (logLevel > ERROR_LEVEL) {
			return;
		}

		logOut(thSTE, flg, "ERROR", str);

	}

	/**
	 * Infoレベルログ出力
	 * @param thSTE StackTraceElement 大体は new Throwable().getStackTrace()[0] を指定
	 * @param flg Exception
	 * @param str 表示文字列
	 */
	public static void info(StackTraceElement thSTE, boolean flg, String str) {

		if (logLevel > INFO_LEVEL) {
			return;
		}

		logOut(thSTE, flg, "INFO ", str);

	}

	/**
	 * debugレベルログ出力
	 * @param thSTE StackTraceElement 大体は new Throwable().getStackTrace()[0] を指定
	 * @param flg Exception
	 * @param str 表示文字列
	 */
	public static void debug(StackTraceElement thSTE, boolean flg, String str) {

		if (logLevel > DEBUG_LEVEL) {
			return;
		}

		logOut(thSTE, flg, "DEBUG", str);

	}

	/**
	 * traceレベルログ出力
	 * @param thSTE StackTraceElement 大体は new Throwable().getStackTrace()[0] を指定
	 * @param flg Exception
	 * @param str 表示文字列
	 */
	public static void trace(StackTraceElement thSTE, boolean flg, String str) {

		if (logLevel > TRACE_LEVEL) {
			return;
		}

		logOut(thSTE, flg, "TRACE", str);

	}

	/**
	 * ログをコンソールに出力
	 * @param thSTE
	 * @param flg
	 * @param level
	 * @param str
	 */
	private static void logOut(StackTraceElement thSTE, boolean flg, String level, String str) {

		String threadName = Thread.currentThread().getName();

		StringBuilder sb = new StringBuilder()
				.append(toStrDateTime(LocalDateTime.now(), DATETIME_FORMAT))
				.append(" [")
				.append(threadName)
				.append("] ")
				.append(level)
				.append(' ')
				.append(thSTE.getClassName())
				.append('#')
				.append(thSTE.getMethodName())
				.append('(')
				.append(thSTE.getLineNumber())
				.append(") ")
				.append(str);

		System.out.println(sb.toString());

		if(flg) {
			(new Throwable()).printStackTrace();
		}
	}

	/**
	 * 日時フォーマット
	 * @param localDateTime
	 * @param format
	 * @return
	 */
    private static String toStrDateTime(LocalDateTime localDateTime, String format) {

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(format);
        return localDateTime.format(dateTimeFormatter);

    }

}
