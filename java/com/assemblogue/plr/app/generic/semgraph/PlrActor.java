package com.assemblogue.plr.app.generic.semgraph;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.assemblogue.plr.app.lib.ProfileManager;
import com.assemblogue.plr.app.lib.SystemAccount;
import com.assemblogue.plr.contentsdata.ontology.OntologyItem;
import com.assemblogue.plr.io.PlrEntry;
import com.assemblogue.plr.io.PlrEntry.Origin;
import com.assemblogue.plr.io.Storage;
import com.assemblogue.plr.lib.EntityNode;
import com.assemblogue.plr.lib.EntityNode.SyncCallback;
import com.assemblogue.plr.lib.FileNode;
import com.assemblogue.plr.lib.LiteralNode;
import com.assemblogue.plr.lib.Node;
import com.assemblogue.plr.lib.NodeNotFoundException;
import com.assemblogue.plr.lib.PLR;
import com.assemblogue.plr.lib.model.Channel;
import com.assemblogue.plr.lib.model.Friend;
import com.assemblogue.plr.lib.model.PlrAccount;
import com.assemblogue.plr.lib.model.ProfileItem;
import com.assemblogue.plr.lib.model.Root;
import com.assemblogue.plr.lib.model.Timeline.TimelineState;
import com.assemblogue.plr.lib.model.dataSetting.DataSetting;
import com.assemblogue.plr.lib.model.dataSetting.Schema;
import com.assemblogue.plr.util.NodeSchemas;

import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.HBox;
import javafx.stage.StageStyle;

/**
 * PLR2操作レイヤ
 * コマンドシェル処理を参考に、必要最小限の処理を実装。
 * @arrenged <a href="mailto:kaneko@cri-mw.co.jp">KANEKO, yukinori</a>
 */
public class PlrActor {

	//private Logger logger = LogManager.getLogger();

	// 暗号化設定（true：暗号化する、false：暗号化しない）
	private static final boolean ENCRYPT_FLAG = true;

	private static final String LANG_NONE_SYMBOL = "*";

	private boolean err_print = false;
	private boolean out_print = false;

	private PLR plr;
	private Storage storage;
	private static Boolean passphrase = false;
	private static Boolean passphrase_waiting = false;
	private String passphraseValue;

	/** PLR-ID (プロファイル取得時の識別に使用） */
	private String plrId;
	public String getPlrId() {
		return plrId;
	}

	private String storagetype;
	public String getStoragetype() {
		return storagetype;
	}

	private static Boolean friend_setup = false;

	public class NodeName {
		public String name;
		String id;
		public String uri;
		public EntityNode node;

		NodeName(String name, String id, String uri, EntityNode node) {
			this.name = name;
			this.id = id;
			this.uri = uri;
			this.node = node;
		}
	}

	// ストレージルートノード
	public NodeInfo<EntityNode> storageRootNode = null;
	// アプリルートフォルダノード
	private NodeInfo<Node> aplRootFolderNode = null;
	//for channel list AW 2020/06/15 START
	//root channel for graph
	private Channel rootChannel = null;
	//for channel list AW 2020/06/15 END

	// ノードリストテンポラリ
	private List<NodeInfo<Node>> propList = new ArrayList<>();

	/**
	 * コンストラクタ
	 */
	public PlrActor() {
	}

	public void setPlr(PLR plr) {
		this.plr = plr;
	}

	public void setStorage(Storage storage) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

 		if (storage == null) {
			return;
		}

		if (this.storage != null) {
			if (this.storage.equals(storage)) {
				return;
			}
			if (aplRootFolderNode != null) {
				aplRootFolderNode = null;
			}
		}

		friend_setup = false;
		this.storage = storage;
	}

	/**
	 * rootNode 準備
	 *
	 * @throws Exception
	 */
	public void prepare() {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		EntityNode node = null;

		try {
			TinyLog.trace(new Throwable().getStackTrace()[0], false, "PlrActor prepare getRootNode(storage) 前");

			node = plr.getRootNode(storage);
 			outPrint("Loading root node... ");
			outPrint("done.\n");

			TinyLog.trace(new Throwable().getStackTrace()[0], false, "PlrActor prepare getRootNode(storage) 後");
			passphrase = true;

		} catch (IllegalStateException e) {
			TinyLog.trace(new Throwable().getStackTrace()[0], false, "PlrActor prepare getRootNode(storage) Exception発生　鍵ペアなし");

			if (passphrase_waiting) {
				return;
			}
			passphrase_waiting = true;

			try {

				TextInputDialog textIn = new TextInputDialog();
				textIn.setTitle(Messages.getString("dialog.passphrase.title"));
				textIn.getDialogPane().setHeaderText(Messages.getString("dialog.passphrase.label"));
				textIn.initStyle(StageStyle.UTILITY);
				TxtList.set("passphrase for " + getUserID());
				textIn.setHeaderText(getUserID());
				PasswordField pwd = new PasswordField();
				HBox content = new HBox();
				content.setAlignment(Pos.CENTER_LEFT);
				content.setSpacing(10);
				content.getChildren().addAll(pwd);
				textIn.getDialogPane().setContent(content);
				textIn.setResultConverter(dialogButton -> {
					if (dialogButton == ButtonType.OK) {
 						passphraseValue = pwd.getText().toString();
 						return pwd.getText();
					}
					return null;
				});

				String str = textIn.showAndWait().orElse("");
				storage.postPassphrase(str);
 				node = plr.getRootNode(storage);
				passphrase = true;
				passphrase_waiting = false;
			} catch (Exception ee) {
				passphrase_waiting = false;
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Passphrase wrong");
				alert.setContentText("Please enter the correct passphrase");
				errPrint("Error: " + ee.getLocalizedMessage());
				return;
			}
		} catch (Exception e) {
			errPrint("Error: " + e.getLocalizedMessage());
			return;
		}

		if (node == null) {
			outPrint("Cancelled");
			return;
		}

		if (storageRootNode == null) {
			TinyLog.trace(new Throwable().getStackTrace()[0], false, "PlrActor prepare storageRootNode = NULL");

			storageRootNode = new NodeInfo<>(node, "root");
		} else {
			TinyLog.trace(new Throwable().getStackTrace()[0], false, "PlrActor prepare storageRootNode != NULL");

			storageRootNode.set(node, "root");
		}

		if (!friend_setup) {
			friend_setup = true;
			setpupFriends();
		}

		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆END");
	}

	/**
	 * 同期
	 * 指定ノードの同期をとる
	 * 指定が無い場合、rootNodeと同期をとる
	 *
	 * @param node
	 */
	public synchronized void sync(EntityNode node) {
		TinyLog.debug(new Throwable().getStackTrace()[0], false, "◆START sync nodeId = " + getId(node));

		SyncHandler h = new SyncHandler();
		synchronized (h) {
			try {
				if (node == null) {
					if (storageRootNode == null) {
						//logger.debug("◆sync内 prepare 同期しないで終了");
 						prepare();
						return;
					}
					node = storageRootNode.node;
					//logger.debug("◆sync内 storageRootNodeで同期 nodeId={}, nodeName={}", getId(node), getName(node));
				}

				//logger.trace("◆node.sync前");
				node.sync(true, h);
				//logger.trace("◆node.sync後");

				// notify()まで待つ

				//logger.trace("◆h.wait前");
				h.wait();
				//logger.trace("◆h.wait後");

			} catch (InterruptedException e) {
				errPrint("Error: " + e.getLocalizedMessage());
			}
		}

		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆END sync");
	}

	/**
	 * 同期
	 */
	private class SyncHandler extends HandlerBase implements SyncCallback {
		private boolean updated;

		@Override
		public synchronized void onUpdate(EntityNode node, Origin origin) {
			TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START onUpdate");
			if (origin != null && "LOCAL".equals(origin.toString())) {
				//logger.debug("onUpdate: notify. origin={}", origin.toString());
				notify();
			} else {
				//logger.debug("onUpdate: no notify. origin={}", origin.toString());
			}
			updated = true;

			TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆END onUpdate");
		}

		@Override
		public synchronized void onFinish(EntityNode node) {
			TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START onFinish");
			if (updated) {
			}
			// wait解放：sync終了まで排他するため
			notify();

			TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆END onFinish");;
		}
	}

	/**
	 * 同期
	 * 指定ノードの同期をとる
	 * @param node
	 */
	public void syncSimple(EntityNode node) {

		// 2021/07 AW start 同期中でも同期を投げて良いので、判定しなくする
		// 同期中の場合は何もせず戻る
		//if (node.inSync()) {
		//	TinyLog.debug(new Throwable().getStackTrace()[0], false, "◆START syncSymple NO sync" + node);
		//} else {
			TinyLog.debug(new Throwable().getStackTrace()[0], false, "◆START syncSymple sync" + node);
			node.sync();
		//}
		// 2021/07 AW end 同期中でも同期を投げて良いので、判定しなくする
	}



	/**
	 * ストレージのUserIDを取得する
	 *
	 * @return
	 */
	public String getUserID() {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		try {
			return storage.getUserId();
		} catch (Exception e) {
			errPrint("Error: " + e.getLocalizedMessage());
		}

		return null;
	}

	/**
	 * アプリルートフォルダノードの取得
	 * ストレージルート直下のアプリ用ルートフォルダノードを返す
	 * なければ作成する
	 * @return アプリルートフォルダノード
	 */
	public EntityNode getAplRootFolderNode() {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		if (aplRootFolderNode != null) {
			// 取得済
			return aplRootFolderNode.node.asEntity();
		}

		try {
			EntityNode storage_root = plr.getRootNode(storage);
			if (storage_root == null) {
				// まだストレージの準備ができていない。
				return null;
			}
		} catch (Exception e) {
			errPrint("Error: " + e.getLocalizedMessage());
			return null;
		}

		TxtList.debug("PlrAct:getAplRootFolderNode: sync");
		while (true) {
			sync(null);
			list(null, null);
			for (NodeInfo ni : propList) {
				if (ni.name.equals(AppProperty.CAPRAIBEX_ROOT_FOLDER)) {
					aplRootFolderNode = new NodeInfo<>(ni.node, ni.name);
					return aplRootFolderNode.node.asEntity();
				}
			}

			EntityNode nwnode = createFolderNode(AppProperty.CAPRAIBEX_ROOT_FOLDER, "GraphEditorRoot");
			if (nwnode != null) {
				sync(nwnode);
				continue;
			}

			break;
		}

		return null;
	}
	// 2021/2 AW ADD start
	/**
	 *ドライブ名格納
	 *
	 */
	public void setStragetype(String stragetype) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		this.storagetype = stragetype;
	}
	// 2021/2 AW ADD end

	/**
	 *  ノードのプロパティを集める
	 *	nodegがnullの場合、ストレージノードのプロパティを集める
	 */
	public synchronized void list(List<NodeInfo<Node>> list, EntityNode node) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		if (list == null) {
			list = propList;
		}

		if (node == null) {
			if (storageRootNode == null) {
				return;
			}

			node = storageRootNode.node;
		}

		list.clear();

		for (String name : node.propertyNames()) {
			for (Node n : node.getProperty(name)) {
				list.add(new NodeInfo<>(n, name));
			}
		}
	}

	/**
	 *  ノードのプロパティを集める
	 *	nodegがnullの場合、ストレージノードのプロパティを集める
	 */
	public synchronized Map<String, Node> listToMap(EntityNode node) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		Map<String, Node> map = new HashMap<>();

		if (node == null) {
			if (storageRootNode == null) {
				return map;
			}

			node = storageRootNode.node;
		}

		for (String name : node.propertyNames()) {
			for (Node n : node.getProperty(name)) {
				map.put(name, n);
			}
		}

		return map;
	}

	/**
	 * ノードのリテラルを取得
	 * ノードの指定が無い場合、ストレージノードのリテラルを取得
	 * @return
	 */
	public synchronized Map<String, String> getliterals(EntityNode node) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		Map<String, String> map = new HashMap<>();

		if (node == null) {
			if (storageRootNode == null) {
				return map;
			}

			node = storageRootNode.node;
		}

		if (node.inSync()) {
			// 蜷梧悄荳ｭ
			return map;
		}

		propList.clear();

		// プロパティ値の一時格納場所
		for (Map.Entry<String, List<Node>> property : node.getAllProperties().entrySet()) {
			String key = property.getKey();
			List<Node> valueNodes = property.getValue();
			for (Node valueNode : valueNodes) {
				propList.add(new NodeInfo<Node>(valueNode, key));

				if (valueNode.isLiteral()) {
					String value = String.valueOf(valueNode.asLiteral().getValue());
					if (value != null && !value.equals("null")) {
						map.put(key, value);
					}
				}
			}
		}

		return map;
	}

	public synchronized Map<String, String> getLiteralsToMap(EntityNode node) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		Map<String, String> resultMap = new HashMap<>();

		propList.clear();

		// プロパティ値の一時格納場所
		for (Map.Entry<String, List<Node>> property : node.getAllProperties().entrySet()) {
			String key = property.getKey();
			List<Node> valueNodes = property.getValue();
			for (Node valueNode : valueNodes) {
				propList.add(new NodeInfo<Node>(valueNode, key));

				if (valueNode.isLiteral()) {
					String value = String.valueOf(valueNode.asLiteral().getValue());
					if (value != null && !value.equals("null")) {
						resultMap.put(key, value);
					}
				}
			}
		}

		return resultMap;
	}

	/**
	 * プロパティの存在確認
	 * 直前に取得したプロパティリストから探す
	 * @param name プロパティ名
	 * @return
	 */
	public synchronized Boolean getproperty(String name) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		NodeInfo nodeinfo = getProperty(name);

		if (nodeinfo == null) {
			return false;
		}

		return true;
	}

	/**
	 * プロパティの存在確認
	 * nodeがnullの場合、直前に取得したプロパティリストから探す
	 * @param name プロパティ名
	 * @return
	 */
	public synchronized Boolean getproperty(EntityNode node, String name) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		if (node != null) {
			list(this.propList, node);
		}
		return getproperty(name);
	}

	/**
	 *  フォルダノード生成
	 *  ストレージルート直下に生成する
	 * @param name ノード名前
	 * @param type ノードタイプ
	 */
	private EntityNode createFolderNode(String name, String type) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		EntityNode newNode;

		try {
			//TODO encrypt修正
			boolean folder = true, encrypt = true, inhelitPermissions = true;
			encrypt = ENCRYPT_FLAG;

			newNode = newEntityOuter(name, type, folder, encrypt, inhelitPermissions);
			setLiteral(newNode, AppProperty.LT_NAME, "*", name);
		} catch (Exception e) {
			errPrint("Error: " + e.getLocalizedMessage());
			return null;
		}

		return newNode;
	}


	Collection<EntityNode> resultList = null;

	/**
	 * タイムラインノードリストを取得
	 * @param rootChannelNode
	 * @return
	 */
	public Collection<EntityNode> getTimelineItemsWithSync(EntityNode rootChannelNode) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START sync getTimeline");

		Calendar startCal = Calendar.getInstance();
		startCal.set(1970, 0, 1);

		Calendar endCal = Calendar.getInstance();
		endCal.set(2037, 11, 31);

		/*
		//2021/3 AW REP start
		DateFormat df = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT,Locale.JAPAN);
		//DateFormat df = DateFormat.getInstance();
		//2021/3 AW REP end
		String startDay = null;
		String endDay = null;
		try {
			startDay = new SimpleDateFormat(DATEFORMAT).format(df.parse("2016/1/1 00:00"));
			endDay = new SimpleDateFormat(DATEFORMAT).format(new Date());
			LocalDateTime d = LocalDateTime.parse(endDay.substring(0,endDay.lastIndexOf("+")));
			d = d.plusHours(1);
			endDay = new SimpleDateFormat(DATEFORMAT).format(df.parse(d.toString().replace("T", " ").replace("-","/")));


		} catch (ParseException e) {
			e.printStackTrace();
		}
		*/

		//return rootChannelNode.getTimelineItems(startCal, endCal, true).getItems();

		//return rootChannelNode.getTimelineItems(startCal, true, 100000, true).getItems();
		resultList = null;

		TimelineCallback timelineCallback = new TimelineCallback();
		synchronized (timelineCallback) {

			try {

				rootChannelNode.getTimelineItems(startCal, true, 100000, true, timelineCallback);
				timelineCallback.wait();

			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return resultList;
	}
	// 2021/2 AW ADD end

	/**
	 * タイムラインアイテム取得のコールバック
	 */
	private class TimelineCallback implements EntityNode.TimelineItemsCallback {

		@Override
		public synchronized void onFinish(EntityNode arg0) {
			TinyLog.debug(new Throwable().getStackTrace()[0], false, "◆START onFinish");
			notify();

		}

		@Override
		public synchronized void onError(EntityNode arg0, PlrEntry arg1, Exception arg2, Origin arg3) {
			TinyLog.debug(new Throwable().getStackTrace()[0], false, "◆START onError");
			arg2.printStackTrace();
			notify();
		}

		@Override
		public synchronized void onUpdate(EntityNode arg0, Collection<EntityNode> arg1) {
			TinyLog.debug(new Throwable().getStackTrace()[0], false, "◆START onUpdate count=" + arg1.size());
			resultList = arg1;
		}
	}


	// 2021/3 AW ADD start
	public EntityNode makeNewTimelineItem(EntityNode channelNode, String type) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		EntityNode newNode = null;

		try {
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mmZ");
			String nowAsISO = df.format(new Date());

			//TODO encrypt修正
			newNode = channelNode.newTimelineItem(type, nowAsISO, ENCRYPT_FLAG);
			//newNode = channelNode.newTimelineItem(type, nowAsISO, true);
			//newNode = folderNode.newTimelineItem(type, nowAsISO, false);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return newNode;
	}

	public static void setProperties(EntityNode targetNode, Map<String, List<Object>> propertyMap) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		try {

			for (Map.Entry<String, List<Object>> m: propertyMap.entrySet()) {

				String property = m.getKey();
				List<Object> values = m.getValue();

				// begin と end は専用のメソッドがある
				// これらの形式はStringであること、かつ、リストの行数は1個であること
				if (property.equals("begin")) {

					String dayTime = (String) values.get(0);
					targetNode.setBegin(dayTime);

				} else if (property.equals("end")) {

					String dayTime = (String) values.get(0);
					targetNode.setEnd(dayTime);

				} else {

					// 登録プロパティを削除
					// 2021/07 AW start if文追加
					if (targetNode.hasProperty(property)) {
						TinyLog.debug(new Throwable().getStackTrace()[0], false, "remove property start. =" + property);
						targetNode.removeProperty(property);
						TinyLog.debug(new Throwable().getStackTrace()[0], false, "remove property end.  =" + property);
					} else {
						TinyLog.debug(new Throwable().getStackTrace()[0], false, "NOT remove.  =" + property);
					}
					// 2021/07 AW end if文追加

					if (values == null || values.isEmpty()) {
						// 値が無い場合は、プロパティの削除のみ行う
						TinyLog.debug(new Throwable().getStackTrace()[0], false, "remove only. =" + property);
						continue;

					} else {

						// 値が無い場合は、プロパティの削除のみ行う
						boolean emptyFlag = true;
						for (Object o: values) {
							if (o == null || String.valueOf(o) == null || String.valueOf(o).equals("")) {

							} else {
								emptyFlag = false;
							}
						}
						if (emptyFlag) {
							TinyLog.debug(new Throwable().getStackTrace()[0], false, "remove only. =" + property);
							continue;
						}

						// プロパティ登録
						// 値がクラスの場合は、インナーノード
						// それ以外の場合はリテラルノードを作成
						//FIXME 上記は未対応
						TinyLog.debug(new Throwable().getStackTrace()[0], false, "new property start. =" + property);
						LiteralNode propertyNode = targetNode.newLiteral(property);

						for (Object o: values) {
							propertyNode.setValue(o);
						}
						TinyLog.debug(new Throwable().getStackTrace()[0], false, "new property end. =" + property);
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static Map<String, List<Node>> getProperties(EntityNode targetNode) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		Map<String, List<Node>> propertyMap = targetNode.getAllProperties();

		// 位置情報、リンク情報、グラフセッティングのプロパティは削除する
		propertyMap.remove("@node");
		propertyMap.remove("@link");
		propertyMap.remove("graphSetting");

		return propertyMap;
	}

	/*
	//下記のメソッドは上記で置き換え
	public static Map<String, List<Node>> getPropertiesMap(EntityNode node) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		Map<String, List<Node>> resultMap = new HashMap<>();

		if (node == null) {
			return resultMap ;
		}

		for (String name : node.propertyNames()) {

			List<Node> nodeList = new ArrayList<>();
			for (Node n : node.getProperty(name)) {
				nodeList.add(n);
			}
			resultMap.put(name, nodeList);
		}
		return resultMap;
	}
	*/

	// 2021/3 AW ADD end




	// 2021/2 AW ADD start
	/**
	 * タイムラインのitem作成
	 * @param rootNode
	 * @param classType
	 * @return EntityNodde Timelinenode
	 */
	public EntityNode createTimelineItem(EntityNode rootNode, String classType) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		EntityNode newNode = makeNewTimelineItem(rootNode, classType);

		TinyLog.debug(new Throwable().getStackTrace()[0], false, "createTimelineItem内plrId=" + this.plrId);
		TinyLog.debug(new Throwable().getStackTrace()[0], false, "createTimelineItem内storageType=" + this.storagetype);

		// リテラル作成、creatorプロパティはここで作成
		Map<String, List<Object>> literalMap = new HashMap<>();

		// 2021/07 AW start ストレージタイプが取得できない場合の対応
		//String creatorValue = this.storagetype + ":" +  getUserID();
		String creatorValue = this.plrId;
		// 2021/07 AW end ストレージタイプが取得できない場合の対応

		List<Object> creatorList = List.of(creatorValue);
		literalMap.put("creator", creatorList);
		setProperties(newNode, literalMap);

		//TODO
		//テスト用50kデータのプロパティ
		//String testData = "$".repeat(60000);
		//setLiteral(newNode, "test", "*", testData);
		//TODO

		return newNode;
	}
	// 2021/2 AW ADD end




	/**
	 * タイムラインのitemとして、トップグラフノードを作成
	 * @param channelRootNode
	 * @return EntityNodde
	 */
	public EntityNode createTopGraphNode(EntityNode channelRootNode) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		// トップグラフノード作成
		// 2021/07 AW start トップグラフのノードのクラス変更
		//EntityNode newNode = createTimelineItem(channelRootNode, "M");
		EntityNode newNode = createTimelineItem(channelRootNode, "RootGraph");
		// 2021/07 AW end トップグラフのノードのクラス変更

		if(newNode == null) {
			return null;
		}

		// チャネルのルートノード取得
		//EntityNode channelNode = getRootChannelNode();
		// グラフのトップノードのId
		String topGraphNodeId = newNode.getNodeId();
		try {
			channelRootNode.newLiteral(AppProperty.LITERAL_TOP_GRAPH).setValue(topGraphNodeId);
			//channelNode.newLiteral(AppProperty.LITERAL_TOP_GRAPH).setValue(topGraphNodeId);
			sync(channelRootNode);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return newNode;
	}

	/**
	 * 内部ノード生成
	 *
	 * @param name
	 * @param type
	 */
	public EntityNode createInnerNode(EntityNode ptnode, String name, String type) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		EntityNode newNode;

		//TODO encrypt修正
		boolean encrypt = ENCRYPT_FLAG;
		//boolean encrypt = true;
		//boolean encrypt = false;

		try {
			newNode = ptnode.newInnerEntity(name, type, encrypt);
		} catch (Exception e) {
			errPrint("Error: " + e.getLocalizedMessage());
			return null;
		}

		return newNode;
	}
	// 2021/2 AW ADD start

	/**
	 * @param node
	 * @param name
	 * @param lang
	 * @param val
	 */
	public void setCoordsValue(EntityNode node, String name, String lang, double val) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		//既にプロパティがある場合は削除する。
		//System.out.println(getproperty(node, name));
		//System.out.println(val);
		if (getproperty(node, name)) {

			NodeInfo nodeinfo = getProperty(name);
			try {
				nodeinfo.node.asLiteral().removeAllValues();

			} catch (NodeNotFoundException e) {
				errPrint("Error: " + e.getLocalizedMessage());
			}
		}
		//プロパティ作成
		try {
			LiteralNode lt_node = node.newLiteral(name);
			if (LANG_NONE_SYMBOL.equals(lang)) {
				lang = LiteralNode.LANG_NONE;
			}
			Object value = null;
			value =val;
			if (value != null) {
				lt_node.setValue(lang, value);
			} else if (!lt_node.removeValue(lang)) {
				outPrint("LANG " + lang + " not found.");
			}
		} catch (Exception e) {
			errPrint("Error: " + e.getLocalizedMessage());
		}
	}
	// 2021/2 AW ADD end

	/**
	 * プロパティの設定
	 * もし指定名プロパティがなければ作成する。
	 *
	 * @param name
	 * @param lang
	 * @param val
	 */
	public void setLiteral(EntityNode node, String name, String lang, String val) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		if (getproperty(node, name)) {
			// すでに存在する
			String cmnd[] = { name, "set", lang, val };
			replace(name + " set " + lang + " " + val, cmnd);
		} else {
			try {
				LiteralNode lt_node = node.newLiteral(name);
				setLiteralNodeValue(lt_node, lang, val);
			} catch (Exception e) {
				errPrint("Error: " + e.getLocalizedMessage());
			}
		}
	}

	/**
	 * プロパティ値の更新
	 *
	 * @param commandLine
	 * @param commands
	 */
	private void replace(String commandLine, String[] commands) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		try {
			NodeInfo nodeinfo = getProperty(commands[0]);
			if (nodeinfo != null) {
				nodeCommand(commandLine, commands, nodeinfo);
			}
		} catch (Exception e) {
			errPrint("Error: " + e.getLocalizedMessage());
		}
	}

	private void nodeCommand(String commandLine, String[] commands, NodeInfo nodeInfo) throws Exception {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		if (nodeInfo.node.isLiteral()) {
			literalNodeCommandI(commandLine, commands, nodeInfo.node.asLiteral(), 0, "value", true, true);
		}
	}

	/**
	 * 外部実体ノードの作成
	 * ストレージノード直下に作成する
	 * @param propName           ノード名
	 * @param type               Ontology Class名
	 * @param folder             true:フォルダノード false:ファイルノード
	 * @param encrypt            暗号化の有無(true/false)
	 * @param inhelitPermissions
	 * @return ノードオブジェクト
	 * @throws Exception
	 */
	private EntityNode newEntityOuter(String propName, String type, boolean folder, boolean encrypt,
			boolean inhelitPermissions) throws Exception {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		if (storageRootNode == null) {
			return null;
		}

		return storageRootNode.node.newEntity(propName, type, folder, encrypt, inhelitPermissions);
	}

	private void literalNodeCommandI(String commandLine, String[] commands, LiteralNode node, int offset,
			String valueName, boolean enableTypeCommand, boolean showMessage) throws NodeNotFoundException {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		switch (commands[1 + offset].charAt(0)) {
		case 't':
			if (!enableTypeCommand) {
				return;
			}

			String type;
			if (commands.length == (2 + offset)) {
				type = null;
			} else if (commands.length > (2 + offset)) {
				type = commands[2 + offset];
			} else {
				return;
			}

			setliteralNodeType(node, type);

			break;

		case 's':
			if (checkCommand(commands, 3 + offset)) {
				String lang = commands[2 + offset];
				if (LANG_NONE_SYMBOL.equals(lang)) {
					lang = LiteralNode.LANG_NONE;
				}
				Object value;
				{
					String valueStr;
					{
						String[] s = commandLine.split("\\s+", 4 + offset);
						if (s.length < (4 + offset)) {
							valueStr = null;
						} else {
							valueStr = s[3 + offset];
						}
					}

					if (valueStr != null) {
						try {
							value = Integer.valueOf(valueStr);
						} catch (NumberFormatException e) {
							try {
								value = Float.valueOf(valueStr);
							} catch (NumberFormatException e2) {
								String lowerValueStr = valueStr.toLowerCase();
								if ("true".equals(lowerValueStr)) {
									value = Boolean.TRUE;
								} else if ("false".equals(lowerValueStr)) {
									value = Boolean.FALSE;
								} else {
									value = valueStr;
								}
							}
						}
					} else
						value = null;
				}
				if (showMessage) {
					outPrint(((value != null) ? "Set" : "Remove") + " "
							+ (LiteralNode.LANG_NONE.equals(lang) ? "" : lang + " ") + valueName
							+ ((value != null) ? " to " + value + " [" + value.getClass().getSimpleName() + "]" : "")
							+ ".");
				}

				if (value != null) {
					node.setValue(lang, value);
				} else if (!node.removeValue(lang)) {
					outPrint("LANG " + lang + " not found.");
				}

				break;
			}

		default:
		}
	}

	private boolean checkCommand(String[] commands, int num) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		if (commands.length < num) {
			return false;
		}

		return true;
	}

	private void setliteralNodeType(LiteralNode node, String type) throws NodeNotFoundException {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		if (type == null) {
			node.removeType();
		} else {
			node.setType(type);
		}
	}

	private void setLiteralNodeValue(LiteralNode node, String lang, String valueStr) throws NodeNotFoundException {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		if (LANG_NONE_SYMBOL.equals(lang)) {
			lang = LiteralNode.LANG_NONE;
		}

		Object value = null;

		/* The section of code has been modified in order to rectify the bug" "0" in the beginning of digit string
		 *  disappears in synchronization."
		 */

		/*if (valueStr != null) {
			try {
				value = Integer.valueOf(valueStr);
			} catch (NumberFormatException e) {
				try {
					value = Float.valueOf(valueStr);
				} catch (NumberFormatException e2) {
					String lowerValueStr = valueStr.toLowerCase();
					if ("true".equals(lowerValueStr)) {
						value = Boolean.TRUE;
					} else if ("false".equals(lowerValueStr)) {
						value = Boolean.FALSE;
					} else {
						value = valueStr;
					}
				}
			}
		}*/
		value =valueStr;

		if (value != null) {
			node.setValue(lang, value);
		} else if (!node.removeValue(lang)) {
			outPrint("LANG " + lang + " not found.");
		}
	}

	/**
	 * プロパティリストから指定のプロパティノードを返す
	 * 事前にlist()でプロパティリストを作成しておく
	 *
	 * @param propIndexStr
	 * @return
	 */
	private NodeInfo getProperty(String propIndexStr) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		NodeInfo nodeInfo = null;

		try {
			for (NodeInfo ni : propList) {
				if (propIndexStr.equals(ni.name)) {
					nodeInfo = ni;
					break;
				}
			}
		} catch (NumberFormatException e) {
			errPrint("PROP-INDEX must be an integer: " + propIndexStr);
			return null;
		} catch (IndexOutOfBoundsException e) {
			errPrint("PROP-INDEX not found: " + propIndexStr);
			return null;
		}

		if (nodeInfo == null) {
			errPrint("Property " + propIndexStr + " was removed.");
			return null;
		}
		return nodeInfo;
	}

	private abstract class HandlerBase {
		public synchronized void onError(EntityNode node, PlrEntry entry, Exception exception, Origin origin) {
			//logger.trace("◆START");
			onError(entry, exception, origin);
		}

		public synchronized void onError(PlrEntry entry, Exception exception, Origin origin) {
			//logger.debug("◆START");

			String id;
			if (entry != null) {
				if ((id = entry.getId()) == null) {
					id = "unset entry";
				} else {
					id = null;
				}

				errPrint("Error" + ((origin != null) ? " from " + origin : "") + ((id != null) ? " on " + id : "")
						+ ":");
				// exception.printStackTrace();
			}
		}

	}

	private void errPrint(String str) {
		if (err_print) {
			TxtList.set(str);
		}
	}

	private void outPrint(String str) {
		if (out_print) {
			TxtList.set(str);
		}
	}

	/**
	 * フォルダルートのノードリストを取得する
	 *
	 * @return
	 */
	public List<NodeName> getRootNodeList() {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		List<NodeName> list = new ArrayList<>();

		for (NodeInfo ni : propList) {
			if (ni.node.isEntity()) {
				EntityNode en = ni.node.asEntity();
				NodeName nn = new NodeName(ni.name, ni.node.getId(), en.getURI().toString(), en);
				list.add(nn);
			}
		}

		return list;
	}

	/**
	 * uriからストレージのrootノードを取得する
	 *
	 * @paramuri
	 * @return
	 */
	public EntityNode getRootNode(EntityNode node) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		try {
			// 指定uriから取得したノードのストレージから、ストレージルートノードを取得する。
			Storage strg = node.getStorage();
			return plr.getRootNode(strg);
		} catch (Exception e) {
			errPrint("Error: " + e.getLocalizedMessage());
		}
		return null;
	}

	/**
	 * Node 名前を取得
	 *
	 * @param node
	 * @return
	 */
	public String getName(EntityNode node) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		if (node == null) {
			return null;
		}
		Map<String, com.assemblogue.plr.lib.Node> properties = listToMap(node);
		if (properties.containsKey(AppProperty.LT_NAME)) {
			com.assemblogue.plr.lib.Node literal = properties.get(AppProperty.LT_NAME);
 			return literal.asLiteral().getValue().toString();
		}
		return null;
	}

	/**
	 * Node IDを取得
	 *
	 * @param node
	 * @return
	 */
	public String getId(EntityNode node) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		if (node == null) {
			return null;
		}
		return node.getNodeId();
	}

	/**
	 * Node URIを取得
	 *
	 * @param node
	 * @return
	 */
	public String getUriStr(EntityNode node) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		if (node == null) {
			return null;
		}
		return node.getURI().toString();
	}

	/**
	 * 指定Nodeから属性を削除
	 * @param node 親ノード
	 * @param name 属性名
	 */
	public void removeProperty(EntityNode root, Node node, String name) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		try {
			root.removeProperty(name, node);
		} catch (Exception e) {
			errPrint("removeProperty:" + e.toString());
		}
	}

	/**
	 * 指定Nodeから属性を削除
	 * @param name 属性名
	 */
	public void removeProperty(EntityNode root, String name) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		try {
			root.removeProperty(name);
		} catch (Exception e) {
			errPrint("removeProperty:" + e.toString());
		}
	}

	/**
	 * パスフレーズ設定済みか？
	 * @return 設定済み:true
	 */
	public boolean passphrase() {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		return passphrase;
	}

	/**
	 * フレンドリストの取得
	 * @return フレンドのリスト
	 */
	public List<Friend> getFriends() {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		if (storage != null) {
			try {
				Root root = plr.getRoot(storage);
				List<Friend> friends = root.listFriends();
				return friends;
			} catch (Exception e) {
				errPrint("Error: " + e.getLocalizedMessage());
			}
		}

		return null;
	}

	/**
	 * フレンド公開用情報のセットアップ
	 */
	public void setpupFriends() {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		// フレンド公開用情報のセットアップ
		frientToMe f2m = new frientToMe();
		f2m.start();
	}

	/**
	 * フレンド公開用情報のセットアップスレッド
	 */
	class frientToMe extends Thread {
		public void run() {
			TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

			shareAplRootFolderToFriend();

			TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆END");
		}
	}

	/**
	 * フレンドへ公開する属性の設定を確認、必要に応じて新規に値をセットする
	 * わたしからあなたへ。。。
	 */
	private void shareAplRootFolderToFriend() {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		List<Friend> friends = getFriends();
		if (friends == null) {
			return;
		}

		List<String> friend_ids = new ArrayList<>();
		for (Friend friend : friends) {
			friend_ids.add(friend.getUserId());
		}

		EntityNode my_folder = getAplRootFolderNode();

		// MeToFriendノードの実体は１つ、最初に見つけたものにノードリンクとshare設定を行う
		for (Friend friend : friends) {
			try {
				// 情報公開用アプリフォルダノードを取得し、現在の値を書き込む
				List<EntityNode> m2f = getMeToFriendNode(friend, my_folder);
				if (m2f != null && m2f.size() > 0) {
					EntityNode me_to_friend = m2f.get(0);

					if (m2f.size() == 1) {
						// LinkedNodeなし
						Node new_node = me_to_friend.addProperty(AppProperty.CAPRAIBEX_ME_TO_FRIEND_PROPRTY, my_folder);
						new_node.asEntity().share(false, friend_ids);
					} else {
						// 既存LinkedNodeへのshare設定
						m2f.get(1).share(false, friend_ids);
					}

					sync(me_to_friend);
				}
			} catch (Exception e) {
				errPrint("Error: " + e.getLocalizedMessage());
			}
		}

		TxtList.set("Complete Me to Friend setup.");
		//logger.debug("◆END");
	}

	/**
	 * フレンドへ公開する属性の設定の確認
	 * @param friend フレンド
	 * @param my_folder 自アプリフォルダノード
	 * @return 0:me to friend Node, 1:linkedNode
	 */
	private List<EntityNode> getMeToFriendNode(Friend friend, EntityNode my_folder) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		List<EntityNode> rt = new ArrayList<>();

		try {
			EntityNode me_to_friend = friend.getMeToFriendRootNode();
			if (me_to_friend == null) {
				return null;
			}

			rt.add(me_to_friend); // rt:0

			// 既存の同名属性をチェック
			sync(me_to_friend);
			for (Node n : me_to_friend.getProperty(AppProperty.CAPRAIBEX_ME_TO_FRIEND_PROPRTY)) {
				if (n.asEntity().getURI().equals(my_folder.getURI())) {
					rt.add(n.asEntity());
					// 同じノードがセットされている
					return rt; // rt.size() > 1
				}
				// 一旦属性を削除
				me_to_friend.removeProperty(AppProperty.CAPRAIBEX_ME_TO_FRIEND_PROPRTY);
			}

			return rt;
		} catch (Exception e) {
			errPrint("Error: " + e.getLocalizedMessage());
		}

		return null;
	}

	/**
	 * フレンドのアプリフォルダノード直下のノードリストを取得する
	 * @param friend フレンド
	 * @return
	 */
	public List<NodeName> getFriendRootNodeList(Friend friend) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		try {
			EntityNode friend_root = friend.getFriendToMeRootNode();
			if (friend_root == null) {
				return null;
			}

			sync(friend_root);

			for (Node n : friend_root.getProperty(AppProperty.CAPRAIBEX_ME_TO_FRIEND_PROPRTY)) {
				EntityNode node = n.asEntity();
				sync(node);
				list(null, node);
				return getRootNodeList();
			}
		} catch (Exception e) {
			errPrint("Error: " + e.getLocalizedMessage());
		}

		return null;
	}

	/**
	 * ノードへのリンクをプロパティとして設定する
	 * もし指定名プロパティがなければ作成する。
	 * @param node リンクを追加するノード
	 * @param name　プロパティ名
	 * @param target_node　リンク先ノード
	 */
	public void setLinkedNode(EntityNode node, String name, EntityNode target_node) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		try {
			for (Node n : node.getProperty(name)) {
				node.removeProperty(name);
				break;
			}

			node.addProperty(name, target_node);
		} catch (Exception e) {
			errPrint("Error: " + e.getLocalizedMessage());
		}
	}

	/**
	 * プロパティとして保存したノードへのリンクを取得する
	 * @param node ノード
	 * @param name　プロパティ名
	 * @return リンク先ノード
	 */
	public EntityNode getLinkedNode(EntityNode node, String name) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		try {
			for (Node n : node.getProperty(name)) {
				return n.asEntity();
			}
		} catch (Exception e) {
			errPrint("Error: " + e.getLocalizedMessage());
		}

		return null;
	}

	/**
	 * ノード内のリテラルnameを取得
	 *
	 * @param node
	 * @return
	 */
	public String getNodeName(EntityNode node) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		try {
			// sync(node);
			Map<String, Node> properties;
			properties = listToMap(node);
			if (properties.containsKey(AppProperty.LT_NAME)) {
				String str = properties.get(AppProperty.LT_NAME).asLiteral().getValue().toString();
				return str;
			}
		} catch (Exception e) {
			errPrint("Error: " + e.getLocalizedMessage());
		}

		return null;
	}

	/**
	 * 共有設定: 基本の共有設定（Read/Write、Anybody)
	 *
	 * @param node
	 */
	public void share(EntityNode node) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		try {
			node.share(false, (String) null);
		} catch (Exception e) {
			errPrint("Error: " + e.getLocalizedMessage());
		}
	}

	static public EntityNode getMyHyperNode(EntityNode node) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		if (node == null) {
			return null;

		}
		List<Node> valList = node.getProperty("hyperNode");
		if (valList == null || valList.isEmpty()) {
			return null;
		}
		return valList.get(0).asEntity();
	}

	/**
	 * 表示ノードに@nodeを持つノードはハイパーノード
	 * @param node
	 * @return boolean
	 *
	 */
	static public boolean isHyperNode(EntityNode node) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		if (node == null) {
			return false;
		}
		// 2021/2 AW REP start
		List<Node> hyperNodes = node.getProperty("@node");
		//List<Node> hyperNodes = node.getProperty("hyperNode");
		// 2021/2 AW REP end

		if (hyperNodes.isEmpty()) {
			return false;
		// 2021/2 AW DEL start

		//}
		//EntityNode hyperNode = hyperNodes.get(0).asEntity();
		//List<Node> innerNodes = hyperNode.getProperty("#Entity");

		//if (innerNodes.isEmpty()) {
		//	return false;
		// 2021/2 AW DEL end

		} else {
			return true;
		}
	}

	static public void addMyHyperNode(EntityNode node, EntityNode hyperNode) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		if (node == null) {
			return;
		}
		try {
			node.addProperty("hyperNode", hyperNode);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

//	public Map<String, List<Node>> getChildrenFromHypernode(EntityNode hyperNode) {
//		// do not need to sync
//		Map<String, List<Node>> map = new HashMap<>();
//
//		if (hyperNode == null) {
//			return map;
//		}
//
//		if (hyperNode.hasProperty("vertex")) {
//			map.put("vertex", hyperNode.getProperty("vertex"));
//		}
//		if (hyperNode.hasProperty("edge")) {
//			map.put("edge", hyperNode.getProperty("edge"));
//		}
//		return map;
//	}

//	public void addChildrenToHypernode(EntityNode hyperNode, List<Node> vertex, List<Node> edge) {
//		if ((vertex==null || vertex.isEmpty()) && (edge==null || edge.isEmpty())) {
//			return;
//		}
//		try {
//			if (vertex != null) {
//				for (Node v : vertex) {
//					hyperNode.addProperty("vertex", v);
//				}
//			}
//			if (edge != null) {
//				for (Node e : edge) {
//					hyperNode.addProperty("edge", e);
//				}
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//		asyncNode(hyperNode);

	//for channel list AW 2020/06/15 START
	/**
	 * チャネル名取得
	 * ChannelDetailsFragmentのgetChannelName(Channel channel)を参考に
	 * @param channel チャネル名を取得したい対象チャネル
	 * @return チャネル名(取得失敗した場合はチャネルID)
	 */
	public String getChannelName(Channel channel) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		String name = channel.getDefaultName();
  		if (name != null) {
			//チャネル名を取得できた場合
			return name;
		}
		return channel.getId();
	}

	/**
	 * 指定ルート下にチャネルを作成する
	 * @param root チャネル作成のルートノード
	 */
	public void createChannel(String name, String desc) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		Root root = null;
		try {
			EntityNode rnode = plr.getRootNode(storage);
			root = new Root(rnode);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		if (root == null) {
			return;
		}

		//チャネル名
		Map<String, String> nameMap = new HashMap<String, String>();
		//チャネル説明
		Map<String, String> descriptionMap = new HashMap<String, String>();

		nameMap.put(LiteralNode.LANG_NONE, name);
		descriptionMap.put(LiteralNode.LANG_NONE, desc);
		//チャネル作成
		try {
			Channel channel = root.newChannel(nameMap, descriptionMap);

			channel.setTimelineState(TimelineState.WRITABLE);

			Storage rootst = root.getStorage();
			DataSetting dsetst = SystemAccount.getCommunicationDataSetting(rootst);
			sync(dsetst.getNode());

			channel.setChannelDataSetting(dsetst);
			dsetst.createInitialItems(channel.getNode(), root.getPlrId());
			channel.sync(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * グラフのルートチャネルをセットする
	 * グラフのopen、create前にセットする
	 * @param c setするチャネル
	 */
	public void setRootChannel(Channel c) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		this.rootChannel = c;
	}

	// 2021/2 AW ADD start
	/**
	 * グラフのルートチャネルに設定されている名を取得する。
	 * 2021_2_3 新設
	 * @return チャネルに設定されたデフォルト値
	 *
	 */
	/*
	public String getRootChannelDefaultName() {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		return this.rootChannel.getDefaultName();
	}
	*/
	// 2021/2 AW ADD end

	/**
	 * グラフのルートチャネルのノードを取得
	 * @return channelのノード
	 */
	public EntityNode getRootChannelNode() {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		if (rootChannel == null) {
			return null;
		}
		return rootChannel.getNode();
	}
	//for channel list AW 2020/06/15 END



	//2021/3 AW ADD start
	// オントロジー対応

	/** graphSettingプロパティ名 */
	private static final String GRAPH_SETTING = "graphSetting";

	// スキーマ名、デフォルトで用意されているもの
	private String[] schemaNames = {NodeSchemas.CLASS_SELF_SCHEMA,NodeSchemas.CLASS_GENERAL_SCHEMA};

	/**
	 * 共通スキーマ、自身、一般のFileNodeを返す。
	 * @param node
	 * @return　List<FileNode>　fileNodeList
	 */
	public List<FileNode> getChannelSchemaOntology(Channel channel) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		// チャネルのDataSetting、同期で読み込まないとデータ取得できない
		DataSetting dataSetting = channel.getChannelDataSetting();
		dataSetting.syncAndWait();

		List<FileNode> fileNodeList = new ArrayList<>();

		// 共通スキーマ
		fileNodeList.addAll(dataSetting.listOss());

		// 自身・一般スキーマ
		for (String schemaName :  schemaNames) {
			Schema schema = dataSetting.getEmbeddedSchema(schemaName);
			if (schema != null) {
				fileNodeList.addAll(schema.listOss());
			}
		}

		return fileNodeList;
	}

	/**
	 * GraphSettingにオントロジー（FileNode）を設定する
	 * @param setFileNodeList
	 * @param graphNode
	 */
	public void setGraphSetting(List<FileNode> setFileNodeList, EntityNode graphNode) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		// graphSettingが無い場合は戻る
		if(!isGraphSetting(graphNode)) {
			return ;
		}

		// GraphSettingを取得
		EntityNode dataSettingInnerNode = getGraphInnerNode(graphNode);
		DataSetting dataSetting = new DataSetting(dataSettingInnerNode);

		// オントロジーを設定
		try {
			dataSetting.setOss(setFileNodeList);
		} catch (Exception e) {
			e.printStackTrace();
		}

		// 同期
		syncSimple(dataSetting.getNode());
	}

	/**
	 * GraphSettingからオントロジーを取得
	 * @param graphNode
	 * @return
	 */
	public List<FileNode> getGraphSetting(EntityNode graphNode) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		List<FileNode> fileNodeList = new ArrayList<>();

		// graphSettingが無い場合は戻る
		if(!isGraphSetting(graphNode)) {
			return fileNodeList;
		}

		// GraphSettingを取得
		EntityNode dataSettingInnerNode = getGraphInnerNode(graphNode);
		DataSetting dataSetting = new DataSetting(dataSettingInnerNode);

		fileNodeList.addAll(dataSetting.listOss());
		return fileNodeList;
	}

	/**
	 * 「graphSetting」プロパティを持つか
	 * @param node
	 * @return　true/false
	 */
	public	boolean isGraphSetting(EntityNode node) {
		return node.hasProperty(GRAPH_SETTING);
	}

	/**
	 * graphSettingプロパティを作成する
	 * @param node
	 */
	public void makeGraphSetting(EntityNode graphNode) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		// graphSettingがある場合は戻る
		if(isGraphSetting(graphNode)) {
			return;
		}

		// 名称
		String type = "GraphSetting";
		String name = GRAPH_SETTING;

		// インナーノードとしてGraphSettingを作成
		EntityNode dataSettingInnerNode = createInnerNode(graphNode, name ,type);
		DataSetting dataSetting  = new DataSetting(dataSettingInnerNode);
		try {
			for(String schemaName : schemaNames) {
				dataSetting.newEmbeddedSchema(schemaName);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		// チャネルノードのデータ設定では以下を設定していたが、
		// ここでは設定しない。
		//dataSetting.createInitialItems(graphNode, getUserPlrId());

		// 同期
		syncSimple(dataSetting.getNode());
	}

	/**
	 * ノードの「graphSetting」プロパティのノードをEntityNodeを返す。
	 * @param graphNode
	 * @return
	 */
	private EntityNode getGraphInnerNode(EntityNode graphNode) {
		return graphNode.getProperty(GRAPH_SETTING).get(0).asEntity();
	}


	/**
	 * プロパティの値が1つの場合、指定されたプロパティの値を取得する
	 * @param entityValues
	 * @param property
	 * @return
	 */
	public static Object getPropertySingleValue(EntityNode node, String property) {

		List<Node> propertyList = node.getProperty(property);
		if (propertyList == null || propertyList.isEmpty()) {
			return null;
		}
		Object value = propertyList.get(0).asLiteral().getValue();

		return value;
	}


	/**
	 * プロフィールを取得
	 */
	public synchronized void getProfile() {

		EntityNode storageNode = null;
		try {
			storageNode = plr.getPublicRootNode(storage);
			storageNode.syncAndWait();
		} catch (Exception e) {
			e.printStackTrace();
		}
		Root storageRoot = new Root(storageNode);
		this.plrId = storageRoot.getPlrId();

		ProfileCallback profileCallback = new ProfileCallback();
		synchronized (profileCallback) {

			try {

				ProfileManager.get(storageRoot, true, true, profileCallback);
				profileCallback.wait();

			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * 開示ユーザのプロフィールを取得
	 */
	public synchronized void getProfile(PlrAccount user) {

		// 引数が自分のアカウントと同じ場合はプロフィール取得しない
		String userPlrId = user.getPlrId();
		if (userPlrId.equals(this.plrId)) {
			return;
		}

		ProfileCallback profileCallback = new ProfileCallback();
		synchronized (profileCallback) {

			try {

				ProfileManager.get(user, true, true, profileCallback);
				profileCallback.wait();

			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * プロフィール取得のコールバック
	 */
	private class ProfileCallback implements PLR.MultiResultCallback<Map<OntologyItem, List<ProfileItem>>>{

		@Override
		public synchronized void onResult(Map<OntologyItem, List<ProfileItem>> ontologyItemListMap, PlrEntry.Origin origin) {
			TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

			// プロフィールが取得できて、キャッシュに入ればいいので、何もしない

			/*
			String name = FriendInfoUtils.getName(ontologyItemListMap);
			BufferedImage image = FriendInfoUtils.getPictureThumbnailImage(ontologyItemListMap);
			if (image == null) {
				System.out.println("image is null");
			} else {
				System.out.println("image width =" + image.getWidth());
			}
			Image iimage = SwingFXUtils.toFXImage(image, null);

			profileMap = ontologyItemListMap;
			*/
		}

		@Override
		public synchronized void onError(Exception exception, PlrEntry.Origin origin) {
			TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");
			exception.printStackTrace();
		}

		@Override
		public synchronized void onFinish() {
			TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");
			notify();
		}
	}

	//////////////
	// あとでダイアログ側に移動

	/**
	 * オントロジー選択ダイアログでのOSS取得
	 * @param graphNode
	 * @param parentGraphNode
	 * @return
	 */
	public List<FileNode> getSelectedOssList(EntityNode graphNode,EntityNode parentGraphNode) {

		//対象ノードに「GraphSetting」プロパティがある場合取得
		if(isGraphSetting(graphNode))
		{
			return getGraphSetting(graphNode);
		}


		//FIXME  ルートチャネルの取得方法があっているのか

		//対象ノードに「GraphSetting」プロパティがない場合、まず、ルートチャネルノードの
		//プロパティ「Top Graph」のIDと一致する＝トップグラフノードか確認する。
		String topGraphId =
		(String) rootChannel.getProperty(AppProperty.LITERAL_TOP_GRAPH).get(0).asLiteral().getValue();
		//トップグラフノードなら、チャネルノードのdatasettingから取得
		if(graphNode.getId().equals(topGraphId)) {
			return getChannelSchemaOntology(rootChannel);
		}
		//トップグラフノードでない場合、親ノードから取得する。
		if(parentGraphNode != null) {
			if(isGraphSetting(parentGraphNode))
			{
				return getGraphSetting(parentGraphNode);
			}
		}

		List<FileNode> emptyList = new ArrayList<>();
		return emptyList;
	}


	/*
	private String getUserPlrId() throws IllegalStateException, Exception {
		if(userPlrId == null) {
			EntityNode rootNoode = plr.getRootNode(storage);
			syncSimple(rootNoode);
	        Root root = new Root(rootNoode);
	        root.sync();
	        userPlrId =root.getPlrId();
		}
		return userPlrId;
	}

	 */
	//2021/3 AW ADD end
}
