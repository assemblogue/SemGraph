package com.assemblogue.plr.app.generic.semgraph.ui.control;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.assemblogue.plr.app.generic.semgraph.App;
import com.assemblogue.plr.app.generic.semgraph.GraphActor;
import com.assemblogue.plr.app.generic.semgraph.OssActor;
import com.assemblogue.plr.app.generic.semgraph.PlrActor;
import com.assemblogue.plr.app.generic.semgraph.ui.controller.AppController;
import com.assemblogue.plr.app.generic.semgraph.ui.controller.RootLayoutController;
import com.assemblogue.plr.contentsdata.PLRContentsData;
import com.assemblogue.plr.lib.EntityNode;
import com.assemblogue.plr.lib.FileNode;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class OntologyDialog extends BorderPane {

	private Stage stage;
	private FileNode baseNode;
	private static final  double HEIGHT = 500;
	private static final  double WIDTH = 400;
	private static final String ja = "ja";

	public OntologyDialog(List<FileNode> ossList, Map<String, String> ossMap,
			boolean ossThreadFlag, RootLayoutController rootlayout, String lang) {

		PlrActor plrAct = AppController.plrAct;

		//fileNode「Base」の表示リスト除外、返り値リストの追加、表示リストのユニーク化
		List<FileNode> displayOssList = remeveBaseIndisplayOssListAndInBaseNode(ossList, ossMap);

		EntityNode graphNode = rootlayout.getGraphAct().getGraphData().getGraphNode();
		EntityNode parentgraphNode = rootlayout.getGraphAct().getGraphData().getParentNode();

		List<FileNode> selectedOssList = new ArrayList<>();
		try {
			//トップグラフノードの場合、GraphSettingプロパティがある場合、そこから選択済みの
			//ファイルノードを取得、プロパティがない場合、チャネルからdatasettingのファイルノードを取得する。
			//ハイパーノードの場合、GraphSettingプロパティがある場合、そこから選択済みの
			//ファイルノードを取得、プロパティがない場合、第二引数の親ノードからファイルノードを取得する。


			//FIXME このメソッドは本クラス内へ
			selectedOssList = plrAct.getSelectedOssList(graphNode,parentgraphNode);

		} catch (Exception e) {
			e.printStackTrace();
		}

		//上部に緑背景タイトルとその配下にチェックボックスを配置
		this.setCenter(setbody(lang,displayOssList,selectedOssList));

		stage = new Stage();
		this.setBottom(setButton(lang,this,plrAct,rootlayout,displayOssList,graphNode, ossMap, ossThreadFlag));


		String titleJa = "OSS設定";
		String titleEn = "OSSSetting";
		String titleString = null;
		if(lang.equals(ja)) {
			titleString = titleJa;
		}else {
			titleString = titleEn;
		}
		stage.setTitle(titleString);

		//モーダル処理
		stage.initModality(Modality.APPLICATION_MODAL);
		//ウィンドウサイズを固定
		stage.setResizable(false);
		//セマンティックエディタアイコンを取得
		stage.getIcons().add(new Image(App.class.getResourceAsStream("ui/img/editor.jpg")));

		//FIXME サイズ
		stage.setScene(new Scene(this,WIDTH,HEIGHT));
	}

	/**
	 * ダイアログ表示
	 */
	public void showAndWait() {
		stage.showAndWait();
	}


	/**
	 * ボタンの配置、アクション設定
	 * @param lang
	 * @param borderPane
	 * @param plrAct
	 * @param rootlayout
	 * @param displayOssList
	 * @param graphNode
	 * @return
	 */
	private HBox setButton(String lang,BorderPane borderPane,PlrActor plrAct,
			RootLayoutController rootlayout,List<FileNode> displayOssList,EntityNode graphNode,
			Map<String, String> ossMap, boolean ossThreadFlag) {

		//botomm配置のボタン設定
		int topBottomSpan = 5;
		int leftRightSpan = 15;
		Insets buttonInsets = new Insets(topBottomSpan,leftRightSpan,topBottomSpan,leftRightSpan);

		Button okButton = new Button("OK");
		okButton.setPadding(buttonInsets);
		Button cancelButton =  new Button("Cancel");
		cancelButton.setPadding(buttonInsets);

		int buttonSpan = 60;
		HBox hbox = new HBox(buttonSpan);
		int padding = 20;
		hbox.setPadding(new Insets(padding));

		hbox.getChildren().addAll(cancelButton ,okButton);
		hbox.setAlignment(Pos.CENTER);

		//ボタンアクション設定
		cancelButton.setOnAction((ActionEvent) ->{
			stage.close();
		});

		okButton.setOnAction((ActionEvent) ->{
			List<FileNode> selectedOssList =  new ArrayList<>();
			selectedOssList.add(baseNode);
			ScrollPane scrollPane = (ScrollPane) borderPane.centerProperty().getValue();
			VBox chekboxitems = (VBox) scrollPane.getContent();
			int checkboxClassRow = 2;
			//VBoxの最初の要素は「オントロジー」というタイトル要素なので
			//1から始める
			for(int i = 1 ; i < chekboxitems.getChildren().size() ;i++) {
				HBox chekboxitem = (HBox) chekboxitems.getChildren().get(i);
				CheckBox checkBox = (CheckBox) chekboxitem.getChildren().get(checkboxClassRow);
				if(checkBox.isSelected()) {
					//Vboxの要素と、displayOssListのリストはn:n-1となるので、
					//取り出しは、i-1で実施する。
					selectedOssList.add(displayOssList.get(i-1));
				}
			}

			try {
				//OssActor osAct = new OssActor(returnOssList);
				OssActor ossActor = rootlayout.getOssActor();

				//rootlayout.setForLinkContentsData(osAct.getContentsData());
				//List<FileNode> forClassOssList = new ArrayList<>();
				//for(FileNode fNode : selectedOssList) {
				//	osAct = new OssActor(fNode);
				//	PLRContentsData data = osAct.getContentsData();
				//	if(data.topClasses().get(0).id.equals("#Event")) {
				//		forClassOssList.add(fNode);
				//	}
				//}
				//osAct = new OssActor(forClassOssList);
				//rootlayout.setForClassContentsData(osAct.getContentsData());

				stage.close();

				// PLRContentsdata作成
				ossActor.makeContentsData(selectedOssList, ossMap, ossThreadFlag);
				PLRContentsData eventContentsData = ossActor.getEventOss();
				PLRContentsData linkContentsData = ossActor.getLinkOss();
				GraphActor graphActor = rootlayout.getGraphAct();
				graphActor.getGraphData().setContentsData(eventContentsData);
				graphActor.getGraphData().setLinkContentsData(linkContentsData);

				// GraphSettingがない場合は、GraphSettingを作成する
				if(!plrAct.isGraphSetting(graphNode)) {
					plrAct.makeGraphSetting(graphNode);
				}

				// GraphSettingに選択されたオントロジーを設定
				plrAct.setGraphSetting(selectedOssList, graphNode);

				// 再描画、グラフを読み込むところからopen
				EntityNode node = graphActor.getGraphData().getGraphNode();
				EntityNode parentNode = graphActor.getGraphData().getParentNode();
				//TODO ここで同期が走る
				graphActor.openGraph(node, parentNode);
				rootlayout.repaint(false);

			} catch (Exception e) {

				String headerJa = "エラー";
				String headerEn = "Error";
				String headerString = null;
				String errorMsgJa = "コンテンツデータを生成できませんでした。選択内容を確認してください。";
				String errorMsgEn = "Could not generate content data. Check your selection.";
				String errorMsgString = null;

				if(lang.equals(ja)) {
					headerString = headerJa;
					errorMsgString = errorMsgJa;
				}else {
					headerString = headerEn;
					errorMsgString = errorMsgEn;
				}

				AlertDialog  dialog = new AlertDialog(AlertType.ERROR,headerString,errorMsgString,stage);
				dialog.show();
				e.printStackTrace();
			}
		});

		//ボタンアクション設定完了
		return hbox;

	}

	/**
	 *上部にタイトルを配置し、その下にファイルノードタイトルとチェックボックスを配置する。
	 * @return
	 */
	private static ScrollPane setbody(String lang,List<FileNode> displayOssList,List<FileNode> selectedOssList) {
		String discourseGraph = "Discourse Graph";
		VBox selectBoxes = new VBox(0);
		selectBoxes.getChildren().add(setTitle(lang));
		Insets hboxSpan = new Insets(10);
		//Pane space = new Pane();
		for(FileNode fileNode : displayOssList) {
			Label checkBoxLabel = new Label(fileNode.getTitle(lang));
			CheckBox checkBox = new CheckBox();
			if(fileNode.getTitle().toString().endsWith("!")) {
					checkBox.setSelected(true);
			}else if(fileNode.getTitle().toString().contentEquals(discourseGraph)) {
				checkBox.setSelected(true);
				checkBox.setOnAction((ActionEvent) ->{
					if(!checkBox.isSelected()) {
						checkBox.setSelected(true);
					}
				});
			}
			if(!checkBox.isSelected()) {
				for(FileNode selectedFileNode : selectedOssList) {
					if(fileNode.getId().equals(selectedFileNode.getId())) {
						checkBox.setSelected(true);
						break;
					}
				}
			}

			HBox hbox = new HBox(0);
			marginWidth(hbox);
			hbox.setPadding(hboxSpan);

			//テキストとチェックボックスを両端にするためのスペース用
			Pane space = new Pane();
			//Labelとチェックボックスを両端に配置するため、以下の設定が必要
			hbox.setHgrow(space, Priority.ALWAYS);
			hbox.getChildren().addAll(checkBoxLabel,space,checkBox);
			selectBoxes.getChildren().add(hbox);
		}
		ScrollPane scrollPane = new ScrollPane();
		scrollPane.setContent(selectBoxes);
		scrollPane.setPrefWidth(WIDTH);
		return scrollPane;
	}

	private static void marginWidth(Region region) {
		 region.setPrefWidth(WIDTH -15);
	}


	/**
	 * ボタン装飾メソッド
	 * @param name
	 * @return
	 */
	private static Button generateButton(String name) {
		int topBottomSpan = 10;
		int leftRightSpan = 25;
		//青背景
		String buttonBackground = "-fx-background-color:#0000FF;";
		Button button = new Button(name);
		button.setStyle(buttonBackground);
		button.setTextFill(Color.WHITE);
		button.setPadding( new Insets(topBottomSpan,leftRightSpan,topBottomSpan,leftRightSpan));
		return button;
	}

	/**
	 * 画面上部のタイトル設定
	 * @return
	 */
	private static HBox setTitle(String lang) {
		String titleJa = "オントロジー";
		String titleEn = "Ontology";
		String titleString = null;
		if(lang.equals(ja)) {
			titleString = titleJa;
		}else {
			titleString = titleEn;
		}
		Label title = new Label(titleString);
		int fontSize = 20;
		title.setStyle("-fx-font-size: "+fontSize+"px;");
		title.setTextFill(Color.WHITE);
		title.setPadding(new Insets(10));
		HBox titleBox = new HBox(0);
		titleBox.getChildren().add(title);
		String colorGreen = "#008000";
		titleBox.setStyle("-fx-background-color:" + colorGreen + ";");
		marginWidth(titleBox);

		return titleBox;
	}

	/**
	 * コントラクタで渡されたfileNodeListからfileNode「Base」を除外し、帰り値のfileNodeListに追加
	 * また、コントラクタで渡されたfileNodeListのユニーク化
	 * @param ossList
	 * @param ossMap
	 * @return
	 */
	private List<FileNode> remeveBaseIndisplayOssListAndInBaseNode(List<FileNode> ossList, Map<String, String> ossMap) {
		List<FileNode> fileNodeListUnique = new ArrayList<>();
		boolean containsNode = false;
		String BaseName = "Base!";
		for(FileNode fileNode : ossList) {

			// ossMapにidが含まれないfileNodeは対象外
			String fileId = fileNode.getId();
			if (!ossMap.containsKey(fileId)) {
				continue;
			}

			for(FileNode findFileNode : fileNodeListUnique) {
				if(fileNode.getId().equals(findFileNode.getId())) {
					containsNode = true;
					break;
				}
			}
			if(!containsNode && !fileNode.getTitle().equals(BaseName)) {
				fileNodeListUnique.add(fileNode);
			}else if(!containsNode && fileNode.getTitle().equals(BaseName)) {
				baseNode = fileNode;
			} else {
				containsNode = false;
			}
		}
		return fileNodeListUnique;
	}

}
