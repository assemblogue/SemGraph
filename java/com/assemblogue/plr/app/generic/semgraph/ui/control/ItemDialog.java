package com.assemblogue.plr.app.generic.semgraph.ui.control;

import java.util.List;
import java.util.Map;

import com.assemblogue.plr.app.generic.semgraph.GraphActor;
import com.assemblogue.plr.app.generic.semgraph.PlrActor;
import com.assemblogue.plr.app.generic.semgraph.ui.controller.RootLayoutController;
import com.assemblogue.plr.lib.EntityNode;
import com.assemblogue.plr.lib.Node;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Bounds;
import javafx.scene.control.Alert;
import javafx.stage.Stage;
import javafx.stage.Window;

public class ItemDialog extends Alert {

	//private Logger logger = LogManager.getLogger();

	/**
	 * コンストラクタ
	 */
	public ItemDialog(EntityNode node, Stage stage, RootLayoutController rootLayoutController) {
		super(Alert.AlertType.INFORMATION);

		// graphActor,DEBUG情報
		GraphActor graphActor = rootLayoutController.getGraphAct();

		// ヘッダ領域、アイコンマークは消去
		setHeaderText(null);
		setGraphic(null);

		// タイトル
		setTitle("Node");

		// 表示するテキスト
		StringBuilder sb = new StringBuilder();

		// ノードのプロパティ
		Map<String, List<Node>> propertiesMap = PlrActor.getProperties(node);

		// とりあえず、ノードのリテラル、無言語の値を取得
		for (Map.Entry<String, List<Node>> e: propertiesMap.entrySet()) {

			String propertyName = e.getKey();
			//if (propertyName.startsWith("#cnt")) {
			//	continue;
			//}

			List<Node> propertyValues = e.getValue();
			for (Node n: propertyValues) {
				if (n.isLiteral()) {
					sb.append(propertyName)
					.append(':')
					.append(n.asLiteral().getValue().toString())
					.append(System.lineSeparator());
				}
			}

		}

		//DEBUG情報

		// ノードID
		String nodeId = node.getNodeId();
		// 位置をもつノードID
		String graphId = graphActor.getGraphData().getGraphNodeInfo(nodeId).getCoordinateNodeId();
		// 位置を取得
		double coordsx = graphActor.getGraphData().getGraphNodeInfo(nodeId).getX();
		double coordsy = graphActor.getGraphData().getGraphNodeInfo(nodeId).getY();
		String coords = String.valueOf(coordsx) + ", " + String.valueOf(coordsy);
		// サイズ
		double graphX = graphActor.getGraphData().getGraphSizeX();
		double graphY = graphActor.getGraphData().getGraphSizeY();
		double winX = graphActor.getGraphData().getWindowSizeX();
		double winY = graphActor.getGraphData().getWindowSizeY();
		double rootX = graphActor.getGraphData().getWindowRootX();
		double rootY = graphActor.getGraphData().getWindowRootY();
		double zoom = graphActor.getGraphData().getZoom();

		sb.append("nodeId: ")
		.append(nodeId)
		.append(System.lineSeparator())
		.append("coords:")
		.append(coords)
		.append(System.lineSeparator())
		.append(System.lineSeparator())
		.append("coordinateNodeId: ")
		.append(graphId)
		.append(System.lineSeparator())
		.append("graphSize: ")
		.append(graphX)
		.append(", ")
		.append(graphY)
		.append(System.lineSeparator())
		.append("windowSize: ")
		.append(winX)
		.append(", ")
		.append(winY)
		.append(System.lineSeparator())
		.append("windowRoot: ")
		.append(rootX)
		.append(", ")
		.append(rootY)
		.append(System.lineSeparator())
		.append("zoom:")
		.append(zoom);

		//DEBUG情報


		setContentText(sb.toString());

		// ダイアログを親ウィンドウの中央に出すためのリスナー追加
		Window owner = stage;
		this.getDialogPane().layoutBoundsProperty().addListener(
				new ChangeListener<Bounds>() {
			@Override
			public void changed(ObservableValue<? extends Bounds> observable,
					Bounds oldValue, Bounds newValue) {
				if(newValue != null
						&& newValue.getWidth() > 0 && newValue.getHeight() > 0) {
					double x = owner.getX() + owner.getWidth() / 2;
					double y = owner.getY() + owner.getHeight() / 2;
					setX(x - newValue.getWidth() / 2);
					setY(y - newValue.getHeight() / 2);
					getDialogPane().layoutBoundsProperty().removeListener(this);
				}
			}
		});

		// 削除確認のウィンドウを開いたとき、閉じたとき
		this.showingProperty().addListener(((observable, oldValue, newValue) -> {
			//logger.debug("itemDialog showingProperty old={}, new={}", oldValue, newValue);

			if (oldValue == true && newValue == false) {
				// ダイアログを閉じた時

				// 描画中フラグ
				rootLayoutController.graphRepaintingFlag = false;

			} else if (oldValue == false && newValue == true) {
				// ダイアログを開いたとき

				// 描画中フラグ
				rootLayoutController.graphRepaintingFlag = true;
			}
		}));
	}
}