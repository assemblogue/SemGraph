package com.assemblogue.plr.app.generic.semgraph.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import com.assemblogue.plr.app.generic.semgraph.GraphActor;
import com.assemblogue.plr.app.generic.semgraph.TinyLog;
import com.assemblogue.plr.app.generic.semgraph.Utils;

import javafx.geometry.BoundingBox;
import javafx.geometry.Point2D;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;

public class GraphCoordinate {

	//private Logger logger = LogManager.getLogger();

	/** ウィンドウサイズ初期値 */
	private double initWindowSizeX = Utils.getWindowWidth();
	private double initWindowSizeY = Utils.getWindowHeight();
	//private double initWindowSizeX = Utils.getWindowWidth() * 0.7;
	//private double initWindowSizeY = Utils.getWindowHeight() * 0.7;

	/** ウィンドウサイズ */
	private Point2D windowSize;

	/** グラフサイズ */
	private Point2D graphSize;

	/** sceneのサイズ */
	private Point2D sceneSize;

	/** バウンディングボックス */
	private BoundingBox boundingBox;

	/** バウンディングボックスで必ず表示される領域長さ */
	private double overlapX = 50;
	private double overlapY = 50;

	/** ウィンドウ左上の絶対座標 */
	private Point2D windowPoint;

	/** グラフ座標→相対座標のための基準座標 */
	private Point2D basePoint;

	/* ズーム比 */
	private double scale;

	/** 再描画が必要かフラグ */
	private boolean repaintFlag = false;


	// グラフを初めて開いたとき
	public GraphCoordinate(GraphActor graphAct, List<DraggableNode> draggableNodeList) {
		TinyLog.debug(new Throwable().getStackTrace()[0], false, "◆START");

		List<BoundingBox> boundingBoxList = getBoundingBoxList(draggableNodeList, false);

		// ウィンドウサイズ
		double windowSizeX = graphAct.getGraphData().getWindowSizeX();
		double windowSizeY = graphAct.getGraphData().getWindowSizeY();

		// ウィンドウサイズ=(0.0)の場合はtrue、はじめて開くグラフの場合trueになる
		boolean windowSizeZero = false;
		if (windowSizeX == 0.0 && windowSizeY == 0.0) {
			// ウィンドウサイズが指定されてない場合は今まで通り
			windowSizeX = initWindowSizeX;
			windowSizeY = initWindowSizeY;
			graphAct.getGraphData().setWindowSizeX(windowSizeX);
			graphAct.getGraphData().setWindowSizeY(windowSizeY);
			//logger.debug("Default WindowSize Setting  x={}, y={}", windowSizeX, windowSizeY);
			windowSizeZero = true;
		}
		windowSize = new Point2D(windowSizeX, windowSizeY);

		// BoundingBoxの計算
		calcBoundingBox(boundingBoxList);

		// グラフサイズ
		double graphSizeX = graphAct.getGraphData().getGraphSizeX();
		double graphSizeY = graphAct.getGraphData().getGraphSizeY();

		if (boundingBoxList.isEmpty()) {
			// ノードがない場合はウィンドウと同じ
			graphSizeX = windowSizeX;
			graphSizeY = windowSizeY;
			graphAct.getGraphData().setGraphSizeX(graphSizeX) ;
			graphAct.getGraphData().setGraphSizeY(graphSizeY);
			//logger.debug("Default graphSize Setting. no node.  x={}, y={}", graphSizeX, graphSizeY);
			graphSize = new Point2D(graphSizeX, graphSizeY);

		} else {
			// ノードがある場合はバウンディングボックスから求める
			calcGraphSize(boundingBoxList);
			graphAct.getGraphData().setGraphSizeX(graphSize.getX());
			graphAct.getGraphData().setGraphSizeY(graphSize.getY());
			//logger.debug("Default graphSize Setting. node exist.  x={}, y={}", graphSize.getX(), graphSize.getY());
		}

		// ウィンドウ位置の設定
		// ウィンドウ位置は、(0,0)で保存されることもあり得るので、
		// はじめてのグラフであるかの判定は、他パラメータからとる必要がある。
		// ここではwindowSize=(0,0)から判断する
		double windowRootX = graphAct.getGraphData().getWindowRootX();
		double windowRootY = graphAct.getGraphData().getWindowRootY();
		if (windowSizeZero) {
			// ウィンドウ位置が指定されてない場合
			if (boundingBoxList.isEmpty()) {
				// ノードがない場合は左上に設定
				windowRootX = 0;
				windowRootY = 0;
			} else {
				// ノードがある場合は、BoundingBox左上部の絶対座標に合わせる
				windowRootX = windowSize.getX() - overlapX;
				windowRootY = windowSize.getY() - overlapY;
			}

			graphAct.getGraphData().setWindowRootX(windowRootX);
			graphAct.getGraphData().setWindowRootY(windowRootY);
			//logger.debug("WindowRoot Setting  x={}, y={}", windowRootX, windowRootY);
		}
		windowPoint = new Point2D(windowRootX, windowRootY);


		// ベースポイント
		double basePointX = graphAct.getGraphData().getBasePointX();
		double basePointY = graphAct.getGraphData().getBasePointY();
		// ベースポイントが指定されてない場合
		if (boundingBoxList.isEmpty()) {
			// ノードがない場合はウィンドウの真ん中を設定
			basePointX = windowSizeX / 2;
			basePointY = windowSizeY / 2;
			graphAct.getGraphData().setBasePointX(basePointX) ;
			graphAct.getGraphData().setBasePointY(basePointY);
			//logger.debug("Default basePoint Setting. no node.  x={}, y={}", basePointX, basePointY);
			basePoint = new Point2D(basePointX, basePointY);
		} else {
			// ノードがある場合
			calcBasePoint(boundingBoxList);
			graphAct.getGraphData().setBasePointX(basePoint.getX());
			graphAct.getGraphData().setBasePointY(basePoint.getY());
			//logger.debug("Default basePoint Setting. node exist.  x={}, y={}", basePoint.getX(), basePoint.getY());
		}

		// ズーム比
		double zoom = graphAct.getGraphData().getZoom();
		if (zoom == 0.0) {
			// ズーム比が設定されていない場合は1.0を設定
			zoom = 1.0;
			graphAct.getGraphData().setZoom(zoom);
			//logger.debug("Default Zoom Setting 1.0");
		}
		scale = zoom;

		// 再描画フラグ
		repaintFlag = true;

		TinyLog.debug(new Throwable().getStackTrace()[0], false, "init WindowSize = (" + windowSize.getX() + ", " + windowSize.getY() + ")");
		TinyLog.debug(new Throwable().getStackTrace()[0], false, "init boundingBoxPoint = (" + boundingBox.getMinX() + ", " + boundingBox.getMinY() + ")");
		TinyLog.debug(new Throwable().getStackTrace()[0], false, "init boundingBoxLength = (" + boundingBox.getWidth() + ", " + boundingBox.getHeight() + ")");
		TinyLog.debug(new Throwable().getStackTrace()[0], false, "init GraphSize = (" + graphSize.getX() + ", " + graphSize.getY() + ")");
		TinyLog.debug(new Throwable().getStackTrace()[0], false, "init WindowPoint = (" + windowPoint.getX() + ", " + windowPoint.getY() + ")");
		TinyLog.debug(new Throwable().getStackTrace()[0], false, "init basePoint = (" + basePoint.getX() + ", " + basePoint.getY() + ")");
		TinyLog.debug(new Throwable().getStackTrace()[0], false, "init Scale = (" + scale + ")");
		TinyLog.debug(new Throwable().getStackTrace()[0], false, "init repaintFlag = (" + repaintFlag + ")");
		TinyLog.debug(new Throwable().getStackTrace()[0], false, "◆END");

	}


	/**
	 * 初期シーンサイズ取得
	 * @param draggableNodes
	 * @param root
	 * @param scene
	 */
	public void getinitScene(List<DraggableNode> draggableNodes, Pane root, Scene scene) {
		TinyLog.debug(new Throwable().getStackTrace()[0], false, "◆START");

		List<BoundingBox> boundingBoxList = getBoundingBoxList(draggableNodes, true);

		// ウィンドウのシーンサイズ
        calcSceneSize(boundingBoxList, root, scene);
        //logger.info("init sceneSize: x=" + sceneSize.getX() + " y=" + sceneSize.getY());

	}

	/**
	 * draggableNodeのリストを計算用にBoundingBoxに変換
	 * @param draggableNodes
	 * @param calcRelative 座標体系を相対座標に変換する（入力座標体系が絶対座標の場合はtrue）
	 * @return
	 */
	private List<BoundingBox> getBoundingBoxList(List<DraggableNode> draggableNodes, boolean calcRelative) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		List<BoundingBox> boundingBoxList = new ArrayList<>();
		for (DraggableNode node: draggableNodes) {
			Point2D coord = new Point2D(node.getLayoutX(), node.getLayoutY());
			Point2D relativeCoord;
			if (calcRelative) {
				relativeCoord = getRelativeCoordinate(coord);
			} else {
				relativeCoord = coord;
			}
			// 2021/2 AW REP start
			//logger.debug("nodeId = "+ node.node.getId());
			//logger.debug("nodeWidth = "+ node.getCalcWidth());
			//logger.debug("nodeHeight = " +  node.getCalcHeight());

			BoundingBox nodeBb = node.getWidth() == 0 && node.getHeight() == 0 ?
				new BoundingBox(relativeCoord.getX(), relativeCoord.getY(), node.getCalcWidth(), node.getCalcHeight())
				:new BoundingBox(relativeCoord.getX(), relativeCoord.getY(), node.getWidth(), node.getHeight());
			//BoundingBox nodeBb = new BoundingBox(relativeCoord.getX(), relativeCoord.getY(), node.getCalcWidth(), node.getCalcHeight());
			boundingBoxList.add(nodeBb);
			// 2021/2 AW REP end
		}
		return boundingBoxList;
	}


	/**
	 * バウンディングボックス変更時（ノード追加・削除・移動、テキスト入力時）の座標計算
	 * @param draggableNodes
	 * @param root
	 * @param scene
	 */
	public void calcGraphCoordinate(List<DraggableNode> draggableNodes, Pane root, Scene scene) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		// 比較対象を事前に取得しておく
		Point2D oldBasePoint = new Point2D(basePoint.getX(), basePoint.getY());
		Point2D oldRelativeTopLeftBbPoint = new Point2D(boundingBox.getMinX(), boundingBox.getMinY());
		Point2D oldAbsoluteTopLeftScenePoint = new Point2D(windowPoint.getX(), windowPoint.getY());

		// ここで取得するノードリストはグラフの絶対座標。
		// 相対座標に変換
		List<BoundingBox> boundingBoxList = getBoundingBoxList(draggableNodes, true);

		// ウィンドウサイズ
		// 変更なし

		// ウィンドウのシーンサイズ
        calcSceneSize(boundingBoxList, root, scene);

		// BoundingBoxの計算
		calcBoundingBox(boundingBoxList);

		// グラフサイズの計算、再描画フラグ判定
		calcGraphSize(boundingBoxList);

		// 基準座標
		calcBasePoint(boundingBoxList);

		// スクロール比率
		calcWindowPoint(boundingBoxList, oldBasePoint, oldRelativeTopLeftBbPoint, oldAbsoluteTopLeftScenePoint);

		//logger.info("calc sceneSize: x=" + sceneSize.getX() + " y=" + sceneSize.getY());
		//logger.info("calc boundingBoxPoint = (" + boundingBox.getMinX() + ", " + boundingBox.getMinY() + ")");
		//logger.info("calc boundingBoxLength = (" + boundingBox.getWidth() + ", " + boundingBox.getHeight() + ")");
		//logger.info("calc graphSize = (" + graphSize.getX() + ", " + graphSize.getY() + ")");
		//logger.info("calc repaintFlag = (" + repaintFlag + ")");
		//logger.info("calc basePoint = (" + basePoint.getX() + ", " + basePoint.getY() + ")");
		//logger.info("calc windowPoint = (" + windowPoint.getX() + ", " + windowPoint.getY() + ")");

		//logger.trace("◆END");
	}


	/**
	 * ウィンドウ変更時（ズーム・ウィンドウサイズ変更時）の座標計算
	 * @param draggableNodes
	 * @param root
	 * @param scene
	 * @param windowX
	 * @param windowY
	 */
	public void calcGraphCoordinateByWindowChange(List<DraggableNode> draggableNodes, Pane root, Scene scene,
			Double windowX, Double windowY) {

		TinyLog.debug(new Throwable().getStackTrace()[0], false, "calcWinBefore windowSize: x=" + windowSize.getX() + " y=" + windowSize.getY());
		TinyLog.debug(new Throwable().getStackTrace()[0], false, "calcWinBefore sceneSize: x=" + sceneSize.getX() + " y=" + sceneSize.getY());
		TinyLog.debug(new Throwable().getStackTrace()[0], false, "calcWinBefore graphSize = (" + graphSize.getX() + ", " + graphSize.getY() + ")");
		TinyLog.debug(new Throwable().getStackTrace()[0], false, "calcWinBefore repaintFlag = (" + repaintFlag + ")");
		TinyLog.debug(new Throwable().getStackTrace()[0], false, "calcWinBefore basePoint = (" + basePoint.getX() + ", " + basePoint.getY() + ")");
		TinyLog.debug(new Throwable().getStackTrace()[0], false, "calcWinBefore windowPoint = (" + windowPoint.getX() + ", " + windowPoint.getY() + ")");


		// ここで取得するノードリストはグラフの絶対座標。
		// 相対座標に変換
		List<BoundingBox> boundingBoxList = getBoundingBoxList(draggableNodes, true);

		// ウィンドウサイズ
		if (windowX != null && windowY != null) {
			windowSize = new Point2D(windowX, windowY);
		}

		// ウィンドウのシーンサイズ
		calcSceneSize(boundingBoxList, root, scene);
		/*
		Point2D sceneToLocal = root.sceneToLocal(0,0);
        Point2D sceneToLocalBottomRight = root.sceneToLocal(scene.getWidth(), scene.getHeight());
        sceneSize = sceneToLocalBottomRight.subtract(sceneToLocal);
        */

		// グラフサイズの計算、再描画フラグ判定
		calcGraphSize(boundingBoxList);

		// 基準座標
		calcBasePoint(boundingBoxList);

		// ウィンドウ左上の絶対座標
		//TODO
		windowPoint = root.sceneToLocal(0,0);

		TinyLog.debug(new Throwable().getStackTrace()[0], false, "calcWinAfter windowSize: x=" + windowSize.getX() + " y=" + windowSize.getY());
		TinyLog.debug(new Throwable().getStackTrace()[0], false, "calcWinAfter sceneSize: x=" + sceneSize.getX() + " y=" + sceneSize.getY());
		TinyLog.debug(new Throwable().getStackTrace()[0], false, "calcWinAfter graphSize = (" + graphSize.getX() + ", " + graphSize.getY() + ")");
		TinyLog.debug(new Throwable().getStackTrace()[0], false, "calcWinAfter repaintFlag = (" + repaintFlag + ")");
		TinyLog.debug(new Throwable().getStackTrace()[0], false, "calcWinAfter basePoint = (" + basePoint.getX() + ", " + basePoint.getY() + ")");
		TinyLog.debug(new Throwable().getStackTrace()[0], false, "calcWinAfter windowPoint = (" + windowPoint.getX() + ", " + windowPoint.getY() + ")");
	}


	/**
	 * シーンサイズを計算する
	 * @param nodeList
	 * @param root
	 * @param scene
	 */
	private void calcSceneSize(List<BoundingBox> nodeList, Pane root, Scene scene) {

		Point2D calcViewSize;

		if (nodeList.isEmpty()) {

			// ノードが空の場合
			// ウィンドウサイズと同じとする
			calcViewSize = new Point2D(windowSize.getX(), windowSize.getY());
			//calcViewSize = new Point2D(sceneSize.getX(), sceneSize.getY());

		} else {

			// ウィンドウのシーンサイズ
			Point2D sceneToLocal = root.sceneToLocal(0,0);
			//FIXME ここの固定値はプログラム的に取得すべき
	        Point2D sceneToLocalBottomRight = root.sceneToLocal(scene.getWidth() - 1, scene.getHeight() - 25);
	        calcViewSize = sceneToLocalBottomRight.subtract(sceneToLocal);

		}
		sceneSize = calcViewSize;
	}

	/**
	 * BoundingBox関連を計算する
	 * @param nodeList
	 */
	private void calcBoundingBox(List<BoundingBox> nodeList) {

		// バウンディングボックスを構成する座標（相対座標）
		double minX;
		double minY;
		double maxX;
		double maxY;

		if (nodeList.isEmpty()) {

			// ノードが空の場合
			boundingBox = new BoundingBox(0 ,0, 0, 0);

			return;

		} else if (nodeList.size() == 1) {

			// ノードが1個の場合は、そのノードから求める
			BoundingBox node = nodeList.get(0);
			minX = node.getMinX();
			minY = node.getMinY();
			maxX = node.getMinX() + node.getWidth();
			maxY = node.getMinY() + node.getHeight();

		} else {

			minX = nodeList.stream()
					.map(node -> node.getMinX())
					.mapToDouble(x -> x)
					.min()
					.orElseThrow(NoSuchElementException::new);

			minY = nodeList.stream()
					.map(node -> node.getMinY())
					.mapToDouble(x -> x)
					.min()
					.orElseThrow(NoSuchElementException::new);

			maxX = nodeList.stream()
					.map(node -> node.getMinX() + node.getWidth())
					.mapToDouble(x -> x)
					.max()
					.orElseThrow(NoSuchElementException::new);

			maxY = nodeList.stream()
					.map(node -> node.getMinY() + node.getHeight())
					.mapToDouble(x -> x)
					.max()
					.orElseThrow(NoSuchElementException::new);
		}

		double bbWidth = maxX - minX;
		double bbHeight = maxY - minY;

		boundingBox = new BoundingBox(minX, minY, bbWidth, bbHeight);
	}

	/**
	 * グラフサイズを計算する
	 * 事前にBoundingBoxを計算しておく必要がある
	 * @param nodeList
	 */
	private void calcGraphSize(List<BoundingBox> nodeList) {

		//Point2D oldGraphSize = graphSize;
		Point2D calcGraphSize;

		if (nodeList.isEmpty()) {

			// ノードが空の場合
			// ウィンドウサイズと同じとする
			if (sceneSize == null) {
				calcGraphSize = new Point2D(windowSize.getX(), windowSize.getY());
			} else {
				calcGraphSize = new Point2D(sceneSize.getX(), sceneSize.getY());
			}

		} else {

			// boundingboxから求める
			double graphWidth;
			double graphHeight;
			if (sceneSize == null) {
				graphWidth = boundingBox.getWidth() + 2 * (windowSize.getX() - overlapX);
				graphHeight = boundingBox.getHeight() + 2 * (windowSize.getY() - overlapY);
			} else {
				graphWidth = boundingBox.getWidth() + 2 * (sceneSize.getX() - overlapX);
				graphHeight = boundingBox.getHeight() + 2 * (sceneSize.getY() - overlapY);
			}

			calcGraphSize = new Point2D(graphWidth, graphHeight);
		}

		//FIXME
		// 再描画判定
		//if (oldGraphSize.getX() != calcGraphSize.getX() || oldGraphSize.getY() != calcGraphSize.getY()) {
		//	repaintFlag = true;
		//}

		graphSize = calcGraphSize;
	}

	/**
	 * グラフ座標→相対座標のための基準座標を計算する
	 * 事前にBoundingBoxを計算しておく必要がある
	 * @param nodeList
	 */
	private void calcBasePoint(List<BoundingBox> nodeList) {

		if (nodeList.isEmpty()) {

			// ノードが空の場合
			// ウィンドウサイズの中央にする
			if (sceneSize == null) {
				basePoint = new Point2D(windowSize.getX() / 2, windowSize.getY() / 2);
			} else {
				basePoint = new Point2D(sceneSize.getX() / 2, sceneSize.getY() / 2);
			}

		} else {

			// 新しいグラフでBoundingBox左上部の絶対座標
			Point2D absoluteTopLeftBbPoint;
			if (sceneSize == null) {
				absoluteTopLeftBbPoint = new Point2D(windowSize.getX() - overlapX, windowSize.getY() - overlapY);
			} else {
				absoluteTopLeftBbPoint = new Point2D(sceneSize.getX() - overlapX, sceneSize.getY() - overlapY);
			}

			// BBの左上部の相対座標を引くことで、ベースポイントを求める
			Point2D relativeTopLeftBbPoint = new Point2D(boundingBox.getMinX(), boundingBox.getMinY());

			basePoint = absoluteTopLeftBbPoint.subtract(relativeTopLeftBbPoint);
		}
	}

	/**
	 * ウィンドウ始点を計算する
	 * @param nodeList
	 * @param oldBasePoint
	 * @param oldRelativeTopLeftBbPoint
	 * @param oldAbsoluteTopLeftWindowPoint
	 */
	private void calcWindowPoint(List<BoundingBox> nodeList, Point2D oldBasePoint,
										Point2D oldRelativeTopLeftBbPoint, Point2D oldAbsoluteTopLeftWindowPoint) {
		//logger.trace("◆START");

		//logger.debug("oldBasePoint; = (" + oldBasePoint.getX() + ", " + oldBasePoint.getY() + ")");
		//logger.debug("oldRelativeBoundingBoxPoint; = (" + oldRelativeTopLeftBbPoint.getX() + ", " + oldRelativeTopLeftBbPoint.getY() + ")");
		//logger.debug("oldAbsoluteWindowPoint; = (" + oldAbsoluteTopLeftWindowPoint.getX() + ", " + oldAbsoluteTopLeftWindowPoint.getY() + ")");

		if (nodeList.isEmpty()) {
			// ノードがない場合は変更なし
			//logger.debug("nodeList empty. No windowPoint change.");
			return;
		}

		Point2D relativeTopLeftBbPoint = new Point2D(boundingBox.getMinX(), boundingBox.getMinY());

		if (oldRelativeTopLeftBbPoint.getX() == relativeTopLeftBbPoint.getX() &&
				oldRelativeTopLeftBbPoint.getY() == relativeTopLeftBbPoint.getY()) {

			// バウンディングボックス左上の相対座標が変わらない場合は変更なし
			//logger.debug("No boundingBox change. No windowPoint change.");
			return;
		}

		if (nodeList.size() == 1) {

			//logger.debug("node count = 1");
			//logger.debug("relativeBoundingBoxPoint; = (" + boundingBox.getMinX() + ", " + boundingBox.getMinY() + ")");

			// バウンディングボックス左上の絶対座標（描画前）を取得
			Point2D absoluteBoundingBoxPoint = oldBasePoint.add(relativeTopLeftBbPoint);
			//logger.debug("absoluteBoundingBoxPoint; = (" + absoluteBoundingBoxPoint.getX() + ", " + absoluteBoundingBoxPoint.getY() + ")");

			// ウィンドウ左上の絶対座標（描画前）を取得
			//absoluteTopLeftWindowPoint = new Point2D((windowSize.getX() - overlapX) , (windowSize.getY() - overlapY))
			//													.subtract(absoluteTopLeftBbPoint.subtract(oldAbsoluteTopLeftWindowPoint));
			windowPoint = new Point2D((sceneSize.getX() - overlapX) , (sceneSize.getY() - overlapY))
																		.subtract(absoluteBoundingBoxPoint.subtract(oldAbsoluteTopLeftWindowPoint));

		} else {

			//logger.debug("node count >= 2");
			//logger.debug("relativeBoundingBoxPoint; = (" + boundingBox.getMinX() + ", " + boundingBox.getMinY() + ")");

			// BoundingBoxの左上の差分を取得
			double deltaX = oldRelativeTopLeftBbPoint.getX() - relativeTopLeftBbPoint.getX();
			double deltaY = oldRelativeTopLeftBbPoint.getY() - relativeTopLeftBbPoint.getY();
			//logger.debug("deltaX; = (" + oldRelativeTopLeftBbPoint.getX() + " - " + relativeTopLeftBbPoint.getX() + ")");
			//logger.debug("deltaY; = (" + oldRelativeTopLeftBbPoint.getY() + " - " + relativeTopLeftBbPoint.getY() + ")");

			// deltaがマイナスの場合はウィンドウ位置を変更する必要がない
			//if (deltaX > 0 && deltaY > 0) {

				Point2D newAbsoluteTopLeftWindowPoint = new Point2D((windowPoint.getX() + deltaX), (windowPoint.getY() + deltaY));
				windowPoint = newAbsoluteTopLeftWindowPoint;
				//logger.debug("delta; = (" + deltaX + ", " + deltaY + ")");

				//} else {
			//	System.out.println("delta < 0. absoluteViewPoint no change.");
			//	System.out.println("absoluteViewPoint: = (" + absoluteViewPoint.getX() + ", " + absoluteViewPoint.getY() + ")");
			//}
		}

		//logger.debug("absoluteWindowPoint: = (" + windowPoint.getX() + ", " + windowPoint.getY() + ")");
		//logger.debug("scrollRateX; = (" + windowPoint.getX() + ") / ( " + graphSize.getX() + " - " + sceneSize.getX() + ")");
		//logger.debug("scrollRateY; = (" + windowPoint.getY() + ") / ( " + graphSize.getY() + " - " + sceneSize.getY() + ")");
	}


	/**
	 * グラフの絶対座標から相対座標を求める
	 * @param graphCoordinate
	 * @return
	 */
	public Point2D getRelativeCoordinate(Point2D absoluteCoordinate) {
		double pointX = absoluteCoordinate.getX();
		double pointY = absoluteCoordinate.getY();

		Point2D relativeCoordinate = new Point2D(pointX - basePoint.getX(), pointY - basePoint.getY());

		//logger.debug("絶対→相対: (" + pointX + ", " + pointY + ") → (" + relativeCoordinate.getX() + ", " + relativeCoordinate.getY() + ")");

		return relativeCoordinate;
	}

	/**
	 * 相対座標からグラフの絶対座標を求める
	 * @param relativeCoordinate
	 * @return
	 */
	public Point2D getAbsoluteCoordinate(Point2D relativeCoordinate) {
		double pointX = relativeCoordinate.getX();
		double pointY = relativeCoordinate.getY();

		Point2D absoluteCoordinate = new Point2D(pointX + basePoint.getX(), pointY + basePoint.getY());

		//logger.debug("相対→絶対: (" + pointX + ", " + pointY + ") → (" + absoluteCoordinate.getX() + ", " + absoluteCoordinate.getY() + ")");

		return absoluteCoordinate;
	}


	/**
	 * 縦横スクロール比からウィンドウ始点を設定する
	 * @param hvalue
	 * @param vvalue
	 */
	public void setAbsoluteViewPoint(double hvalue, double vvalue) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		double absoluteViewPointX = (graphSize.getX() - sceneSize.getX()) * hvalue;
		double absoluteViewPointY = (graphSize.getY() - sceneSize.getY()) * vvalue;

		windowPoint = new Point2D(absoluteViewPointX, absoluteViewPointY);

		//logger.trace("windowPoint = ({}, {})", absoluteViewPointX, absoluteViewPointY);
	}

	/**
	 * ウィンドウ始点座標、グラフサイズ、シーンサイズからスクロール比を取得
	 * @return
	 */
	public double getHvalue() {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		double hvalue = 0.0;
		if (graphSize.getX() == sceneSize.getX()) {
			hvalue = 0.0;
		} else {
			hvalue = windowPoint.getX() / (graphSize.getX() - sceneSize.getX());
		}
		//logger.trace("hvalue; = {}", hvalue);

		return hvalue;
	}

	/**
	 * ウィンドウ始点座標、グラフサイズ、シーンサイズからスクロール比を取得
	 * @return
	 */
	public double getVvalue() {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		double vvalue = 0.0;
		if (graphSize.getY() == sceneSize.getY()) {
			vvalue = 0.0;
		} else {
			vvalue = windowPoint.getY() / (graphSize.getY() - sceneSize.getY());
		}
		//logger.trace("vvalue; = {}", vvalue);

		return vvalue;
	}

	/**
	 * ズーム比が変わったかを返す
	 * @param inValue
	 * @return
	 */
	public boolean isChangeScaleValue(double inValue) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		if (inValue == scale) {
			return false;
		}
		return true;
	}

	/**
	 * @return windowSize
	 */
	public Point2D getWindowSize() {
		return windowSize;
	}

	/**
	 * @return graphSize
	 */
	public Point2D getGraphSize() {
		return graphSize;
	}

	/**
	 * @return sceneSize
	 */
	public Point2D getSceneSize() {
		return sceneSize;
	}


	/**
	 * @return boundingBox
	 */
	public BoundingBox getBoundingBox() {
		return boundingBox;
	}

	/**
	 * @return basePoint
	 */
	public Point2D getBasePoint() {
		return basePoint;
	}

	/**
	 * @return repaintFlag
	 */
	public boolean isRepaintFlag() {
		return repaintFlag;
	}

	/**
	 * @return windowPoint
	 */
	public Point2D getWindowPoint() {
		return windowPoint;
	}

	/**
	 * @param windowPoint セットする windowPoint
	 */
	public void setWindowPoint(Point2D windowPoint) {
		this.windowPoint = windowPoint;
	}

	/**
	 * @return scale
	 */
	public double getScale() {
		return scale;
	}

	/**
	 * @param scale セットする scale
	 */
	public void setScale(double scale) {
		this.scale = scale;
	}
}
