package com.assemblogue.plr.app.generic.semgraph.ui.control;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.Locale;

import com.assemblogue.plr.contentsdata.PLRContentsData;
import com.assemblogue.plr.contentsdata.PLRContentsData.InputtableProperty;
import com.assemblogue.plr.contentsdata.ontology.OntologyItem;
import com.assemblogue.plr.util.I18nUtils;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class SelectClassDialog {

	/** 無視idリスト */
	private static List<String> ignoreIdList = List.of("#Dummy","#M");			//★★UPDATE  [フェーズ2] Mクラスを表示しない
	//private static List<String> ignoreIdList = List.of("#Dummy");					//★★

	/** 画面サイズ */
	private static final  double HEIGHT = 600;
	private static final  double WIDTH = 600;

	/** PLRContentsData */
	private PLRContentsData contentsData;

	/** 選択したクラス */
	private String selectClassType = null;
	public String getSelectClassType() {
		return selectClassType;
	}

	/** 画面遷移保存用 */
	private Deque<SceneAndTitle> sceneStack = new ArrayDeque<>();

	/** ダイアログのStage */
	Stage stage = new Stage();

	/**
	 * コンストラクタ
	 * @param contentsData
	 */
	public SelectClassDialog(PLRContentsData contentsData) {
	//public SelectClassDialog(PLRContentsData contentsData,ActionEvent event) {

		this.contentsData = contentsData;

		VBox contentsVBox = makeSelectTopClassMenu();

		stage.setScene(new Scene(contentsVBox));
		stage.setMaxHeight(HEIGHT);
		stage.setMaxWidth(WIDTH);

		//縮小は初期値の七割まで許容
		//double minPerCent = 0.7;
		//stage.setMinHeight(HEIGHT * minPerCent);
		//stage.setMinWidth(WIDTH * minPerCent);

		// モーダル指定でダイアログ表示
		stage.initModality(Modality.APPLICATION_MODAL);

		//Window owner =((MenuItem)event.getSource()).getParentPopup().getOwnerWindow();
		//親ウィンドウの真ん中に配置する
		//double x = owner.getX() + owner.getWidth() / 2;
		//double y = owner.getY() + owner.getHeight() / 2;
		//stage.setX(x - stage.getScene().getRoot().getLayoutBounds().getWidth() / 2);
		//stage.setY(y - stage.getScene().getRoot().getLayoutBounds().getHeight() / 2);

        stage.showAndWait();
	}

	/**
	 * トップクラスからメニューコンテンツを構築
	 * @return
	 */
    private VBox makeSelectTopClassMenu() {

    	// トップクラス
    	List<OntologyItem> topList = contentsData.topClasses();

    	// タイトル
    	StringBuilder title = new StringBuilder();

    	// メニューリストの作成
    	List<OntologyItem> menuItemList = new ArrayList<>();

    	for (OntologyItem i: topList) {
    		List<OntologyItem> tempList = contentsData.getMenuItem(i);

    		// メニューアイテムがない場合は、タイトル表示しない。
    		if (!tempList.isEmpty()) {
    			menuItemList.addAll(tempList);

        		title.append(getDisplayText(i));
        		title.append(' ');
    		}
    	}

       	// タイトル設定
    	stage.setTitle(title.toString());

    	// 表示するアイテムの根チェックを行い、メニューリストを取得
    	menuItemList = getDisplayRootItem(menuItemList);

    	// メニューの中身
    	VBox vBoxMenu = makeVBox(menuItemList);

      	// コンテンツ
    	VBox vBox = new VBox(5d);
    	ScrollPane scrollPane = new ScrollPane();
		scrollPane.setContent(vBoxMenu);
		scrollPane.setMinHeight(400);
		scrollPane.setMinWidth(400);
    	//scrollPane.setMinHeight(HEIGHT * 0.8);

     	// ボタン行
    	HBox buttonRowHBox = new HBox(30);

    	// キャンセルボタン
    	Button cancelButton = new Button("Cancel");
    	cancelButton.setOnAction(mouseEvent -> execCancelButton());
    	cancelButton.setCancelButton(true);

    	/*
    	Button backButton = new Button("<  Back");
    	backButton.setOnAction(mouseEvent -> {
    		if(sceneStack.size() > 0) {
    			SceneAndTitle backScene =  sceneStack.pop();
    			stage.setTitle(backScene.getTitile());
    			stage.getScene().setRoot(backScene.getParent());
    		}else {
    			String headerString = "警告";
    			String masege = "これ以上戻れません。";
    			AlertDialog dialog = new AlertDialog(AlertType.WARNING,headerString,masege,stage);
    			dialog.show();
    		}

    	});
    	*/

    	buttonRowHBox.getChildren().addAll(cancelButton);
		buttonRowHBox.setPadding(new Insets(10));
    	buttonRowHBox.setAlignment(Pos.CENTER);

    	vBox.getChildren().addAll(scrollPane,buttonRowHBox);

    	return vBox;
    }

    /**
     * アイテムリストが1個で、そのアイテムが根の場合は
     * そのアイテムを基にして（1階層すすめて）展開する
     * @param oiList
     * @return
     */
    private List<OntologyItem> getDisplayRootItem(List<OntologyItem> oiList) {

    	// 戻り
    	List<OntologyItem> resultList = new ArrayList<>();

    	if (oiList.size() == 1 && oiList.get(0).isRoot()) {
    		OntologyItem oi = oiList.get(0);
    		resultList.addAll(oi.superClassOf);
    	} else {
    		resultList.addAll(oiList);
    	}

    	return resultList;
    }

    /**
     * 引数のクラスからメニューコンテンツを構築
     * @param item
     * @return
     */
    private VBox makeSelectChildClassMenu(OntologyItem item) {

		//DEBUG
    	/*
		System.out.println(I18nUtils.getValue(item.label, "ja"));
		System.out.println("class=" + item.isClass());
		System.out.println("property=" + item.isProperty());
		System.out.println("root=" + item.isRoot());
		System.out.println("input=" + item.isInputtable());
		System.out.println("hidden=" + item.isHidden());
		System.out.println("range=" + item.range);
		if (item.format != null) {
			System.out.println("format=" + item.format.get("ja"));
		}
		// サブクラス
		List<OntologyItem> subClassList = getSubClass(item);
		for (OntologyItem oi: subClassList) {
			System.out.println("subClass=" + oi.id);
		}
		System.out.println();
		*/

    	// タイトル
    	String title = getDisplayText(item);
    	// タイトル設定
    	stage.setTitle(title);

    	// メニュー
    	// PLRcontentsData.getMenuItemだと、子がすべて展開されるので
    	// 階層が作れない
    	List<OntologyItem> menuItemList = item.superClassOf;
    	//List<OntologyItem> menuItemList = contentsData.getMenuItem(item);

    	// メニューの中身
    	VBox vBoxMenu = makeVBox(menuItemList);

    	// コンテンツ
    	VBox vBox = new VBox(5d);
    	ScrollPane scrollPane = new ScrollPane();
		scrollPane.setContent(vBoxMenu);
		scrollPane.setMinHeight(400);
		scrollPane.setMinWidth(400);
    	//scrollPane.setMinHeight(HEIGHT * 0.8);

     	// キャンセルボタン行
    	HBox buttonRowHBox = new HBox(30);
    	Button cancelButton = new Button("Cancel");
    	cancelButton.setOnAction(mouseEvent -> execCancelButton());
    	cancelButton.setCancelButton(true);

    	// backボタン
    	Button backButton = new Button("< Back");
    	backButton.setOnAction(mouseEvent -> {
    		if(sceneStack.size() > 0) {
    			SceneAndTitle backScene =  sceneStack.pop();
    			stage.setTitle(backScene.getTitile());
    			stage.getScene().setRoot(backScene.getParent());
    		} else {
    			String headerString = "警告";
    			String masege = "これ以上戻れません。";
    			AlertDialog dialog = new AlertDialog(AlertType.WARNING,headerString,masege,stage);
    			dialog.show();
    		}

    	});

    	buttonRowHBox.getChildren().addAll(backButton, cancelButton);
		buttonRowHBox.setPadding(new Insets(10));
    	buttonRowHBox.setAlignment(Pos.CENTER);

    	vBox.getChildren().addAll(scrollPane, buttonRowHBox);

    	return vBox;
    }

    /**
     * メニューコンテンツ構築（実体）
     * @param itemList
     * @return
     */
    private VBox makeVBox(List<OntologyItem> itemList) {

    	// 戻り
    	VBox vBox = new VBox();

    	//TODO
    	// 無視するクラスリストとチェック
    	// 本来いらない処理のハズだが、現状はダミーデータがあり、
    	// それを表示させないため。本処理を実施
    	//------																	★★ UPDATE [フェーズ2] Mクラスを表示しない処理の追加 ここから↓
		for(int i =  itemList.size() - 1 ;i >=0; i--) {
			OntologyItem oi = itemList.get(i);
    		if (ignoreIdList.contains(oi.id)) {
    			itemList.remove(i);
    		}
		}
//    	boolean isDel = false;
//    	int i = 0;
//    	for (i = 0; i < itemList.size(); i++) {
//    		OntologyItem oi = itemList.get(i);
//    		String id = oi.id;
//    		if (ignoreIdList.contains(id)) {
//    			isDel = true;
//    			break;
//    		}
//    	}
//    	if (isDel) {
//    		itemList.remove(i);
//    	}
    	//------																	★★ UPDATE [フェーズ2] Mクラスを表示しない処理の追加 ここまで↑
    	//TODO ここまで

    	// フォント指定
    	//String fontStyle = "-fx-font-size: 16px;";
    	for (OntologyItem item: itemList) {

    		//DEBUG
    		/*
    		System.out.println(I18nUtils.getValue(item.label, "ja"));
			System.out.println("class=" + item.isClass());
			System.out.println("property=" + item.isProperty());
			System.out.println("root=" + item.isRoot());
			System.out.println("input=" + item.isInputtable());
			System.out.println("hidden=" + item.isHidden());
			System.out.println("range=" + item.range);

			if (item.format != null) {
				System.out.println("format=" + item.format.get("ja"));
			}
			// サブクラス
			List<OntologyItem> subClassList = getSubClass(item);
			for (OntologyItem oi: subClassList) {
				System.out.println("subClass=" + oi.id);
			}
			System.out.println();
			//DEBUG
    		*/

    		// ラベル作成
        	Label label = makeLabel(item);
            label.setWrapText(true);// 折り返しあり
            //label.setStyle(AppProperty.REL_LABEL_STYLE + fontStyle);
            label.setPrefWidth(120);

            // ラベルをHBoxに入れてからVBoxに入れる。横への展開のため
        	HBox hBox = new HBox(5d);
			hBox.getChildren().add(label);
			// 子がある場合は、子をHBoxに入れる
			// 但し根の場合は展開しないので、子を渡さない
			if (!item.isRoot()) {

				List<OntologyItem> childList = item.superClassOf;
		    	if (childList != null) {
		    		VBox childVBox = makeVBox(childList);
		    		hBox.getChildren().add(childVBox);
		    	}
			}
			vBox.getChildren().add(hBox);
    	}
    	return vBox;
    }

    /**
     * ontologyItemからラベルを作成
     * @param item
     * @return
     */
    public Label makeLabel(OntologyItem item) {

    	Label label = null;

    	// クラスではない場合はnullを返す
    	if (!item.isClass()) {
    		return label;
    	}

    	// 隠し属性は、仮にラベル表示なしにしておく
    	//TODO
    	//if (item.isHidden()) {
    	//	label = new Label("");
    	//	return label;
    	//}

    	if (item.isInputtable()) {
    		// 入力可能な場合はラベル設定
    		label = new Label(getDisplayText(item));
    		//label = new OntologyLabel(item);
    		label.setStyle("-fx-background-color:rgb(137,255,206); -fx-border-style:solid; -fx-border-width:1.0; -fx-border-insets:0; -fx-border-color:white; -fx-font-size: 16px;");
    		// アクションメソッド設定
    		label.setOnMouseClicked(event -> getClassType(item));

    	} else {

    		if (item.isRoot()) {
    	   		// 入力不可能な場合、かつ根の場合はラベル設定かつ”＞”表示

    			String str = getDisplayText(item) + "   >";
    			label = new Label(str);
    			//label = new OntologyLabel(item);
    			//label.setText(label.getText() + "  >");
    			label.setStyle("-fx-background-color:rgb(137,255,206); -fx-border-style:solid; -fx-border-width:1.0; -fx-border-insets:0; -fx-border-color:white; -fx-font-size: 16px;");
    			// アクションメソッド設定
    			label.setOnMouseClicked(event -> setChildMenu(item));

    		} else {
    			// 入力不可能かつ根でない場合は、薄字ラベル（クリック不可）
    			label = new Label(getDisplayText(item));
    			//label = new OntologyLabel(item);
    			label.setStyle("-fx-background-color:rgb(220, 224, 204); -fx-border-style:solid; -fx-border-width:1.0; -fx-border-insets:0; -fx-border-color:white; -fx-font-size: 16px;");
    			//label.setStyle("-fx-background-color:rgb(220, 224, 204); -fx-border-style:solid; -fx-border-width:1.0; -fx-border-insets:0; -fx-border-color:white; -fx-text-fill:gray; -fx-font-size: 16px;");
    		}
    	}
   		return label;
    }

    /**
     * クラスのLabelを選択したときのアクションメソッド
     * @param item
     */
    private void getClassType(OntologyItem item) {

    	selectClassType = item.id;
    	stage.close();
    }

    /**
     * キャンセルボタンのアクションメソッド
     */
    private void execCancelButton() {
		stage.close();
	}

    /**
     * 根のクラスのLabelを選択したときのアクションメソッド
     * @param item
     */
    private void setChildMenu(OntologyItem item) {

    	// 表示コンテンツをセーブ
		sceneStack.push(new SceneAndTitle(stage.getTitle(), stage.getScene().getRoot()));

    	// 引数のクラスからメニューを作成
    	VBox childMenu = makeSelectChildClassMenu(item);

		// タイトル
    	String title = getDisplayText(item);
    	stage.setTitle(title);

    	stage.getScene().setRoot(childMenu);
    }

    /**
     * 画面遷移時の保存用クラス
     */
    private class SceneAndTitle {
    	private String title;
    	private Parent parent;

    	public SceneAndTitle(String title,Parent parent) {
    		this.title = title;
    		this.parent = parent;
    	}
    	public String getTitile() {
    		return this.title;
    	}
    	public Parent getParent() {
    		return this.parent;
    	}


    }
    ///////////////////////
    // ユーティリティ
    ///////////////////////

    /**
     * パラメータのOntologyItemから表示テキストを取得
     * @param ontologyItem
     * @return
     */

    //FIXME　Mクラス処理をさきに
    private String getDisplayText(OntologyItem ontologyItem) {

    	String result = null;

    	if (Locale.getDefault().getLanguage().equals("ja")) {
    		result = I18nUtils.getValue(ontologyItem.label, "ja");
    		//TODO 日本語の定義がない場合（URL等）
    		if (result == null) {
    			result = I18nUtils.getValue(ontologyItem.label);
    		}
    	} else {
    		result = I18nUtils.getValue(ontologyItem.label);
    	}

    	// Mクラス対応特別処理
    	// Mクラスは、ラベルにデータが入ってない。
    	// コメントに名称が入っているので、そちらも検索する。
    	if ((ontologyItem.id).equals("#M")) {
    		if (result == null) {
    			if (Locale.getDefault().getLanguage().equals("ja")) {
    	    		result = I18nUtils.getValue(ontologyItem.comment, "ja");
    	    		//TODO 日本語の定義がない場合（URL等）
    	    		if (result == null) {
    	    			result = I18nUtils.getValue(ontologyItem.comment);
    	    		}
    			}
    		}
    	}
    	return result;
   	}


    private void getProperties(OntologyItem item) {

    	//List<OntologyItem> properties = contentsData.getProperty(item);
    	List<InputtableProperty> propertyMenu = contentsData.PropertyMenu(item);

    	System.out.println(getDisplayText(item));
		if (item.format != null) {
			System.out.println("format=" + item.format.get("ja"));
		}
    	System.out.println();

    	for (InputtableProperty i: propertyMenu) {

    		System.out.println(getDisplayText(i.item));
    		System.out.println("min=" + i.minCardinality);
			System.out.println("max=" + i.maxCardinality);
			if (i.item.format != null) {
				System.out.println("format=" + i.item.format.get("ja"));
			}
			System.out.println("range=" + i.item.range);
			if (contentsData.getRange(i.item) != null) {
				for (OntologyItem oi: contentsData.getRange(i.item)) {
					System.out.println(getDisplayText(oi));

				}
			}
			System.out.println();

    	}

    	return ;

    }

    private List<OntologyItem> getSubClass(OntologyItem item) {

    	List<OntologyItem> resultList = new ArrayList<>();

    	if (item.subClassOf != null) {
			for (OntologyItem oi: item.subClassOf) {
				if (oi.isClass()) {
					resultList.add(oi);
					List<OntologyItem> tempList = getSubClass(oi);
					resultList.addAll(tempList);
				}
			}
		}

    	return resultList;

    }



}
