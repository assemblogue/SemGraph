package com.assemblogue.plr.app.generic.semgraph.ui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.UUID;

import com.assemblogue.plr.app.generic.semgraph.AppProperty;
import com.assemblogue.plr.app.generic.semgraph.GraphActor;
import com.assemblogue.plr.app.generic.semgraph.OntMenu;
import com.assemblogue.plr.app.generic.semgraph.OntMenu.OntMenuItem;
import com.assemblogue.plr.app.generic.semgraph.TinyLog;
import com.assemblogue.plr.app.generic.semgraph.ui.controller.RootLayoutController;
import com.assemblogue.plr.contentsdata.PLRContentsData;
import com.assemblogue.plr.contentsdata.ontology.OntologyItem;
import com.assemblogue.plr.lib.EntityNode;

import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.binding.DoubleBinding;
import javafx.beans.binding.When;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Point2D;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;
import javafx.scene.text.Font;

public class NodeLink extends Pane {

	//private Logger logger = LogManager.getLogger();

	@FXML
	Line node_link;
	// @FXML
	// Polygon arrowleft;
	@FXML
	public Polygon arrowright;
	// @FXML
	// AnchorPane anchorpaneleft;
	@FXML
	public AnchorPane anchorpaneright;
	@FXML
	Pane linkpane;
	@FXML
	Pane attribute;
	@FXML
	public Circle circle1;
	@FXML
	Circle circleX;
	@FXML
	Circle circleY;

	@FXML
	HBox relation;

	String relationValue = null;
	private GraphActor graphAct;
	private RootLayoutController rootlayout;
	//private OntMenu ontmenu;
	boolean toLineEnd = true;
	//private final DoubleProperty mControlOffsetX = new SimpleDoubleProperty();
	//private final DoubleProperty mControlOffsetY = new SimpleDoubleProperty();
	private final DoubleProperty mControlDirectionX1 = new SimpleDoubleProperty();
	//private final DoubleProperty mControlDirectionY1 = new SimpleDoubleProperty();
	private final DoubleProperty mControlDirectionX2 = new SimpleDoubleProperty();
	//private final DoubleProperty mControlDirectionY2 = new SimpleDoubleProperty();
	private final List<String> mLinkIds = new ArrayList<>();
	//private EventHandler<MouseEvent> ArrowRightVisible;
	private Label relationLabel = new Label();
	private String UNCERTAIN = "uncertain";
	ContextMenu contextmenu = new ContextMenu();
	MenuItem deleteEdge = new MenuItem("Delete Link");
	MenuItem reverseLink = new MenuItem(" Reverse Link ");
	private DraggableNode sourceNode, targetNode = null;
	private String relationType;

	private String sourceClassType;
	private String targetClassType;

	/*
	public NodeLink(GraphActor gact) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		this.graphAct = gact;

		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("NodeLink.fxml"));

		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);

		try {
			fxmlLoader.load();

		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}

		// provide a universally unique identifier for this object
		setId(UUID.randomUUID().toString());
	}
	*/

	public NodeLink(GraphActor gact, String relationValue, RootLayoutController rootlayout, String relationType,
						String sourceClassType, String targetClassType) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		this.graphAct = gact;
		this.relationValue = relationValue;
		this.rootlayout = rootlayout;
		this.relationType = relationType;

		this.sourceClassType = sourceClassType;
		this.targetClassType = targetClassType;

		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("NodeLink.fxml"));

		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);

		try {
			fxmlLoader.load();

		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}

		// provide a universally unique identifier for this object
		setId(UUID.randomUUID().toString());
	}

	@FXML
	private void initialize() {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		mControlDirectionX1.set(100);
		mControlDirectionX2.set(50);
		arrowright.setVisible(true);
		linkpane.setPickOnBounds(false);
		circle1.setVisible(false);
		contextmenu.getItems().addAll(deleteEdge, reverseLink);
		if(AppProperty.TYPE_SYMMETRICK.equals(relationType)) {
 			reverseLink.setDisable(true);
		}
		anchorpaneright.addEventFilter(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				//logger.debug("◆START");

				// System.out.println("arrow click RIGHT");
				// arrowright.setVisible(true);

				event.consume();

			}

		});

		attribute.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

				if (event.getButton().equals(MouseButton.SECONDARY)) {
					//logger.debug("right click.");

					// グラフ画面のコンテキストメニューが表示されている場合は消去
					if(rootlayout.contextmenu_right_pane.isShowing()) {
						rootlayout.contextmenu_right_pane.hide();
					}

					contextmenu.show(attribute, event.getScreenX(), event.getScreenY());
				}
				deleteEdge.setOnAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event1) {
						TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

						Alert alert = new Alert(AlertType.CONFIRMATION);
						alert.setTitle("Delete Link");
						alert.setContentText("Do you want to delete the link?");
						//ButtonType ok = new ButtonType("Ok");
						//ButtonType cancel = new ButtonType("Cancel");
						// alert.getButtonTypes().addAll(ok,cancel);

						// 削除確認のウィンドウを開いたとき、閉じたとき
						alert.showingProperty().addListener(((observable, oldValue, newValue) -> {
							//logger.debug("DeleteLink showingProperty old={}, new={}", oldValue, newValue);

							if (oldValue == true && newValue == false) {
								// ダイアログを閉じた時

								// 描画中フラグ
								rootlayout.graphRepaintingFlag = false;

							} else if (oldValue == false && newValue == true) {
								// ダイアログを開いたとき

								// 描画中フラグ
								rootlayout.graphRepaintingFlag = true;
							}
						}));

						Optional<ButtonType> result = alert.showAndWait();

						if (!ButtonType.CANCEL.equals(result.get())) {
							//DraggableNode dragnode = null;

							Pane parent = (Pane) node_link.getParent();
							attribute.setVisible(false);
							anchorpaneright.setVisible(false);

							EntityNode parant = graphAct.getNode(mLinkIds.get(0));
							EntityNode child = graphAct.getNode(mLinkIds.get(1));

							// リンク削除
							graphAct.deleteEdge(parant, child);

							attribute.setVisible(false);
							arrowright.setVisible(false);
							circle1.setVisible(false);
							parent.getChildren().remove(node_link);
							mLinkIds.remove(node_link.getId());// added on 8dec2019
     							event.consume();

 						}
					}
				});

				reverseLink.setOnAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event2) {
						TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

						NodeLink nodeLinkPane = (NodeLink) node_link.getParent();
 						//EntityNode parent_node = graphAct.getNode(mLinkIds.get(0));
						//EntityNode child_node = graphAct.getNode(mLinkIds.get(1));
						//String relation = relationValue;
						Pane parent = (Pane) node_link.getParent();
						attribute.setVisible(false);
						anchorpaneright.setVisible(false);
						EntityNode parant = graphAct.getNode(mLinkIds.get(0));
						EntityNode child = graphAct.getNode(mLinkIds.get(1));

						// リンク削除
						// 2021/2 AW REP start
						graphAct.reverseEdge(parant, child);
						//graphAct.deleteEdge(parant, child);
						// 2021/2 AW REP end

						attribute.setVisible(false);
						arrowright.setVisible(false);
						circle1.setVisible(false);
						parent.getChildren().remove(node_link);
						mLinkIds.remove(node_link.getId());

						// リンク追加
 						rootlayout.drawNodeLink(nodeLinkPane.targetNode, nodeLinkPane.sourceNode,
								nodeLinkPane.relationValue);
					}
				});
			}
		});
		mControlDirectionX1.bind(
				new When(node_link.startXProperty().greaterThan(node_link.endXProperty())).then(-1.0).otherwise(1.0));

		mControlDirectionX2.bind(
				new When(node_link.startXProperty().greaterThan(node_link.endXProperty())).then(1.0).otherwise(-1.0));

		/*
		parentProperty().addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				//logger.trace("◆START observe={}, oldValue={}, newValue={}", observable, oldValue, newValue);

			}

		});
		*/

		// 右クリックのコンテキストメニューを開いたとき、閉じたとき
		contextmenu.showingProperty().addListener(((observable, oldValue, newValue) -> {
			//logger.debug("NodeLink　ContextMenu showingProperty old={}, new={}", oldValue, newValue);

			if (oldValue == true && newValue == false) {
				// メニューを閉じた時

				// 描画中フラグ
				rootlayout.graphRepaintingFlag = false;

			} else if (oldValue == false && newValue == true) {
				// メニューを開いたとき

				// 描画中フラグ
				rootlayout.graphRepaintingFlag = true;
			}
		}));

		setRelationValue(relationValue, sourceClassType, targetClassType);

	}

	public void registerLink(String linkId) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		mLinkIds.add(linkId);
	}


	private void setRelationValue(String newValue, String sourceClassType, String targetClassType) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		relationValue = newValue;
 		relationLabel.setFont(new Font("Arial", 19));
		relationLabel.setStyle("-fx-font-weight: bold");

		relationLabel.setWrapText(true);
 		relationLabel.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				//logger.debug("◆START");

				relationLabel.setPrefWidth(relationLabel.getText().length() * 12); // why 7? Totally trial number.
			}
		});

		//relation.setHgrow(relationLabel, Priority.ALWAYS);
 		HBox.setHgrow(relationLabel, Priority.ALWAYS);
		relation.setMaxWidth(Double.MAX_VALUE);
		relation.setMaxHeight(Double.MAX_VALUE);
		createOntMenu(relationValue, sourceClassType, targetClassType);
 		attribute.setVisible(true);
	}

	private OntMenuItem getRelationValue(String relationValue ,List<OntMenu.OntMenuItem> sub_omi_list) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

 		OntMenuItem ont_Menu_Item= null;
		for (OntMenuItem sub_omi : sub_omi_list) {
 			if((sub_omi.en_label != null &&  sub_omi.en_label.equals(relationValue)) || (sub_omi.ja_label != null &&  sub_omi.ja_label.equals(relationValue))) {
 				ont_Menu_Item = sub_omi;
				break;

			}

			if(ont_Menu_Item==null && sub_omi.child != null) {
				ont_Menu_Item = getRelationValue(relationValue,sub_omi.child);
			}else {
				return ont_Menu_Item;
			}
		}

		return ont_Menu_Item;
	}


	private void createOntMenu(String relationValue, String sourceClassType, String targetClassType) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		// リンク用オントロジーのトップ
		String item_id = "#Entity";

		// リンク用のコンテンツデータ
		PLRContentsData linkContentsData = graphAct.getGraphData().getLinkContentsData();

		// リンク用のプロパティ名
		// 本来はgraphRelationなのだが、現状はオントロジーの古いものが登録されているため
		// 従来のものも入れておく。
		String omi_id = null;
		List<String> omiIdList = List.of("#graphRelation", "#rel");
		for (String omiId : omiIdList) {
			if (linkContentsData.getNode(omiId) != null) {
				omi_id = omiId;
				break;
			}
		}

		// リンクの値
		String val = relationValue;

		// リンクの元・先のクラス取得
		List<String> sourceClassList = getSubClass(linkContentsData, sourceClassType);
		List<String> targetClassList = getSubClass(linkContentsData, targetClassType);

		// リンクメニュー構築
		OntMenu ontMenu = new OntMenu(rootlayout, rootlayout.getOssActor(), linkContentsData.getNode(item_id));
 		List<OntMenu.OntMenuItem> sub_omi_list = ontMenu.makeMenuList2(omi_id, sourceClassList, targetClassList);
 		//List<OntMenu.OntMenuItem> sub_omi_list = ontMenu.makeMenuList2(omi_id);

 		//OntologyItem ontologyItem = ontMenu.getOntMenuItem().ontologyItem;
 		OntMenuItem sub_omi_val =null;
		if (relationValue != null) {
			sub_omi_val = getRelationValue(relationValue, sub_omi_list);
 			if (sub_omi_val != null) {
				if (Locale.getDefault().getLanguage().equals("en")) {
					val=sub_omi_val.en_label;
				}else if(Locale.getDefault().getLanguage().equals("ja")) {
					val=sub_omi_val.ja_label;
				}
 			}
		} else {
			sub_omi_val = sub_omi_list.get(0);
		}

 		for (OntMenu.OntMenuItem sub_omi : sub_omi_list) {
			// 設定済みの属性値(OntologyIte.label)をラベル名とする
			// fxLabelには、初期状態ではオントロジ属性名が入る


			sub_omi.fxLabel.setTextFill(Color.BLACK);
			sub_omi.fxLabel.setFont(new Font("Arial", 19));
			sub_omi.fxLabel.setStyle("-fx-font-weight: bold");
			// sub_omi.fxLabel.setWrapText(true);
			// sub_omi.fxLabel.setPrefWidth(150);

			sub_omi.fxLabel.textProperty().addListener(new ChangeListener<String>() {
				@Override
				public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
					//logger.debug("◆START observe={}, oldValue={}, newValue={}", observable, oldValue, newValue);

					if (Locale.getDefault().getLanguage().equals("en")) {
						sub_omi.fxLabel.setPrefWidth(sub_omi.fxLabel.getText().length() * 12);
					} else {
						sub_omi.fxLabel.setPrefWidth(sub_omi.fxLabel.getText().length() * 20);
					}
					sub_omi.fxLabel.setMinWidth(sub_omi.fxLabel.getText().length()); // why 7? Totally trial
					sub_omi.fxLabel.setStyle("-fx-font-weight: bold");
					relation.setMinWidth(sub_omi.fxLabel.getText().length());
				}
			});

			sub_omi.fxLabel.setTextFill(Color.GRAY);

			if (val != null && !val.equals("")) {
				// child.dispRelStr = val;

				sub_omi.fxLabel.setText(val);
 				if (Locale.getDefault().getLanguage().equals("en")) {
					 sub_omi.fxLabel.setPrefWidth(sub_omi.fxLabel.getText().length() * 12);

				} else {
				 	sub_omi.fxLabel.setPrefWidth(sub_omi.fxLabel.getText().length() * 20);
				}
				sub_omi.fxLabel.setMinWidth(sub_omi.fxLabel.getText().length());
				relation.setMinWidth(sub_omi.fxLabel.getText().length());

			} else {
				sub_omi.fxLabel.setStyle("-fx-font-weight: bold");
 			}
			relation.getChildren().clear();
			relation.getChildren().add(sub_omi.fxLabel);


			// 属性値が選択されたら、ラベル値を書き換えるので、ラベル値のリスナで選択された属性値を取得する
			sub_omi.fxLabel.textProperty().addListener(new ChangeListener<String>() {
				@Override
				public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
					//logger.debug("◆START observe={}, oldValue={}, newValue={}", observable, oldValue, newValue);

					if (newValue != null) {
						sub_omi.fxLabel.setMinWidth(sub_omi.fxLabel.getText().length() * 12);
 						OntMenuItem omi = sub_omi.getOntMenuItem(newValue);

						if (UNCERTAIN.equals(newValue)) {
							newValue = "?";
						}

						// condition checking for symmetric values
						if (omi != null && AppProperty.TYPE_SYMMETRICK.equals(omi.ontologyItem.type)) {
							arrowright.setVisible(false);
							circle1.setVisible(true);
							reverseLink.setDisable(true);
						} else {
							arrowright.setVisible(true);
							circle1.setVisible(false);

						}

						sub_omi.fxLabel.setText(newValue);

						//FIXME
						setRelationValue(newValue, sourceClassType, targetClassType);
						//setRelationValue(newValue);

						if (mLinkIds.size() == 2) {
							//logger.debug("linkLabel changed.");

							sub_omi.fxLabel.setText(newValue);

							// 2021/07 AW リンク対応
							// リンク編集
							//graphAct.editEdge(mLinkIds.get(0).toString(), mLinkIds.get(1).toString(),
							//		newValue + "-" + omi.ontologyItem.type);
							String id = omi.ontologyItem.id;
							if (id.startsWith("#")) {
								id = id.substring(1);
							}
							graphAct.editEdge(mLinkIds.get(0).toString(), mLinkIds.get(1).toString(), id);
						}
					}
				}
			});
		}

		// attribute.getChildren().add(relation);

		attribute.setVisible(true);
	}

	/*
	private String getAtributeProperty(String value, List<OntMenu.OntMenuItem> sub_omi_list) {
		String propertyVal = null;

		for (OntMenu.OntMenuItem sub_omi : sub_omi_list) {
			if (sub_omi.id.equals(AppProperty.ITEM_ID_REL)) {
				// 関係性は属性メニューに表示しない
				continue;
			}
			if (sub_omi.fxLabel != null && sub_omi.fxLabel.getText() != null
					&& sub_omi.fxLabel.getText().equals(value)) {
				propertyVal = sub_omi.ontologyItem.type;

				// break;
			}
			if (sub_omi.child.size() > 0) {
				for (OntMenuItem omitm : sub_omi.child) {
					if (omitm.id.equals(AppProperty.ITEM_ID_REL)) {
						// 関係性は属性メニューに表示しない
						continue;
					}
					if (omitm.fxLabel != null && omitm.fxLabel.getText() != null
							&& omitm.fxLabel.getText().equals(value)) {
						propertyVal = omitm.ontologyItem.type;
						// break;
					}
				}
			}

		}
		return propertyVal;
	}

	public void setStart(Point2D startPoint) {

		node_link.setStartX(startPoint.getX());
		node_link.setStartY(startPoint.getY());

	}

	public void setEnd(Point2D endPoint) {

		node_link.setEndX(endPoint.getX());
		node_link.setEndY(endPoint.getY());

		attribute.setLayoutX(endPoint.getX());
		attribute.setLayoutY(endPoint.getY());

		anchorpaneright.setLayoutX(endPoint.getX());
		anchorpaneright.setLayoutY(endPoint.getY());
	}

	public void setArrowDropped(Point2D endPoint) {

		attribute.setLayoutX(endPoint.getX() - 20);
		attribute.setLayoutY(endPoint.getY() - 20);

		anchorpaneright.setLayoutX(endPoint.getX());
		anchorpaneright.setLayoutY(endPoint.getY());

	}
	*/

	public void bindEnds(DraggableNode source, DraggableNode target, Point2D endPoint, NodeLink target1) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		//DraggableNode dragnode;
		target1.sourceNode = source;
		target1.targetNode = target;
		node_link.startXProperty()
				.bind(source.layoutXProperty().add(source.translateXProperty()).add(source.widthProperty().divide(2)));
		node_link.startYProperty()
				.bind(source.layoutYProperty().add(source.translateYProperty()).add(source.heightProperty().divide(2)));
		node_link.endXProperty()
				.bind(target.layoutXProperty().add(target.translateXProperty()).add(target.widthProperty().divide(2)));
		node_link.endYProperty()
				.bind(target.layoutYProperty().add(target.translateYProperty()).add(target.heightProperty().divide(2)));

		attribute.layoutXProperty()
				.bind((Bindings.add((node_link.endXProperty()), node_link.startXProperty()).divide(2)));
		attribute.layoutYProperty()
				.bind(Bindings.add((node_link.endYProperty()), node_link.startYProperty()).divide(2));

		circle1.layoutXProperty()
				.bind((Bindings.add((node_link.endXProperty()), node_link.startXProperty()).divide(2)));
		circle1.layoutYProperty().bind(Bindings.add((node_link.endYProperty()), node_link.startYProperty()).divide(2));

		// circle2.layoutXProperty().bind((Bindings.add((node_link.endXProperty()),
		// node_link.startXProperty())));
		// circle2.layoutYProperty().bind(Bindings.add((node_link.endYProperty()),
		// node_link.startYProperty()));

		double size = 12; // Arrow size
		StackPane arrow = new StackPane();
		arrow.setStyle(
				"-fx-background-color:#333333;-fx-border-width:1px;-fx-border-color:black;-fx-shape: \"M0,-4L4,0L0,4Z\"");//
		arrow.setPrefSize(size, size);
		arrow.setMaxSize(size, size);
		arrow.setMinSize(size, size);

		// Determining the arrow visibility unless there is enough space between dots.
		DoubleBinding xDiff = node_link.endXProperty().subtract(node_link.startXProperty());
		DoubleBinding yDiff = node_link.endYProperty().subtract(node_link.startYProperty());
		BooleanBinding visible = (xDiff.lessThanOrEqualTo(size).and(xDiff.greaterThanOrEqualTo(-size))
				.and(yDiff.greaterThanOrEqualTo(-size)).and(yDiff.lessThanOrEqualTo(size))).not();
		arrow.visibleProperty().bind(visible);

		// Determining the x point on the line which is at a certain distance.
		DoubleBinding tX = Bindings.createDoubleBinding(() -> {
			double xDiffSqu = (node_link.getEndX() - node_link.getStartX())
					* (node_link.getEndX() - node_link.getStartX());
			double yDiffSqu = (node_link.getEndY() - node_link.getStartY())
					* (node_link.getEndY() - node_link.getStartY());
			double lineLength = Math.sqrt(xDiffSqu + yDiffSqu);
			double dt;

			if (toLineEnd) {
				// When determining the point towards end, the required distance is total length
				// minus (radius + arrow half width)
				dt = lineLength - (target.getWidth() / 2) - (arrow.getWidth() / 2);
			} else {
				// When determining the point towards start, the required distance is just
				// (radius + arrow half width)
				dt = (source.getWidth() / 2) + (arrow.getWidth() / 2);
			}

			double t = dt / lineLength;
			double dx = ((1 - t) * node_link.getStartX()) + (t * node_link.getEndX());
			return dx;
		}, node_link.startXProperty(), node_link.endXProperty(), node_link.startYProperty(), node_link.endYProperty());

		// Determining the y point on the line which is at a certain distance.
		DoubleBinding tY = Bindings.createDoubleBinding(() -> {
			double xDiffSqu = (node_link.getEndX() - node_link.getStartX())
					* (node_link.getEndX() - node_link.getStartX());
			double yDiffSqu = (node_link.getEndY() - node_link.getStartY())
					* (node_link.getEndY() - node_link.getStartY());
			double lineLength = Math.sqrt(xDiffSqu + yDiffSqu);
			double dt;
			if (toLineEnd) {
				dt = lineLength - (target.getHeight() / 2) - (arrow.getHeight() / 2);
			} else {
				dt = (source.getHeight() / 2) + (arrow.getHeight() / 2);
			}
			double t = dt / lineLength;
			double dy = ((1 - t) * node_link.getStartY()) + (t * node_link.getEndY());
			return dy;
		}, node_link.startXProperty(), node_link.endXProperty(), node_link.startYProperty(), node_link.endYProperty());

		arrow.layoutXProperty().bind(tX.subtract(arrow.widthProperty().divide(2)));
		arrow.layoutYProperty().bind(tY.subtract(arrow.heightProperty().divide(2)));

		DoubleBinding endArrowAngle = Bindings.createDoubleBinding(() -> {
			double stX = toLineEnd ? node_link.getStartX() : node_link.getEndX();
			double stY = toLineEnd ? node_link.getStartY() : node_link.getEndY();
			double enX = toLineEnd ? node_link.getEndX() : node_link.getStartX();
			double enY = toLineEnd ? node_link.getEndY() : node_link.getStartY();
			double angle = Math.toDegrees(Math.atan2(enY - stY, enX - stX));
			if (angle < 0) {
				angle += 360;
			}
			return angle;
		}, node_link.startXProperty(), node_link.endXProperty(), node_link.startYProperty(), node_link.endYProperty());
		arrow.rotateProperty().bind(endArrowAngle);

// bind function for arrow to make angle parallel with node link

		arrowright.rotateProperty().bind(Bindings.createDoubleBinding(() -> {
			//double x = node_link.startYProperty().getValue() - node_link.startXProperty().getValue();
			//double y = node_link.endYProperty().getValue() - node_link.endXProperty().getValue();

			//double d = Math.hypot(x, y);

			double y1 = node_link.getStartY();
			double y2 = node_link.getEndY();
			double x1 = node_link.getStartX();
			double x2 = node_link.getEndX();
			double angle = Math.atan2(y2 - y1, x2 - x1) * 180 / 3.14;
			if (angle >= 90 && angle < 180) {
				y1 = y1 - (y1 - y2);
			}
			if (angle > 0 && angle < 90) {
				x1 = x1 - (x1 - x2);
				y1 = y1 - (y1 - y2);
			}
			if (angle <= 0 && angle > -90) {
				x1 = x1 - (x1 - x2);
			}
			angle = angle + 90;

			//double offset = 10;
			return angle;

		}, node_link.endXProperty(), node_link.endYProperty(), node_link.startXProperty(), node_link.startYProperty()));
		DoubleBinding wgtSqrHalfWidth = anchorpaneright.widthProperty().divide(2);
		DoubleBinding wgtSqrHalfHeight = anchorpaneright.heightProperty().divide(2);
		DoubleBinding lineXHalfLength = node_link.endXProperty().subtract(node_link.startXProperty()).divide(2);
		DoubleBinding lineYHalfLength = node_link.endYProperty().subtract(node_link.startYProperty()).divide(2);

		anchorpaneright.layoutXProperty()
				.bind(node_link.startXProperty().add(lineXHalfLength.subtract(wgtSqrHalfWidth)));
		anchorpaneright.layoutYProperty()
				.bind(node_link.startYProperty().add(lineYHalfLength.subtract(wgtSqrHalfHeight)));

		source.registerLink(getId());
		target.registerLink(getId());
		source.toFront();
		target.toFront();

		attribute.toFront();
		node_link.toBack();
		registerLink(source.node.getNodeId());
		registerLink(target.node.getNodeId());

	}

	private List<String> getSubClass(PLRContentsData contentsData, String classType) {

		List<String> classList = new ArrayList<String>();
		if (classType != null) {

			if (!classType.startsWith("#")) {
				classType = '#' + classType;
			}

			classList.add(classType);
			OntologyItem classOi = contentsData.getNode(classType);

			// サブクラスで、クラス属性のものをクラスリストに入れる
			if (classOi.subClassOf != null) {
				for (OntologyItem oi: classOi.subClassOf) {
					if (oi.isClass()) {

						List<String> childClassList = getSubClass(contentsData, oi.id);
						classList.addAll(childClassList);
					}
				}
			}

			//FIXME 古いオントロジー対応
			// Entityを強制的にクラスに加える
			if (!classList.contains("#Entity")) {
				classList.add("#Entity");
			}

		}

		return classList;
	}

}
