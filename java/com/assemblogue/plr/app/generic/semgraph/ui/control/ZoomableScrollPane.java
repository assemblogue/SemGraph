package com.assemblogue.plr.app.generic.semgraph.ui.control;

import com.assemblogue.plr.app.generic.semgraph.TinyLog;
import com.assemblogue.plr.app.generic.semgraph.ui.GraphCoordinate;
import com.assemblogue.plr.app.generic.semgraph.ui.controller.RootLayoutController;

import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.VBox;

public class ZoomableScrollPane extends ScrollPane {

	public double getScaleValue() {
		return scaleValue;
	}

	public void setScaleValue(double scaleValue) {
		this.scaleValue = scaleValue;
	}

	public double getZoomIntensity() {
		return zoomIntensity;
	}

	public void setZoomIntensity(double zoomIntensity) {
		this.zoomIntensity = zoomIntensity;
	}

	/** ズーム比 */
	private double scaleValue = 1;

	private double zoomIntensity = 0.013;
	//private Node target;
	private Node zoomNode;

	private RootLayoutController rootLayoutController;

	public ZoomableScrollPane(Node target, RootLayoutController rootLayoutController) {
		super();

		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		//this.target = target;
		this.zoomNode = new Group(target);
		this.rootLayoutController = rootLayoutController;
		setContent(outerNode(zoomNode));
		setPannable(true);
		setHbarPolicy(ScrollBarPolicy.ALWAYS);
		setVbarPolicy(ScrollBarPolicy.ALWAYS);
		setFitToHeight(true); // center
		setFitToWidth(true); // center

		updateScale();
	}

	private Node outerNode(Node node) {
		Node outerNode = centeredNode(node);

		outerNode.setOnScroll(e -> {
			TinyLog.debug(new Throwable().getStackTrace()[0], false, "◆START");

			/*
			double scale = (e.getTextDeltaY() >= 0) ? scaleValue * SCALE_RATIO : scaleValue / SCALE_RATIO;
			if(scale < 0.5){
				scale = 0.5;
			}
			if(scale > 2.0){
				scale = 2.0;
			}

			scaleValue = scale;
			TinyLog.trace(new Throwable().getStackTrace()[0], false, "scale=" + scaleValue);

			updateScale(scaleValue);
			//this.layout();
			*/

			// スクロールバーの位置はズームでは変わらない
			//TinyLog.debug(new Throwable().getStackTrace()[0], false, "mouseX=" + e.getX());
			//TinyLog.debug(new Throwable().getStackTrace()[0], false, "mouseY=" + e.getY());
			//TinyLog.debug(new Throwable().getStackTrace()[0], false, "sceneX=" + e.getSceneX());
			//TinyLog.debug(new Throwable().getStackTrace()[0], false, "sceneY=" + e.getSceneY());

			//TinyLog.debug(new Throwable().getStackTrace()[0], false, "hvalue=" + this.getHvalue());
			//TinyLog.debug(new Throwable().getStackTrace()[0], false, "vvalue=" + this.getVvalue());

			//TinyLog.debug(new Throwable().getStackTrace()[0], false, "graphWidth=" + node.getLayoutBounds().getWidth());
			//TinyLog.debug(new Throwable().getStackTrace()[0], false, "graphHeight=" + node.getLayoutBounds().getHeight());
			//TinyLog.debug(new Throwable().getStackTrace()[0], false, "viewportWidth=" + this.getViewportBounds().getWidth());
			//TinyLog.debug(new Throwable().getStackTrace()[0], false, "viewportHeight=" + this.getViewportBounds().getHeight());

			e.consume();
			onScroll(Math.abs(e.getTextDeltaY()) == 0.0 ? e.getDeltaY() : e.getTextDeltaY(), e.getX(), e.getY());
		});
		return outerNode;
	}

    private Node centeredNode(Node node) {
        VBox vBox = new VBox(node);
        vBox.setAlignment(Pos.CENTER);
        return vBox;
    }

    private void updateScale() {
        //target.setScaleX(scaleValue);
        //target.setScaleY(scaleValue);
    	zoomNode.setScaleX(scaleValue);
		zoomNode.setScaleY(scaleValue);
    }

    /**
     * 指定された値でtargetをZoomする
     * @param value
     */
    public void updateScale(double value){
    	TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		scaleValue = value;
		TinyLog.debug(new Throwable().getStackTrace()[0], false, "scale=" + scaleValue);

		//target.setScaleX(value);
		//target.setScaleY(value);
		zoomNode.setScaleX(value);
		zoomNode.setScaleY(value);
	}

	private void onScroll(double wheelDelta, double x, double y) {
		double zoomFactor = Math.exp(wheelDelta * zoomIntensity);
		double oldScale = scaleValue;
		scaleValue = scaleValue * zoomFactor;

		GraphCoordinate gc = rootLayoutController.getGraphCoordinate();

		if(scaleValue < 0.3 && scaleValue < oldScale){
			scaleValue = oldScale;
			gc.setScale(scaleValue);
			TinyLog.debug(new Throwable().getStackTrace()[0], false, "scale=" + scaleValue);
			return;
		}
		if(scaleValue > 4.0 && scaleValue > oldScale){
			scaleValue = oldScale;
			gc.setScale(scaleValue);
			TinyLog.debug(new Throwable().getStackTrace()[0], false, "scale=" + scaleValue);
			return;
		}

		TinyLog.debug(new Throwable().getStackTrace()[0], false, "scale=" + scaleValue);
		gc.setScale(scaleValue);

		//Bounds innerBounds = zoomNode.getLayoutBounds();
		//Bounds viewportBounds = getViewportBounds();
		// calculate pixel offsets from [0, 1] range
		//double valX = this.getHvalue() * (innerBounds.getWidth() - viewportBounds.getWidth());
		//double valY = this.getVvalue() * (innerBounds.getHeight() - viewportBounds.getHeight());
		updateScale();
		this.layout(); // refresh ScrollPane scroll positions & target bounds
		// convert target coordinates to zoomTarget coordinates
		//Point2D posInZoomTarget = target.parentToLocal(zoomNode.parentToLocal(new Point2D(x,y)));
		// calculate adjustment of scroll position (pixels)
		//Point2D adjustment = target.getLocalToParentTransform().deltaTransform(posInZoomTarget.multiply(zoomFactor - 1));
		// convert back to [0, 1] range
		// (too large/small values are automatically corrected by ScrollPane)
		//Bounds updatedInnerBounds = zoomNode.getBoundsInLocal();
		//this.setHvalue((valX + adjustment.getX()) / (updatedInnerBounds.getWidth() - viewportBounds.getWidth()));
		//this.setVvalue((valY + adjustment.getY()) / (updatedInnerBounds.getHeight() - viewportBounds.getHeight()));
	}
}
