package com.assemblogue.plr.app.generic.semgraph.ui.control;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Bounds;
import javafx.scene.control.Alert;
import javafx.stage.Stage;
import javafx.stage.Window;

public class AlertDialog extends Alert {

	/**
	 * コンストラクタ
	 * @param type
	 * @param headText
	 * @param message
	 */
	public AlertDialog(AlertType type,String headText,String message) {
		this(type, headText, message, null);
	}

	/**
	 * コンストラクタ
	 * @param type
	 * @param headText
	 * @param message
	 * @param stage
	 */
	public AlertDialog(AlertType type,String headText,String message, Stage stage) {
		super(type);
		this.setHeaderText(headText);
		this.setContentText(message);

		// stageがnullで無い場合はウィンドウ中央にダイアログを出力する。
		if (stage != null) {

			// ダイアログを親ウィンドウの中央に出すためのリスナー追加
			Window owner = stage;
			this.getDialogPane().layoutBoundsProperty().addListener(
					new ChangeListener<Bounds>() {
				@Override
				public void changed(ObservableValue<? extends Bounds> observable,
						Bounds oldValue, Bounds newValue) {
					if(newValue != null
							&& newValue.getWidth() > 0 && newValue.getHeight() > 0) {
						double x = owner.getX() + owner.getWidth() / 2;
						double y = owner.getY() + owner.getHeight() / 2;
						setX(x - newValue.getWidth() / 2);
						setY(y - newValue.getHeight() / 2);
						getDialogPane().layoutBoundsProperty().removeListener(this);
					}
				}
			});
		}
	}
}
