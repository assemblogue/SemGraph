package com.assemblogue.plr.app.generic.semgraph.ui.control;


import java.util.Objects;
import java.util.regex.Pattern;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *プロパティ編集ダイアログ
 */
public class PropertyEditDialog {

	/** ダイアログのStage */
	private Stage stage = new Stage();

    /** コンストラクタ引数 **/
	private boolean result;
    private String propertyValue;
    private boolean checkFlg;

    /**
     * コンストラクタ
     */
    public PropertyEditDialog() {
        this("","",false);
    }

    /**
     * コンストラクタ
     * @param propetyName プロパティ名
     * @param properyValue プロパティ値
     * @param checkFlg 入力チェックの有無
     */
    public PropertyEditDialog(String propetyName,String propertyValue,boolean checkFlg) {

    	this.result = false;

       	this.propertyValue =	Objects.isNull(propertyValue) ? "" :propertyValue;
        this.checkFlg =	checkFlg;

        stage.setScene(makSceane());

        stage.setTitle(Objects.isNull(propetyName) ? "" : propetyName);

        stage.initModality(Modality.APPLICATION_MODAL);
        stage.showAndWait();
    }


    /**************************************************************************
     *
     * Public API
     *
     **************************************************************************/
    // プロパティ値
    public final String getPropertyValue() {
        return propertyValue;
    }

    // プロパティ値
    public  boolean getPropetyEditDialogResult() {
        return result;
    }

    //  showAndWaitメソッド
    //public void  showAndWait() {
    //	stage.showAndWait();
    //}


    /**************************************************************************
    *
    * Private Implementation
    *
    **************************************************************************/
    /*-----------------------------------*
     * 画面 コンテンツ構築（実体）
     *-----------------------------------*/
    private Scene makSceane () {

//        // -- textfield    //TextField  (プロパティ値を1行にしたいときは、こちらを使う)
//        this.textInput = new TextField(properyValue);
//        this.textInput.setMaxWidth(Double.MAX_VALUE);
//        this.textInput.setPrefWidth(200);

       	TextArea textInput = new TextArea(propertyValue);
        textInput.setMaxWidth(Double.MAX_VALUE);
        textInput.setPrefWidth(200);
        textInput.setPrefHeight(50);

        textInput.requestFocus();

        //レイアウト調整用のラベル
        Label labelDummy = new Label("");

        // エラーメッセージ用ラベル
        Label errorLabel = new Label("");
        errorLabel.getStyleClass().add("content");
        errorLabel.setTextFill(Color.RED);
        errorLabel.setWrapText(true);
        errorLabel.setPrefWidth(360);
        errorLabel.setPrefWidth(Region.USE_COMPUTED_SIZE);

        // OKボタン
    	Button btnOK = new Button("OK");
    	btnOK.setPrefSize(80, 20);
    	btnOK.setStyle("-fx-background-color: #336699;");
    	btnOK.setTextFill(Color.WHITE);

    	// キャンセルボタン
    	Button btnCancel = new Button("Cancel");
    	btnCancel.setPrefSize(80, 20);

        btnOK.setOnMouseClicked(event -> btnOKEvent(textInput, errorLabel));
        btnCancel.setOnMouseClicked(event -> btnCancelEvent());


        //ボタンのレイアウト調整用にHBoxの作成
    	HBox hboxBtn = new HBox();
    	hboxBtn.setPadding(new Insets(15,12, 15, 12));
   	 	hboxBtn.setSpacing(10);
	   	hboxBtn.getChildren().addAll(btnCancel,btnOK);
	   	hboxBtn.setAlignment(Pos.CENTER_RIGHT);

	   	//ルートVBox
        VBox vBoxRoot = new VBox();
        vBoxRoot.setAlignment(Pos.CENTER_LEFT);
        vBoxRoot.setPadding(new Insets(20, 10, 10, 10));

        vBoxRoot.getChildren().addAll(textInput, labelDummy, errorLabel, hboxBtn);

        return new Scene(vBoxRoot,300,160);
    }

    /*------------------------------*
     * OKボタン クリックイベント
     *------------------------------*/
    private void btnOKEvent(TextArea textInput, Label errorLabel) {
    	String value = textInput.getText();

    	if(checkFlg) {
        	boolean ret = Pattern.matches("^([1-9]\\d*|0)(\\.\\d+)?$|^(-[1-9]\\d*|0)(\\.\\d+)?$", value);
        	if (!ret) {
        		errorLabel.setText("数字をいれてください");
        		textInput.requestFocus();
        		return;
        	}
    	}

    	propertyValue = value;
		result = true;
		stage.close();
    }

    /*-----------------------------------*
     * キャンセルボタン クリックイベント
     *-----------------------------------*/
    private void btnCancelEvent() {
    		result = false;
    		stage.close();
    }

}
