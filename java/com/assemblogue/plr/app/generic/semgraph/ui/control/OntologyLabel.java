package com.assemblogue.plr.app.generic.semgraph.ui.control;

import com.assemblogue.plr.app.generic.semgraph.OssActor;
import com.assemblogue.plr.contentsdata.PLRContentsData;
import com.assemblogue.plr.contentsdata.ontology.OntologyItem;

import javafx.scene.control.Label;

public class OntologyLabel extends Label {

	/** オントロジーのid */
	private String ontologyId;

	/** リストの時のindex */
	private Integer index;

	/**
	 * コンストラクタ
	 * @param contentsData
	 * @param ontologyId
	 */
	public OntologyLabel(PLRContentsData contentsData, String ontologyId) {
		super();

		OntologyItem item = contentsData.getNode('#' + ontologyId);

		setOntologyLabelProperty(item);
	}

	/**
	 * コンストラクタ
	 * @param item
	 */
	public OntologyLabel(OntologyItem item) {
		super();

	}

	/**
	 * このクラスの属性を設定
	 * @param item
	 */
	private void setOntologyLabelProperty(OntologyItem item) {

		String dispText = OssActor.getDisplayText(item);
		setText(dispText);

		this.ontologyId = item.id;

		setStyle("-fx-border-color:gray; -fx-border-width: 1; -fx-border-style: solid; -fx-background-color: lightgray; -fx-padding: 3");
		setMinHeight(25);
		setMinWidth(120);
		setMaxWidth(120);
		setWrapText(true);
	}

	public String getOntologyId() {
		return ontologyId;
	}

	public void setOntologyId(String ontologyId) {
		this.ontologyId = ontologyId;
	}

	public Integer getIndex() {
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}

}
