package com.assemblogue.plr.app.generic.semgraph.ui.controller;

//////////////////////////////////////
// This class is no longer used.
//////////////////////////////////////

/**
 * 譌｢蟄倥ヵ繧ｩ繝ｫ繝�繝弱�ｼ繝峨が繝ｼ繝励Φ繧ｦ繧｣繝ｳ繝峨え縺ｮ繧ｳ繝ｳ繝医Ο繝ｼ繝ｩ繧ｯ繝ｩ繧ｹ縺ｧ縺吶��
 *
 * @author <a href="mailto:kaneko@cri-mw.co.jp">KANEKO, yukinori</a>
 */
//public class OpenGraphController implements Initializable {
public class OpenGraphController {

//	@FXML
//	private Button graphApply;
//	@FXML
//	private Button graphCancel;
//	@FXML
//	private Button graphRefButton;
//	@FXML
//	private TextField graph_uri;
//
//	private AppController appcontroller;
//	private PlrActor plrAct;
//	private Stage ownerStage;
//	private Stage stage;
//	private Stage tvStage;
//
//	private Map<String, PlrActor.NodeName> nodeNameMap;
//
//	private TreeItem<String> rootNode;
//
//	private List nodeList;
//
//	@Override
//	public void initialize(URL location, ResourceBundle resources) {
//		initializeUI();
//	}
//
//	private void initializeUI() {
//		graphApply.setOnAction(mouseEvent -> exec_open());
//		graphRefButton.setOnAction(mouseEvent -> exec_select(true));
//		graphCancel.setOnAction(mouseEvent -> exec_cancel());
//	}
//
//	public void setStage(Stage stage) {
//		this.stage = stage;
//	}
//
//	public void setOwnerStage(Stage stage) {
//		ownerStage = stage;
//	}
//
//	public void setAppController(AppController ctrl) {
//		appcontroller = ctrl;
//	}
//
//	private void exec_cancel() {
//		close();
//	}
//
//	/**
//	 * 繧ｰ繝ｩ繝穂ｸ�隕ｧ縺ｮ陦ｨ遉ｺ�ｼ夂峩謗･陦ｨ遉ｺ逕ｨ 縺ゅｉ縺九§繧√�《etAppController繧定｡後▲縺ｦ縺翫￥縺薙→
//	 */
//	public void openSelector(boolean sync_flag) {
//		exec_select(sync_flag);
//	}
//
//	/**
//	 * 繝ｫ繝ｼ繝医ヮ繝ｼ繝峨�ｮ繝弱�ｼ繝峨Μ繧ｹ繝医ｒ陦ｨ遉ｺ縺吶ｋ 驕ｸ謚槭＠縺溘ヮ繝ｼ繝牙錐繧偵ユ繧ｭ繧ｹ繝医ヵ繧｣繝ｼ繝ｫ繝峨↓繧ｻ繝�繝医☆繧九��
//	 * 莠句燕縺ｫplrActor.sync, plrActor.list繧貞ｮ滓命縺励※縺翫￥縺薙→縲�
//	 *
//	 * 隱ｲ鬘鯉ｼ壹ヵ繝ｬ繝ｳ繝峨�ｮ繝弱�ｼ繝峨Μ繧ｹ繝医ｂ陦ｨ遉ｺ縺吶ｋ繧医≧縺ｫ縺吶ｋ縲�
//	 * 繝輔Ξ繝ｳ繝牙錐縺ｮ荳九↓繝弱�ｼ繝牙錐縺梧擂繧九ｈ縺�縺ｫ縺励◆縺�縲�
//	 */
//	private void exec_select(boolean syncflag) {
//		tvStage = new Stage();
//		tvStage.getIcons().add(new Image(App.class.getResourceAsStream("ui/img/editor.jpg")));
//		tvStage.setTitle(" List of graph- " + appcontroller.plrAct.getUserID());
//		nodeNameMap = new HashMap<>();
//
//		rootNode = new TreeItem<>(Messages.getString("graph.open.graph.list"));
//		rootNode.setExpanded(true);
//
//		// 繧｢繝励Μ繝ｫ繝ｼ繝医ｒ讀懃ｴ｢
//		EntityNode root = appcontroller.plrAct.getAplRootFolderNode();
//
//		if (root == null) {
//			return;
//		}
//		if (syncflag == true) {
//			TxtList.debug("OpenGraphController/exec_select:77/sync root");
//			appcontroller.plrAct.sync(root);
//		}
//
//		TreeItem<String> my_item = new TreeItem<>(appcontroller.plrAct.getUserID());
//		my_item.setExpanded(true);
//		for (Node n : root.getProperty("hypernode_catalog")) {
//			my_item.getChildren().add(new TreeItem<String>(String.valueOf(n.asLiteral().getValue())));
//		}
//		rootNode.getChildren().add(my_item);
//
//		// 繝輔Ξ繝ｳ繝峨ヮ繝ｼ繝峨Μ繧ｹ繝医い繝�繝励�ｯ髱槫酔譛溘〒
//		ListFriend lf = new ListFriend();
//		lf.start();
//
//		TreeView<String> tree = new TreeView<>(rootNode);
//		tree.setPrefWidth(640); // 蟷�縺ｯ陦ｨ遉ｺ譁�蟄怜�鈴聞縺ｫ蜷医ｏ縺帙◆縺�縺後�ゅ�ゅ��
//		tree.setEditable(true);
//
//
//		// 繧｢繧､繝�繝�縺碁∈謚槭＆繧後◆譎ゅ�ｮ蜃ｦ逅�
//		tree.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<TreeItem<String>>() {
//			@Override
//			public void changed(ObservableValue<? extends TreeItem<String>> observable, TreeItem<String> oldValue,
//					TreeItem<String> newValue) {
//				TreeItem selectitem = (TreeItem) newValue;
//				selectitem.setExpanded(true);
//				String val = selectitem.getValue().toString();
//				DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mmZ");
//				String nowAsISO = df.format(new Date());
// 				for (EntityNode n : (root.getTimelineItems(nowAsISO,false,1000)).getItems()) {
// 					Map<String, com.assemblogue.plr.lib.Node> properties = appcontroller.plrAct.listToMap(n);
//					String value = null;
//					if (properties.containsKey(AppProperty.LT_NAME)) {
//						com.assemblogue.plr.lib.Node literal = properties.get(AppProperty.LT_NAME);
//						value = literal.asLiteral().getValue().toString();
//						if (val.equals(value)) {
// 							appcontroller.openGraph(n);
//							tvStage.close();
//							break;
//						}
//					}
//
//				}
//			}
//		});
//
//		tvStage.showingProperty().addListener((observable, oldValue, newValue) -> {
//			if (oldValue == true && newValue == false) {
//				appcontroller.close_openGraph();
//			}
//		});
//
//		tvStage.setScene(new Scene(tree));
//		tvStage.show();
//	}
//
//	/**
//	 * 繝ｫ繝ｼ繝医ヮ繝ｼ繝峨�ｮ繝弱�ｼ繝峨Μ繧ｹ繝医ｒ陦ｨ遉ｺ縺吶ｋ 驕ｸ謚槭＠縺溘ヮ繝ｼ繝牙錐繧偵ユ繧ｭ繧ｹ繝医ヵ繧｣繝ｼ繝ｫ繝峨↓繧ｻ繝�繝医☆繧九��
//	 * 莠句燕縺ｫplrActor.sync, plrActor.list繧貞ｮ滓命縺励※縺翫￥縺薙→縲�
//	 *
//	 * 隱ｲ鬘鯉ｼ壹ヵ繝ｬ繝ｳ繝峨�ｮ繝弱�ｼ繝峨Μ繧ｹ繝医ｂ陦ｨ遉ｺ縺吶ｋ繧医≧縺ｫ縺吶ｋ縲�
//	 * 繝輔Ξ繝ｳ繝牙錐縺ｮ荳九↓繝弱�ｼ繝牙錐縺梧擂繧九ｈ縺�縺ｫ縺励◆縺�縲�
//	 */
//	public List exec_hypernode_select(boolean syncflag) {
//		tvStage = new Stage();
//		nodeNameMap = new HashMap<>();
//		List nodeList = null;
//
//		rootNode = new TreeItem<>(Messages.getString("graph.open.graph.list"));
//		rootNode.setExpanded(true);
//
//		// 繧｢繝励Μ繝ｫ繝ｼ繝医ｒ讀懃ｴ｢
//		EntityNode root = appcontroller.plrAct.getAplRootFolderNode();
//		if (root == null) {
//			return null;
//		}
//		if (syncflag == true) {
//			TxtList.debug("OpenGraphControlle/exec_select:77/sync root");
//			appcontroller.plrAct.sync(root);
//		}
//		appcontroller.plrAct.list(null, root);
//
//		TreeItem<String> my_item = new TreeItem<>(appcontroller.plrAct.getUserID());
//		my_item.setExpanded(true);
//		for (PlrActor.NodeName pnn : appcontroller.plrAct.getRootNodeList()) {
//
//			String name = pnn.name + " -> " + pnn.uri;
//
//			nodeNameMap.put(name, pnn);
//			TreeItem<String> item = new TreeItem<>(name);
//			my_item.getChildren().add(item);
//
//		}
//		rootNode.getChildren().add(my_item);
//
// 		ListFriend lf = new ListFriend();
//		lf.start();
//
//		TreeView<String> tree = new TreeView<>(rootNode);
//		tree.setPrefWidth(640);
//
//		// 繧｢繧､繝�繝�縺碁∈謚槭＆繧後◆譎ゅ�ｮ蜃ｦ逅�
//		tree.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<TreeItem<String>>() {
//			@Override
//			public void changed(ObservableValue<? extends TreeItem<String>> observable, TreeItem<String> oldValue,
//					TreeItem<String> newValue) {
//				TreeItem selectitem = (TreeItem) newValue;
//				String val = selectitem.getValue().toString();
//
//				if (val.contains(" -> plr:")) {
//					graph_uri.setText(val);
//					tvStage.close();
//					setNodeList();
//					exec_open();
//				}
//			}
//		});
//
//		tvStage.showingProperty().addListener((observable, oldValue, newValue) -> {
//			if (oldValue == true && newValue == false) {
//				appcontroller.close_openGraph();
//			}
//		});
//
//		tvStage.setScene(new Scene(tree));
//		tvStage.show();
//		return nodeList;
//	}
//
//	public List getNodeList() {
//		return nodeList;
//
//	}
//
//	/**
//	 * 蜑埼擇縺ｸ
//	 */
//	public void toFront() {
//		if (tvStage != null) {
//			tvStage.toFront();
//		}
//	}
//
//	public void close() {
//		if (tvStage != null) {
//			tvStage.close();
//		}
//	}
//
//	private void exec_open() {
//		if (nodeNameMap.containsKey(graph_uri.getText())) {
//			PlrActor.NodeName nn = nodeNameMap.get(graph_uri.getText());
//			appcontroller.openGraph(nn.node.asEntity());
//
//		} else {
//			TxtList.set("Node is not exist.");
//		}
//
//		close(); //
//	}
//
//	public void setNodeList() {
//
//		if (nodeNameMap.containsKey(graph_uri.getText())) {
//			PlrActor.NodeName nn = nodeNameMap.get(graph_uri.getText());
//			GraphActor gact = new GraphActor("", nn.node.asEntity(), false);
//			this.nodeList = gact.getEntryNodes();
//		} else {
//			TxtList.set("Node is not exist.");
//		}
//
//		close(); //
//
//	}
//
//	/**
//	 * 繝輔Ξ繝ｳ繝峨�ｮ繧ｰ繝ｩ繝輔ｒ繝ｪ繧ｹ繝医い繝�繝� rootNode縺ｫ霑ｽ蜉�縺吶ｋ縺ｨ陦ｨ遉ｺ縺輔ｌ繧�
//	 */
//	class ListFriend extends Thread {
//		public void run() {
//			listFriendGraph();
//		}
//	}
//
//	private void listFriendGraph() {
//		String title = Messages.getString("graph.open.graph.list");
//		String s_title = title + Messages.getString("graph.open.graph.search.friend");
//		rootNode.setValue(s_title);
//
//		// 繝輔Ξ繝ｳ繝峨ｂ縺ゅｌ縺ｰ
//		List<Friend> friends = appcontroller.plrAct.getFriends();
//
//		if (friends != null) {
//			rootNode.setValue(s_title + Messages.getString("graph.open.graph.search.friend.n") + friends.size());
//
//			for (Friend friend : friends) {
//				TreeItem<String> friend_item = new TreeItem<>(friend.getUserId());
//				friend_item.setExpanded(true);
//
//				// 繝ｫ繝ｼ繝育峩荳九�ｮ繝弱�ｼ繝牙錐繝ｪ繧ｹ繝医ｒ蜿門ｾ励＠縲√い繧､繝�繝�縺ｫ逋ｻ骭ｲ
//				List<PlrActor.NodeName> pnns = appcontroller.plrAct.getFriendRootNodeList(friend);
//				if (pnns != null) {
//					for (PlrActor.NodeName pnn : pnns) {
//						String name = pnn.name + " -> " + pnn.uri;
//						nodeNameMap.put(name, pnn);
//						TreeItem<String> item = new TreeItem<>(name);
//						friend_item.getChildren().add(item);
//					}
//				}
//
//				rootNode.getChildren().add(friend_item);
//			}
//		}
//
//		rootNode.setValue(title);
//	}


}
