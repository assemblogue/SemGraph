package com.assemblogue.plr.app.generic.semgraph.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Random;

import com.assemblogue.plr.app.generic.semgraph.AppProperty;
import com.assemblogue.plr.app.generic.semgraph.GraphActor;
import com.assemblogue.plr.app.generic.semgraph.OssActor;
import com.assemblogue.plr.app.generic.semgraph.PlrActor;
import com.assemblogue.plr.app.generic.semgraph.TinyLog;
import com.assemblogue.plr.app.generic.semgraph.ui.control.ZoomableScrollPane;
import com.assemblogue.plr.app.generic.semgraph.ui.controller.RootLayoutController;
import com.assemblogue.plr.contentsdata.ontology.OntologyItem;
import com.assemblogue.plr.lib.EntityNode;

import javafx.event.Event;
import javafx.geometry.Bounds;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;

public class DraggableNode extends AnchorPane {

	//private Logger logger = LogManager.getLogger();

	//private TextArea editingArea;

	//public TextArea getTextArea() {
	//	return editingArea;
	//}

	//private StackPane innerPane;
	//Border innerBorders = new Border(
	//		new BorderStroke(Color.TRANSPARENT, BorderStrokeStyle.DASHED, CornerRadii.EMPTY, new BorderWidths(0)));
	//Border transparentBorders = new Border(
	//		new BorderStroke(Color.DARKTURQUOISE, BorderStrokeStyle.DOTTED, CornerRadii.EMPTY, new BorderWidths(0)));

	Rectangle nw_anchor = new Rectangle(0, 0, 0, 0), ne_anchor = new Rectangle(0, 0, 0, 0),
			sw_anchor = new Rectangle(0, 0, 0, 0), se_anchor = new Rectangle(0, 0, 0, 0);

	private GraphActor graphAct;
	//private Point2D mDragOffset = new Point2D(0.0, 0.0);
	private final DraggableNode self = this;
	private NodeLink mDragLink = null;

	private final List<String> mLinkIds = new ArrayList<String>();
	//public static NodeCell parent;
	public EntityNode node;
	//private Text textHolder;
	//private double oldHeight = 0;
	private RootLayoutController rootlayout;

	//private double x, y, centerY;

	//private boolean firstWidthHeight = true;

	// 2020/11 AW ADD start
	/** バウンディングボックス計算用 */
	private double calcWidth;
	private double calcHeight;

	public double getCalcWidth() {
		return calcWidth;
	}

	public double getCalcHeight() {
		return calcHeight;
	}
	// 2020/11 AW ADD end



	/**
	 * コンストラクタ
	 *
	 * @param gact
	 * @param node
	 * @param lockStatus
	 * @param root
	 */
	public DraggableNode(GraphActor gact, EntityNode node, boolean lockStatus, RootLayoutController root) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		// idはリンクのソースとターゲットの識別で使用する
		this.setId(new Random().doubles() + "");

		this.graphAct = gact;
		this.node = node;
		this.rootlayout = root;

		initialize();
	}

	private void initialize() {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		rootlayout.isRepainting = true;

		// ノードのクラス名取得
		String className = node.getType().get(0);
		//String className = graphAct.getClassType(node.getNodeId());
		// クラスの表示名
		OntologyItem classOi = graphAct.getGraphData().getContentsData().getNode('#' + className);
		String classDispText = OssActor.getDisplayText(classOi);
		// ノードの内容取得
		List<String> cntList = graphAct.getCntList(node.getNodeId());
		// ノードの作成者、プロフィールの名前
		//String creator = (String) PlrActor.getPropertySingleValue(node, "creator");
		String profileName = graphAct.getDisplayName(node.getNodeId());
		// ノードのアイコン、プロフィールから取得したもの
		Image image = graphAct.getImage(node.getNodeId());

		// ノードの最外枠
		HBox nodeHBox = new HBox();
		nodeHBox.setAlignment(Pos.TOP_LEFT);

		// アイコン
		if (image != null) {
			ImageView imageView = new ImageView();
			imageView.setImage(image);
			imageView.setFitWidth(40);
			imageView.setFitHeight(40);
			nodeHBox.getChildren().add(imageView);
		}

		// 名前入りのコンテンツ
		VBox nameVBox = new VBox();

		// プロフィールの名前
		Text nameText = new Text(profileName);
		nameVBox.getChildren().add(nameText);

		// ノードの中身
		VBox contentsVBox = new VBox();
		contentsVBox.setStyle(" -fx-border-color: black; -fx-border-width: 1; -fx-border-style: solid;");

		// ハイパーノードの場合
		if (PlrActor.isHyperNode(node)) {
			contentsVBox.setStyle(" -fx-border-color: black; -fx-border-width: 3; -fx-border-style: solid;");
		}

		// クラス名表示
		// Mクラスの場合はクラス名表示なし
		HBox classHBox = new HBox();
		classHBox.setStyle("  -fx-background-color: white; -fx-padding: 3;");
		Text classText = null;

		if (!"M".equals(className)) {

			classText = new Text(classDispText);
			classText.setWrappingWidth(120);
			classHBox.getChildren().add(classText);
			contentsVBox.getChildren().add(classHBox);
		}

		// ノード高さ取得用
		List<Text> textList = new ArrayList<>();

		// 内容表示
		// Mクラスの場合は、中身がなくても表示する。
		HBox cntHBox = new HBox();
		//cntHBox.setStyle(" -fx-border-color:black; -fx-border-width: 1; -fx-border-style: solid none none none; -fx-background-color: lightgray; -fx-padding: 3;");

		// ノード作成者により背景色が変わる
		String styleBackgroundColor = "";
		if (graphAct.isMyNode(node)) {
			// 自作のノード
			styleBackgroundColor = "-fx-background-color: #E0F2F1;";
		} else {
			// フレンド作のノード
			styleBackgroundColor = "-fx-background-color: #FFFFCC;";
		}

		if ("M".equals(className)) {

			if (cntList.isEmpty()) {

				Text cntText = new Text("empty");
				cntText.setWrappingWidth(125);
				//cntHBox.setStyle(" -fx-border-color:black; -fx-border-width: 0; -fx-border-style: none none none none; -fx-background-color: lightgray; -fx-padding: 3;");
				cntHBox.setStyle(" -fx-border-color:black; -fx-border-width: 0; -fx-border-style: none none none none; -fx-padding: 3;" + styleBackgroundColor);
				cntHBox.getChildren().add(cntText);
				contentsVBox.getChildren().add(cntHBox);

				textList.add(cntText);

			} else {

				for (int i = 0; i < cntList.size(); i++) {

					String cnt = cntList.get(i);
					Text cntText = new Text(cnt);
					cntText.setWrappingWidth(125);
					// はじめのcntの時は中の枠線は不要
					if (i == 0) {
						//cntHBox.setStyle(" -fx-border-color:black; -fx-border-width: 0; -fx-border-style: none none none none; -fx-background-color: lightgray; -fx-padding: 3;");
						cntHBox.setStyle(" -fx-border-color:black; -fx-border-width: 0; -fx-border-style: none none none none; -fx-padding: 3;" + styleBackgroundColor);
					} else {
						//cntHBox.setStyle(" -fx-border-color:black; -fx-border-width: 1; -fx-border-style: solid none none none; -fx-background-color: lightgray; -fx-padding: 3;");
						cntHBox.setStyle(" -fx-border-color:black; -fx-border-width: 1; -fx-border-style: solid none none none; -fx-padding: 3;" + styleBackgroundColor);
					}
					cntHBox.getChildren().add(cntText);
					contentsVBox.getChildren().add(cntHBox);

					textList.add(cntText);
				}
			}

		} else {

			for (String s: cntList) {

				Text cntText = new Text(s);
				cntText.setWrappingWidth(125);
				cntHBox.getChildren().add(cntText);
				//cntHBox.setStyle(" -fx-border-color:black; -fx-border-width: 1; -fx-border-style: solid none none none; -fx-background-color: lightgray; -fx-padding: 3;");
				cntHBox.setStyle(" -fx-border-color:black; -fx-border-width: 1; -fx-border-style: solid none none none; -fx-padding: 3;" + styleBackgroundColor);
				contentsVBox.getChildren().add(cntHBox);

				textList.add(cntText);
			}
		}

		contentsVBox.setPrefWidth(135);

		nameVBox.getChildren().add(contentsVBox);
		nodeHBox.getChildren().add(nameVBox);


		// Pan対応
		//innerPane.addEventFilter(MouseEvent.MOUSE_ENTERED, e -> {
		this.addEventFilter(MouseEvent.MOUSE_ENTERED, e -> {
			TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

			this.setCursor(Cursor.HAND);
			((ZoomableScrollPane) getParent().getParent().getParent().getParent().getParent().getParent())
					.setPannable(false);
		});
		this.addEventFilter(MouseEvent.MOUSE_EXITED, e -> {
			TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

			this.setCursor(Cursor.DEFAULT);
			((ZoomableScrollPane) getParent().getParent().getParent().getParent().getParent().getParent())
					.setPannable(true);
		});

		// RESIZE ANCHORS
		// nw_anchor.setFill(Color.SNOW );
		nw_anchor.setCursor(Cursor.NW_RESIZE);
		setTopAnchor(nw_anchor, 0.0);
		setLeftAnchor(nw_anchor, 0.0);
		// ne_anchor.setFill(Color.LIGHTBLUE);
		ne_anchor.setCursor(Cursor.NE_RESIZE);
		setTopAnchor(ne_anchor, 0.0);
		setRightAnchor(ne_anchor, 0.0);
		// sw_anchor.setFill(Color.LIGHTBLUE);
		sw_anchor.setCursor(Cursor.SW_RESIZE);
		setBottomAnchor(sw_anchor, 0.0);
		setLeftAnchor(sw_anchor, 0.0);
		// se_anchor.setFill(Color.LIGHTBLUE);
		se_anchor.setCursor(Cursor.SE_RESIZE);
		setBottomAnchor(se_anchor, 0.0);
		setRightAnchor(se_anchor, 0.0);

		this.getChildren().add(nodeHBox);

		/*
		double newY = centerY - ((textHolder.getLayoutBounds().getHeight()+10) / 2);
			if (newY > 0) {
				this.setLayoutY(newY);
			}

		editingArea.setPrefHeight(textHolder.getLayoutBounds().getHeight() + 10);
		innerPane.setMinHeight(textHolder.getLayoutBounds().getHeight() + 10);
		this.setMinHeight(textHolder.getLayoutBounds().getHeight() + 10);
		textHolder.setWrappingWidth(editingArea.getWidth() - 10);
	*/

		// BoundingBox計算用
		calcWidth = contentsVBox.getPrefWidth();
		calcHeight = contentsVBox.getPadding().getTop() + contentsVBox.getPadding().getBottom();

		// Mクラス以外の場合
		if (!"M".equals(className)) {
			calcHeight += classText.getLayoutBounds().getHeight() + classHBox.getPadding().getTop() + classHBox.getPadding().getBottom();
		}

		for (Text text: textList) {
			calcHeight += text.getLayoutBounds().getHeight();
			//FIXME paddingはcssでないとこで設定すべき
			calcHeight += classHBox.getPadding().getTop();
			calcHeight += classHBox.getPadding().getBottom();
		}

		// 展開おわり

		String rel_type = AppProperty.TYPE_SYMMETRICK;

		//TODO 2021/3 rootLayoutControllerを渡すように
		mDragLink = new NodeLink(graphAct, "", rootlayout, rel_type, null, null);
		//mDragLink = new NodeLink(graphAct, "", null, rel_type);
		mDragLink.setVisible(true);

		this.addEventFilter(ContextMenuEvent.CONTEXT_MENU_REQUESTED, Event::consume);

		rootlayout.isRepainting = false;
	}



	void registerLink(String linkId) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		mLinkIds.add(linkId);
	}

	/**
	 * マウスドラッグ時アクションメソッド 座標書き込みなし
	 *
	 * @param p
	 */
	public void relocateToPoint(Point2D p, Point2D mousePoint) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		rootlayout.isRepainting = true;
		// relocates the object to a point that has been converted to
		// scene coordinates

		/*
		 * Point2D localCoords = getParent().sceneToLocal(p); double relocX =
		 * localCoords.getX() - mDragOffset.getX(); double relocY = localCoords.getY() -
		 * mDragOffset.getY(); if (relocX < 0) { relocX = 0; } if (relocY < 0) { relocY
		 * = 0; } relocate((int) (relocX), (int) (relocY));
		 * mDragLink.attribute.setLayoutX(localCoords.getX());
		 * mDragLink.attribute.setLayoutY(localCoords.getY()); String coords = relocX +
		 * "," + relocY + "," + editingArea.getPrefHeight();
		 * graphAct.changeCoords(node.getNodeId(), coords);
		 */

		// recalcPoint(p);
		recalcPoint(p, mousePoint);

		rootlayout.isRepainting = false;
	}

	/**
	 * マウスリリース時アクションメソッド 座標書き込みあり
	 *
	 * @param p
	 */
	public void setToPoint(Point2D p, Point2D mousePoint) {
		TinyLog.debug(new Throwable().getStackTrace()[0], false, "◆START NodeDrag end (x,y)=" + p.getX() + "," + p.getY());

		rootlayout.isRepainting = true;

		// Point2D recalcPoint = recalcPoint(p);
		Point2D recalcPoint = recalcPoint(p, mousePoint);

		double relocX = recalcPoint.getX();
		double relocY = recalcPoint.getY();

		// 2020/11 AW ADD start
		// 相対座標に変換
		GraphCoordinate graphCoordinate = rootlayout.getGraphCoordinate();
		Point2D relativeCoord = graphCoordinate.getRelativeCoordinate(new Point2D(relocX, relocY));
		// 2020/11 AW ADD end

	    // 2020/11 AW REP start
	    //String coords = relativeCoord.getX() + "," + relativeCoord.getY() + "," + editingArea.getPrefHeight();
		//String coords = relocX + "," + relocY + "," + editingArea.getPrefHeight();
		// 2020/11 AW REP end

		//logger.debug("coords={}", coords);
	    // 2021/2 AW REP start
	    graphAct.updateNodeCoords(node.getNodeId(), relativeCoord.getX(),relativeCoord.getY());
		//graphAct.updateNodeCoords(node.getNodeId(), coords);
		// 2021/2 AW REP end

		//updateNodeCenter();

		// 2020/11 AW ADD start
		// グラフ座標再計算
		graphCoordinate.calcGraphCoordinate(rootlayout.getDraggableNodes(), rootlayout.getRight_pane(), rootlayout.getRootLayoutScene());

		// 再描画判定
		if (graphCoordinate.isRepaintFlag()) {
			// 再描画
			TinyLog.debug(new Throwable().getStackTrace()[0], false, "mouse release repaint.");
			rootlayout.repaint(false);
		}
		// 2020/11 AW ADD end
		rootlayout.isRepainting = false;
	}

	/**
	 * reCalculate point
	 *
	 * @param inPoint
	 * @return
	 */
	// private Point2D recalcPoint(Point2D inPoint) {
	private Point2D recalcPoint(Point2D inPoint, Point2D mousePoint) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		// offset
		final double offsetX = 3;
		final double offsetY = 3;

		Bounds bounds = getBoundsInParent();
		double orgX = inPoint.getX();
		double orgY = inPoint.getY();
		double sceneX = getScene().getWidth();
		double sceneY = getScene().getHeight();

		boolean recalc = false;

		/*

		//TinyLog.debug(new Throwable().getStackTrace()[0], false, "nodeBoundsX=" + bounds.getWidth() + " nodeBoundsY=" + bounds.getHeight());
		TinyLog.debug(new Throwable().getStackTrace()[0], false, "clickSceneX=" + orgX + " clickSceneY=" + orgY);
		//TinyLog.debug(new Throwable().getStackTrace()[0], false, "clickNodeX=" + mousePoint.getX() + " clickNodeY=" + mousePoint.getY());
		TinyLog.debug(new Throwable().getStackTrace()[0], false, "cornerNodeX=" + (orgX -mousePoint.getX()) + " cornerNodeY=" + (orgY - mousePoint.getY()));
		//TinyLog.debug(new Throwable().getStackTrace()[0], false, "sceneX=" + getScene().getWidth() + " sceneY=" + getScene().getHeight());
		//TinyLog.debug(new Throwable().getStackTrace()[0], false, "layoutNodeMinX=" + getLayoutBounds().getMinX() + " layoutNodeMinY=" + getLayoutBounds().getMinY());
		//TinyLog.debug(new Throwable().getStackTrace()[0], false, "layoutNodeMaxX=" + getLayoutBounds().getMaxX() + " layoutNodeMaxY=" + getLayoutBounds().getMaxY());

		// クリックされたノードの左上座標（Scene）
		double nodeTopLeftX = orgX - mousePoint.getX();
		double nodeTopLeftY = orgY - mousePoint.getY();

		// ノードの右座標
		double nodeRightX = nodeTopLeftX + bounds.getWidth();

		// ノードの下座標
		double nodeBottomY = nodeTopLeftY + bounds.getHeight();

		// Windowの枠チェック

		// right
		//if (orgX > 0 && orgX + bounds.getWidth() > getScene().getWidth()) {
		//	orgX = getScene().getWidth() - bounds.getWidth() - offsetX;
		//	recalc = true;
		//}

		if (nodeRightX >= sceneX) {
			TinyLog.debug(new Throwable().getStackTrace()[0], false, "right nodeRightX=" + nodeRightX + " sceneX=" + sceneX);
			orgX = sceneX - bounds.getWidth() - offsetX;
			TinyLog.debug(new Throwable().getStackTrace()[0], false, "new orgX=" + orgX);
			recalc = true;
		}

		// bottom
		//if (orgY > 0 && orgY + bounds.getHeight() > getScene().getHeight()) {
		//	orgY = getScene().getHeight() - bounds.getHeight() - offsetY;
		//	recalc = true;
		//}

		if (nodeBottomY >= sceneY) {
			TinyLog.debug(new Throwable().getStackTrace()[0], false, "bottom nodeRightX=" + nodeRightX + " sceneX=" + sceneX);
			orgY = sceneY - bounds.getHeight() - offsetY;
			recalc = true;
		}

		// left
		//if (orgX <= 0) {
		//	orgX = offsetX;
		//	recalc = true;
		//}

		if (nodeTopLeftX <= 0) {
			TinyLog.debug(new Throwable().getStackTrace()[0], false, "left  nodeRightX=" + nodeRightX + " sceneX=" + sceneX);
			orgX = offsetX;
			recalc = true;
		}

		// top
		//if (orgY <= 0) {
		//	orgY = offsetY;
		//	recalc = true;
		//}

		if (nodeTopLeftY <= 0) {
			TinyLog.debug(new Throwable().getStackTrace()[0], false, "top  nodeRightX=" + nodeRightX + " sceneX=" + sceneX);
			orgY = offsetY;
			recalc = true;
		}

		// 座標を変更した場合は変更後の座業で後続処理を実施
		if (recalc) {
			Point2D recalcPoint2D = new Point2D(orgX, orgY);
			inPoint = recalcPoint2D;
		}
		*/

		// relocates the object to a point that has been converted to
		// scene coordinates
		Point2D localCoords = getParent().sceneToLocal(inPoint);
		double relocX = localCoords.getX();
		double relocY = localCoords.getY();
		//double relocX = localCoords.getX() - mDragOffset.getX();
		//double relocY = localCoords.getY() - mDragOffset.getY();

		TinyLog.debug(new Throwable().getStackTrace()[0], false, "backClickX=" + relocX + " backClickY=" + relocY);

		if (relocX < 0) {
			relocX = 0;
			//logger.debug("relocateX = 0");
		}
		if (relocY < 0) {
			relocY = 0;
			//logger.debug("relocateY = 0");
		}

		//FIXME
		// 一旦コメント
		/*
		double graphX = graphAct.getGraphData().getGraphSizeX();
		double graphY = graphAct.getGraphData().getGraphSizeY();
		if (relocX > 0 && relocX + bounds.getWidth() + offsetX > graphX) {
			relocX = graphX - bounds.getWidth() - offsetX;
		}
		if (relocY > 0 && relocY + bounds.getHeight() + offsetY > graphY) {
			relocY = graphY - bounds.getHeight() - offsetY;
		}
		*/

		// relocate((int) (relocX), (int) (relocY));
		// mDragLink.attribute.setLayoutX(localCoords.getX());
		// mDragLink.attribute.setLayoutY(localCoords.getY());

		// ドラッグ後の座標とクリック時のポインタ位置の差を反映
		double relocX2 = relocX - mousePoint.getX();
		double relocY2 = relocY - mousePoint.getY();

		relocate((int) (relocX2), (int) (relocY2));

		mDragLink.attribute.setLayoutX(localCoords.getX());
		mDragLink.attribute.setLayoutY(localCoords.getY());

//		return new Point2D(relocX, relocY);
		return new Point2D(relocX2, relocY2);
	}


	public void dragNodeDelete() {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		// 2020/11 AW REP start
		//AnchorPane parent = (AnchorPane) self.getParent();
		Pane parent = (Pane) self.getParent();
		// 2020/11 AW REP end

		if (parent != null && parent.getChildren() != null) {
			parent.getChildren().remove(self);
		}

		// iterate each link id connected to this node
		// find it's corresponding component in the right-hand
		// AnchorPane and delete it.

		// Note: other nodes connected to these links are not being
		// notified that the link has been removed.
		for (ListIterator<String> iterId = mLinkIds.listIterator(); iterId.hasNext();) {

			String id = iterId.next();

			for (ListIterator<Node> iterNode = parent.getChildren().listIterator(); iterNode.hasNext();) {

				Node node = iterNode.next();

				if (node.getId() == null)
					continue;

				if (node.getId().equals(id))
					iterNode.remove();
			}

			iterId.remove();
		}
		deleteNode();
	}

	public EntityNode getEntityNode() {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");
		//System.out.println("in getEntityNode");
		return this.node;
	}


	private void deleteNode() {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		graphAct.deleteNode(node);
	}

}

