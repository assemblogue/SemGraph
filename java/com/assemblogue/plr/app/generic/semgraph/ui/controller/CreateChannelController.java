package com.assemblogue.plr.app.generic.semgraph.ui.controller;

import java.net.URL;
import java.util.ResourceBundle;

import com.assemblogue.plr.app.generic.semgraph.App;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 * チャネル作成ウィンドウのコントローラクラスです。
 *
 */
public class CreateChannelController implements Initializable {
	@FXML
	private Button channelCancel;
	@FXML
	private Button channelCreate;
	@FXML
	private TextField channelName;
	@FXML
	private TextField description;

	private AppController appcontroller;
	private Stage ownerStage;
	private Stage stage;

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		initializeUI();
	}

	private void initializeUI() {

		channelCancel.setOnAction(mouseEvent -> exec_cancel());
		channelCreate.setOnAction(mouseEvent -> exec_create());
	}

	public void setStage(Stage stage) {
		this.stage = stage;

	}

	public void setOwnerStage(Stage stage) {
		ownerStage = stage;
	}

	public void setAppController(AppController ctrl) {
		appcontroller = ctrl;
	}

	private void exec_cancel() {
		stage.close();
	}

	private void exec_create() {
		String name = channelName.getText();
		String desc = description.getText();

		if (name.compareTo("") == 0) {
			name = null;
		}
		if (desc.compareTo("") == 0) {
			desc = null;
		}

		stage.getIcons().add(new Image(App.class.getResourceAsStream("ui/img/editor.jpg")));
		appcontroller.createChannel(name, desc);
		stage.close();
	}
}
