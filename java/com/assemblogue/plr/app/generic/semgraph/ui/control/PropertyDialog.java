package com.assemblogue.plr.app.generic.semgraph.ui.control;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.assemblogue.plr.app.generic.semgraph.GraphActor;
import com.assemblogue.plr.app.generic.semgraph.OssActor;
import com.assemblogue.plr.app.generic.semgraph.PlrActor;
import com.assemblogue.plr.app.generic.semgraph.ui.controller.RootLayoutController;
import com.assemblogue.plr.contentsdata.PLRContentsData;
import com.assemblogue.plr.contentsdata.PLRContentsData.InputtableProperty;
import com.assemblogue.plr.contentsdata.ontology.OntologyItem;
import com.assemblogue.plr.lib.EntityNode;
import com.assemblogue.plr.lib.Node;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class PropertyDialog {

	private static final  double HEIGHT = 400;
	private static final  double WIDTH = 400;

	Stage stage = new Stage();

   	/** プロパティ追加用 */
	Stage childStage;

	/** 表示プロパティマップ */
	Map<String, List<Object>> dispPropertyMap = new LinkedHashMap<>();

	/** 非表示プロパティリスト */
	List<String> noDispPropertyList = new ArrayList<>();;

	/** 受け取ったEntityNode */
	EntityNode entityNode;

	/** GraphActor */
	GraphActor graphActor;

	/** PLRContentsData */
	PLRContentsData contentsData;

	/** クラス */
	String classType = null;
	/** クラスに対応するOntologyItem */
	OntologyItem classOntologyItem;

																									//--------------------------★★ADD [フェーズ2] ここから↓
	/** 定数　 "creator" プロパティ名・プロパティ追加ダイアログサイズ */
	private static final  String  PROPERTY_CREATER = "creator";
	private static final  double ADD_WINDOW_HEIGHT = 400;
	private static final  double ADD_WINDOW_WIDTH = 250;

	//** このクラスでノードを作成する/しない
	private boolean isCreateNode = false;

	//** 作成するノードの位置
	private double coordsX;
	private double coordsY;

	//呼び出しクラス
	private RootLayoutController rootLayoutController;

	/*
	 * コンストラクタ　
	 * 		このクラスで呼び出し元にノードを作成する処理用コンストラクタ
	 * @param 	rootLayoutController
	 * @param	classType
	 * @param	plrAct
	 * @param	coordsX	:呼び出し元の座標
	 * @param	coordsY	:呼び出し元の座標
	 */
	public PropertyDialog(RootLayoutController rootLayoutController,String classType,PlrActor plrAct,double coordsX,double coordsY) {

		//このクラスでノード作成フラグ
		isCreateNode=true;

		//呼び出し次の位置
		this.coordsX = coordsX;
		this.coordsY = coordsY;

		//呼び出しクラス
		this.rootLayoutController = rootLayoutController;

		// GraphActor
		this.graphActor = rootLayoutController.getGraphAct();

		// PLRContentsData
		this.contentsData = rootLayoutController.getGraphAct().getGraphData().getContentsData();

		// クラスのOntologyItem
		this.classOntologyItem = contentsData.getNode(classType);

		// クラス
		this.classType = classType;
		if (this.classType .startsWith("#")) {
			this.classType = classType.substring(1);
		}

		// このコンストラクタは、ノード新規作成時にのみ呼ばれる。
		// 必ず作成されるプロパティのcreatorとbeginを表示プロパティマップに設定する。
		List<InputtableProperty> inputProperties = contentsData.AllPropertyMenu(classOntologyItem);

		// creator
		// 2021/07 AW start ストレージタイプにNULLが入る場合の対策
		//String creatorValue = plrAct.getStoragetype() + ":" +  plrAct.getUserID();
		String creatorValue = plrAct.getPlrId();
		// 2021/07 AW end ストレージタイプにNULLが入る場合の対策

		dispPropertyMap.put(PROPERTY_CREATER, setListOneValue(creatorValue));

		// begin
		List<Object> beginList = new ArrayList<>();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mmZ");
		String beginValue = df.format(new Date());
		beginList.add(beginValue);
		dispPropertyMap.put("begin", beginList);

		// endプロパティはオントロジー定義に合わせる
		InputtableProperty endIp = getOntologyItemProperty(inputProperties, "end");
		if (endIp != null) {

			// 多重度チェック
			int checkDisplay = checkDisplayProperty(endIp);
			if (checkDisplay > 0) {
				// 表示対象
				dispPropertyMap.put("end", new ArrayList<Object>());

			} else if (checkDisplay == 0) {
				noDispPropertyList.add("end");

			} else {
				//表示しない
			}
		}

		// 上記以外のパラメータを表示プロパティマップ、非表示プロパティリストに振り分け
		for (InputtableProperty ip: inputProperties) {

			// begin,end,creator は処理済みなので何もしない
			String propertyId = ip.item.id;
			// 頭の#を取り除く
			propertyId = propertyId.substring(1);
			if (propertyId.equals("begin") || propertyId.equals("end") || propertyId.contentEquals("creator")) {
				continue;
			}

			// 特殊処理 im（重要度）は表示しない
			if (propertyId.equals("im")) {
				continue;
			}

			// 多重度チェック
			int checkDisplay = checkDisplayProperty(ip);
			if (checkDisplay > 0) {
				// 表示対象
				dispPropertyMap.put(propertyId, new ArrayList<Object>());

			} else if (checkDisplay == 0) {
				noDispPropertyList.add(propertyId);

			} else {
				//表示しない
			}
		}

		stageShow();
	}
																									//--------------------------★★ADD ここまで↑

	/**
	 * コンストラクタ
	 * @param node
	 * @param rootLayoutController
	 */
	public PropertyDialog(EntityNode node, RootLayoutController rootLayoutController) {

		this.entityNode = node;
		// GraphActor
		this.graphActor = rootLayoutController.getGraphAct();
		// PLRContentsData
		this.contentsData = rootLayoutController.getGraphAct().getGraphData().getContentsData();
		// クラス
		this.classType = node.getType().get(0);
		// クラスのOntologyItem
		this.classOntologyItem = contentsData.getNode('#' + classType);

		// クラスのプロパティ取得
		//List<OntologyItem> classProperties = contentsData.getProperty(classOntologyItem);

		// EntityNodeから値を取得
		Map<String, List<Node>> entityValues = PlrActor.getProperties(node);

		// 表示プロパティマップを作成
		divideDisplayProperty(entityValues);

																//--------------------------★★移動 stageShow()メソッドに移動 [フェーズ2] ここから↓
		stageShow();
//		// 表示コンテンツの作成
//		VBox contentsVBox = makeContents();
//		//2021/3 AW ADD start
//		//contentsVBox.setBackground(new Background(new BackgroundFill(Paint.valueOf("#EEEEEE"),new  CornerRadii(0), new Insets(0))));
//		//2021/3 AW ADD end
//
//		stage.setScene(new Scene(contentsVBox,WIDTH,HEIGHT));
//
//		stage.setTitle(OssActor.getDisplayText(classOntologyItem));
//        stage.initModality(Modality.APPLICATION_MODAL);
//        stage.showAndWait();
																//--------------------------★★移動 stageShow()メソッドに移動 [フェーズ2] ここまで↑
	}
																//--------------------------★★ADD [フェーズ2] ここから↓
	/*
	 *ダイアログStage作成
	 */
	private  void stageShow() {
		// 表示コンテンツの作成
		VBox contentsVBox = makeContents();
		//2021/3 AW ADD start
		//contentsVBox.setBackground(new Background(new BackgroundFill(Paint.valueOf("#EEEEEE"),new  CornerRadii(0), new Insets(0))));
		//2021/3 AW ADD end

		stage.setScene(new Scene(contentsVBox,WIDTH,HEIGHT));

		stage.setTitle(OssActor.getDisplayText(this.classOntologyItem));
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.showAndWait();

	}
															//--------------------------★★ADD [フェーズ2] ここまで↑
	private VBox makeContents() {

		// コンテンツ
		VBox contentsVBox = new VBox();

		// プロパティ行VBox
		VBox propertyVBox = new VBox(5);

		//2021/3 AW ADD start
		//contentsVBox.setPadding(new Insets(10));
		//Paint white = Paint.valueOf("white");
		//Border contentsBorder = new Border(new BorderStroke(white,white,white,white,BorderStrokeStyle.NONE,BorderStrokeStyle.NONE,
		//		BorderStrokeStyle.SOLID,BorderStrokeStyle.NONE,new CornerRadii(0),BorderWidths.DEFAULT,new Insets(0)));
		//2021/3 AW ADD end


		for (Map.Entry<String, List<Object>> e: dispPropertyMap.entrySet()) {

			// プロパティ名
			String propertyName = e.getKey();
			// プロパティ値
			List<Object> propertyValueList = e.getValue();

			// プロパティがリストかチェック
			//if (propertyValue instanceof List) {
				//List<Object> propertyValueList = (List<Object>) propertyValue;

			if (propertyValueList.isEmpty()) {

				// プロパティ値が空のとき
				HBox rowHBox = new HBox(5);
				rowHBox.setPadding(new Insets(5));

				OntologyLabel propertyNameLabel = new OntologyLabel(contentsData, propertyName);
				propertyNameLabel.setIndex(0);

				String propertyValue = "";

				propertyNameLabel.setOnMouseClicked(event -> editPropertyMenu(propertyName, propertyNameLabel.getIndex(), propertyValue));

				Label propertyValueLabel = new Label(propertyValue);
				propertyValueLabel.setPadding(new Insets(5,0,5,0));
				propertyValueLabel.setAlignment(Pos.CENTER_LEFT);

				rowHBox.getChildren().addAll(propertyNameLabel, propertyValueLabel);
				propertyVBox.getChildren().add(rowHBox);

			} else {

				for (int i = 0; i < propertyValueList.size(); ++i) {

					HBox rowHBox = new HBox(5);
					rowHBox.setPadding(new Insets(5));

					OntologyLabel propertyNameLabel = new OntologyLabel(contentsData, propertyName);
					propertyNameLabel.setIndex(i);
					String propertyValue = String.valueOf(propertyValueList.get(i));

					// 値の表示文字列
					String dispPropertyValue = null;
																																			//ADD [フェーズ2] createrラベルは色を替えてイベントを追加しない
					if(propertyName.equals(PROPERTY_CREATER))
					{
						propertyNameLabel.setStyle("-fx-background-color: #f5f5f5;");
						propertyNameLabel.setBorder(new Border(new BorderStroke(Color.GRAY, BorderStrokeStyle.SOLID, null, new BorderWidths(1))));

						// craatorの場合は、表示文字列を変換する
						// 新規作成時は「自分」固定
						if (isCreateNode) {
							//FIXME
							// ロケールが日本の場合は"自分"(Personary準拠）
							if (Locale.getDefault().getLanguage().equals("ja")) {
								dispPropertyValue = "自分";
							} else {
								dispPropertyValue = "myself";
							}

						} else {
							dispPropertyValue = graphActor.getDisplayName(entityNode.getNodeId());
						}
					}
					else {
						propertyNameLabel.setOnMouseClicked(event -> editPropertyMenu(propertyName, propertyNameLabel.getIndex(), propertyValue));

						dispPropertyValue = propertyValue;
					}
					Label propertyValueLabel = new Label(dispPropertyValue);
					propertyValueLabel.setPadding(new Insets(5,0,5,0));
					propertyValueLabel.setAlignment(Pos.CENTER_LEFT);

					rowHBox.getChildren().addAll(propertyNameLabel, propertyValueLabel);
					propertyVBox.getChildren().add(rowHBox);
				}
			}
		}

		// ScrollPane
    	ScrollPane scrollPane = new ScrollPane();
    	scrollPane.setMinHeight(340);
    	scrollPane.setMaxHeight(340);
		scrollPane.setContent(propertyVBox);

		contentsVBox.getChildren().add(scrollPane);

		// ボタン行用VBox
		VBox buttonVBox = new VBox(5);
		buttonVBox.setMinHeight(60);
		buttonVBox.setMaxHeight(60);

		// プロパティ追加ボタン行
		if (noDispPropertyList.size() != 0) {

			HBox propertyButtonRowHBox = new HBox(5);

			Button propertyButton = new Button("add Property");
			HBox.setMargin(propertyButton, new Insets(5,0,0,25));
			propertyButton.setOnAction(mouseEvent -> execAddPropertyButton());
			propertyButtonRowHBox.getChildren().add(propertyButton);

			buttonVBox.getChildren().add(propertyButtonRowHBox);
		}

		// ボタン行
		HBox buttonRowHBox = new HBox(5);
		buttonRowHBox.setPadding(new Insets(5));
		buttonRowHBox.setAlignment(Pos.CENTER);
		//buttonRowHBox.setMinHeight(50);
		//buttonRowHBox.setMaxHeight(50);

		Button cancelButton = new Button("Cancel");
		cancelButton.setOnAction(mouseEvent -> execCancelButton());
		buttonRowHBox.getChildren().add(cancelButton);

		Button okButton = new Button("OK");
		okButton.setOnAction(mouseEvent -> execOkButton());
		buttonRowHBox.getChildren().add(okButton);

		buttonVBox.getChildren().add(buttonRowHBox);

		contentsVBox.getChildren().add(buttonVBox);

		return contentsVBox;
	}



	/**
	 * 表示プロパティと非表示プロパティを振り分け
	 * @param entityValues
	 */
	private void divideDisplayProperty(Map<String, List<Node>> entityValues) {

		// 設定先のクリア
		dispPropertyMap.clear();
		noDispPropertyList.clear();

		// クラスのプロパティ取得
		List<InputtableProperty> inputProperties = contentsData.AllPropertyMenu(classOntologyItem);

		// 固定プロパティ、begin、end、creator
		// これらのプロパティは存在する場合は、最初に表示

		// creatorは絶対に存在する、表示する																			//ここに移動 [フェーズ２] ここから↓
		String creatorValue = (String) getPropertySingleValue(entityValues, "creator");
		dispPropertyMap.put(PROPERTY_CREATER, setListOneValue(creatorValue));				//ここまで↑

		// beginは絶対に存在する、表示する
		List<Object> beginList = new ArrayList<>();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mmZ");

		if(this.isCreateNode) {															//ADD[フェーズ2]if文追加（既存コードはelse）
			String beginValue = df.format(new Date());
			beginList.add(beginValue);
		}else {
			for (Date d: entityNode.getBegin()) {
				String beginValue = df.format(d);
				beginList.add(beginValue);
			}
		}

		//OntologyItem propertyBeginOi = contentsData.getNode("#begin");
		//String dispBegin = OssActor.getDisplayText(propertyBeginOi);

		dispPropertyMap.put("begin", beginList);

		// endはプロパティチェックが必要
		List<Object> endList = new ArrayList<>();
		// 表示値
		if(this.isCreateNode) {																//ADD[フェーズ2]if文追加（既存コードはelse）

		}else {
			for (Date d: entityNode.getEnd()) {
				String endValue = df.format(d);
				endList.add(endValue);
			}
		}

		//if (isExistOntologyItemProperty(inputProperties, "end")) {
		//	OntologyItem oi = getSubClassOntologyItem(classOntologyItem, "end");
		//	if (isDisplayProperty(oi)) {
		InputtableProperty endIp = getOntologyItemProperty(inputProperties, "end");
		if (endIp != null) {

			// 多重度チェック
			int checkDisplay = checkDisplayProperty(endIp);
			if (checkDisplay > 0) {
				// 表示対象
				// 値がない場合は、値は空文字として入力行作成
				if (endList.size() == 0) {

					dispPropertyMap.put("end", new ArrayList<Object>());
				} else {

					dispPropertyMap.put("end", endList);
				}

			} else if (checkDisplay == 0) {
				// 多重度チェックで引っ掛からなくても
				// 値が設定されていれば表示する
				if (endList.size() == 0) {

					noDispPropertyList.add("end");
				} else {

					dispPropertyMap.put("end", endList);
				}
			} else {

				//表示しない
			}
		}

//		// creatorは絶対に存在する、表示する																			//beginの前に移動 [フェーズ２] ここから↓
//		String creatorValue = (String) getPropertySingleValue(entityValues, "creator");
//		dispPropertyMap.put("creator", setListOneValue(creatorValue));									//ここまで↑

		// 上記以外のパラメータ
		for (InputtableProperty ip: inputProperties) {

			// begin,end,creator は処理済みなので何もしない
			String propertyId = ip.item.id;
			// 頭の#を取り除く
			propertyId = propertyId.substring(1);
			if (propertyId.equals("begin") || propertyId.equals("end") || propertyId.contentEquals("creator")) {
				continue;
			}

			// 特殊処理 im（重要度）は表示しない
			if (propertyId.equals("im")) {
				continue;
			}

			// OntologyItemの取得（クラスのプロパティのもの）
			//OntologyItem propertyOi = getSubClassOntologyItem(classOntologyItem, propertyId);

			// プロパティの値取得
			// 値が設定されていない場合はnullが返ってくる
			List<Object> propertyValue = getPropertyValue(entityValues, propertyId);

			// 多重度チェック
			int checkDisplay = checkDisplayProperty(ip);
			if (checkDisplay > 0) {
				// 表示対象
				// 値がない場合は、値は空文字として入力行作成
				if (propertyValue == null) {

					dispPropertyMap.put(propertyId, new ArrayList<Object>());
				} else {

					dispPropertyMap.put(propertyId, propertyValue);
				}


			} else if (checkDisplay == 0) {
				// 多重度チェックで引っ掛からなくても
				// 値が設定されていれば表示する
				if (propertyValue == null) {

					noDispPropertyList.add(propertyId);
				} else {

					dispPropertyMap.put(propertyId, propertyValue);
				}
			} else {

				//表示しない
			}
		}
	}

	private Object getPropertySingleValue(Map<String, List<Node>> entityValues, String property) {

		List<Node> entityValueList = entityValues.get(property);
		if (entityValueList == null) {
			return null;
		}
		Object value = entityValueList.get(0).asLiteral().getValue();

		return value;
	}

	private List<Object> getPropertyValue(Map<String, List<Node>> entityValues, String propertyId) {

		List<Node> entityValueList = entityValues.get(propertyId);
		if (entityValueList == null || entityValueList.size() == 0) {

			return null;
		}

		/*
		if (entityValueList.size() == 1) {
			Node n = entityValueList.get(0);

			if (n.isLiteral()) {

				return n.asLiteral().getValue();

			} else if (n.isEntity()) {

				return n.asEntity();

			} else {

				return null;
			}
		}
		*/

		List<Object> resultList = new ArrayList<>();
		for (Node n: entityValueList) {

			if (n.isLiteral()) {

				resultList.add(n.asLiteral().getValue());

			} else if (n.isEntity()) {

				resultList.add(n.asEntity());
			}
		}

		return resultList;
	}

	private void execOkButton() {

		stage.close();
																							//--------------------------★★ADD [フェーズ2] if文追加(既存コードはelse)
		if(this.isCreateNode) {
			//呼び出し元クラスでノードを追加
			EntityNode createNode = rootLayoutController.addNode(this.coordsX,this.coordsY,this.classType) ;

			//dispPropertyMapを退避
			Map<String, List<Object>> plist = new LinkedHashMap<>();
			plist.putAll(dispPropertyMap);
			//呼び出し元クラス追加したノードのプロパティマップを作成し、退避したマップをコピー
			Map<String, List<Node>> entityValues = PlrActor.getProperties(createNode);
			divideDisplayProperty(entityValues);
			plist.forEach((key, value) -> dispPropertyMap.put(key, value));

			//プロパティマップをノードに設定
			graphActor.addPropertiesByDialog(createNode, dispPropertyMap);

		}else {
			graphActor.addPropertiesByDialog(entityNode, dispPropertyMap);
		}

	}

	private void execCancelButton() {
		stage.close();
	}

	private void execAddPropertyButton() {
		addPropertyDialog();
	}

	/**
	 * プロパティ編集ダイアログを表示し、変更後の値を取得
	 * 表示プロパティマップに反映する
	 * @param propertyName
	 * @param index
	 * @param propertyValue

	 */
	private void editPropertyMenu(String propertyName, Integer index, String propertyValue) {

		/*
		TextInputDialog tid = new TextInputDialog(propertyValue);
		tid.setTitle(propertyName);
		tid.initModality(Modality.APPLICATION_MODAL);
		tid.showAndWait();

		String result = tid.getEditor().getText();
		*/

		OntologyItem oi = contentsData.getNode('#' + propertyName);
		String dispText = OssActor.getDisplayText(oi);

		PropertyEditDialog ped = new PropertyEditDialog(dispText, propertyValue, false);
		//ped.showAndWait();

		// プロパティ編集ダイアログでOKボタンが押された場合
		if (ped.getPropetyEditDialogResult()) {

			Object result = ped.getPropertyValue();
			editDispPropertyMap(propertyName, index, result);
	   	}
	}

	private void editDispPropertyMap(String propertyName, Integer index, Object newValue) {

		// 編集したプロパティが既に存在する場合は、値を置換する
		if (dispPropertyMap.containsKey(propertyName)) {

			List<Object> valueList = dispPropertyMap.get(propertyName);

			//TODO リスト対応時要チェック
			if (valueList.isEmpty()) {
				valueList.add(newValue);
			} else {
				valueList.set(index, newValue);
			}

			dispPropertyMap.put(propertyName, valueList);

		} else {
			// 存在しない場合は新規作成
			List<Object> valueList = new ArrayList<>();
			valueList.add(newValue);
			dispPropertyMap.put(propertyName, valueList);

			// 非表示プロパティリストにプロパティがある場合は
			// リストから削除
			if (noDispPropertyList.contains(propertyName)) {
				noDispPropertyList.remove(propertyName);
			}
		}

		//TODO
		// ダイアログ再表示

		// 表示コンテンツの作成
		VBox contentsVBox = makeContents();
		stage.setScene(new Scene(contentsVBox));
	}

	private List<Object> setListOneValue(Object obj) {

		List<Object> objectList = new ArrayList<>();
		objectList.add(obj);

		return objectList;

	}

	/**
	 * 入力可能プロパティリストから、与えられたプロパティが存在するかチェック
	 * @param classProperties
	 * @param id
	 * @return
	 */
	private boolean isExistOntologyItemProperty(List<InputtableProperty> classProperties, String id) {

		boolean result = false;

		for (InputtableProperty ip: classProperties) {

			String oiId = ip.item.id;
			if (oiId.equals('#' + id)) {
				result = true;
				break;
			}
		}

		return result;
	}

	/**
	 * 入力可能プロパティリストから、与えられたプロパティの入力可能オントロジーアイテムを返す
	 * @param classProperties
	 * @param id
	 * @return
	 */
	private InputtableProperty getOntologyItemProperty(List<InputtableProperty> classProperties, String id) {

		for (InputtableProperty ip: classProperties) {

			String oiId = ip.item.id;
			if (oiId.equals('#' + id)) {
				return ip;
			}
		}
		return null;
	}

	/**
	 * デフォルトで表示対象かチェック（多重度でチェック）
	 * 戻り：1 デフォルト表示  0 追加表示 -1 非表示
	 * @return
	 */
	private int checkDisplayProperty(InputtableProperty ip) {

		// 多重度＝０（min, max=0)，または多重度指定がないものは表示しない
		// 多重度が０以上の指定(max=xx, min=0)がある場合は表示する。
		// 多重度がxx以下の指定(max=xxのみ)は表示しない。
		// 多重度が0未満は非表示（未対応）

		Integer maxCardinality = ip.maxCardinality;
		Integer minCardinality = ip.minCardinality;

		if (maxCardinality == null || minCardinality == null) {
			return 0;
		}

		if (minCardinality == 0 ||  minCardinality == 0) {
			return -1;
		}

		if (maxCardinality > 0 && minCardinality >= 0) {
			return 1;
		}

		return 0;
	}

	/**
	 * プロパティのOntologiItemを取得
	 * @param classOi
	 * @param id
	 * @return
	 */
	private OntologyItem getSubClassOntologyItem(OntologyItem classOi, String property) {

		String propertyId = '#' + property;

		for (OntologyItem subOi: classOi.subClassOf) {

			String onProperty = subOi.onProperty;
			if (onProperty != null && onProperty.equals(propertyId)) {
				return subOi;
			}
		}

		return null;
	}

	/**
	 * プロパティ追加ダイアログ
	 */
    private void addPropertyDialog() {

    	childStage = new Stage();

    	// 表示コンテンツの作成
    	VBox contentsVBox = new VBox();

    	// プロパティ用VBox
    	VBox propertyVBox = new VBox(5);

    	for (String s: noDispPropertyList) {
    		HBox rowHBox = new HBox(5);
    		rowHBox.setPadding(new Insets(5));

    		Label label = new OntologyLabel(contentsData, s);
    		label.setOnMouseClicked(event -> execChildLabel(s));
    		rowHBox.getChildren().add(label);

    		propertyVBox.getChildren().add(rowHBox);
    	}

      	ScrollPane scrollPane = new ScrollPane();
      	scrollPane.setMinHeight(340);
    	scrollPane.setMaxHeight(340);
		scrollPane.setContent(propertyVBox);

		contentsVBox.getChildren().add(scrollPane);

    	// ボタン行
		HBox buttonRowHBox = new HBox();
		Button cancelButton = new Button("Cancel");
		cancelButton.setOnAction(mouseEvent -> execChildCancelButton());
		buttonRowHBox.getChildren().add(cancelButton);
    	buttonRowHBox.setPadding(new Insets(5,0,5,0));
    	buttonRowHBox.setAlignment(Pos.CENTER);

		contentsVBox.getChildren().add(buttonRowHBox);

     	childStage.setScene(new Scene(contentsVBox,  ADD_WINDOW_WIDTH, ADD_WINDOW_HEIGHT));
    	childStage.setTitle(OssActor.getDisplayText(classOntologyItem));
    	childStage.initModality(Modality.APPLICATION_MODAL);
    	childStage.showAndWait();
    }


	private void execChildLabel(String propertyName) {

		// 編集ダイアログ
		editPropertyMenu(propertyName, 0, null);

		childStage.close();

	}

	private void execChildCancelButton() {
		childStage.close();
	}
	/*
	private void checkList() {

		// リストか否か
		if (propertyValue instanceof List) {
			List<Object> propertyValueList = (List<Object>) propertyValue;

			for (int i = 0; i < propertyValueList.size(); ++i) {

				resultMap.put(propertyName + '[' + i + ']', propertyValueList.get(i));

			}
		} else {

		}
	}
	*/
}
