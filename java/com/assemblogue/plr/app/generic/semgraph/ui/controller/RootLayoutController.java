package com.assemblogue.plr.app.generic.semgraph.ui.controller;

import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Queue;
import java.util.Timer;
import java.util.TimerTask;

import javax.imageio.ImageIO;

import com.assemblogue.plr.app.generic.semgraph.App;
import com.assemblogue.plr.app.generic.semgraph.AppProperty;
import com.assemblogue.plr.app.generic.semgraph.GraphActor;
import com.assemblogue.plr.app.generic.semgraph.GraphActor.LinkInfo;
import com.assemblogue.plr.app.generic.semgraph.LocalFileIO;
import com.assemblogue.plr.app.generic.semgraph.OssActor;
import com.assemblogue.plr.app.generic.semgraph.PlrActor;
import com.assemblogue.plr.app.generic.semgraph.Point2dSerial;
import com.assemblogue.plr.app.generic.semgraph.TinyLog;
import com.assemblogue.plr.app.generic.semgraph.ui.DraggableNode;
import com.assemblogue.plr.app.generic.semgraph.ui.GraphCoordinate;
import com.assemblogue.plr.app.generic.semgraph.ui.NodeLink;
import com.assemblogue.plr.app.generic.semgraph.ui.control.AlertDialog;
import com.assemblogue.plr.app.generic.semgraph.ui.control.ItemDialog;
import com.assemblogue.plr.app.generic.semgraph.ui.control.MessageDialog;
import com.assemblogue.plr.app.generic.semgraph.ui.control.OntologyDialog;
import com.assemblogue.plr.app.generic.semgraph.ui.control.PropertyDialog;
import com.assemblogue.plr.app.generic.semgraph.ui.control.SelectClassDialog;
import com.assemblogue.plr.app.generic.semgraph.ui.control.ZoomableScrollPane;
import com.assemblogue.plr.contentsdata.PLRContentsData;
import com.assemblogue.plr.contentsdata.ontology.OntologyItem;
import com.assemblogue.plr.lib.EntityNode;

import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
//import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.BoundingBox;
import javafx.geometry.Point2D;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.transform.Scale;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class RootLayoutController extends BorderPane {

	//private Logger logger = LogManager.getLogger();

	/** AppController */
	private AppController appController;

	/** appControler のStage */
	private Stage ownerStage;
	/** 自分のStage */
	private Stage stage;

	public Stage getStage() {
		return stage;
	}

	/** グラフ描画Pane */
	// 2020/11 AW REP start
	//AnchorPane right_pane;
	Pane right_pane;

	/**
	 * @return right_pane
	 */
	public Pane getRight_pane() {
		return right_pane;
	}
	// 2020/11 AW REP end

	/** スクロールPane */
	ZoomableScrollPane scrollPane;

	// 2020/11 AW ADD start
	/** シーン */
	Scene scene;

	/**
	 * @return scene
	 */
	public Scene getRootLayoutScene() {
		return scene;
	}

	/** グラフ座標計算クラス */
	private GraphCoordinate graphCoordinate;

	/**
	 * @return graphCoordinate
	 */
	public GraphCoordinate getGraphCoordinate() {
		return graphCoordinate;
	}
	// 2020/11 AW ADD end

	//@FXML
	//BorderPane borderpane;

	/** GraphActor */
	private GraphActor graphAct;

	public GraphActor getGraphAct() {
		return graphAct;
	}

	public void setGraphAct(GraphActor graphAct) {
		this.graphAct = graphAct;
	}

	private OssActor ossActor;
	public OssActor getOssActor() {
		return ossActor;
	}

	/** PlrActor */
	private PlrActor plrAct;

	/** マウスイベント リンク作成時のリンク元、リンク先 */
	private DraggableNode sourceNode = null, targetNode = null;

	/** ノードリスト */
	private List<DraggableNode> draggableNodes = new ArrayList<>();

	public List<DraggableNode> getDraggableNodes() {
		return draggableNodes;
	}


	/**
	 * フラグ、マウスイベント ドラッグ中か 画面のドラッグ制御に使用
	 */
	public boolean mouseDragFlag = false;

	/**
	 * 同期を抑止するか否かを表すフラグ
	 * 何らかのダイアログ表示中
	 * マウスドラッグ中（グラフのPan中、ノードのドラッグ中）
	 * リレーションのターゲット待ち
	 * のときはフラグをTrueにして、バックグラウンド同期を抑止する
	 */
	private boolean syncFlag = false;
	public boolean isSyncFlag() {
		return syncFlag;
	}

	/**
	 * フラグ、描画中か
	 */
	public boolean graphRepaintingFlag = false;

	// いろいろ使っているので残しておく
	public boolean isRepainting = false;

	/** ノードの右クリックメニュー */
	ContextMenu contextmenu = new ContextMenu();
	MenuItem toLink = new MenuItem(" Select the target node ");
	MenuItem deleteNode = new MenuItem("Delete Node");
	//MenuItem Copy = new MenuItem("Copy");
	MenuItem hypernode = new MenuItem("Hypernode");
	//MenuItem Cut = new MenuItem("Cut");
	//MenuItem Paste = new MenuItem("Paste");
	MenuItem itemDialogue = new MenuItem("Dialogue");
	MenuItem infoDialogue = new MenuItem("Info");
	MenuItem copynode = new MenuItem("Copy Node");
	MenuItem duplicatenode = new MenuItem("Duplicate Node");


	public ContextMenu contextmenu_right_pane = new ContextMenu();

	MenuItem createMessageMenuItem = new MenuItem("Create Message");
	MenuItem createOtherNodeMenuItem = new MenuItem("Create Other Node");
	SeparatorMenuItem menuSeparator = new SeparatorMenuItem();

	MenuItem pasteNode = new MenuItem("Paste Node");
	MenuItem pasteNode_ID = new MenuItem("Paste Node ID");


	/** 画面のメニュー */
	//MenuBar menuBar = new MenuBar();
	//Menu editMenu = new Menu("Edit");


	public List<DraggableNode> clipboard_nodes = new ArrayList<>();
	public List<LinkInfo> clipboard_edges = new ArrayList<>();

	/** タイトル文言 */
	private String thisTitle;

	//------------------------------------------- ★★ADD [フェーズ2] ここから↓
		/** Mクラス 固定値 */
		static final  String mclassId = "M";
		//** Mクラス　表示名 */
		String mclassDisplayText ="";

		/** 無視idリスト */
		static final  List<String> ignoreIdList = List.of("#Dummy");

		/** メッセージ登録プロパティ名 */
		static final String PROPERTY_NAME_CNT = "cnt";

		/** PLRContentsData */
		PLRContentsData contentsData;

		/** Mクラスの有無 */
		boolean isExitMClass = false;

		/**  リサイズ対応 */
		//リサイズ中フラグ
		boolean isSceneResize = false;
		//タイマー
		Timer timerSceneResize;

	//------------------------------------------- ★★ ADD ここまで↑
	/**
	 * コンストラクタ
	 *
	 * @param appController
	 * @param gact
	 */
	public RootLayoutController(AppController appController, GraphActor gact, OssActor ossActor) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		this.plrAct = AppController.plrAct;

		this.ossActor = ossActor;
		//this.contentsData = ossActor.getEventOss();
		//this.linkContentsData = ossActor.getOss();

		this.appController = appController;
		this.graphAct = gact;
		this.ownerStage = appController.getStage();

		// このRootLayoutControleerのstage
		this.stage = new Stage();

		// AppController画面が消された時の処理
		ownerStage.showingProperty().addListener(((observable, oldValue, newValue) -> {

			if (oldValue && !newValue) {
				stage.close();
			}
		}));

		initialize();
	}

	private void initialize() {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		// ウィンドウ設定読み込み
		LocalFileIO.readWindowSizeFile(graphAct);

		// GraphActorからノード読み込み
		List<DraggableNode> draggableNodeList = new ArrayList<>();
		for (EntityNode node : graphAct.getEntityNodeList()) {

			DraggableNode dragnode = new DraggableNode(graphAct, node, false, this);
			//FIXME
			Queue<Double> coords = graphAct.getNodeCoords(node.getNodeId());
			double x = coords.poll();
			double y = coords.poll();

			//String coords = graphAct.getNodeCoords(node.getNodeId());
			//String[] coordsArray = coords.split(",");
			//double x = Double.parseDouble(coordsArray[0]);
			//double y = Double.parseDouble(coordsArray[1]);
			// 2021/2 AW REP end

			dragnode.setLayoutX(x);
			dragnode.setLayoutY(y);
			draggableNodeList.add(dragnode);
		}

		// グラフ座標計算クラス
		// ここで、ウィンドウサイズ、位置、ズーム比を設定している
		graphCoordinate = new GraphCoordinate(graphAct, draggableNodeList);

		right_pane = new Pane();

		// ウィンドウメニュー作成
		//menuBar.getMenus().add(editMenu);

		// 右クリックメニュー作成（ノード用）
		//contextmenu.getItems().addAll(toLink, deleteNode, Copy, Cut, Paste, hypernode, itemDialogue, copynode,duplicatenode);
		contextmenu.getItems().addAll(toLink, deleteNode, hypernode, itemDialogue, infoDialogue, copynode, duplicatenode);

		contextmenu_right_pane.getItems().addAll(createMessageMenuItem,	createOtherNodeMenuItem, menuSeparator,pasteNode, pasteNode_ID);	//★★UPDATE [フェーズ2]
		//contextmenu_right_pane.getItems().addAll( pasteNode, pasteNode_ID);																//★★↑↑
		//contextmenu_right_pane.getItems().addAll(createNodeMenuItem, menuSeparator, pasteNode, pasteNode_ID);

		// editMenu.getItems().addAll(undo,redo);

		// Undoアクションメソッド
		/*
		 * undo.setOnAction(new EventHandler<ActionEvent>() {
		 *
		 * @Override public void handle(ActionEvent event) { logger.trace("◆START");
		 *
		 * undoManager.undo(); }
		 *
		 * });
		 *
		 * // Redoアクションメソッド redo.setOnAction(new EventHandler<ActionEvent>() {
		 *
		 * @Override public void handle(ActionEvent event) { logger.trace("◆START");
		 *
		 * undoManager.redo(); } });
		 */

		/*
		right_pane.addEventHandler(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>() {
			public void handle(MouseEvent event) {
				TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");
				if (event.getButton().equals(MouseButton.PRIMARY)) {
					isRepainting=true;

					if (event.getClickCount() == 2) {

						isRepainting = true;

						// クラス選択ダイアログ
						SelectClassDialog selectClassDialog = new SelectClassDialog(graphAct.getGraphData().getContentsData());

						// 選択したクラス
						String classType = selectClassDialog.getSelectClassType();
						// 選択されなかった場合はノード作成しない
						if (classType == null) {
							return;
						}

						// クラスのidは1文字目が#なので、それを取り除く
						if (classType.startsWith("#")) {
							classType = classType.substring(1);
						}

						double coordsX = event.getX() - 50;
						double coordsY = event.getY() - 5;

						// 相対座標に変換
				        Point2D relativeCoord = graphCoordinate.getRelativeCoordinate(new Point2D(coordsX, coordsY));

						// PLRとGraphNodeに新規ノード情報かきこみ
				        EntityNode node = graphAct.addNode(relativeCoord.getX(), relativeCoord.getY(), classType);

				        // 描画用ノードリストに追加
						DraggableNode dragnode = generateDragNode(graphAct, node);
						dragnode.setLayoutX(coordsX);
						dragnode.setLayoutY(coordsY);
						draggableNodes.add(dragnode);

						// グラフ座標再計算
						graphCoordinate.calcGraphCoordinate(draggableNodes, right_pane, scene);

						// 再描画判定
					    if (graphCoordinate.isRepaintFlag()) {
					    	// 再描画
					    	repaint(false);
					    } else {
					    	// 再描画しないでノード追加
					    	right_pane.getChildren().add(dragnode);
					    }
						event.consume();

						isRepainting = false;

					}
				}
			}
			});
			*/

		/**
		 * グラフのPaneのイベントハンドラー
		 */
		right_pane.addEventHandler(MouseEvent.MOUSE_RELEASED, new EventHandler<MouseEvent>() {
			public void handle(MouseEvent event) {
				TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

				// 既に表示されていた場合は消す
				if (contextmenu_right_pane.isShowing()) {
					contextmenu_right_pane.hide();
				}

				if (event.getButton().equals(MouseButton.PRIMARY)) {

					if (event.getClickCount() == 2) {
																																	//--------------------------★★ADD [フェーズ2] ここから↓
						List<OntologyItem> oilist = getRegistedClassList();

						if(oilist.size() < 1) {
							//何もGraph Settingされていない場合ワーニング
							AlertDialog alertdialog = new AlertDialog(AlertType.WARNING,null,"No class is available in the current ontology");
							alertdialog.showAndWait();
							return;

						}
						else if (!isExitMClass && oilist.size()==1)
						{
							// Mクラスが存在しない＋他のクラスが１つだけ存在する
							execCallPropertyDialog(oilist.get(0).id,event);		//該当クラスのプロパティダイアログ表示
							return;

						}

						else if (isExitMClass)
						{
							// Mクラスが存在する
							//     MessageDialogを表示する
							isRepainting = true;

							MessageDialog mdialog ;
							boolean isOtherNode = false; //"Other Node Type"ボタン有無フラグ
							if (oilist.size() > 1) {
								//Mクラス+他のクラスの場合　"Other Node Type"ボタン設置フラグON
								isOtherNode = true;
							}
							// MessageDialog 表示
							mdialog =  new MessageDialog(mclassDisplayText,"",isOtherNode);
							// クラス選択ダイアログボタンクリック以外の場合
							if(!mdialog.getOtherNodeClicked()) {
								//メッセージダイアログ OKボタン押下
								if(mdialog.getMessageDialogResult()) {
									EntityNode node  = addNode(event,mclassId);
									setMClassMessage(node,mdialog.getPropertyValue());//メッセージをノードのcntプロパティに追加
								}

								// 再描画
								repaint(true);
								event.consume();
								isRepainting = false;
								return;
							}
							// Mクラスと他のクラスが１つだけの場合
							//    プロパティダイアログを表示
							if (oilist.size()==2) {
								//他のクラスのIDを取得
								String ctype = "";
								for(OntologyItem oi:oilist) {
									if(oi.id.equals("#" + mclassId)) {
										continue;
									}else {
										ctype = oi.id;
									}
								}
								if(!ctype.equals("")) {
									execCallPropertyDialog(ctype,event);		//該当クラスのプロパティダイアログ表示
									return;
								}
							}

						}
																												//--------------------------★★ADD ここまで↑
						isRepainting = true;
						// クラス選択ダイアログ
						SelectClassDialog selectClassDialog = new SelectClassDialog(graphAct.getGraphData().getContentsData());

						// 選択したクラス
						String classType = selectClassDialog.getSelectClassType();
						// 選択されなかった場合はノード作成しない
						if (classType == null) {
							return;
						}
						execCallPropertyDialog(classType,event);		//選択クラスのプロパティダイアログ表示
						//addNode(event,classType);							//復活するかもしらないので残しておきます

						event.consume();
						isRepainting = false;

					}

				} else if (event.getButton().equals(MouseButton.SECONDARY)) {

					contextmenu_right_pane.show(right_pane, event.getScreenX(), event.getScreenY());

					pasteNode.setOnAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event1) {
							List<DraggableNode> copyNodes = appController.getCopyNodes();
							double nodeWidth = 0;
							if (copyNodes != null && copyNodes.size() > 0) {
								java.util.Iterator<DraggableNode> itr = copyNodes.iterator();
								while (itr.hasNext()) {
									DraggableNode copyNode = (DraggableNode) itr.next();
									double coordsX = event.getX() - 50;
									double coordsY = event.getY() - 5;
									double graphX = graphAct.getGraphData().getGraphSizeX();
									double graphY = graphAct.getGraphData().getGraphSizeY();
									if (coordsX < 5) {
										coordsX = 5;
									}
									if (coordsX > graphX - 150) {
										coordsX = graphX - 150;
									}
									if (coordsY < 25) {
										coordsY = 25;
									}
									if (coordsY > graphY - 50) {
										coordsY = graphY - 50;
									}
									coordsX = coordsX + nodeWidth;
									nodeWidth = nodeWidth + 140;

									// 2020/11 AW REP start
									// need this code because of 2020/11-AW-update, maybe....
									// 相対座標に変換
									Point2D relativeCoord = graphCoordinate.getRelativeCoordinate(new Point2D(coordsX, coordsY));

									// 2021/2 AW DEL start
									//String coords = relativeCoord.getX() + "," + relativeCoord.getY() + ",0";
									// 2021/2 AW DEL end


									//String coords = coordsX + "," + coordsY + ",0";

									EntityNode node = graphAct.pasteNode(relativeCoord.getX(), relativeCoord.getY(), copyNode.node);
									//EntityNode node = graphAct.addNode(relativeCoord.getX(), relativeCoord.getY(), copyNode.node.getType().get(0));
									//EntityNode node = graphAct.addNode("#Entity", coords);

									DraggableNode dragnode = generateDragNode(graphAct, node);
									//dragnode.setDisplayText(copyNode.getTextArea().getText(),
									//		copyNode.getTextArea().getPrefHeight());
									dragnode.setLayoutX(coordsX);
									dragnode.setLayoutY(coordsY);

									// 2020/11 AW DEL start
									//right_pane.getChildren().add(dragnode);
									// 2020/11 AW DEL end

									//dragnode.updateNodeCenter();
									draggableNodes.add(dragnode);
									//TextArea textArea = dragnode.getTextArea();
									//textArea.setContextMenu(contextmenu);

									// 2020/11 AW ADD start
									// グラフ座標再計算
									graphCoordinate.calcGraphCoordinate(draggableNodes, right_pane, scene);

									// 再描画判定
								    if (graphCoordinate.isRepaintFlag()) {
								    	// 再描画
								    	repaint(false);
								    } else {
								    	// 再描画しないでノード追加
								    	right_pane.getChildren().add(dragnode);
								    }
								    // 2020/11 AW ADD end

									event.consume();
								}
								appController.clearCopyNodes();
							}
						}
					});
					pasteNode_ID.setOnAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event1) {
							paste_nodes(graphAct);
						}
					});
																																	//--------------------------★★ADD [フェーズ2] ここから↓
					/*   グラフ画面右クリック→Create Message : Mクラスノードの生成    */
					createMessageMenuItem.setOnAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event2) {
							MessageDialog mdialog =  new MessageDialog(mclassDisplayText,"",false);
							if(mdialog.getMessageDialogResult()) {
								EntityNode node  = addNode(event,mclassId);
								setMClassMessage(node,mdialog.getPropertyValue());//メッセージをノードのcntプロパティに追加
								// 再描画
								repaint(true);
							}
						}
					});
					/*   グラフ画面右クリック→Create Other Node : Mクラス以外のノードの生成    */
					createOtherNodeMenuItem.setOnAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event3) {
							String sclassType = "";

							//
							List<OntologyItem> oilist = getRegistedClassList();
							for(OntologyItem oi:oilist) {
								if(oi.id.equals("#" + mclassId)) {
									oilist.remove(oi);
									break;
								}
							}

							//クラスが１つの場合（Mクラス以外）
							if (oilist.size() == 1) {
								sclassType = oilist.get(0).id;		//１つのときはクラス選択ダイアログは不要
							}else {
							// クラス選択ダイアログ
									SelectClassDialog selectClassDialog = new SelectClassDialog(graphAct.getGraphData().getContentsData());
									// 選択したクラス
									sclassType = selectClassDialog.getSelectClassType();
							}

							// プロパティダイアログ
							if (!Objects.isNull(sclassType) && !sclassType.equals("")) {
								execCallPropertyDialog(sclassType,event);		//選択クラスのプロパティダイアログ表示
							}
						}
					});
																																//--------------------------★★ADD ここまで↑
		/* 	pasteNode_ID.setOnAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event1) {
							System.out.println("paste id");
							List<DraggableNode> copyNodes = appController.getCopyNodes();
							double nodeWidth = 0;
							if (copyNodes != null && copyNodes.size() > 0) {
								java.util.Iterator<DraggableNode> itr = copyNodes.iterator();
								while (itr.hasNext()) {
									DraggableNode copyNode = (DraggableNode) itr.next();
									double coordsX = event.getX() - 50;
									double coordsY = event.getY() - 5;
									double graphX = graphAct.getGraphData().getGraphSizeX();
									double graphY = graphAct.getGraphData().getGraphSizeY();
									if (coordsX < 5) {
										coordsX = 5;
									}
									if (coordsX > graphX - 150) {
										coordsX = graphX - 150;
									}
									if (coordsY < 25) {
										coordsY = 25;
									}
									if (coordsY > graphY - 50) {
										coordsY = graphY - 50;
									}
									coordsX = coordsX + nodeWidth;
									nodeWidth = nodeWidth + 140;

									// 2020/11 AW REP start
									// need this code because of 2020/11-AW-update, maybe....
									 // 相対座標に変換
									Point2D relativeCoord = graphCoordinate.getRelativeCoordinate(new Point2D(coordsX, coordsY));
									//String coords = relativeCoord.getX() + "," + relativeCoord.getY() + ",0";
									//String coords = coordsX + "," + coordsY + ",0";
									// 2020/11 AW REP end
									DraggableNode dragnode = generateDragNode(graphAct, copyNode.getEntityNode());
 									dragnode.setLayoutX(coordsX);
									dragnode.setLayoutY(coordsY);
 									// 2020/11 AW DEL start
									//right_pane.getChildren().add(dragnode);
									// 2020/11 AW DEL end

									//dragnode.updateNodeCenter();
									draggableNodes.add(dragnode);
 									//TextArea textArea = dragnode.getTextArea();
									//textArea.setContextMenu(contextmenu);
							    	right_pane.getChildren().add(dragnode);


									// 2020/11 AW ADD start
									// グラフ座標再計算
									graphCoordinate.calcGraphCoordinate(draggableNodes, right_pane, scene);

									// 再描画判定
								    if (graphCoordinate.isRepaintFlag()) {
								    	// 再描画
								    	repaint(false);
								    } else {
								    	// 再描画しないでノード追加

								    }
								    // 2020/11 AW ADD end

									event.consume();
								}
								appController.clearCopyNodes();
							}
						}
					}); */
				}
			}
		});

		/**
		 * Paneをドラッグしたとき（パンしたとき）のイベントハンドラー
		 * ノードをドラッグしているときも動いてしまう場合があるので、ノードドラッグ中は何もせずキャンセルさせる
		 */
		right_pane.addEventHandler(MouseEvent.MOUSE_DRAGGED, new EventHandler<MouseEvent>() {
			public void handle(MouseEvent event) {
				TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");
				if (event.getButton().equals(MouseButton.PRIMARY)) {
					if (mouseDragFlag) {
						//TinyLog.debug(new Throwable().getStackTrace()[0], false, "node drag now.");
						event.consume();
					} else {
						//TinyLog.debug(new Throwable().getStackTrace()[0], false, "node not drag.");
					}
				}
			}
		});

		/**
		 * コンテキストメニューを開いたとき、閉じたとき
		 */
		contextmenu.showingProperty().addListener(((observable, oldValue, newValue) -> {
			//logger.debug("ContextMenu showingProperty old={}, new={}", oldValue, newValue);

			if (oldValue == true && newValue == false) {
				// ダイアログを閉じた時

				// 描画中フラグ
				graphRepaintingFlag = false;

			} else if (oldValue == false && newValue == true) {
				// ダイアログを開いたとき

				// 描画中フラグ
				graphRepaintingFlag = true;
			}
		}));

																				//--------------------------★★ADD [フェーズ2] ここから↓
		//PLRContentsDataの更新処理
		updateContentsData();
		//Mクラス　タイトル取得
		OntologyItem oi =contentsData.getNode("#" + mclassId);
		mclassDisplayText	= OssActor.getDisplayText(oi);					//--------------------------★★ADD ここまで↑

		// グラフ描画
		drawGraphPane(false);

	}

																																		//--------------------------★★ADD [フェーズ2] オーバーロードメソッド追加
	/*
	 * ノード追加メソッド
	 * @param MouseEvent event
	 * @param String classType
	 */
	private EntityNode  addNode( MouseEvent event,String classType){
		return addNode(event.getX(),event.getY() ,classType);
	}
	/*
	 * ノード追加メソッド
	 * @param MouseEvent event
	 * @param String classType
	 * 				★★メソッド型：void→EntityNodeに変更　：ダブルクリック/右クリックでメッセージノード機能追加のため
	 * 				★★引数 :MouseEvent event → coordsX,coordsY
	 * 				★★private → publice
	 */																																//--------------------------★★ADD↑
	public EntityNode  addNode(double coordsX,double coordsY,String classType){
		TinyLog.debug(new Throwable().getStackTrace()[0], false, "◆START");

		// クラスのidは1文字目が#なので、それを取り除く
		if (classType.startsWith("#")) {
			classType = classType.substring(1);
		}
																																		//★★UPDATE[フェーズ2]↓
		 coordsX = coordsX- 50;
		 coordsY = coordsY - 5;
		//double coordsX = event.getX() - 50;
		//double coordsY = event.getY() - 5;
																																		//★★UPDATE[フェーズ2]↑
		// 相対座標に変換
        Point2D relativeCoord = graphCoordinate.getRelativeCoordinate(new Point2D(coordsX, coordsY));

		// PLRとGraphNodeに新規ノード情報かきこみ
        EntityNode node = graphAct.addNode(relativeCoord.getX(), relativeCoord.getY(), classType);

        TinyLog.debug(new Throwable().getStackTrace()[0], false, "draggable node add start.");
        // 描画用ノードリストに追加
		DraggableNode dragnode = generateDragNode(graphAct, node);
		dragnode.setLayoutX(coordsX);
		dragnode.setLayoutY(coordsY);
		draggableNodes.add(dragnode);
		TinyLog.debug(new Throwable().getStackTrace()[0], false, "draggable node add end.");

		// グラフ座標再計算
		graphCoordinate.calcGraphCoordinate(draggableNodes, right_pane, scene);

		// 再描画判定
	    if (graphCoordinate.isRepaintFlag()) {
	    	TinyLog.debug(new Throwable().getStackTrace()[0], false, "repaintFlag=true repaint start.");
	    	// 再描画
	    	repaint(false);
	    	TinyLog.debug(new Throwable().getStackTrace()[0], false, "repaintFlag=true repaint end.");
	    } else {
	    	TinyLog.debug(new Throwable().getStackTrace()[0], false, "repaintFlag=false add node start.");
	    	// 再描画しないでノード追加
	    	right_pane.getChildren().add(dragnode);
	    	TinyLog.debug(new Throwable().getStackTrace()[0], false, "repaintFlag=false add node end.");
	    }

	    return node;
	}

	/**
	 * 編集エリアを作成
	 *
	 * @param nd_name ノード名
	 */
	public void createGraphArea(String nd_name) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		// グラフ編集エリア設定
		scrollPane = new ZoomableScrollPane(right_pane, this);
		this.setCenter(scrollPane);

		scene = new Scene(this);

		scene.getStylesheets().add(App.class.getResource("ui/css/app.css").toExternalForm());
		stage.getIcons().add(new Image(App.class.getResourceAsStream("ui/img/editor.jpg")));
		thisTitle = nd_name + " -  " + plrAct.getUserID();
		stage.setTitle(thisTitle);
		stage.setScene(scene);

		// ウィンドウ消したときの動作
		stage.showingProperty().addListener((observable, oldValue, newValue) -> {
			if (oldValue && !newValue) {
				this.windowClose();
			}
		});

		// メニュー設定
		Menu Options = new Menu("Edit");
		MenuItem undo = new MenuItem("Undo");
		MenuItem save = new MenuItem("Save");
		MenuItem resetView = new MenuItem("Reset view");
		Options.getItems().addAll(undo, resetView, save);

		Menu graphOptions = new Menu("Graph");
		MenuItem graphSettingItem = new MenuItem("Graph Setting");
		//TODO
		// 万が一GraphSettingがおかしくなってしまったときに、GraphSettingを削除するメニュー
		MenuItem removeGraphSettingItem = new MenuItem("Remove Graph Setting");
		//graphOptions.getItems().add(graphSettingItem);
		graphOptions.getItems().addAll(graphSettingItem, removeGraphSettingItem);

		MenuBar menuBar = new MenuBar();
		menuBar.getMenus().addAll(Options, graphOptions);
		this.setTop(menuBar);

		// メニューアクション
		undo.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event1) {
				//logger.trace("◆START");

				// 描画中フラグ
				graphRepaintingFlag = true;

				graphAct.exc_undo();
				// excUndo();

				// 描画中フラグ
				graphRepaintingFlag = false;

				drawGraphPane(false);
			}
		});

		save.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				//logger.trace("◆START");

				captureAndSaveDisplay();
			}
		});

		// MENU RELOCATE FUNCTION
		resetView.setOnAction(event1 -> {
			//logger.trace("◆START");

			relocateAction();
		});

		// GraphSetting
		graphSettingItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {

				if(appController.isOssThreadFlag()) {

					// オントロジー読み込み中はエラーダイアログを表示
					String title = "オントロジー読み込み中";
					String messege = "しばらくしてから、再実行してください。";
					AlertDialog  dialog = new AlertDialog(AlertType.INFORMATION,title,messege,stage);
					dialog.show();

				} else {
					//TODO 第5引数は固定jaにしているが将来多言語対応時適切な変数に変更する。
					OntologyDialog ontlogyDialog
							= new OntologyDialog(appController.getOssList(), appController.getOssMap(),
									appController.isOssThreadFlag(), RootLayoutController.this, "ja");
					ontlogyDialog.showAndWait();

					//PLRContentsDataの更新処理
					updateContentsData();																//★★ADD [フェーズ2]

				}
			}
		});

		// remove GraphSetting
		removeGraphSettingItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {

				graphAct.removeGraphSetting();

				//PLRContentsDataの更新処理
				updateContentsData();																//★★ADD [フェーズ2]
			}
		});


		/**
		 * スクロールバー
		 */
		scrollPane.vvalueProperty().addListener((ObservableValue<? extends Number> observable,
																Number oldValue, Number newValue) -> {

			TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START change vvalue; = (" + oldValue + " → " + newValue + ")");
			double vvalue = (double) newValue;
			double hvalue = scrollPane.getHvalue();

			graphCoordinate.setAbsoluteViewPoint(hvalue, vvalue);

			if (graphCoordinate.isChangeScaleValue(scrollPane.getScaleValue())) {

				graphCoordinate.setScale(scrollPane.getScaleValue());
				graphCoordinate.calcGraphCoordinateByWindowChange(draggableNodes, right_pane, scene, null, null);

				TinyLog.trace(new Throwable().getStackTrace()[0], false, "zoom = (" + scrollPane.getScaleValue() + ")");

				// 再描画
				//TODO ズーム時は再描画なしで問題なさそう
				//repaint(false);
			}
		});

		/**
		 * スクロールバー
		 */
		scrollPane.hvalueProperty().addListener((ObservableValue<? extends Number> observable,
																Number oldValue, Number newValue) -> {

			TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START change hvalue; = (" + oldValue + " → " + newValue + ")");
			double hvalue = (double) newValue;
			double vvalue = scrollPane.getVvalue();

			graphCoordinate.setAbsoluteViewPoint(hvalue, vvalue);

			if (graphCoordinate.isChangeScaleValue(scrollPane.getScaleValue())) {

				graphCoordinate.setScale(scrollPane.getScaleValue());
				graphCoordinate.calcGraphCoordinateByWindowChange(draggableNodes, right_pane, scene, null, null);

				TinyLog.trace(new Throwable().getStackTrace()[0], false, "zoom = (" + scrollPane.getScaleValue() + ")");

				// 再描画
				//TODO ズーム時は再描画なしで問題なさそう
				//repaint(false);
			}
		});

		/**
		 * ウィンドウ
		 */
	    scene.widthProperty().addListener((ObservableValue<? extends Number> observable,
																Number oldValue, Number newValue) -> {

			TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START oldWidth:" + oldValue + "  newWidth:" + newValue);

			if ((double)oldValue == 0) {
				return;
			}

			/*
			Double windowX = null;
			Double windowY = null;
			// アイコン化してない場合
			if (!stage.isIconified()) {
				 windowX = scrollPane.getWidth();
				 windowY = scrollPane.getHeight();
			}

			graphCoordinate.calcGraphCoordinateByWindowChange(draggableNodes, right_pane, scene, windowX, windowY);

			// 再描画
			//repaint(false);
			 */

			// Sceneリサイズ開始処理(Width)																												 *
			if (!isSceneResize) {
				isSceneResize = true;
				sceneResizeStart();
			}

		});

	    /**
		 * ウィンドウ
		 */
	    scene.heightProperty().addListener((ObservableValue<? extends Number> observable,
																Number oldValue, Number newValue) -> {

			TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START oldHeight:" + oldValue + "  newHeight:" + newValue);

			if ((double)oldValue == 0) {
				return;
			}

			/*
			Double windowX = null;
			Double windowY = null;
			// アイコン化してない場合
			if (!stage.isIconified()) {
				 windowX = scrollPane.getWidth();
				 windowY = scrollPane.getHeight();
			}

			graphCoordinate.calcGraphCoordinateByWindowChange(draggableNodes, right_pane, scene, windowX, windowY);

			// 再描画
			//repaint(false);
			 */

			// Sceneリサイズ開始処理(Height)																												 *
			if (!isSceneResize) {
				isSceneResize = true;
				sceneResizeStart();
			}

		});

		// 2020/11 AW DEL start
		// グラフ拡張
		//expand.setOnAction(event -> {
		//	//logger.trace("◆START");

		//	expandGraphSizeAction();
		//});
		// 2020/11 AW DEL end

	    // 2020/11 AW ADD start
		right_pane.setMinHeight(graphCoordinate.getGraphSize().getY());
		right_pane.setMinWidth(graphCoordinate.getGraphSize().getX());
		scrollPane.updateScale(graphCoordinate.getScale());
		scrollPane.setPrefSize(graphCoordinate.getWindowSize().getX(), graphCoordinate.getWindowSize().getY());
		// 2020/11 AW ADD end

		// グラフ編集エリア表示
		stage.show();

		// 2020/11 AW ADD start
		// グラフ座標計算、初期設定
		graphCoordinate.getinitScene(draggableNodes, right_pane, scene);

		// スクロール位置は遅延で設定
		final double scrollHvalue = graphCoordinate.getHvalue();
		final double scrollVvalue = graphCoordinate.getVvalue();
		// 2020/11 AW ADD end

		// スクロール位置調整
		// Platform.runLater(()->this.requestFocus());
		Platform.runLater(() -> {
			TinyLog.debug(new Throwable().getStackTrace()[0], false, "Scroll set by createGraphArea.");

			scrollPane.setHvalue(scrollHvalue);
			scrollPane.setVvalue(scrollVvalue);

		});
	}

	/*
	 * Sceneリサイズ開始時の処理
	 * 		ResizeEnd検出用タイマー設定
	 */
	private void sceneResizeStart(){
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

        if(null != timerSceneResize){
        	this.timerSceneResize.cancel();
        	this.timerSceneResize = null;
        }
        this.timerSceneResize = new Timer(); // Timer インスタンスを生成
        SceneResizeTimerTask timerTask = new SceneResizeTimerTask(this);
        this.timerSceneResize.schedule(timerTask,0,200);
	}

	/*
	 * Sceneリサイズ完了時の処理
	 */
	public void sceneResizeEnd() {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		this.timerSceneResize.cancel();										//タイマーキャンセル
		this.timerSceneResize = null;
		isSceneResize = false;

		Double windowX = null;
		Double windowY = null;
		// アイコン化してない場合
		if (!stage.isIconified()) {
			 windowX = scrollPane.getWidth();
			 windowY = scrollPane.getHeight();
		}

		graphCoordinate.calcGraphCoordinateByWindowChange(draggableNodes, right_pane, scene, windowX, windowY);

		// 再描画
		repaint(false);
	}

	// 2020/11 AW DEL start
	/**
	 * グラフ拡張アクション
	 */
	/*
	private void expandGraphSizeAction() {

		// 描画中フラグ
		graphRepaintingFlag = true;

		// グラフサイズを500広げる
		// 座標を250加える
		final double graphDelta = 500;
		final double coordsDelta = 250;

		// グラフぐサイズ(GraphData,PLR)
		double graphSizeX = graphAct.getGraphData().getGraphSizeX();
		double graphSizeY = graphAct.getGraphData().getGraphSizeY();

		graphSizeX += graphDelta;
		graphSizeY += graphDelta;

		graphAct.getGraphData().setGraphSizeX(graphSizeX);
		graphAct.getGraphData().setGraphSizeY(graphSizeY);
		graphAct.changeGraphSize();

		// グラフサイズ（pane)
		right_pane.setMinWidth(graphSizeX);
		right_pane.setMinHeight(graphSizeY);
		right_pane.setMaxWidth(graphSizeX);
		right_pane.setMaxHeight(graphSizeY);
		right_pane.setPrefWidth(graphSizeX);
		right_pane.setPrefHeight(graphSizeY);

		// 座標(GraphData,PLR)
		graphAct.changeAllCoords(coordsDelta, coordsDelta);

		// 描画中フラグ
		graphRepaintingFlag = false;

		// 再描画
		drawGraphPane(null);
	}
	*/
	// 2020/11 AW DEL end

	/**
	 * ResetView Action
	 */
	private void relocateAction() {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		// 描画中フラグ
		graphRepaintingFlag = true;

		// 2020/11 AW DEL start
		/*
		// 最も端の座標を取得
		double minX = 0;
		double maxX = 0;
		double minY = 0;
		double maxY = 0;
		List<DraggableNode> draggableNodeList = getDraggableNodes();
		if (!draggableNodeList.isEmpty()) {
			minX = Collections.min(getDraggableNodes(), Comparator.comparing(javafx.scene.Node::getLayoutX))
					.getLayoutX();
			maxX = Collections.max(getDraggableNodes(), Comparator.comparing(javafx.scene.Node::getLayoutX))
					.getLayoutX() + 135; // 135はたぶんデフォルトのノード幅
			minY = Collections.min(getDraggableNodes(), Comparator.comparing(javafx.scene.Node::getLayoutY))
					.getLayoutY();
			maxY = Collections.max(getDraggableNodes(), Comparator.comparing(javafx.scene.Node::getLayoutY))
					.getLayoutY() + 35; // 35はたぶんデフォルトのノード高さ
		}
		//logger.debug("minX={},  minY={}, maxX={}, maxY={}", minX, minY, maxX, maxY);

		// ズーム再計算
		double xRate = scrollPane.getWidth() / (maxX - minX);
		double yRate = scrollPane.getHeight() / (maxY - minY);

		scrollPane.updateScale(Math.min(xRate, yRate) * 0.9);
		// scrollPane.layout();

		// スクロール位置再計算
		double scrollHValue = minX / (right_pane.getWidth() - scrollPane.getWidth());
		double scrollVValue = minY / (right_pane.getHeight() - scrollPane.getHeight());
		scrollPane.setHvalue(scrollHValue);
		scrollPane.setVvalue(scrollVValue);
		*/
		// 2020/11 AW DEL end

		// 2020/11 AW ADD start
		// バウンディングボックス取得
		BoundingBox boundingBox = graphCoordinate.getBoundingBox();

		// 一旦、現状のバウンディングボックスに位置づけ
		// ここで位置づけすると、後続の処理が空振りしにくくなる。
		// 画面上に何もノードがないと、空振りの確率が大きくなる実感あり・・・・・
		Point2D absoluteBoundingBoxPoint = graphCoordinate.getAbsoluteCoordinate(new Point2D(boundingBox.getMinX(), boundingBox.getMinY()));
		graphCoordinate.setWindowPoint(absoluteBoundingBoxPoint);

		/*
		// スクロール位置設定
		double scrollHValueTemp = graphCoordinate.getHvalue();
		double scrollVValueTemp = graphCoordinate.getVvalue();
		scrollPane.setHvalue(scrollHValueTemp);
		scrollPane.setVvalue(scrollVValueTemp);

		//////////////////
		// ズーム再計算
		//////////////////

		// 現Scale値
		double scale = scrollPane.getScaleValue();
		// Scale値、小数点第2位切り捨て
		BigDecimal bd = BigDecimal.valueOf(scale);
		BigDecimal bd2 = bd.setScale(1, RoundingMode.DOWN);
		scale = bd2.doubleValue();

		// BB
		double boundingBoxWidth = boundingBox.getWidth();
		double boundingBoxHeight = boundingBox.getHeight();

		// シーン
		double sceneX = graphCoordinate.getSceneSize().getX();
		double sceneY = graphCoordinate.getSceneSize().getY();

		// BBがシーンサイズより大きい場合は、
		// 現状のズームから0.1ずつ小さくしていく（最小値0.3)
		// BBがシーンサイズより小さくなったら、そのズーム値を採用
		// 逆の場合
		// 現状のズームから0.1ずつ大きくしていく（最大値4)
		// BBがシーンサイズより大きくなったら、前の値を採用

		double i;
		if (boundingBoxWidth >= sceneX || boundingBoxHeight >= sceneY) {
			//logger.debug("boundingBoxSize > sceneSize");
			for (i = scale; i >= 0.5; i = i - 0.1) {
				//logger.debug("loopScale = ({})", i);
				scrollPane.updateScale(i);
				scrollPane.applyCss();
				scrollPane.layout();

				sceneX = graphCoordinate.getSceneSize().getX();
				sceneY = graphCoordinate.getSceneSize().getY();
				//logger.debug("boundingBoxSize = ({}, {})", boundingBoxWidth, boundingBoxHeight);
				//logger.debug("sceneSize = ({}, {})", graphCoordinate.getSceneSize().getX(), graphCoordinate.getSceneSize().getY());
				if (boundingBoxWidth < sceneX && boundingBoxHeight < sceneY) {
					break;
				}
			}
		} else {
			//logger.debug("boundingBoxSize < sceneSize");
			for (i = scale; i <= 2.0; i = i + 0.1) {
				//logger.debug("loopScale = ({})", i);
				scrollPane.updateScale(i);
				scrollPane.applyCss();
				scrollPane.layout();

				sceneX = graphCoordinate.getSceneSize().getX();
				sceneY = graphCoordinate.getSceneSize().getY();
				//logger.debug("boundingBoxSize = ({}, {})", boundingBoxWidth, boundingBoxHeight);
				//logger.debug("sceneSize = ({}, {})", graphCoordinate.getSceneSize().getX(), graphCoordinate.getSceneSize().getY());
				if (boundingBoxWidth >= sceneX || boundingBoxHeight >= sceneY) {
					// ループのひとつ前の値に戻す
					i = i - 0.1;
					break;
				}
			}
		}
		scale = i;

		//logger.debug("sceneSize = ({}, {})", graphCoordinate.getSceneSize().getX(), graphCoordinate.getSceneSize().getY());
		//logger.debug("boundingBoxPoint = ({}, {})", boundingBox.getMinX(), boundingBox.getMinY());
		//logger.debug("boundingBoxSize = ({}, {})", boundingBoxWidth, boundingBoxHeight);
		//logger.debug("newScale = ({})", scale);

		scrollPane.updateScale(scale);
		scrollPane.layout();

		// ウィンドウ位置づけ
		//Point2D absoluteBoundingBoxPoint = graphCoordinate.getAbsoluteCoordinate(new Point2D(boundingBox.getMinX(), boundingBox.getMinY()));
		absoluteBoundingBoxPoint = graphCoordinate.getAbsoluteCoordinate(new Point2D(boundingBox.getMinX(), boundingBox.getMinY()));
		graphCoordinate.setWindowPoint(absoluteBoundingBoxPoint);

		*/

		// スクロール位置設定
		double scrollHValue = graphCoordinate.getHvalue();
		double scrollVValue = graphCoordinate.getVvalue();

		Platform.runLater(() -> {
			//logger.debug("◆START Platform.runLater. Scroll By relocateAction.");
			scrollPane.setHvalue(scrollHValue);
			scrollPane.setVvalue(scrollVValue);

			//logger.debug("◆END Platform.runLater. Scroll hvalue=" + scrollHValue + " vvalue=" + scrollVValue);
		});
		// 2020/11 AW ADD end

		//logger.debug("Rellocate: xRate={},  yRate={}, scrollX={}, scrollY={}", xRate, yRate, scrollHValue,
		//		scrollVValue);

		// 描画中フラグ
		graphRepaintingFlag = false;
	}

	/**
	 * Capture Action
	 */
	private void captureAndSaveDisplay() {

		// 描画中フラグ
		graphRepaintingFlag = true;

		FileChooser fileChooser = new FileChooser();

		// Set extension filter
		fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("png files (*.png)", "*.png"));

		// Prompt user to select a file
		File file = fileChooser.showSaveDialog(null);

		if (file != null) {

			try {
				final WritableImage writableImage = new WritableImage((int) right_pane.getWidth(),
						(int) right_pane.getHeight());
				SnapshotParameters snapshotParameters = new SnapshotParameters();
				snapshotParameters.setTransform(new Scale(2.5, 2.5));
				WritableImage image = right_pane.snapshot(snapshotParameters, writableImage);
				RenderedImage renderedImage = SwingFXUtils.fromFXImage(image, null);
				// Write the snapshot to the chosen file
				ImageIO.write(renderedImage, "png", file);
			} catch (IOException e) {
				System.out.println("Failed to save image: " + e);
				e.printStackTrace();
			}
		}

		// 描画中フラグ
		graphRepaintingFlag = false;
	}

	/**
	 * ノードペインを閉じる ノードリスト一覧を開いていたら、一緒に閉じる。 複数同じノードリストを開いた際の整合性が取れなくなるため。
	 */
	private void windowClose() {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		// グラフマップのロック取得
		appController.setGraphMapLock(true);

		// ウィンドウ情報
		// 2020/11 AW REP start
		//double windowSizeX = scrollPane.getWidth();
		//double windowSizeY=  scrollPane.getHeight();
		//double windowSizeX = stage.getWidth();
		//double windowSizeY=  stage.getHeight();
		//graphAct.getGraphData().setWindowSizeX(windowSizeX);
		//graphAct.getGraphData().setWindowSizeY(windowSizeY);

		Point2D windowSize = graphCoordinate.getWindowSize();
		graphAct.getGraphData().setWindowSizeX(windowSize.getX());
		graphAct.getGraphData().setWindowSizeY(windowSize.getY());
		// 2020/11 AW REP end

		// 2020/11 AW REP start
		//double windowRootX = scrollPane.getHvalue();
		//double windowRootY = scrollPane.getVvalue();
		//graphAct.getGraphData().setWindowRootX(windowRootX);
		//graphAct.getGraphData().setWindowRootY(windowRootY);

		Point2D windowRoot = graphCoordinate.getWindowPoint();
		graphAct.getGraphData().setWindowRootX(windowRoot.getX());
		graphAct.getGraphData().setWindowRootY(windowRoot.getY());
		// 2020/11 AW REP end

		TinyLog.debug(new Throwable().getStackTrace()[0], false, "save windowPointX =" + windowRoot.getX());
		TinyLog.debug(new Throwable().getStackTrace()[0], false, "save windowPointY =" + windowRoot.getY());

		// 2020/11 AW REP start
		//double zoom = scrollPane.getScaleValue();
		double zoom = graphCoordinate.getScale();
		// 2020/11 AW REP end
		graphAct.getGraphData().setZoom(zoom);

		TinyLog.debug(new Throwable().getStackTrace()[0], false, "save zoom =" + zoom);

		// 2020/11 AW ADD start
		// グラフサイズ
		Point2D graphSize = graphCoordinate.getGraphSize();
		graphAct.getGraphData().setGraphSizeX(graphSize.getX());
		graphAct.getGraphData().setGraphSizeY(graphSize.getY());

		// ベースポイント
		Point2D basePoint = graphCoordinate.getBasePoint();
		graphAct.getGraphData().setBasePointX(basePoint.getX());
		graphAct.getGraphData().setBasePointY(basePoint.getY());
		// 2020/11 AW ADD end

		// ウィンドウ情報を保存
		LocalFileIO.writeWindowInfoFile(graphAct);


		// 2020/11 AW DEL start
		// グラフサイズ
		//double graphSizeX = right_pane.getMinWidth();
		//double graphSizeY = right_pane.getMinHeight();
		//graphAct.getGraphData().setGraphSizeX(graphSizeX);
		//graphAct.getGraphData().setGraphSizeY(graphSizeY);

		// グラフサイズをPLRに保存
		//graphAct.changeGraphSize();
		// 2020/11 AW DEL end

		// 同期
		// 2021/2 AW REP start
		plrAct.sync(graphAct.getGraphData().getGraphNode());
		//plrAct.sync(graphAct.getGraphNode());
		// 2021/2 AW REP end


		// グラフ管理Mapから、このRootLayoutControllerを削除
		// 2021/2 AW REP start
		appController.getGraphMap().remove(graphAct.getGraphData().getGraphNode().getNodeId());
		//appController.getGraphMap().remove(graphAct.getGraphData().getParentNode().getNodeId());
		// 2021/2 AW REP end
		// このウィンドウを消去
		stage.close();

		// ロック解除
		appController.setGraphMapLock(false);
		// 2021/2 AW REP start
		appController.clearOpenStage(graphAct.getGraphData().getGraphNode().getNodeId());
		//appController.clearOpenStage(graphAct.getGraphNode().getNodeId());
		// 2021/2 AW REP end
		// GraphManager.close(graphAct);
	}


	public void drawGraphPane(boolean isSyncFlag) {
		TinyLog.debug(new Throwable().getStackTrace()[0], false, "◆START");

		isRepainting = true;

		// 描画中フラグが立っている場合は描画しない
		if (graphRepaintingFlag) {
			//logger.debug("◆END graphRepaintingFlag.");

			return;
		}

		// 描画中フラグ
		graphRepaintingFlag = true;

		// バックグラウンド同期の場合、バウンディングボックス、グラフサイズ計算
		if (isSyncFlag) {
			List<DraggableNode> draggableNodeList = new ArrayList<>();// 同期で取得したノードリスト
			for (EntityNode node : graphAct.getEntityNodeList()) {
				DraggableNode dragnode = createDragNode(node);
				draggableNodeList.add(dragnode);
			}
			// グラフ座標再計算
			graphCoordinate.calcGraphCoordinate(draggableNodeList, right_pane, scene);
		}

		// スクロールバー位置づけ
		Double hvalueLater = isSyncFlag ? graphCoordinate.getHvalue(): null;
		Double vvalueLater = isSyncFlag ? graphCoordinate.getVvalue(): null;
		//if (isSyncFlag) {
		//	hvalueLater = graphCoordinate.getHvalue();
		//	vvalueLater = graphCoordinate.getVvalue();
		//}

		/*
		// 引数がある場合は時間を比較する
		if (syncGraphActor != null) {

			// グラフデータの更新時間取得
			long orgTime = graphAct.getGraphData().getUpdateTime();
			long syncTime = syncGraphActor.getGraphData().getUpdateTime();

			//logger.debug("orgTime={}", orgTime);
			//logger.debug("syncTime={}", syncTime);

			// 同期側のほうが更新時間が新しい場合は、同期側のグラフデータを有効にする
			if (syncTime - orgTime > 0) {

				// 値の比較 座標とテキスト、およびリンク情報が同じ場合は描画しない
				if (graphAct.compareGraphData(graphAct.getGraphData(), syncGraphActor.getGraphData())) {

					// 描画中フラグ
					graphRepaintingFlag = false;

					//logger.debug("◆END GraphData is equal. GraphActor not switched.");
					return;
				}
				setGraphAct(syncGraphActor);

				// 2020/11 AW ADD Start
				List<DraggableNode> draggableNodeList = new ArrayList<>();

				//logger.debug("graphName={}", graphAct.getGraphName());

				// 同期で取得したノードリスト
				for (EntityNode node : graphAct.getEntityNodeList()) {
					// 2021/2 AW DEL start
					//if (graphAct.isPropertyVisibleRoot(node)) {
					// 2021/2 AW DEL end
						//DraggableNode dragnode = new DraggableNode(graphAct, node, false, this);
						DraggableNode dragnode = createDragNode(node);
						draggableNodeList.add(dragnode);

						//logger.debug("node={}", node.getNodeId());
					// 2021/2 AW DEL start
					//}
					// 2021/2 AW DEL end
					}

				//logger.debug("draggableNodeListSize={}", draggableNodeList.size());

				// グラフ座標再計算
				graphCoordinate.calcGraphCoordinate(draggableNodeList, right_pane, scene);
				// 2020/11 AW ADD end

				//logger.debug("GraphActor switched.");

			} else {
				// 同期側のほうが古い場合は描画しない

				// 描画中フラグ
				graphRepaintingFlag = false;

				//logger.debug("◆END OrgGraph is newer. GraphActor not switched.");
				return;
			}
		}
		*/

		repaint(true);

		if (isSyncFlag) {

			// スクロールバー位置づけ
			//Double hvalueLater = graphCoordinate.getHvalue();
	        //Double vvalueLater = graphCoordinate.getVvalue();

			Platform.runLater(() -> {
				TinyLog.debug(new Throwable().getStackTrace()[0], false, "Scroll set by drawGraphPane(background sync).");

	        	if (hvalueLater != null) {
	        		TinyLog.debug(new Throwable().getStackTrace()[0], false, "Scroll set hvalue=" + hvalueLater);
	            	scrollPane.setHvalue(hvalueLater);
	            }
	            if (vvalueLater != null) {
	            	TinyLog.debug(new Throwable().getStackTrace()[0], false, "Scroll set vvalue=" + vvalueLater);
	            	scrollPane.setVvalue(vvalueLater);
	            }
			});
		}

		TinyLog.debug(new Throwable().getStackTrace()[0], false, "◆END");
	}


	public void repaint(boolean initFlag) {
		TinyLog.debug(new Throwable().getStackTrace()[0], false, "◆START");

		// 描画中フラグ
		graphRepaintingFlag = true;

		// 画面クリア
		draggableNodes.clear();
		right_pane.getChildren().clear();

		Point2D graphSize = graphCoordinate.getGraphSize();
        right_pane.setMinHeight(graphSize.getY());
        right_pane.setMinWidth(graphSize.getX());

		// ノードの描画
		for (EntityNode node : graphAct.getEntityNodeList()) {

			DraggableNode dragnode = createDragNode(node);
			draggableNodes.add(dragnode);
			right_pane.getChildren().add(dragnode);
		}

		// リンクの描画
		for (GraphActor.LinkInfo linkInfo: graphAct.getEdges()) {

			DraggableNode source = getDragNode(linkInfo.getSourceNodeId());
			DraggableNode target = getDragNode(linkInfo.getTargetNodeId());

			if ((source != null) && (target != null)) {
				drawNodeLink(source, target, linkInfo.getLinkType());
			}
		}

		// これをつけないと、スクロールバーの位置が動かないことがあるらしい
		right_pane.applyCss();
		right_pane.layout();

		// 描画中フラグ
		graphRepaintingFlag = false;

		graphAct.undoSwitch = true;
		isRepainting = false;

		if (initFlag) {

			// 初期描画時、及びバックグラウンドによる同期時

			// Focus取得 タイミングを遅くする
			Platform.runLater(()-> {
				TinyLog.debug(new Throwable().getStackTrace()[0], false, "Focus set by repaint.");
				this.requestFocus();
			});

		} else {

			Double hvalueLater = graphCoordinate.getHvalue();
            Double vvalueLater = graphCoordinate.getVvalue();

			Platform.runLater(() -> {
				TinyLog.debug(new Throwable().getStackTrace()[0], false, "Scroll set by repaint.");

	        	if (hvalueLater != null) {
	            	scrollPane.setHvalue(hvalueLater);
	            }
	            if (vvalueLater != null) {
	            	scrollPane.setVvalue(vvalueLater);
	            }
			});
		}

		//PLRContentsDataの更新処理
		updateContentsData();																//★★ADD [フェーズ2]

		TinyLog.debug(new Throwable().getStackTrace()[0], false, "◆END");
	}

	private DraggableNode getDragNode(String nodeId) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		for (DraggableNode d : this.draggableNodes) {
			if (d.node.getNodeId().equals(nodeId)) {
				return d;
			}
		}
		return null;
	}

	private DraggableNode createDragNode(EntityNode node) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		DraggableNode dragnode = generateDragNode(graphAct, node);
		// 2021/2 AW REP start
		Queue<Double> coords = graphAct.getNodeCoords(node.getNodeId());
		double x = coords.poll();
		double y = coords.poll();
		//String coords = graphAct.getNodeCoords(node.getNodeId());
 		//String[] coordsArray = coords.split(",");
		//double x = Double.parseDouble(coordsArray[0]);
		//double y = Double.parseDouble(coordsArray[1]);
		// 2021/2 AW REP end
		double h = 0;
		// 2021/2 AW DEL start
		//if (coordsArray.length > 2) {
		//	h = Double.parseDouble(coordsArray[2]);
		//}
		// 2021/2 AW DEL end

		// 2020/11 AW REP start

		// 相対座標を絶対座標に変換
		Point2D absoluteCoordinate = graphCoordinate.getAbsoluteCoordinate(new Point2D(x, y));
		dragnode.setLayoutX(absoluteCoordinate.getX());
		dragnode.setLayoutY(absoluteCoordinate.getY());

		//dragnode.setLayoutX(x);
		//dragnode.setLayoutY(y);

		// 2020/11 AW REP end

		dragnode.setPrefHeight(h + 10);

		// 書き戻す必要なし
		//String newCoords = String.valueOf(x) + "," + String.valueOf(y) + "," + String.valueOf(h);
		//graphAct.changeCoords(node.getNodeId(), newCoords);

		// 2020/11 AW DEL start
		//if (maxY < y) {
		//	maxY = y;
		//}
		// 2020/11 AW DEL end

		// テキスト取得
		// Map<String, com.assemblogue.plr.lib.Node> node_properties =
		// plrAct.listToMap(node);
		// String cnt = getDisplayContents(node, node_properties);

		// 2021/3 AW DEL start
		//String cnt = graphAct.getNodeText(node.getNodeId());

		//if (cnt == "" || cnt == null) {
		//	dragnode.setDefaultText("enter the text here");
		//} else {
		//	dragnode.setDisplayText(cnt, h);
		//}
		// 2021/3 AW DEL end
		//logger.trace("◆END");
		return dragnode;
	}

	/*
	 * private DraggableNode createHyperDragNode(GraphActor actor, EntityNode node)
	 * { DraggableNode dragnode = generateDragNode(actor, node); String coords =
	 * actor.getNodeCoords(node.getNodeId()); String[] coordsArray =
	 * coords.split(","); double x = Double.parseDouble(coordsArray[0]); double y =
	 * Double.parseDouble(coordsArray[1] + maxY); double h = 0; if
	 * (coordsArray.length > 2) { h = Double.parseDouble(coordsArray[2]); }
	 *
	 * dragnode.setLayoutX(x); dragnode.setLayoutY(y); dragnode.setPrefHeight(h +
	 * 10); if (maxHyperY < y) { maxHyperY = y; } Map<String,
	 * com.assemblogue.plr.lib.Node> node_properties = plrAct.listToMap(node);
	 * String cnt = getDisplayContents(node, node_properties);
	 *
	 * if (cnt == "" || cnt == null) {
	 * dragnode.setDefaultText("enter the text here"); } else {
	 * dragnode.setDisplayText(cnt, h); } return dragnode; }
	 */

	// 2020/11 AW DEL start
	// used to set the graph
	/*	private void calculateGraphCoords() {
		//logger.trace("◆START");

		double midX = 0, midY = 0;
		double graphH = 0, graphW = 0;
		double minXNodeY = 0, maxXNodeY = 0, maxxY = 0, minYNodeX, maxYnodeX, maxYnodeH = 0;
		// double maxxY = 0, maxYnodeH = 0;

		// for (EntityNode node1 : graphAct.getEntryNodes()) {
		// if (graphAct.isVisibleRoot(node1)) {
		for (EntityNode node1 : graphAct.getEntityNodeList()) {
			if (graphAct.isPropertyVisibleRoot(node1)) {
				String coords = graphAct.getNodeCoords(node1.getNodeId());
				String[] coordsArray = coords.split(",");
				double x = Double.parseDouble(coordsArray[0]);
				double y = Double.parseDouble(coordsArray[1]);
				double h = 0;
				if (coordsArray.length > 2) {
					h = Double.parseDouble(coordsArray[2]);
				}
				if (minX > x || minX == 0) {
					minX = x;
					minXNodeY = y;

				}

				if (x > maxX) {

					maxX = x;
					maxXNodeY = y;
				}

				if (minY > y || minY == 0) {

					minY = y;
					minYNodeX = x;
				}
				if (maxxY < y) {

					maxxY = y;
					maxYnodeX = x;
					maxYnodeH = h;
				}
			}
		}
		maxxY = maxxY + maxYnodeH;
		maxX = maxX + 135;
		graphH = maxxY - minY;
		graphW = maxX - minX;
		if (graphH < Utils.getWindowHeight()) {
			graphH = Utils.getWindowHeight();
		}
		if (graphW < Utils.getWindowWidth()) {
			graphW = Utils.getWindowWidth();
		}

		midY = (graphH * 1.5) - (Utils.getWindowHeight() / 2);
		midX = (graphW * 1.5) - (Utils.getWindowWidth() / 2);
		offsetX = (midX - minX) / 2;
		offsetY = (midY - minY) / 2;
		if (offsetX < 0) {
			offsetX = 0;
		}
		if (offsetY < 0) {
			offsetY = 0;
		}

	}
	*/
	// 2020/11 AW DEL end

	// read initial information with new function

	/*
	 * private void createNodeLinks() { ObservableList<Node> nodes =
	 * right_pane.getChildren(); ArrayList nodeList = new ArrayList(); for (Node
	 * node : nodes) { nodeList.add((DraggableNode) node); }
	 *
	 * DraggableNode source = (DraggableNode) nodeList.get(0); DraggableNode target
	 * = (DraggableNode) nodeList.get(1); }
	 */

	// 2021/2 AW ADD start
	public void drawNodeLink(DraggableNode source, DraggableNode target, String relation) {
		drawNodeLink(source,target, relation,false);
	}
	// 2021/2 AW ADD end

	// 2021/2 AW REP start
	public void drawNodeLink(DraggableNode source, DraggableNode target, String relation,boolean newLink) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		// 2021/2 AW DEL start
		//OntologyItem ontItem = graphAct.getOss().ontology;
		// 2021/2 AW DEL end

		// 2021/07 AW start リンク修正
		// relationから、表示値とタイプを取得
		PLRContentsData linkContentsData = graphAct.getGraphData().getLinkContentsData();
		OntologyItem linkOntologyItem = null;
		if (!relation.startsWith("#")) {
			linkOntologyItem = linkContentsData.getNode('#' + relation);
		} else {
			linkOntologyItem = linkContentsData.getNode(relation);
		}

		String linkType = "";
		String linkValue = "";

		// OntologyItemがnullの場合、保存値がLabel値の場合がある
		// →以前はバグでtypeではなく値を保存していたため
		// その対応として、値からも検索する
		if (linkOntologyItem == null) {
			String[] relation_values = relation.split("-");
			linkValue = relation_values[0];
			if ("uncertain".equals(linkValue)) {
				linkValue = "?";
			}
			linkType = "";
			if (relation_values.length > 1) {
				linkType = relation_values[1];
			}
		} else {
			linkType = linkOntologyItem.type;
			linkValue = OssActor.getDisplayText(linkOntologyItem);
		}
		// 2021/07 AW end リンク修正

		// 2021/2 AW ADD start
		boolean containsLink = graphAct.addEdgeJudge(source.node, target.node);
		//boolean notGeneratedNewLink = graphAct.addEdge(source.node, target.node, relation);

		if(!(containsLink) && newLink) {
			graphAct.addEdge(source.node, target.node, relation);
		}

		if((containsLink && !(newLink)) || (!(containsLink) && newLink)) {

			// 2021/07 AW start リンク修正
			/*
			// 2021/2 AW ADD end
			String[] relation_values = relation.split("-");
			String rel_val = relation_values[0];
			if ("uncertain".equals(rel_val)) {
				rel_val = "?";
			}
			String rel_type = "";
			if (relation_values.length > 1) {
				rel_type = relation_values[1];
			}
			*/
			String rel_val = linkValue;
			String rel_type = linkType;
			// 2021/07 AW end リンク修正

			String sourceClassType = source.node.getType().get(0);
			String targetClassType = target.node.getType().get(0);

			NodeLink link = new NodeLink(graphAct, rel_val, this, rel_type, sourceClassType, targetClassType);
			right_pane.getChildren().add(link);
			link.bindEnds(source, target, new Point2D((source.getLayoutX() + source.getPrefWidth()),
				(source.getLayoutY() + (source.getPrefHeight() / 2))), link);

			if (rel_type.equals(AppProperty.TYPE_SYMMETRICK)) {
				link.arrowright.setVisible(false);
				link.circle1.setVisible(true);
			} else {
				link.arrowright.setVisible(true);
				link.circle1.setVisible(false);
			}
			// 2021/2 AW ADD start
		}
	// 2021/2 AW ADD end
	// 2021/2 AW DEL start
	//graphAct.addEdge(source.node, target.node, relation, ontItem);
	// 2021/2 AW DEL end

		//logger.trace("◆END");
	}

	/**
	 * グラフに表示するノードを作成する (渡されたnodeをもとにグラフに実際に表示するものを作成する）
	 *
	 * @param graphAct ノードを追加するグラフのGraphActor
	 * @param node     グラフに表示したいEntityNode
	 * @return dragnode 表示するノード
	 */
	public DraggableNode generateDragNode(GraphActor graphAct, EntityNode node) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		sourceNode = null;

		DraggableNode dragnode = new DraggableNode(graphAct, node, false, this);
		dragnode.addEventFilter(MouseEvent.ANY, new EventHandler<MouseEvent>() {
			long startTime;

			// ノード始点計算用(初回クリック時のノード0,0からのマウスポインタの位置を保持）
			double mousePointX;
			double mousePointY;

			@Override
			public void handle(MouseEvent event) {
				TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");
				//logger.trace("◆START x,y={},{} Scenex,y={},{} Screenx,y={},{}", event.getX(), event.getY(),
						//event.getSceneX(), event.getSceneY(), event.getScreenX(), event.getScreenY());

				// グラフ画面のコンテキストメニューが表示されている場合は消去
				if (contextmenu_right_pane.isShowing()) {
					contextmenu_right_pane.hide();
				}

				if (event.getEventType().equals(MouseEvent.MOUSE_PRESSED) && event.getButton() != null
						&& event.getButton() == MouseButton.PRIMARY) {

					// ダブルクリック
					if (event.getClickCount() == 2) {

						String classType =  node.getType().get(0);

						if(classType.equals(mclassId)) {

							//メッセージクラスの処理
							Map<String, List<com.assemblogue.plr.lib.Node>> entityValuesMap = PlrActor.getProperties(node);// EntityNodeから入力済みメッセージを取得
							List<com.assemblogue.plr.lib.Node> entityValueList = entityValuesMap.get("cnt");
							String inputedMsg = "";
							if (!Objects.isNull(entityValueList)) {
								for (com.assemblogue.plr.lib.Node n: entityValueList) {
									if (n.isLiteral()) {
										inputedMsg =(String) n.asLiteral().getValue();
										break;
									}
								}
							}

							MessageDialog mdialog = new MessageDialog(mclassDisplayText,inputedMsg,false); //メッセージダイアログ

							if(mdialog.getMessageDialogResult()) {
								setMClassMessage(node,mdialog.getPropertyValue());	//メッセージをノードのcntプロパティに追加
							}
						} else {

							//メッセージクラス以外の処理
							PropertyDialog propertyDialog = new PropertyDialog(node, RootLayoutController.this);
						}

							// 再描画
							repaint(true);
							event.consume();
							//isRepainting = false;
						// ノードダブルクリック終了

					} else {

						// 描画中フラグ
						graphRepaintingFlag = false;

						// Select the target nodeボタン押下後に別のノードをクリックすると、矢印（ノードリンク）追加
						targetNode = (DraggableNode) event.getSource();
						if (sourceNode != null && targetNode != null && sourceNode.getId() != targetNode.getId()) {
							// 2021/2 AW REP start
							//新規作成のみ、第四引数、trueを追加
							drawNodeLink(sourceNode, targetNode, "uncertain",true);
							//drawNodeLink(sourceNode, targetNode, "uncertain");
							// 2021/2 AW REP end

							sourceNode = null;
							targetNode = null;

							//logger.debug("Link drawing.");

						} else {
							//logger.debug("no action..");
						}
						startTime = System.currentTimeMillis();

						mousePointX = event.getX();
						mousePointY = event.getY();
					}

				} else if (event.getEventType().equals(MouseEvent.MOUSE_DRAGGED) && event.getButton() != null
						&& event.getButton() == MouseButton.PRIMARY) {
					//logger.debug("Mouse primary drag, x,y={},{} Scenex,y={},{} Screenx,y={},{}", event.getX(),
							//event.getY(), event.getSceneX(), event.getSceneY(), event.getScreenX(), event.getScreenY());

					isRepainting = true;
					// マウスドラッグ中フラグ
					mouseDragFlag = true;
					// 描画中フラグ
					graphRepaintingFlag = true;

					// dragnode.relocateToPoint(new Point2dSerial(event.getSceneX(),
					// event.getSceneY()));
					dragnode.relocateToPoint(new Point2dSerial(event.getSceneX(), event.getSceneY()),
							new Point2dSerial(mousePointX, mousePointY));
					// getParent().setOnDragOver(null);
					// getParent().setOnDragDropped(null);
					// editingArea.deselect();
					// event.consume();
					isRepainting = false;

				} else if (event.getEventType().equals(MouseEvent.MOUSE_RELEASED) && event.getButton() != null
						&& event.getButton() == MouseButton.PRIMARY) {
					//logger.debug("Mouse primary release, x,y={},{} Scenex,y={},{} Screenx,y={},{}", event.getX(),
//							event.getY(), event.getSceneX(), event.getSceneY(), event.getScreenX(), event.getScreenY());

					isRepainting = true;
					if (mouseDragFlag) {
						// dragnode.setToPoint(new Point2dSerial(event.getSceneX(), event.getSceneY()));
						dragnode.setToPoint(new Point2dSerial(event.getSceneX(), event.getSceneY()),
								new Point2dSerial(mousePointX, mousePointY));
						// マウスドラッグ中フラグ
						mouseDragFlag = false;
						// 描画中フラグ
						graphRepaintingFlag = false;
						//logger.debug("mouse drag end.");
					} else {
						//logger.debug("no action..");
					}
					// getParent().setOnDragOver(null);
					// getParent().setOnDragDropped(null);
					// editingArea.deselect();
					// event.consume();
					isRepainting = false;

				} else if (event.getEventType().equals(MouseEvent.MOUSE_RELEASED) && event.getButton() != null
						&& event.getButton() == MouseButton.SECONDARY) {
					//logger.debug("Mouse secondary release, x,y={},{} Scenex,y={},{} Screenx,y={},{}", event.getX(),
							//event.getY(), event.getSceneX(), event.getSceneY(), event.getScreenX(), event.getScreenY());

					// ノード右クリック時の処理
					targetNode = null;
					contextmenu.show(dragnode, event.getScreenX(), event.getScreenY());
					//TextArea textArea = dragnode.getTextArea();

					/*
					Copy.setOnAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event4) {
							//logger.trace("◆START");

							textArea.selectAll();
							textArea.copy();
						}
					});
					Cut.setOnAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event5) {
							//logger.trace("◆START");

							textArea.selectAll();
							textArea.cut();
						}
					});
					Paste.setOnAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event6) {
							//logger.trace("◆START");

							textArea.paste();
						}
					});
					*/

					hypernode.setOnAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event7) {
							//logger.trace("◆START");

							// 既に開かれている場合は、最前部に表示
							String nodeId = node.getNodeId();
							if (appController.openStage.containsKey(nodeId)) {
								appController.openStage.get(nodeId).toFront();

							} else {

								// ハイパーノードグラフを開く
								appController.openHyperNodeGraph(node, graphAct.getGraphData().getGraphNode(), graphAct.getChannel());
							}
						}
					});

					// ↓Select the target nodeボタン押下時
					toLink.setOnAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event2) {
							//logger.trace("◆START");

							// 描画中フラグ
							graphRepaintingFlag = true;

							sourceNode = (DraggableNode) event.getSource();
							if (sourceNode != null && targetNode != null && sourceNode.getId() != targetNode.getId()) {
								// drawNodeLink(sourceNode, targetNode, "");
								sourceNode = null;
								targetNode = null;
							}
						}
					});
					deleteNode.setOnAction(new EventHandler<ActionEvent>() {
						public void handle(ActionEvent event2) {
							//logger.trace("◆START");

							Alert alert = new Alert(AlertType.CONFIRMATION);
							alert.setTitle("Delete Node");
							alert.setContentText("Do you want to delete the node?");
							alert.showingProperty().addListener(((observable, oldValue, newValue) -> {
								//logger.debug("DeleteLink showingProperty old={}, new={}", oldValue, newValue);

								if (oldValue == true && newValue == false) {
									// ダイアログを閉じた時

									// 描画中フラグ
									graphRepaintingFlag = false;

								} else if (oldValue == false && newValue == true) {
									// ダイアログを開いたとき

									// 描画中フラグ
									graphRepaintingFlag = true;
								}
							}));
							Optional<ButtonType> result = alert.showAndWait();

							if (!ButtonType.CANCEL.equals(result.get())) {
								DraggableNode nodeToDelete = (DraggableNode) event.getSource();
								if (nodeToDelete != null) {
									nodeToDelete.dragNodeDelete();

									// 2020/11 AW ADD start
									// 削除対象検索
									String delNodeId = nodeToDelete.node.getNodeId();
									int idx = 0;
									boolean delFlag = false;
									for (DraggableNode draggableNode: draggableNodes) {
										String nodeId = draggableNode.node.getNodeId();
										if (nodeId.equals(delNodeId)) {
											delFlag = true;
											break;
										}
										idx++;
									}
									if (delFlag) {
										 draggableNodes.remove(idx);
									}

									// グラフ座標再計算
									graphCoordinate.calcGraphCoordinate(draggableNodes, right_pane, scene);
									// 再描画判定
									if (graphCoordinate.isRepaintFlag()) {
										// 再描画
										repaint(false);
									}
									// 2020/11 AW ADD start
								}
							}
						}
					});

					// アイテムダイアログ（プロパティダイアログ）
					itemDialogue.setOnAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent e) {

							PropertyDialog propertyDialog = new PropertyDialog(node, RootLayoutController.this);

							// 再描画
							repaint(false);
						}
					});

					// デバッグダイアログ
					infoDialogue.setOnAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent e) {
							//logger.debug("◆START");

							ItemDialog itemDialog = new ItemDialog(node, stage, RootLayoutController.this);
							itemDialog.showAndWait();
						}
					});

					copynode.setOnAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event) {
							copyNodes(dragnode);
						}
					});

					duplicatenode.setOnAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event) {
							copyNodes(dragnode);
						}
					});
				}
			}
		});

		//logger.trace("◆END");
		return dragnode;

	}

// copy and paste of node function

	public void copyNodes(DraggableNode node) {
 		appController.copyNodes(node);
	}

	public void paste_nodes(GraphActor actor) {
		Map<String, String> idMap = new HashMap<String, String>();
		for (DraggableNode node : clipboard_nodes) {
			// 2021/2 AW REP start
			Queue<Double> coords = graphAct.getNodeCoords(node.getId());
			//String coords = graphAct.getNodeCoords(node.getId());
			// 2021/2 AW REP end
			// 2021/2 AW ADD start
			double x = coords.poll();
			double y = coords.poll();

			EntityNode entity_node = actor.pasteNode(x, y, node.node);
			//EntityNode entity_node = actor.addNode(x, y, node.node.getType().get(0));
			//EntityNode entity_node = actor.addNode(x, y);
			//EntityNode entity_node = actor.addNode("#Entity", coords);

			DraggableNode dragnode = generateDragNode(actor, entity_node);
			//dragnode.setDefaultText(node.getTextArea().getPromptText());
			dragnode.setLayoutX(node.getLayoutX());
			dragnode.setLayoutY(node.getLayoutY());
			right_pane.getChildren().add(dragnode);
			draggableNodes.add(dragnode);
			//TextArea textArea = dragnode.getTextArea();
			//textArea.setContextMenu(contextmenu);
			idMap.put(node.getId(), dragnode.getId());
		}
		for (LinkInfo e : clipboard_edges) {
			DraggableNode source = null;
			DraggableNode target = null;
			// 2021/2 AW REP start
			source = getDragNode(idMap.get(e.getSourceNodeId()));
			//source = getDragNode(idMap.get(e.getParent_node().getNodeId()));
			// 2021/2 AW REP end
			if (e.getTargetNodeId() == null) {
				target = null;
			} else {
				// 2021/2 AW REP start
				target = getDragNode(e.getTargetNodeId());
				//target = getDragNode(e.getChild_node().getNodeId());
				// 2021/2 AW REP end
			}
			if ((source != null) && (target != null)) {
				drawNodeLink(source, target, e.getLinkType());
			}
		}
	}

	/**
	 * 一時的にタイトルを変更する
	 * @param title
	 */
	public void changeTitle(String title) {
		stage.setTitle(title);
	}

	/**
	 * 一時的に変えたタイトルを元に戻す
	 */
	public void undoTitle() {
		stage.setTitle(thisTitle);
	}

	//--------------------------------★★★　ADD [フェーズ2] ここから↓
	/*
	* プロパティダイアログ表示
	* 		ダイアログOKで、ノードを作成する
	* @param classType ：クラスタイプ
	* @param event：MouseEvent
	*/
	private void execCallPropertyDialog(String classType ,MouseEvent event) {
			isRepainting = true;

			//存在するクラスのプロパティダイアログ表示
			PropertyDialog propertyDialog = new PropertyDialog(RootLayoutController.this,classType,plrAct,event.getX(),event.getY());
			// 再描画
			repaint(true);
			event.consume();
			isRepainting = false;
	}

		/*
		 * この画面に登録してあるClassの取得
		 * isExitMClassフラグの更新
		 */
		private List<OntologyItem> getRegistedClassList(){

			List<OntologyItem> tmpoiList = new ArrayList<OntologyItem>();
			List<OntologyItem> retoiList = new ArrayList<OntologyItem>();		//Return用

			List<OntologyItem> topList = contentsData.topClasses();
			for (OntologyItem i: topList) {
				List<OntologyItem> tempList = contentsData.getMenuItem(i);
	    		// メニューアイテムがない場合は、設定しない
	    		if (!tempList.isEmpty()) {
	    			tmpoiList.addAll(tempList);
	    		}
			}
			int listsize = tmpoiList.size() -1;
			for(int j = listsize ; j >=0; j--) {
				OntologyItem oi = tmpoiList.get(j);
	    		if (ignoreIdList.contains(oi.id)) {
	    			tmpoiList.remove(j);
	    		}
			}
    		for(OntologyItem oi : tmpoiList) {
    				List<OntologyItem> childList = oi.superClassOf;
    		    	if (childList != null) {
    		    		retoiList.addAll(getRegistedChildClassList(childList));
    		    	}
    		}
    		for(OntologyItem oi : tmpoiList) {
    			List<OntologyItem> childList = oi.superClassOf;
    			if (oi.isInputtable() && Objects.isNull(childList) && oi.isClass()) {
    				retoiList.add(oi);
    			}
    		}

    		//Mクラス有無フラグの更新
    		this.isExitMClass = false;
			for(OntologyItem oi : retoiList) {
				if(oi.id.equals("#" + mclassId)) {
					this.isExitMClass = true;
					break;
				}
			}
    		return  retoiList;

		}

		/*
		 * 子OntlogyItem取得（再起処理）
		 * @param list:子リスト
		 */
		private List<OntologyItem> getRegistedChildClassList(List<OntologyItem> list){

			List<OntologyItem> retoiList = new ArrayList<OntologyItem>();

    		for (OntologyItem oi : list) {
        		if (oi.isInputtable()) {
        			retoiList.add(oi);
        		}
    			if (!oi.isRoot()) {

    				List<OntologyItem> childList = oi.superClassOf;
    		    	if (childList != null) {
    		    		//List<OntologyItem> tmpList = getRegistedChildClassList(childList);
    		    		retoiList.addAll(getRegistedChildClassList(childList));
    		    	}
    			}
    		}
    		return retoiList;
		}



		/*
		 * MessageDialog でOK終了時の処理（メッセージをMクラスのcntプロパティに追加）
		 * @param node : 対象ノード
		 * @param messageValue : MessageDialogで入力したメッセージ
		 */
		private void setMClassMessage(EntityNode node,String messageValue) {
			Map<String, List<Object>> setPropertyMap = new HashMap<>();
			setPropertyMap.put("cnt", new ArrayList<Object>());
			List<Object> propertyValue  = new ArrayList<Object>();

            propertyValue.add(messageValue);
			setPropertyMap.put("cnt", propertyValue);
			graphAct.addPropertiesByDialog(node, setPropertyMap);
		}

		/*
		 * PLRContentsDataの更新処理
		 * 		Graph Setting で クラスの変更時に呼び出される
		 */
		private void updateContentsData() {
			contentsData = graphAct.getGraphData().getContentsData();

			boolean isCancreateMessage = false;
			boolean isCancreatOtherNode = false;

			List<OntologyItem> oilist = getRegistedClassList();
			if (oilist.size() > 0) {
				if (this.isExitMClass) {
					isCancreateMessage = true;
					if (oilist.size() > 1) {
						isCancreatOtherNode = true;
					}
				} else {
					isCancreatOtherNode = true;
				}
			}
			createMessageMenuItem.setDisable(!isCancreateMessage);
			createOtherNodeMenuItem.setDisable(!isCancreatOtherNode);

		}

	//----------------------------------★★★　ここまで　↑


}

/*
 * Sceneリサイズ終了検出用タイマークラス
 */
class SceneResizeTimerTask extends TimerTask {

	RootLayoutController rootlayoutcontroller;
	//サイズ変更有無の検出用Sceneサイズ(width)
	double oldsceneWidth = -1;
	//サイズ変更有無の検出用Sceneサイズ(height)
	double oldsceneHeight = -1;

	public SceneResizeTimerTask(RootLayoutController rootlayoutcontroller) {
		this.rootlayoutcontroller = rootlayoutcontroller;
	}

    @Override
    public void run() {
    		if (rootlayoutcontroller.getScene().getWidth() == oldsceneWidth && rootlayoutcontroller.getScene().getHeight() == oldsceneHeight) {
    			//サイズ変更がなければリサイズ終了とする
				Platform.runLater( () ->  {
					//呼び出し元のリサイズ終了メソッド呼び出し
					this.rootlayoutcontroller.sceneResizeEnd();
				});

    		} else {
    			//サイズ変更があればリサイズ途中なのでサイズの保存
    			oldsceneWidth = rootlayoutcontroller.getScene().getWidth() ;
    			oldsceneHeight = rootlayoutcontroller.getScene().getHeight();
    		}
    }
}
