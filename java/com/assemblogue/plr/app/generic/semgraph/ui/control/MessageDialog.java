package com.assemblogue.plr.app.generic.semgraph.ui.control;


import java.util.Objects;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *プロパティ編集ダイアログ
 */
public class MessageDialog {

	/** ダイアログのStage */
	private Stage stage = new Stage();

    /** コンストラクタ引数 **/
    private String propertyValue;
    private boolean isOtherNode;

    /** 戻り値 **/
	private boolean result;							//ダイアログ結果（OK/キャンセル）
	private boolean otherNodeclicked = false;		//「other node type」ボタン押下時にtrue

    /**
     * コンストラクタ
     */
    public MessageDialog() {
        this("","",false);
    }

    /**
     * コンストラクタ
     * @param propetyName プロパティ名
     * @param properyValue プロパティ値
     * @param isOtherNode 「other node type」ボタンの有無
     */
    public MessageDialog(String propetyName,String propertyValue,boolean isOtherNode) {

    	this.result = false;

       	this.propertyValue =	Objects.isNull(propertyValue) ? "" :propertyValue;
        this.isOtherNode =isOtherNode;

        stage.setScene(makSceane());

        stage.setTitle(Objects.isNull(propetyName) ? "" : propetyName);

        stage.initModality(Modality.APPLICATION_MODAL);
        stage.showAndWait();
    }


    /**************************************************************************
     *
     * Public API
     *
     **************************************************************************/
    // プロパティ値
    public final String getPropertyValue() {
        return propertyValue;
    }

    // ダイアログ結果（OK/キャンセル）
    public  boolean getMessageDialogResult() {
        return result;
    }

    //「other node type」ボタン押下
    public boolean getOtherNodeClicked() {
    	return otherNodeclicked;
    }

    //  showAndWaitメソッド
    //public void  showAndWait() {
    //	stage.showAndWait();
    //}


    /**************************************************************************
    *
    * Private Implementation
    *
    **************************************************************************/
    /*-----------------------------------*
     * 画面 コンテンツ構築（実体）
     *-----------------------------------*/
    private Scene makSceane () {

       	TextArea textInput = new TextArea(propertyValue);
        textInput.setMaxWidth(Double.MAX_VALUE);
        textInput.setPrefWidth(200);
        textInput.setPrefHeight(50);

        textInput.requestFocus();


        // エラーメッセージ用ラベル
        Button btnOtherClass = new Button("other node type");
        btnOtherClass.setVisible(this.isOtherNode);
        if(this.isOtherNode) {
        	btnOtherClass.setOnMouseClicked(event -> btnOtherClassEvent());
        }
        // OKボタン
    	Button btnOK = new Button("OK");
    	btnOK.setPrefSize(80, 20);

    	// キャンセルボタン
    	Button btnCancel = new Button("Cancel");
    	btnCancel.setPrefSize(80, 20);

        btnOK.setOnMouseClicked(event -> btnOKEvent(textInput));
        btnCancel.setOnMouseClicked(event -> btnCancelEvent());


        //ボタンのレイアウト調整用にHBoxの作成
    	HBox hboxBtn = new HBox();
    	hboxBtn.setPadding(new Insets(15,12, 15, 12));
   	 	hboxBtn.setSpacing(10);
	   	hboxBtn.getChildren().addAll(btnCancel,btnOK);
	   	hboxBtn.setAlignment(Pos.CENTER_RIGHT);

	   	//ルートVBox
        VBox vBoxRoot = new VBox();
        vBoxRoot.setAlignment(Pos.CENTER_LEFT);
        vBoxRoot.setPadding(new Insets(20, 10, 10, 10));
        vBoxRoot.setSpacing(5);
        vBoxRoot.getChildren().addAll(textInput,btnOtherClass, hboxBtn);

        return new Scene(vBoxRoot,300,160);
    }

    /*------------------------------*
     * OKボタン クリックイベント
     *------------------------------*/
    private void btnOKEvent(TextArea textInput) {
    	String value = textInput.getText();
    	propertyValue = value;
		result = true;
		stage.close();
    }

    /*-----------------------------------*
     * キャンセルボタン クリックイベント
     *-----------------------------------*/
    private void btnCancelEvent() {
    		result = false;
    		stage.close();
    }

    /*-------------------------------------------*
     * other node typeボタン クリックイベント
     *-------------------------------------------*/
    private void btnOtherClassEvent() {
    	otherNodeclicked=true;
    	result = false;		//(念のため）
    	stage.close();
    }
}
