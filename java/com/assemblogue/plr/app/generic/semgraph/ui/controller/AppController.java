package com.assemblogue.plr.app.generic.semgraph.ui.controller;

import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import com.assemblogue.plr.app.generic.semgraph.App;
import com.assemblogue.plr.app.generic.semgraph.AppProperty;
import com.assemblogue.plr.app.generic.semgraph.GraphActor;
import com.assemblogue.plr.app.generic.semgraph.GraphManager;
import com.assemblogue.plr.app.generic.semgraph.Messages;
import com.assemblogue.plr.app.generic.semgraph.OssTask;
import com.assemblogue.plr.app.generic.semgraph.PlrActor;
import com.assemblogue.plr.app.generic.semgraph.SyncTask;
import com.assemblogue.plr.app.generic.semgraph.TinyLog;
import com.assemblogue.plr.app.generic.semgraph.TxtList;
import com.assemblogue.plr.app.generic.semgraph.Utils;
import com.assemblogue.plr.app.generic.semgraph.ui.DraggableNode;
import com.assemblogue.plr.app.generic.semgraph.ui.control.AlertDialog;
import com.assemblogue.plr.io.PassphrasePoster;
import com.assemblogue.plr.io.Storage;
import com.assemblogue.plr.io.StorageInfo;
import com.assemblogue.plr.lib.EntityNode;
import com.assemblogue.plr.lib.FileNode;
import com.assemblogue.plr.lib.NodeNotFoundException;
import com.assemblogue.plr.lib.PLR;
import com.assemblogue.plr.lib.model.Channel;
import com.assemblogue.plr.lib.model.DisclosedToUser;
import com.assemblogue.plr.lib.model.Friend;
import com.assemblogue.plr.lib.model.Root;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.Image;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * アプリケーションのコントローラクラスです。
 *
 * @author <a href="mailto:m.ikemoto@runzan.co.jp">IKEMOTO, Masahiro</a>
 * @modified <a href="mailto:kaneko@cri-mw.co.jp">KANEKO, yukinori</a>
 * @see Initializable
 */

public class AppController implements Initializable {

	//private Logger logger = LogManager.getLogger();

	// JavaFx
	@FXML
	private MenuItem menuItemRefresh;
	@FXML
	private MenuItem menuItemAcknowlegements;
	@FXML
	private MenuItem menuItemCloudSetting;
	@FXML
	private MenuItem menuItemSwitchAccount;
	@FXML
	private MenuItem menuItemGraphCreate;
	@FXML
	private MenuItem menuItemGraphOpen;
	// for channel list AW 2020/06/15 START
	@FXML
	private MenuItem menuItemChannelCreate;
	 @FXML
	 private MenuItem menuItemReadme;
	@FXML
	private Button graphApply;
	@FXML
	private Button graphCancel;
	@FXML
	private BorderPane bPane;
	private TreeItem<TreeItemData> rootNode;

	// FriendList AW 2020/08/01 Update Start
	// private List<Channel> cnlfRoot;
	private List<Channel> cnlfRoot = new ArrayList<Channel>();
	// FriendList AW 2020/08/01 Update End

	// for friend channels
	private boolean endMyCnlTree = false;
	// for channel list AW 2020/06/15 END

	// FriendList AW 2020/08/01 Add Start
	private boolean endFriCnl = false;
	// FriendList AW 2020/08/01 Add End

	private List<DraggableNode> copyNodes = new ArrayList<DraggableNode>();

	@FXML
	private MenuItem menuItemJa;
	@FXML
	private MenuItem menuItemEn;
	@FXML
	private MenuItem menuItemZh;

	@FXML
	VBox vbox;
	@FXML
	VBox centerBox;

	/** AppControllerのStage */
	private Stage stage;

	public Stage getStage() {
		return stage;
	}
	// app.javaから呼ばれる
	public void setStage(Stage stage) {
		this.stage = stage;

		/*
		stage.showingProperty().addListener(((observable, oldValue, newValue) -> {
			if (oldValue && !newValue) {
				if (og_ctrlr != null) {
					og_ctrlr.close();
				}
				if (sn_stage != null) {
					sn_stage.close();
				}
			}
		}));
		*/
	}

	// PLR
	private static PLR plr;
	private static Storage storage = null;
	// PLR Lapper
	public static PlrActor plrAct = new PlrActor();

	private List<Stage> stageList = new ArrayList<Stage>();
	public HashMap<String, Stage> openStage = new HashMap<String, Stage>();

	/** グラフ画面の管理マップ(キー：グラフノードid） */
	private ConcurrentHashMap<String, RootLayoutController> graphMap = new ConcurrentHashMap<>();
	public Map<String, RootLayoutController> getGraphMap() {
		return graphMap;
	}
	/** グラフマップロック */
	private boolean graphMapLock = false;

	public boolean isGraphMapLock() {
		return graphMapLock;
	}
	public void setGraphMapLock(boolean graphMapLock) {
		this.graphMapLock = graphMapLock;
	}

	/** 同期タスク（スレッド） */
	public SyncTask syncTask;

	/** ossタスク（スレッド） */
	public OssTask ossTask;

	/** ossスレッド起動フラグ */
	private boolean ossThreadFlag = false;
	public boolean isOssThreadFlag() {
		return ossThreadFlag;
	}
	public void setOssThreadFlag(boolean ossThreadFlag) {
		this.ossThreadFlag = ossThreadFlag;
	}

	/** 取得したossリスト */
	private CopyOnWriteArrayList<FileNode> ossList =  new CopyOnWriteArrayList<>();
	public CopyOnWriteArrayList<FileNode> getOssList() {
		return ossList;
	}
	public void setOssList(CopyOnWriteArrayList<FileNode> ossList) {
		this.ossList = ossList;
	}

	/** ossに対応するJsonデータマップ */
	private ConcurrentHashMap<String, String> ossMap = new ConcurrentHashMap<>();
	public ConcurrentHashMap<String, String> getOssMap() {
		return ossMap;
	}
	public void setOssMap(ConcurrentHashMap<String, String> ossMap) {
		this.ossMap = ossMap;
	}

	/** タイトル保存用 */
	private String thisTitle;

	/**
	 * initialize はじめに呼ばれる
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		GraphManager.setAppController(this);
		initializeUI();
		initializePLR();

	}

	/**
	 * UI初期化
	 * メニューのアクション設定
	 */
	private void initializeUI() {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		menuItemCloudSetting.setOnAction(mouseEvent -> exec_cloudSetting());
		// for channel list AW 2020/06/15 START
		// menuItemGraphCreate.setOnAction(mouseEvent -> exec_createGraph());
		// menuItemGraphOpen.setOnAction(mouseEvent -> exec_openGraph(true));
		menuItemAcknowlegements.setOnAction(mouseEvent -> exec_acknowlegements());
		menuItemChannelCreate.setOnAction(mouseEvent -> exec_createChannel());
		 menuItemReadme.setOnAction(mouseEvent -> execShowReadme());
		menuItemSwitchAccount.setOnAction(mouseEvent -> exec_SwitchAccountSetting());
		/*
		 * TextFlow flow = new TextFlow(); String content =
		 * " \n \n You can create a graph by choosing New Graph from Graph Menu \n パスフレーズによる認証後、GraphからNew Graphを選ぶと新しいグラフを作れます"
		 * ; Text t1 = new Text(); t1.setStyle("-fx-fill: BLACK;-fx-font-weight:bold;");
		 * t1.setText(content); Text t2 = new Text(); t2.setText("\n \n ReadMe");
		 * t2.setStyle("-fx-fill:BLACK;-fx-font-weight:bold;-fx-underline:true;"); Text
		 * t3 = new
		 * Text("\n\n Open Graph lets you open existing graphs including your PLR friend's ones.\n"
		 * + "Open Graphで既存のグラフを開くことができます。ここでPLRのフレンドになっている他者のグラフも開くことができます。");
		 * t3.setStyle("-fx-fill: BLACK;-fx-font-weight:bold;");
		 *
		 * flow.getChildren().addAll(t2,t1,t3); centerBox.getChildren().add(flow);
		 */
		// for channel list AW 2020/06/15 END

		// �X�g���[�W�Z�b�g�A�b�v�����b�Z�[�W
		TxtList.set(Messages.getString("system.plr.setup"));

	}

	/**
	 * PLR取得初期化
	 */
	private void initializePLR() {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		PLR.createBuilder().build(new PLR.BuilderCallback() {
			@Override
			public void onReady(PLR plr) {
				TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

				AppController.plr = plr;
				plrAct.setPlr(plr);
				initializeStorage();
			}

			@Override
			public void onError(Exception e) {
				TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

				Platform.runLater(() -> Utils.createErrorDialog(stage, e).show());
			}
		});
	}


	/**
	 * PLR解放
	 */
	public void destroyPLR() {
		if (plr != null) {
			// plr.destroy();
		}
	}


	/**
	 * ストレージ初期化
	 */
	private void initializeStorage() {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		List<StorageInfo> storageList = getStorageList();
		if (storageList.isEmpty()) {
			return;
		}
		// 2021/2 AW ADD start
		//クラウドドライブ名格納
		plrAct.setStragetype(storageList.get(storageList.size() - 1).getTypeName());
		// 2021/2 AW ADD end

		plr.connectStorage(storageList.get(storageList.size() - 1), new StorageHandler());
	}

	private List<StorageInfo> getStorageList() {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		try {
			return plr.listStorages();
		} catch (Exception e) {
			//onError(e);
			Platform.runLater(() -> Utils.createErrorDialog(stage, e).show());
			return Collections.emptyList();
		}
	}

	/**
	 * ストレージ接続時
	 * 新規ストレージ作成時　コールバッククラス
	 */
	private class StorageHandler implements PLR.StorageConnectionCallback {
		/**
		 * when passphrase need
		 */
		@Override
		public void onKeyPairNotReady(PassphrasePoster passphrasePoster) {
			TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

			Platform.runLater(() -> {
				TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

				Dialog<String> dialog = new Dialog<>();
				dialog.initOwner(stage);
				dialog.setTitle(Messages.getString("dialog.passphrase.title"));
				dialog.setHeaderText(Messages.getString("dialog.passphrase.new.headerText"));
				dialog.initStyle(StageStyle.UTILITY);
				dialog.getDialogPane().getButtonTypes().addAll(ButtonType.APPLY, ButtonType.CANCEL);
				GridPane grid = new GridPane();
				grid.setHgap(10);
				grid.setVgap(10);
				grid.setPadding(new Insets(20, 150, 10, 10));
				TextField username = new TextField();
				username.setPromptText(plrAct.getUserID());
				PasswordField passphrase = new PasswordField();
				passphrase.setPrefColumnCount(32);

				passphrase.setPromptText(Messages.getString("dialog.passphrase.label"));
				PasswordField confirm = new PasswordField();

				confirm.setPrefColumnCount(32);
				confirm.setPromptText(Messages.getString("dialog.passphrase.confirm.label"));

				grid.add(new Label(Messages.getString("dialog.passphrase.label")), 0, 0);
				grid.add(passphrase, 1, 0);
				grid.add(new Label(Messages.getString("dialog.passphrase.confirm.label")), 0, 1);
				grid.add(confirm, 1, 1);

				Node applyButton = dialog.getDialogPane().lookupButton(ButtonType.APPLY);
				applyButton.setDisable(true);

				passphrase.textProperty().addListener((observable, oldValue, newValue) -> {
					applyButton.setDisable(newValue.isEmpty() || !newValue.equals(confirm.getText()));
				});

				confirm.textProperty().addListener((observable, oldValue, newValue) -> {
					applyButton.setDisable(newValue.isEmpty() || !newValue.equals(passphrase.getText()));
				});

				dialog.getDialogPane().setContent(grid);

				Platform.runLater(() -> passphrase.requestFocus());

				dialog.setResultConverter(dialogButton -> {
					if (dialogButton == ButtonType.APPLY) {
						return passphrase.getText();
					}
					return null;
				});

				Optional<String> result = dialog.showAndWait();
				if (result.isPresent()) {
					passphrasePoster.post(result.get());
				} else {
					passphrasePoster.cancel();
				}
				//logger.debug("●END StorageConnection.callback onKeyPairNotReady");
			});
		}

		@Override
		public void onConnect(Storage storage) {
			TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

			Platform.runLater(() -> {
				TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");
				//erase  cloud initial message.
				bPane.setCenter(null);
				try {
					setStorage(storage);
				} catch (Exception e) {
					e.printStackTrace();
				}
				//logger.debug("●END StorageConnection.callback onConnect");
			});
		}

		@Override
		public void onCancel() {
			TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");
			// Nothing to do.
		}

		@Override
		public void onError(Exception e) {
			TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");
			//AppController.this.onError(e);
			Platform.runLater(() -> Utils.createErrorDialog(stage, e).show());
		}
	}

	/**
	 * ストレージ設定
	 * @param storage
	 * @throws Exception
	 */
	private void setStorage(Storage storage) throws Exception {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		long startTime = System.nanoTime();

		stage.showingProperty();

		if ((AppController.storage = storage) == null) {
			return;
		}

		plrAct.setStorage(storage);

		//initial_sync();
		plrAct.prepare();
		plrAct.sync(null);
		this.stage.setTitle(
		Messages.getString("application.title") + " " + AppProperty.VERSION + "  - " + plrAct.getUserID());

		this.thisTitle = stage.getTitle();

		// ログイン者のプロフィール取得(キャッシュに取得）
		plrAct.getProfile();

		// 2021/07 AW start 確認用ログ取得
		TinyLog.debug(new Throwable().getStackTrace()[0], false, "setStorage内plrId=" + plrAct.getPlrId());
		TinyLog.debug(new Throwable().getStackTrace()[0], false, "setStorage内storageType=" + plrAct.getStoragetype());
		// 2021/07 AW end 確認用ログ取得

		// for channel list AW 2020/06/15 START
		exec_select(true);
		// for channel list AW 2020/06/15 END

		long endTime = System.nanoTime();
		long duration = (endTime - startTime);
		long time = duration / 1000000;
	}

	/**
	 * ストレージ取得
	 * @return
	 */
	public static Storage getStorage() {
		return storage;
	}

	/**
	 * 新規グラフを作成
	 * @param name グラフ名（チャネル名が入る）
	 */
	public void createGraph(String name, Channel c) {
		open_graph(name, null, stage, c);
	}

	/**
	 * 既存グラフを開く
	 * @param node プロパティ値にグラフ名を持つノード
	 */
	public void openGraph(EntityNode node,Channel c) {
		open_graph(null, node, stage, c);

	}

	/**
	 * 新規・既存ともにグラフを開く
	 * @param name グラフ名、nullの場合は既存グラフを開く
	 * @param node グラフルートノード（グラフ名を持つノード）、nullの場合は新規グラフを作成
	 * @param ownerStage 主画面のステージ
	 * @param channel
	 */
	private void open_graph(String name, EntityNode node, Stage ownerStage, Channel channel) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

 		if (plrAct.getAplRootFolderNode() == null) {
			return;
		}

 		GraphActor gact = new GraphActor(node, this, channel);
		if (gact.getGraphData().getGraphNode() == null) {
			TxtList.set("Graph " + name + " create Failed.");
			return;
		}

		// チャネルの開示ユーザのプロフィール取得
		List<DisclosedToUser> userList = channel.listDisclosedToUsers(true);
		for (DisclosedToUser disclosedToUser: userList) {
			plrAct.getProfile(disclosedToUser);
		}

		// グラフマップのロック取得
		setGraphMapLock(true);

		RootLayoutController rootLayoutController = new RootLayoutController(this, gact, gact.getOssActor());

		// グラフ管理マップに登録
		String graphNodeId = gact.getGraphData().getGraphNode().getNodeId();
		graphMap.put(graphNodeId, rootLayoutController);

		/*
		//↓で本来なら2重起動チェックが走るはずだが、効いていない（絶対false）
		if (GraphManager.isOpend(gact)) {
			GraphManager.toFront(gact); // �őO�ʂ�
			return;
		}
		*/

		if (ownerStage.isShowing()) {
			stage.show();
 		}

		rootLayoutController.createGraphArea(gact.getGraphData().getGraphName());

		stageList.add(rootLayoutController.getStage());
		openStage.put(gact.getGraphData().getGraphNode().getNodeId(), rootLayoutController.getStage());

		// ロック解除
		setGraphMapLock(false);
	}

	/**
	 * ハイパーノードを開く
	 * @param node
	 * @param parentNode
	 * @param channel
	 * @return
	 */
	public void openHyperNodeGraph(EntityNode node, EntityNode parentNode, Channel channel) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		if (plrAct.getAplRootFolderNode() == null) {
			return;
		}

		GraphActor gact = new GraphActor(node, parentNode, this, channel);
		if (gact.getGraphData().getGraphNode() == null) {
			//TxtList.set("Graph " + name + " create Failed.");
 			return;
		}

		// グラフマップのロック取得
		setGraphMapLock(true);

		RootLayoutController rootLayoutController = new RootLayoutController(this, gact, gact.getOssActor());

		// グラフ管理マップに登録
		String graphNodeId = gact.getGraphData().getGraphNode().getNodeId();
		graphMap.put(graphNodeId, rootLayoutController);

		/*if (GraphManager.isOpend(gact)) {
			GraphManager.toFront(gact); // �őO�ʂ�
			return null;
		}*/

		rootLayoutController.createGraphArea(gact.getGraphData().getGraphName());

		stageList.add(rootLayoutController.getStage());
		openStage.put(gact.getGraphData().getGraphNode().getNodeId(), rootLayoutController.getStage());

		// ロック解除
		setGraphMapLock(false);
	}


	/**
	 * This method is created to close all sub stages while switching the account
	 */
	private void closeChildStages() {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		Iterator itr = stageList.iterator();
		while (itr.hasNext()) {
			Stage stg = (Stage) itr.next();
			if (stg != null) {
				stg.close();
			}
		}
	}

	/**
	 * This method is used to switch account
	 */
	private void exec_SwitchAccountSetting() {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		ChoiceDialog dialogBox;

		List<StorageInfo> regAccounts;

		//2021.3 AW add start
		// oss関係をクリア
		if (!isOssThreadFlag()) {
			ossList.clear();
			ossMap.clear();
			ossTask = null;
		} else {
			AlertDialog alert = new AlertDialog(AlertType.WARNING, "Loading...", "Loading the cloud data. Please try again after a minute.");
			alert.showAndWait();
			return;
		}
		//2021.3 AW add end

		try {
			regAccounts = getStorageList();
		} catch (Exception e) {
			//onError(e);
			Platform.runLater(() -> Utils.createErrorDialog(stage, e).show());

			return;
		}
		// ((StorageInfo)(regAccounts.iterator().next())).getUserId()
		List<String> values = new ArrayList<>();

		Iterator iterator = regAccounts.iterator();
		while (iterator.hasNext()) {
			StorageInfo strInfo = (StorageInfo) iterator.next();
			values.add(strInfo.getUserId());
		}
		dialogBox = new ChoiceDialog("Select", values);
		dialogBox.initOwner(stage);
		dialogBox.setTitle(Messages.getString("dialog.SwitchAcc.title"));
		dialogBox.setHeaderText(Messages.getString("dialog.SwitchAcc.headerText"));
		dialogBox.setContentText(Messages.getString("dialog.SwitchAcc.contentText"));
		Optional selectedAcct = dialogBox.showAndWait();
		if (selectedAcct.isPresent() && !("Select".equals(selectedAcct.get()))) {
			Iterator iteratorAccount = regAccounts.iterator();
			StorageInfo selectedStorageInfo = null;
			while (iteratorAccount.hasNext()) {
				StorageInfo strInfo = (StorageInfo) iteratorAccount.next();
				if (selectedAcct.get().equals(strInfo.getUserId())) {
					selectedStorageInfo = strInfo;

				}
			}
			rootNode.getChildren().clear();
			closeChildStages();

			plr.connectStorage(selectedStorageInfo, new StorageHandler());
		}

	}

	/**
	 * cloud Setting
	 */
	private void exec_cloudSetting() {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		long startTime = System.nanoTime();
		ChoiceDialog<Storage.Type> dialog;
		{

			Collection<Storage.Type> typeList;
			try {
				typeList = plr.listStorageTypes();
			} catch (Exception e) {
				//onError(e);
				Platform.runLater(() -> Utils.createErrorDialog(stage, e).show());

				return;
			}
			dialog = new ChoiceDialog<>(typeList.iterator().next(), typeList);
			dialog.initOwner(stage);
			dialog.setTitle(Messages.getString("dialog.cloudType.title"));
			dialog.setHeaderText(Messages.getString("dialog.cloudType.headerText"));
			dialog.setContentText(Messages.getString("dialog.cloudType.contentText"));
		}
		Optional newAccount = dialog.showAndWait();
		if(newAccount.isPresent()) {
			//logger.debug("●NewStorage START");

			plr.newStorage((Storage.Type)newAccount.get(), new StorageHandler());
			rootNode.getChildren().clear();
			closeChildStages();

			Label initialLabel = new Label("Cloud Initializing...");
			bPane.setCenter(initialLabel);
		}

		//logger.trace("◆END");
	}



	/**
	 * app info.
	 */
	private void exec_acknowlegements() {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		Stage nwstg = new Stage();
		nwstg.initModality(Modality.APPLICATION_MODAL);
		nwstg.initOwner(this.stage);
		nwstg.setTitle(Messages.getString("menuItem.ackowlegements.text"));

		VBox vbox = new VBox();
		HBox hbox = new HBox();
		vbox.getChildren().add(hbox);

		Label label = new Label();
		label.setText(Messages.getString("ackowlegements.jackson"));
		hbox.getChildren().add(label);

		ScrollPane scl_pane = new ScrollPane();
		scl_pane.setContent(vbox);

		nwstg.setScene(new Scene(scl_pane));
		nwstg.getIcons().add(new Image(App.class.getResourceAsStream("ui/img/editor.jpg")));
		nwstg.show();
	}

	// for channel list AW 2020/06/15 START
	/**
	 * ReadMeを表示 Edited 24/09/2020 -Sarada
	 */
	private void execShowReadme() {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		Stage nwstg = new Stage();
		nwstg.initModality(Modality.APPLICATION_MODAL);
		nwstg.initOwner(this.stage);
		nwstg.initStyle(StageStyle.DECORATED);
		nwstg.setTitle(Messages.getString("menuItem.ackowlegements.text"));

		TextFlow flow = new TextFlow();
		flow.setStyle("-fx-background-color: #D9F9F1; -fx-background-radius: 5px;-fx-border-color: black;-fx-border-radius: 10 10 0 0;");
		flow.setPadding(new Insets(5, 5, 5, 5));
		String content = " \n \n You can open the root graph of a channel by clicking the channel in the main window.\n" +
				" \n主ウィンドウでチャネルをクリックするとそのチャネルのルートグラフが表示されます。";
		Text t1 = new Text();
		t1.setStyle("-fx-fill: BLACK;-fx-font-weight:bold;");
		t1.setText(content);
 		Text t2 = new Text();
		t2.setText("How to use SemanticEditor");
		t2.setStyle("-fx-fill:BLACK;-fx-font-weight:bold;-fx-underline:true;");
		Text t3 = new Text("\n\n You can create a new node by left double clicking on the background.\n" +
				"\n"+ "グラフのウィンドウの背景で左ダブルクリックするとノードが作れます。\n");
		t3.setStyle("-fx-fill: BLACK;-fx-font-weight:bold;");

		Text t4 = new Text("\n\n You can create a link from node A to node B by right clicking A, choosing Select the target node, and left clicking B.\n" +
				"\n"+ "ノードを右クリックして現われるメニューでSelect the target nodeを選んで別のノードを左クリックすると第1のノードから第2のノードへのリンクが作れます。\n");
		t4.setStyle("-fx-fill: BLACK;-fx-font-weight:bold;");
		Text t5 = new Text("\n\n You can set the link type (relation) by left clicking the link label.\n" +"\n"+ "リンクのラベルを左クリックして現われるメニューからリンクのタイプを選べます。\n");
		t5.setStyle("-fx-fill: BLACK;-fx-font-weight:bold;");
		Text t6 = new Text("\n\n You can delete or reverse a link by right clicking the link label.\n" +"\n"+ "リンクのラベルを右クリックするとリンクを削除または逆転できます。\n" + "");
		t6.setStyle("-fx-fill: BLACK;-fx-font-weight:bold;");
		flow.getChildren().addAll(t2, t1, t3,t4,t5,t6);
  		nwstg.setScene(new Scene(flow));
		nwstg.getIcons().add(new Image(App.class.getResourceAsStream("ui/img/editor.jpg")));
		nwstg.show();

	}

	/*
	public void createChannelGraph(String name, EntityNode cnlNode) {
		open_graph(name, cnlNode, stage);
	}
	*/

	/**
	 * チャネル作成ダイヤログの表示
	 */
	public void exec_createChannel() {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		//if (!syncGraphNode()) {
		//	return;
		//}

		plrAct.sync(null);
		plrAct.list(null, null);

		if (!plrAct.passphrase()) {
			return;
		}

		Stage nwstg = new Stage();
		nwstg.initModality(Modality.APPLICATION_MODAL);
		nwstg.initOwner(this.stage);
		nwstg.setTitle(Messages.getString("menuItem.channel.create") + "  -  " + plrAct.getUserID());
		nwstg.getIcons().add(new Image(App.class.getResourceAsStream("ui/img/editor.jpg")));

		FXMLLoader loader = new FXMLLoader(App.class.getResource("ui/view/CreateChannelView.fxml"),
				Messages.getResources());

		Parent root;
		try {
			root = loader.load();
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}

		CreateChannelController ctrl;
		ctrl = loader.getController();
		ctrl.setStage(nwstg);
		nwstg.initModality(Modality.APPLICATION_MODAL);
		//nwstg.initOwner(this.stage);
		ctrl.setOwnerStage(this.stage);
		ctrl.setAppController(this);
		Scene scene = new Scene(root, 512, 150);

		nwstg.setScene(scene);
		nwstg.show();
	}

	/**
	 * チャネル作成
	 *
	 * @param name channelName
	 * @param desc channelDescription
	 */
	public void createChannel(String name, String desc) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		plrAct.createChannel(name, desc);
		exec_select(true);
	}

	/**
	 * チャネルリストの表示
	 *
	 * @param syncflag
	 */
	private void exec_select(boolean syncflag) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		// FriendList AW 2020/08/01 Add Start
		cnlfRoot.clear();
		// FriendList AW 2020/08/01 Add End

		rootNode = new TreeItem<>(new TreeItemData(Messages.getString("graph.open.graph.list")));
		rootNode.setExpanded(true);

		// アプリルートを検索
		EntityNode root = plrAct.getAplRootFolderNode();

		if (root == null) {
			return;
		}
		if (syncflag == true) {
			TxtList.debug("OpenGraphController/exec_select:77/sync root");
			plrAct.sync(root);
		}

		// FriendList AW 2020/08/01 Del Start
		// フレンドのチャネルを取得
		// ListFriend lf = new ListFriend();
		// lf.start();
		// FriendList AW 2020/08/01 Del End

		// ルートのEntityNode取得
		EntityNode rootN = plrAct.getRootNode(root);
		if (rootN == null) {
			return;
		}
		Root r = new Root(rootN);

		// 画面タイトル
		changeTitle("Cloud Data Loading...");

		// oss取得スレッド起動
		if (ossTask == null) {
			OssTask ossTask = new OssTask(this, r);
			Thread thread2 = new Thread(ossTask);
			thread2.setPriority(Thread.MIN_PRIORITY);
			thread2.setDaemon(true);
			thread2.start();
			this.ossTask = ossTask;
		}

		// FriendList AW 2020/08/01 Update Start
		// cnlfRoot = r.listChannels();
		List<Channel> myCnlList = r.listChannels();
		cnlfRoot.addAll(myCnlList);

		// フレンドのチャネルを取得
		ListFriend lf = new ListFriend();
		lf.start();
		// FriendList AW 2020/08/01 Update End

		// 表示用
		TreeItem<TreeItemData> cnl_item = new TreeItem<>(new TreeItemData("Channel"));
		cnl_item.setExpanded(true);

		// FriendList AW 2020/08/01 Update Start
		// フレンドチャネルの取得完了を待つ
		while (true) {
			if (isEndFriCnl()) {
				endFriCnl = false;
				break;
			}
			try {
				Thread.sleep(500);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
		}
		// for (Channel c : cnlfRoot) {
		for (Channel c : myCnlList) {
			// FriendList AW 2020/08/01 Update End

			// c.sync(true);
			// 同期
			plrAct.sync(c.getNode());
			String cnm = plrAct.getChannelName(c);
 			cnl_item.getChildren().add(new TreeItem<>(new TreeItemData(cnm, c.getId())));
		}
		rootNode.getChildren().add(cnl_item);
		endMyCnlTree = true;
		//logger.debug("●CHANGE ListFriend LoopFlag endMyCnlTree=TRUE");

		TreeView<TreeItemData> tree = new TreeView<>(rootNode);
		tree.setEditable(true);
		tree.setShowRoot(false);

		// 右クリックでdeleteメニュー表示
		ContextMenu cm = new ContextMenu();
		tree.setContextMenu(cm);
		setDeleteMenu(cm, tree, r);

		tree.setOnMouseClicked(e -> {

			// ossスレッド終了待ち
			if (isOssThreadFlag()) {
				AlertDialog alert = new AlertDialog(AlertType.WARNING, "Loading...", "Loading the cloud data. Please try again after a minute.");
				alert.showAndWait();
				return;
			}

			if (e.getButton() == MouseButton.PRIMARY) {
				TreeItem selectitem = tree.getSelectionModel().getSelectedItem();
				if (selectitem == null) {
					return;
				}
				selectitem.setExpanded(true);
				TreeItemData itemData = (TreeItemData) selectitem.getValue();
				String displayChannelName = itemData.toString();
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mmZ");
				String nowAsISO = df.format(new Date());
				boolean isGraphOpen = false;
				for (Channel c : cnlfRoot) {
					if (c.getId().equals(itemData.getId())) {
						// 選択したチャネルの処理
						c.sync(true);
						EntityNode cnlN = c.getNode();
						plrAct.sync(cnlN);

						// チャネルのユーザプロファイル取得
						plrAct.getProfile(c);

						// チャネル開示ユーザの取得
						List<DisclosedToUser> disclosedToUserList = c.listDisclosedToUsers(true);
						//System.out.println("userList=" +  disclosedToUserList.size());
						for (DisclosedToUser user: disclosedToUserList) {
							//System.out.println("disclosed user=" + user.getPlrId());
							plrAct.getProfile(user);
						}

						// channelName AW 2020/08/24 Update Start
						// トップグラフの取得
						String topGraphId = null;
						Map<String, com.assemblogue.plr.lib.Node> properties = plrAct.listToMap(cnlN);
						if (properties.containsKey(AppProperty.LITERAL_TOP_GRAPH)) {
							com.assemblogue.plr.lib.Node literal = properties.get(AppProperty.LITERAL_TOP_GRAPH);
							topGraphId = literal.asLiteral().getValue().toString();
						}
						if (topGraphId != null && topGraphId != "") {
							// 取得したトップグラフを開く
							plrAct.setRootChannel(c);
							for (EntityNode n : (cnlN.getTimelineItems(nowAsISO, false, 1000)).getItems()) {
								String nodeId = n.getNodeId();
								if (topGraphId.equals(nodeId)) {
									if (openStage.containsKey(nodeId)) {
										openStage.get(nodeId).toFront();
										isGraphOpen = true;
										break;

									} else {

										// 2021/07 AW start
										// topGraphの指し示すEntityNodeのクラスが
										// rootGraphの場合は、そのまま開く
										// Mの場合はrootGraphに変更する
										// rootGraph、Mのいずれでもない場合は、グラフ新規作成扱いとするため
										// ここで、topGraphのリテラルを削除する。

										List<String> types = n.getType();
										String topNodeType = n.getType().get(0);
										if (topNodeType.equals("RootGraph")) {
											// 何もしないで後処理へ

										} else if (topNodeType.equals("M") || topNodeType.equals("rootGraph")) {
											// typeを変更する
											try {
												n.setType("RootGraph");
												n.sync();
											} catch (NodeNotFoundException e1) {
												e1.printStackTrace();
											}
										} else {
											// トップグラフの新規作成に遷移するように
											// topGraphプロパティを削除
											cnlN.removeProperty(AppProperty.LITERAL_TOP_GRAPH);
											cnlN.syncAndWait();
											break;
										}
										// 2021/07 AW end

										openGraph(n, c);
 										// グラフの名前（ウィンドウに表示されてる）は、チャネル表示名（変更可能性あり）を表示したほうがいい
										// openGraph(n, displayChannelName);
										isGraphOpen = true;
										break;
									}

								}
							}
						}
						// トップグラフの作成
						if (!isGraphOpen) {
							// creating graph start
							plrAct.setRootChannel(c);
							createGraph(displayChannelName, c);
 							isGraphOpen = true;
						}
					}
				}
			}
		});

		/*
		this.stage.showingProperty().addListener((observable, oldValue, newValue) -> {
			if (oldValue == true && newValue == false) {
				close_openGraph();
			}
		});
		*/

		// 2021/07 AW start OSSスレッドの終了待ち
		TinyLog.debug(new Throwable().getStackTrace()[0], false, "oss task check. flag=" + ossThreadFlag);
		while (true) {
			if (!isOssThreadFlag()) {
				break;
			}
			try {
				Thread.sleep(500);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
		}
		TinyLog.debug(new Throwable().getStackTrace()[0], false, "oss task check end.");

		// 画面タイトル
		undoTitle();
		// 2021/07 AW end OSSスレッドの終了待ち

		bPane.setCenter(tree);

		// 同期スレッド起動
		if (syncTask == null) {
			SyncTask syncTask = new SyncTask(this);
			Thread thread = new Thread(syncTask);
			thread.setPriority(Thread.MIN_PRIORITY);
			thread.setDaemon(true);
			thread.start();
			this.syncTask = syncTask;
		}
	}

	/**
	 * This method is used to remove the closed stages entry from the OpenStaged hashmap
	 * @param graphNodeId
	 */
	public void clearOpenStage(String graphNodeId) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		Iterator<Entry<String, Stage>> iterator = openStage.entrySet().iterator();
		while (iterator.hasNext()) {
			Entry<String, Stage> entry = iterator.next();
			if (graphNodeId.equals(entry.getKey())) {
				iterator.remove();
			}
		}
	}

	/**
	 * 右クリックでdeleteメニュー表示
	 *
	 * @param tree
	 */
	private void setDeleteMenu(ContextMenu menu, TreeView tree, Root root) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		MenuItem item = new MenuItem("delete");
		item.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				TreeItem selItem = (TreeItem) tree.getSelectionModel().getSelectedItem();
				if (selItem == null) {
					return;
				}
				Channel delChannel = null;
				for (Channel c : cnlfRoot) {
					if ((plrAct.getChannelName(c)).equals(selItem.getValue().toString())) {
						//if (deleteChannel(root, c)) {
						//	// チャネルリストの再描画
						//	exec_select(true);
						//}
						delChannel = c;
						break;
					}
				}
				if (delChannel != null) {
					deleteChannel(root, delChannel);
					// チャネルリストの再描画
					exec_select(true);
				}
			}
		});
		menu.getItems().add(item);
	}

	/**
	 * チャネルを削除する
	 *
	 * @param root
	 * @param c
	 * @return
	 */
	private boolean deleteChannel(Root root, Channel c) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		boolean bool = false;
		try {
			bool = root.removeChannel(c);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bool;
	}

	/**
	 * Treeに持たせるデータクラス
	 *
	 * @author mari
	 *
	 */
	class TreeItemData {
		private final String name; // 名前
		private final String id;

		/**
		 * コンストラクタです。
		 *
		 * @param name 名前
		 * @param type 種類
		 */
		public TreeItemData(String name) {
			this.name = name;
			this.id = null;
		}

		/**
		 * コンストラクタです。
		 *
		 * @param name 名前
		 * @param type 種類
		 */
		public TreeItemData(String name, String id) {
			this.name = name;
			this.id = id;
		}

		/**
		 * 名前を取得します。
		 *
		 * @return 名前
		 */
		public String getName() {
			return this.name;
		}

		/**
		 * IDを取得します。
		 *
		 * @return ID
		 */
		public String getId() {
			return this.id;
		}

		/**
		 * 文字列表現を返します。 {@link #getName()}と同じ文字列を返します。
		 *
		 * @see #getName()
		 */
		@Override
		public String toString() {
			return this.getName();
		}
	}

	/**
	 * friendのチャネルリストを取得、表示
	 */
	class ListFriend extends Thread {

		public void run() {
			TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START ListFriend");

			List<TreeItem<TreeItemData>> fitemList = getFriendChannel();
			while (true) {
				TinyLog.trace(new Throwable().getStackTrace()[0], false, "●START ListFriend Loop");

				if (isEndMyCnlTree()) {
					for (TreeItem<TreeItemData> ftreeItem : fitemList) {
						rootNode.getChildren().add(ftreeItem);
					}
					TinyLog.trace(new Throwable().getStackTrace()[0], false, "●Break ListFriend Loop break");
					endMyCnlTree = false;
					break;
				}
				TinyLog.trace(new Throwable().getStackTrace()[0], false, "●RETRY ListFriend Loop");
				//logger.debug("●RETRY ListFriend LoopEnd");

				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			TinyLog.trace(new Throwable().getStackTrace()[0], false, "●END ListFriend Loop");
		}
	}

	/**
	 * 自チャネルリストの表示処理が終了したかを返す
	 *
	 * @return
	 */
	private boolean isEndMyCnlTree() {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		if (endMyCnlTree) {
			return true;
		}
		return false;
	}

	// FriendList AW 2020/08/01 Add Start
	/**
	 * フレンドのチャネルリストの取得が終了したかを返す
	 *
	 * @return
	 */
	private boolean isEndFriCnl() {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		if (endFriCnl) {
			return true;
		}
		return false;
	}
	// FriendList AW 2020/08/01 Add End

	/**
	 * フレンドから公開されているチャネルを取得 フレンドのチャネルリストを自チャネルリストの後に表示するためTreeItemDataを返している
	 */
	private List<TreeItem<TreeItemData>> getFriendChannel() {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		List<TreeItem<TreeItemData>> frdItemList = new ArrayList<TreeItem<TreeItemData>>();

		List<Friend> listF = plrAct.getFriends();
		for (Friend f : listF) {
			TreeItem<TreeItemData> fcnl_item = new TreeItem<>(new TreeItemData("Channel - " + f.getUserId()));
			fcnl_item.setExpanded(true);
			try {
				// フレンドのルートノード
				EntityNode fnode = f.getFriendToMeRootNode();
				plrAct.sync(fnode);

				Root rootf = new Root(fnode);
				List<Channel> fcnl = rootf.listChannels();

				// FriendList AW 2020/08/01 Add Start
				cnlfRoot.addAll(fcnl);
				// FriendList AW 2020/08/01 Add End

				for (Channel fc : fcnl) {
					plrAct.sync(fc.getNode());
					if (!fc.isRemoved()) {
						// channelName AW 2020/08/24 Update Start
						// TreeItem<TreeItemData> item = new TreeItem<>(new TreeItemData(fc.getName(),
						// fc.getId()));
						TreeItem<TreeItemData> item = new TreeItem<>(new TreeItemData(fc.getDefaultName(), fc.getId()));
						// channelName AW 2020/08/24 Update End
						fcnl_item.getChildren().add(item);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			frdItemList.add(fcnl_item);
		}

		// FriendList AW 2020/08/01 Add Start
		endFriCnl = true;
		// FriendList AW 2020/08/01 Add End

		return frdItemList;
	}

	/**
	 * Method to cache copied nodes
	 *
	 * @return void
	 */
	public void copyNodes(DraggableNode dragNode) {
		this.copyNodes.add(dragNode);

	}

	/**
	 * Method to retrieve copied nodes
	 *
	 * @return List<DraggableNode>
	 */
	public List<DraggableNode> getCopyNodes() {
		return this.copyNodes;
	}

	/**
	 * Method to clear copied nodes
	 *
	 */
	public void clearCopyNodes() {
		this.copyNodes.clear();
	}


	/**
	 * 一時的にタイトルを変更する
	 * @param title
	 */
	public void changeTitle(String title) {
		stage.setTitle(title);
	}

	/**
	 * 一時的に変えたタイトルを元に戻す
	 */
	public void undoTitle() {
		stage.setTitle(thisTitle);
	}


}
