package com.assemblogue.plr.app.generic.semgraph;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

import com.assemblogue.plr.app.generic.semgraph.ui.controller.AppController;
import com.assemblogue.plr.app.lib.SystemAccount;
import com.assemblogue.plr.contentsdata.PLRContentsData;
import com.assemblogue.plr.io.PlrEntry;
import com.assemblogue.plr.io.PlrMeta;
import com.assemblogue.plr.io.Storage;
import com.assemblogue.plr.lib.EntityNode;
import com.assemblogue.plr.lib.FileNode;
import com.assemblogue.plr.lib.model.Root;

import javafx.concurrent.Task;

public class OssTask extends Task<Void> {

	private AppController appController;

	/** アカウントのルート */
	private Root root;

	/**
	 * コンストラクタ
	 * @param appController
	 */
	public OssTask(AppController appController, Root root) {
		super();
		this.appController = appController;
		this.root = root;
	}

	@Override
	protected Void call() throws Exception {
		TinyLog.debug(new Throwable().getStackTrace()[0], false, "◆START");

		appController.setOssThreadFlag(true);

		// 画面タイトル
		//Platform.runLater( () ->  {
		//	appController.changeTitle("Cloud Data Loading...");
		//});

		try {

			// OSS取得
			CopyOnWriteArrayList<FileNode> ossList = getOssList(AppController.getStorage(), root, false);

			// ossMap
			ConcurrentHashMap<String, String> ossMap = new ConcurrentHashMap<>();

			// OSS消去リスト
			List<String> removeList = new ArrayList<>();

			// 取得したOSSをjson形式にしてOSSMapへ設定
			for (FileNode fn: ossList) {

				// ファイルノードのmeta情報から、キャッシュ日付とクラウド日付を比較
				// 異なる場合はクラウドから再取得
				PlrMeta pm = fn.getMeta();
				long netModified = pm.getNetModified();
				long cacheModified = pm.getLocalModified();

				// OSS情報
				InputStream is;
				if (cacheModified == -1) {
					// ローカルにない場合
					TinyLog.debug(new Throwable().getStackTrace()[0], false, "new data.");
					TinyLog.debug(new Throwable().getStackTrace()[0], false, "NET read. id=" + fn.getId() + " title=" + fn.getTitle("ja"));
					is = fn.get(PlrEntry.Origin.NET);

				} else if (netModified != cacheModified) {
					// クラウドが更新されている場合は、強制的にクラウドから
					TinyLog.debug(new Throwable().getStackTrace()[0], false, "cloud data is changed.");
					TinyLog.debug(new Throwable().getStackTrace()[0], false, "NET read. id=" + fn.getId() + " title=" + fn.getTitle("ja"));
					is = fn.get(PlrEntry.Origin.NET);

				} else {
					// 上記以外はキャッシュから、ない場合はクラウドから
					// 下記の処理、例外以外で判定できないか → Personaryでも例外で判定しているので例外判定している
					try {
						TinyLog.debug(new Throwable().getStackTrace()[0], false, "LOCAL read. id=" + fn.getId() + " title=" + fn.getTitle("ja"));
						is = fn.get(PlrEntry.Origin.LOCAL);

					} catch (Exception e2) {
						TinyLog.debug(new Throwable().getStackTrace()[0], false, "NET read. id=" + fn.getId() + " title=" + fn.getTitle("ja"));
						is = fn.get(PlrEntry.Origin.NET);
					}
				}

				String jsonld = readInputStream(is);
				//String jsonld = readInputStream(fn.get(PlrEntry.Origin.NET));
				//String jsonld = readInputStream(fn.get());

				is = null;

				// 取得したjsonldがPLRContentsDataとして正しいかチェック
				// エラー時はオントロジーリストにのせない
				boolean checkResult = checkJson(jsonld);
				if (checkResult) {
					TinyLog.debug(new Throwable().getStackTrace()[0], false, "MAP  id=" + fn.getId() + " title=" + fn.getTitle());

					String fileNodeId = fn.getId();
					ossMap.put(fileNodeId, jsonld);

					// ossListからも消す
					removeList.add(fileNodeId);
				}
			}

			//TODO
			//本来はossListからも削除すべき
			//System.out.println("ossList size=" + ossList.size());

			appController.setOssList(ossList);
			appController.setOssMap(ossMap);

			TinyLog.debug(new Throwable().getStackTrace()[0], false, "ossListSize=" + String.valueOf(ossList.size()));

		} catch (Exception e) {
			e.printStackTrace();
		}

		// 画面タイトル
		//Platform.runLater( () ->  {
		//	appController.undoTitle();
		//});

		appController.setOssThreadFlag(false);

		TinyLog.debug(new Throwable().getStackTrace()[0], false, "◆END");

		return null;
	}

	/**
	 * SystemAccountのルートノードのEntityNodeを取得する
	 * @param storage
	 * @return
	 */
	private EntityNode getSystemAccountRootNode(Storage storage) {

		// SystemAccountのルートノード取得
		EntityNode rootNode = null;
		try {
			rootNode = SystemAccount.getPublicRootNode(storage);
			rootNode.syncAndWait();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return rootNode;
	}

	/**
	 * 自分のアカウントのルートノードのEntityNodeを取得する
	 * @param root
	 * @return
	 */
	private EntityNode getMyAccountRootNode(Root root) {

		// 自分のアカウントのルートノード取得
		EntityNode myRootNode = null;
		try {
			myRootNode = root.getPublicRootNode();
			myRootNode.syncAndWait();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return myRootNode;
	}

	/**
	 * ルートノードのEntityNodeに紐づくオントロジーを取得
	 * @param rootNode
	 * @return
	 */
	private List<FileNode> getOss(EntityNode rootNode,boolean containsGnoreList) {

		rootNode.syncAndWait();

		EntityNode ossNode = null;
		List<com.assemblogue.plr.lib.Node> nodes = rootNode.getProperty("oss");
		for (com.assemblogue.plr.lib.Node node: nodes) {
			if (node.isEntity()) {
				ossNode = node.asEntity();
				break;
			}
		}

		// ossNodeが取得できない場合はnullを返す
		if (ossNode == null) {
			return null;
		}

		ossNode.syncAndWait();

		List<FileNode> ontologyNodes = new ArrayList<FileNode>();
		for (com.assemblogue.plr.lib.Node node : ossNode.getProperty("ontology")){
			if (node.isFile()){
				FileNode fn = node.asFile();
		    	if (containsGnoreList) {
		    		ontologyNodes.add(node.asFile());

		    	} else {

			    	// ファイルのタイトル
			    	// 除外対象確認のため、日本語のタイトルを取得
			    	//TODO 多言語対応時見直し必要？
			    	String titleJa = fn.getTitle("ja");
	//		    	if (!ignoreList.contains(titleJa)) {
			    		ontologyNodes.add(node.asFile());
	//		    	}
		    	}

		    }
		}
		return ontologyNodes;
	}

	/**
	 * 「containsGnoreList」フラグで、gnoreListを含む/除外したリストを返す。
	 * @param storage
	 * @param root
	 * @param containsGnoreList
	 * @return
	 */
	private CopyOnWriteArrayList<FileNode> getOssList(Storage storage, Root root, boolean containsGnoreList) {

		// ossファイルノードリスト
		CopyOnWriteArrayList<FileNode> ontologyNodes = new CopyOnWriteArrayList<FileNode>();

		try {

			// システムアカウントから取得
			EntityNode systemAccountRootNode = getSystemAccountRootNode(storage);
			List<FileNode> sysOntologyNodes = getOss(systemAccountRootNode,containsGnoreList);

			if(sysOntologyNodes != null) {
				ontologyNodes.addAll(sysOntologyNodes);
			}

			// 自分のアカウントから取得
			EntityNode myAccountRootNode = getMyAccountRootNode(root);
			List<FileNode> myOntologyNodes = getOss(myAccountRootNode,containsGnoreList);
			if(myOntologyNodes != null) {
				ontologyNodes.addAll(myOntologyNodes);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return ontologyNodes;
	}

	/**
     * 入力ストリームを読み込みStringとして取得する
     * @param inputStream 入力ストリーム
     * @return 読み込んだファイル内容
     * @throws IOException
     */
    private String readInputStream(final InputStream inputStream) throws IOException {
        return new BufferedReader(new InputStreamReader(inputStream, Charset.forName("UTF-8")))
                .lines()
                .collect(Collectors.joining(System.getProperty("line.separator")));
    }


    /**
     * 読み込んだjsonがPLRContentsDataとして正しいかチェック
     * @param jsonld
     * @return
     */
	private boolean checkJson(String jsonld) {

		try {
			PLRContentsData data = new PLRContentsData(jsonld);
		} catch (Exception e) {

			e.printStackTrace();
			return false;
		}

		return true;
	}

}
