package com.assemblogue.plr.app.generic.semgraph;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.assemblogue.plr.app.generic.semgraph.ui.controller.AppController;
import com.assemblogue.plr.app.generic.semgraph.ui.controller.RootLayoutController;
import com.assemblogue.plr.lib.EntityNode;

/**
 * グラフのノード全体にかかわる処理
 * シングルトン
 *
 * @author <a href="mailto:kaneko@cri-mw.co.jp">KANEKO, yukinori</a>
 */
public class GraphManager {

    // AppControllerは一つだけなので。。
    static AppController appController = null;
    // 全ノードのとりまとめ
    static NodePalette nodepalette = new NodePalette();

    // オープン中グラフ情報
    // ハイパーノード、ノード検索向け
    static class GraphPain {
        GraphActor graph;
        //NodePaneController ctrlr;
        RootLayoutController rootlayout;

        GraphPain(GraphActor graph, RootLayoutController rootlay) {
            this.graph = graph;
            this.rootlayout = rootlay;
        }
    };

    // オープン中のGraphActor
	private static Map<String, GraphPain> map = new LinkedHashMap<>();

    // ノードコピペ用記憶領域
    private static NodeCell target;

    // グラフコピペ用記憶領域（所謂ハイパーノード用）
    private static EntityNode hyperTarget;
    private static String hyperTargetName;

    /**
     * AppControllerの登録
     * @param apc AppController
     */
    public static void setAppController(AppController apc) {
        appController = apc;
    }

    /**
     * AppControllerの取得
     * @return AppController
     */
    public static AppController getAppController() {
        return appController;
    }

    /**
     * グラフの多重オープンチェック
     * @param grpact オープンしたグラフ
     */
	public static synchronized void open(GraphActor grpact, RootLayoutController rootLayoutController) {
        GraphPain fp = new GraphManager.GraphPain(grpact, rootLayoutController);

        // オープン中のグラフ
        // 2021/2 AW REP start
        map.put(appController.plrAct.getUriStr(grpact.getGraphData().getGraphNode()), fp);
        //map.put(appController.plrAct.getUriStr(grpact.getGraphNode()), fp);
        // 2021/2 AW REP end

    }

    public static synchronized void close(GraphActor grpact) {
        // オープン中のグラフリストから削除
        // 2021/2 AW REP start
       	map.remove(appController.plrAct.getUriStr(grpact.getGraphData().getGraphNode()));
        //map.remove(appController.plrAct.getUriStr(grpact.getGraphNode()));
        // 2021/2 AW REP end

        if (map.size() == 0) {
            //  オープン中のグラフがない
            nodepalette.clear();
        }
    }

    public static synchronized boolean isOpend(GraphActor grpact) {
    	// 2021/2 AW REP start
    	if (map.containsKey(appController.plrAct.getUriStr(grpact.getGraphData().getGraphNode()))) {
        //if (map.containsKey(appController.plrAct.getUriStr(grpact.getGraphNode()))) {
        // 2021/2 AW REP end

            //TxtList.set("FolderNode " + grpact.getGraphName() +" is opened.");
            //System.out.println("checking if its opened");
            return true;
        }

        return false;
    }

    public static synchronized void toFront(GraphActor grpact) {
    	// 2021/2 AW REP start
        if (map.containsKey(appController.plrAct.getUriStr(grpact.getGraphData().getGraphNode()))) {
            GraphPain gp = map.get(appController.plrAct.getUriStr(grpact.getGraphData().getGraphNode()));
        //if (map.containsKey(appController.plrAct.getUriStr(grpact.getGraphNode()))) {
        //    GraphPain gp = map.get(appController.plrAct.getUriStr(grpact.getGraphNode()));
        // 2021/2 AW REP end

            gp.rootlayout.toFront();
        }
    }


    /**
     * オープン中のグラフのリストを取得する
     * @return GraphActorのリスト
     */
    public static synchronized List<GraphActor> getOpenedGraphList() {
        List<GraphActor> list = new ArrayList<>();

        for (Iterator<String> iterator = map.keySet().iterator(); iterator.hasNext();) {
            String key = iterator.next();
            GraphPain fp = map.get(key);
            list.add(fp.graph);
        }

        return list;
    }

    /**
     * オープン中のNodePainControllerを取得する
     * @param grpact グラフ
     * @return NodePainContoroller
     */
    public static synchronized RootLayoutController getRootlayout(GraphActor grpact) {
    	// 2021/2 AW REP start
	    String key = appController.plrAct.getUriStr(grpact.getGraphData().getGraphNode());
        //String key = appController.plrAct.getUriStr(grpact.getGraphNode());
        // 2021/2 AW REP end


        if (map.containsKey(key)) {
        }

        return null;
    }

    /**
     * 指定したノードをターゲット登録する
     * @param node　EntityNode
     */
    public static synchronized void setTarget(NodeCell node) { target = node; }

    /**
     * 登録ターゲットを取得する
     * @return EntiryNode
     */
    public static synchronized NodeCell getTarget() {
        return target;
    }

    /**
     * 指定したグラフをターゲット登録する
     * @param node　EntityNode
     */
    public static synchronized void setHyperTarget(EntityNode node, String name) {
        hyperTarget = node;
        hyperTargetName = name;
    }

    /**
     * 登録グラフターゲットを取得する
     * @return EntiryNode
     */
    public static synchronized EntityNode getHyperTarget() {
        return hyperTarget;
    }
    public static synchronized String getHyperTargetName() {
        return hyperTargetName;
    }


    //public static synchronized void openGraphList() {
    //    appController.exec_openGraph(false);
    //}


    /**
     * シングルトン
     */
    private GraphManager() {}
}