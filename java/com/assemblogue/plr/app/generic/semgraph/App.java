package com.assemblogue.plr.app.generic.semgraph;

import java.io.IOException;

import com.assemblogue.plr.app.generic.semgraph.ui.controller.AppController;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.stage.Stage;


/**
 * アプリケーションのメインクラスです。
 * The main class of the application.
 *
 * @author <a href="mailto:m.ikemoto@runzan.co.jp">IKEMOTO, Masahiro</a>
 * @modified <a href="mailto:kaneko@cri-mw.co.jp">KANEKO, yukinori</a>
 * @see Application
 */

public class App extends Application {

//	private static Logger logger = LogManager.getLogger();

	public static void main(String[] args) throws Exception {
		//logger.trace("◆START");

		launch(args);

		System.exit(0);
	}

	private AppController appController;
	//private PlrActor plractor;

	@Override
	public void start(Stage stage) throws Exception {
		//logger.trace("◆START");

		FXMLLoader loader = new FXMLLoader(getClass().getResource("ui/view/AppView.fxml"), Messages.getResources());

		Parent root;
		Application.setUserAgentStylesheet(STYLESHEET_CASPIAN);


		try {

			root = loader.load();


		} catch (IOException e) {
			//e.printStackTrace();
			return;
		}
		appController = loader.getController();
		appController.setStage(stage);
		Scene scene = new Scene(root,480, 580);

		stage.setTitle(Messages.getString("application.title") + " " + AppProperty.VERSION);
		stage.getIcons().add(new Image(App.class.getResourceAsStream("ui/img/editor.jpg")));
		scene.setFill(Color.ALICEBLUE);
		stage.setScene(scene);
        stage.show();
	}


	@Override
	public void stop() {
		//logger.trace("◆START");

		if (appController != null) {
 			appController.destroyPLR();
		}
	}
}
