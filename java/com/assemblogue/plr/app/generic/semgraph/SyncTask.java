package com.assemblogue.plr.app.generic.semgraph;

import java.util.Collection;
import java.util.Map;

import com.assemblogue.plr.app.generic.semgraph.ui.controller.AppController;
import com.assemblogue.plr.app.generic.semgraph.ui.controller.RootLayoutController;
import com.assemblogue.plr.lib.EntityNode;

import javafx.application.Platform;
import javafx.concurrent.Task;

public class SyncTask 	extends Task<Void> {

	private AppController appController;

	//TODO アカウント切り替えやストレージ切り替え時は止める必要がある

	/**
	 * 同期処理、クラウドからのデータ取得
	 */
	@Override
	protected Void call() throws Exception {

		try {

			while (true) {

				Thread.sleep(20000);
				TinyLog.debug(new Throwable().getStackTrace()[0], false, "◆START");

				if (isCancelled()) {
					TinyLog.debug(new Throwable().getStackTrace()[0], false, "◆END task Cancell");
					break;
				}

				// グラフマップロック確認
				if (appController.isGraphMapLock()) {
					// 取得できない場合は戻る
					TinyLog.debug(new Throwable().getStackTrace()[0], false, "◆END GraphMap Locked.");
					continue;
				}

				// グラフ管理Map取得
				Map<String, RootLayoutController> graphMap = appController.getGraphMap();

				// 表示しているグラフごとの処理
				for (Map.Entry<String, RootLayoutController> e : graphMap.entrySet()) {

					String graphId = e.getKey();
					RootLayoutController rootLayoutController = e.getValue();

					// コントローラが取得できないときは、描画画面がないので次へ
					if (rootLayoutController  == null) {
						TinyLog.debug(new Throwable().getStackTrace()[0], false, "◆END GraphNode Not Found in GraphControllMap.");
						continue;
					}

					// GraphActor
					GraphActor graphActor = rootLayoutController.getGraphAct();
					// グラフノード
					EntityNode graphNode = graphActor.getGraphData().getGraphNode();
					// タイムラインルート
					EntityNode graphTimelineRootNode = graphNode.getTimelineRootNode();
					// グラフ名
					String graphName = graphActor.getGraphData().getGraphName();

					// rootContolollerの同期フラグチェック
					if (rootLayoutController.isSyncFlag()) {
						TinyLog.debug(new Throwable().getStackTrace()[0], false, "◆END stop sync by syncFlag graphName=" + graphName);
						continue;
					}

					// 画面タイトル
					Platform.runLater( () ->  {
						rootLayoutController.changeTitle("Syncing...");
					});

					// 同期
					// タイムラインで同期する
					PlrActor plrActor = AppController.plrAct;
					TinyLog.debug(new Throwable().getStackTrace()[0], false, "Start sync getTimeline graphName=" + graphName);

					// 同期開始時間
					long startTime = System.currentTimeMillis();

					// タイムラインアイテムリスト
					Collection<EntityNode> syncTimelineList = plrActor.getTimelineItemsWithSync(graphTimelineRootNode);

					//long endTime = System.currentTimeMillis();
					//System.out.println(LocalDateTime.now() + " 処理時間：" + (endTime - startTime) + " ms");

					TinyLog.debug(new Throwable().getStackTrace()[0], false, "End sync getTimeline graphName=" + graphName);

					// グラフデータの描画判定と描画
					// JavaFXメソッドで実行
					Platform.runLater( () ->  {
						TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START from backGround sync thread");

						RootLayoutController updateRootLayoutController = graphMap.get(graphId);

						// ここに来る間に画面を消している可能性もあるので
						// rootLayoutControllerがNULLの場合は何もしない
						if (updateRootLayoutController == null) {
							TinyLog.debug(new Throwable().getStackTrace()[0], false, "rootLayoutController is NULL. Did nothing.");
						} else {

							TinyLog.debug(new Throwable().getStackTrace()[0], false, "graph repaint from backGround sync. graphName=" + graphName);

							updateRootLayoutController.undoTitle();

							// 2021/07 AW start 同期メソッドがnullを返す場合の処理
							// 同期メソッドでエラー発生時はnullが返ってくる
							// この場合は再描画しない
							if (syncTimelineList != null) {

								// 2021/07 AW start バックグラウンド同期中に更新があった場合、再描画しないように変更
								// 時間チェック
								long updateTime = graphActor.getGraphData().getUpdateTime();
								// バックグラウンド同期開始時間が更新時間より古い場合は再描画しない
								if ((startTime - updateTime) > 0) {
									TinyLog.debug(new Throwable().getStackTrace()[0], false, "SyncTime is newer. Do repaint.=" + graphName);

									// 同期で取得したタイムラインノードを読み込み
									graphActor.openGraph(syncTimelineList);
									// 描画
									updateRootLayoutController.drawGraphPane(true);

								} else {
									TinyLog.debug(new Throwable().getStackTrace()[0], false, "EditTime is newer. Do NOT repaint.=" + graphName);
								}
								// 2021/07 AW end バックグラウンド同期中に更新があった場合、再描画しないように変更

							} else {
								TinyLog.debug(new Throwable().getStackTrace()[0], false, "syncTimelineList is NULL. Do NOT repaint.=" + graphName);
							}
							// 2021/07 AW end 同期メソッドがnullを返す場合の処理
						}
						TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆END from backGround sync thread");
					});
				}

				TinyLog.debug(new Throwable().getStackTrace()[0], false, "◆END");
			}

		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * コンストラクタ
	 * @param appController
	 */
	public SyncTask(AppController appController) {
		super();
		this.appController = appController;
	}
}
