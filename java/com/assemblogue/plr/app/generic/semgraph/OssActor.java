package com.assemblogue.plr.app.generic.semgraph;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import com.assemblogue.plr.contentsdata.PLRContentsData;
import com.assemblogue.plr.contentsdata.ontology.OntologyItem;
import com.assemblogue.plr.io.PlrEntry;
import com.assemblogue.plr.lib.FileNode;
import com.assemblogue.plr.util.I18nUtils;

/**
 * OSS オントロジー、スタイルシートを読み込み、オブジェクトを生成します。
 * @author <a href="mailto:kaneko@cri-mw.co.jp">KANEKO, yukinori</a>
 */
public class OssActor  {

	/** PLRコンテンツデータ */
	PLRContentsData eventContentsData;

	public PLRContentsData getEventOss() {
		return eventContentsData;
	}

	/** PLRコンテンツデータ、リンク用 */
	PLRContentsData linkContentsData;

    public PLRContentsData getLinkOss() {
	    return linkContentsData;
    }

    /**
     * コンストラクタ
     */
    public OssActor() {
    	this(false);
    }

	/**
	 * コンストラクタ
	 * ファイルから読み込み
	 */
	public OssActor(boolean isLocalRead) {

		if (isLocalRead) {
	        String fname_ont_jsonld = "DiscourseGraph_simplified.jsonld";
	        String fname_css_jsonld = "DiscourseGraph_stylesheet.jsonld";

	        //TODO
	        String fNameTestJsonld = "Test.jsonld";
	        String fNameTestJsonld2 = "Test2.jsonld";

		    try {
	            // jarファイルと同じディレクトリにオントロジjsonldを置いておく
	            String jarPath = System.getProperty("java.class.path");
	            String dirPath = jarPath.substring(0, jarPath.lastIndexOf(File.separator)+1);
	            String otjsonld = readJsonldFromDirOrClassPath(dirPath, fname_ont_jsonld);
	            String ssjsonld = readJsonldFromDirOrClassPath(dirPath, fname_css_jsonld);
	            linkContentsData = new PLRContentsData(otjsonld, ssjsonld);

	            //TODO
	            String testJsonld = readJsonldFromDirOrClassPath(dirPath, fNameTestJsonld2);
	            String testJsonld2 = readJsonldFromDirOrClassPath(dirPath, fNameTestJsonld);
	            List<String> testJsonldList = List.of(testJsonld, testJsonld2);
	            eventContentsData = new PLRContentsData(testJsonldList);

	        } catch (Exception e) {
	        	e.printStackTrace();
	        }
		}
	}

	/**
	 * コンテンツデータの作成
	 * @param ontologyNodeList
	 * @param ossMap
	 * @param ossThreadFlag
	 */
	public void makeContentsData(List<FileNode> ontologyNodeList, Map<String, String> ossMap, boolean ossThreadFlag) {
		TinyLog.debug(new Throwable().getStackTrace()[0], false, "◆START ontologyListSize=" + ontologyNodeList.size() + " flag=" + ossThreadFlag);

		try {

			// jsonldのリスト
			List<String> jsonAllList = new ArrayList<>();

			// #Eventのjsonldのリスト
			List<String> jsonEventList = new ArrayList<>();

			for (FileNode fn: ontologyNodeList) {

				// オントロジーのjson取得
				String jsonld = getOntologyJsonld(fn, ossMap, ossThreadFlag);

				// jsonldがnull、ファイルがなくなっている場合
				if (jsonld == null) {
					continue;
				}

				jsonAllList.add(jsonld);

				// #Event判定
				boolean isEvent = false;
				PLRContentsData data = new PLRContentsData(jsonld);
				List<OntologyItem> topList = data.topClasses();
				for (OntologyItem oi: topList) {
					if (oi.id.contentEquals("#Event")) {
						isEvent = true;
						break;
					}
				}
				if (isEvent) {
					jsonEventList.add(jsonld);
				}

			}

			// リンク用
			linkContentsData = new PLRContentsData(jsonAllList);

			// ノード用（#Event)
			eventContentsData = new PLRContentsData(jsonEventList);

		} catch (Exception e) {

			e.printStackTrace();

			//String header = "Error";
			//String errorMsg = "Could not generate PLRcontentData.";
			//AlertDialog  dialog = new AlertDialog(AlertType.ERROR, header, errorMsg);
			//dialog.show();

		}
	}

	/**
	 * Base, DiscourseGraphのコンテンツデータ追加
	 * @param ossList
	 * @param ossMap
	 * @param ossThreadFlag
	 */
	public void addBasicContentsData(List<FileNode> ossList, Map<String, String> ossMap, boolean ossThreadFlag,
															List<FileNode> channelDataSetting) {
		TinyLog.debug(new Throwable().getStackTrace()[0], false, "◆START ontologyListSize=" + channelDataSetting.size() + " flag=" + ossThreadFlag);

		try {

			// graphSettingは基本と談話関係は必ず入れる
			boolean baseExist = false;
			boolean discourseGraphExist = false;

			for (FileNode fn: channelDataSetting) {

				String title = fn.getTitle();
				if (title.endsWith("!")) {
					title = title.substring(0, title.length() - 1);
				}

				if (title.equals("Base")) {
					baseExist = true;
				}

				if (title.equals("Discourse Graph")) {
					discourseGraphExist = true;
				}
			}

			if (!baseExist) {
				for (FileNode fn: ossList) {
					String title = fn.getTitle();
					if (title.endsWith("!")) {
						title = title.substring(0, title.length() - 1);
					}
					if ("Base".equals(title)) {
						channelDataSetting.add(fn);
						TinyLog.debug(new Throwable().getStackTrace()[0], false, "add base ontology.");
						break;
					}
				}
			}
			if (!discourseGraphExist) {
				for (FileNode fn: ossList) {
					String title = fn.getTitle();
					if (title.endsWith("!")) {
						title = title.substring(0, title.length() - 1);
					}
					if ("Discourse Graph".equals(title)) {
						channelDataSetting.add(fn);
						TinyLog.debug(new Throwable().getStackTrace()[0], false, "add discourseGraph ontology.");
						break;
					}
				}
			}

		} catch (Exception e) {

			e.printStackTrace();
		}
	}


	/**
	 * 基本と談話関係のファイルノード追加
	 * @param ossList
	 * @param ossMap
	 * @param ossThreadFlag
	 */
	/*
	public List<FileNode> getBasicFileNode(List<FileNode> ossList, Map<String, String> ossMap, boolean ossThreadFlag,
															List<FileNode> channelDataSetting) {

		TinyLog.debug(new Throwable().getStackTrace()[0], false, "◆START");

		List<FileNode> resultList = new ArrayList<>(channelDataSetting);

		// graphSettingは基本と談話関係は必ず入れる
		boolean baseExist = false;
		boolean discourseGraphExist = false;

		for (FileNode fn: channelDataSetting) {

			String title = fn.getTitle();
			if (title.endsWith("!")) {
				title = title.substring(0, title.length() - 1);
			}

			if (title.equals("Base")) {
				baseExist = true;
			}

			if (title.equals("Discourse Graph")) {
				discourseGraphExist = true;
			}
		}

		if (ossThreadFlag) {

			// 自力で取得

			if (!baseExist) {
				FileNode baseFileNode = getFileNode("Base");
				resultList.add(baseFileNode);
			}

			if (!discourseGraphExist) {
				FileNode dgFileNode = getFileNode("Discourse Graph");
				resultList.add(dgFileNode);
			}

		} else {
			if (!baseExist) {
				for (FileNode fn: ossList) {
					String title = fn.getTitle();
					if ("Base".equals(title)) {
						resultList.add(fn);
						break;
					}
				}
			}
			if (!discourseGraphExist) {
				for (FileNode fn: ossList) {
					String title = fn.getTitle();
					if ("Discourse Graph".equals(title)) {
						resultList.add(fn);
						break;
					}
				}
			}
		}
		return resultList;
	}
	*/

	/**
	 * オントロジーjsonldファイルを取得
	 * @param fileNode
	 * @param ossMap
	 * @param ossThreadFlag
	 * @return
	 */
	private String getOntologyJsonld(FileNode fileNode, Map<String, String> ossMap, boolean ossThreadFlag) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START threadFlag=" + ossThreadFlag);

		String jsonld = null;

		if (!ossThreadFlag) {

			String fileId = fileNode.getId();
			jsonld = ossMap.get(fileId);

		} else {
			// oss読み込みが終わっていない場合は自分で取得
			try {
				jsonld = readInputStream(fileNode.get(PlrEntry.Origin.NET));

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return jsonld;
	}

    private String readJsonldFromDirOrClassPath(String dirPath, String filePath) throws IOException {
        String content;
        try {
            // カレントディレクトリにあるJSONLDをまず読んでみる
            content = readFileWithPath(dirPath + "./"+filePath);
        } catch (Exception e) {
            // 失敗した場合はクラスパスから読む
            InputStream s = getClass().getResourceAsStream(filePath);
            if (s == null) {
                throw new RuntimeException("resource " + filePath + " not found");
            }
            content = readInputStream(s);
        }
        return content;
    }

    /**
     * ファイルを読み込みStringとして取得する
     * @param path ファイルパス
     * @return 読み込んだファイル内容
     * @throws IOException
     */
    private static String readFileWithPath(final String path) throws IOException {
        return Files.lines(Paths.get(path), Charset.forName("UTF-8"))
                .collect(Collectors.joining(System.getProperty("line.separator")));
    }

    /**
     * 入力ストリームを読み込みStringとして取得する
     * @param inputStream 入力ストリーム
     * @return 読み込んだファイル内容
     * @throws IOException
     */
    private static String readInputStream(final InputStream inputStream) throws IOException {
        return new BufferedReader(new InputStreamReader(inputStream, Charset.forName("UTF-8")))
                .lines()
                .collect(Collectors.joining(System.getProperty("line.separator")));
    }

    /**
     * アイテムのラベルを取得する
     * 基本日本語を返す
     * @param item アイテム
     * @return ラベル
     */
    public String getLabel(OntologyItem item) {
        //todo: change how this works to be not dependent on the language
        String str = I18nUtils.getValue(item.label, "ja");
        if (str == null) {
            str = I18nUtils.getValue(item.label, "en");

        }
        if (str == null) {
            str = I18nUtils.getValue(item.label, "zh");
        }
        if (str == null) {
            str = I18nUtils.getValue(item.label, null);
        }


        return str;
    }

    /**
     * アイテムのラベルを取得する
     * 基本日本語を返す
     * @param item_id item ID
     * @return
     */
    public String getLabel(String item_id) {
        OntologyItem item = linkContentsData.getNode(item_id);
         if (item == null) {
            return null;
        }

        return getLabel(item);
    }

    /**
     * アイテムのラベルを全種取得する
     * 基本日本語を返す
     * @param item_id アイテムID
     * @return ラベル
     */
    public Map<String,String> getLabelAll(String item_id) {
        OntologyItem item = linkContentsData.getNode(item_id);
        if (item == null) {
            return null;
        }

        return getLabelAll(item);
    }

    /**
     * アイテムのラベルを全種取得する
     * 基本日本語を返す
     * @param item アイテム
     * @return ラベル
     */
    public Map<String,String> getLabelAll(OntologyItem item) {
        Map<String,String> map = new HashMap<>();
        List<String> keys = Arrays.asList("ja","en");

        for (String key: keys) {
            try {
                String str = I18nUtils.getValue(item.label, key);
                if (str == null) {
                    str = "";
                }
                map.put(key, str);
            } catch (Exception e) {
            }

        }
        //System.out.println("Key values in map-->"+map);
        return map;
    }


	/**
     * パラメータのOntologyItemから表示テキストを取得
     * @param ontologyItem
     * @return
     */
    public static String getDisplayText(OntologyItem ontologyItem) {

    	String result = null;

    	// Mクラス対応特別処理
    	// Mクラスは、ラベルにデータが入ってない。
    	// コメントに名称が入っているので、そちらを検索する。
    	if ((ontologyItem.id).equals("#M")) {
    		if (Locale.getDefault().getLanguage().equals("ja")) {
    	    	result = I18nUtils.getValue(ontologyItem.comment, "ja");
    	    	//TODO 日本語の定義がない場合（URL等）
    	    	if (result == null) {
    	    		result = I18nUtils.getValue(ontologyItem.comment);
    	    	}
    		} else {
        		result = I18nUtils.getValue(ontologyItem.comment);
        	}
    		return result;
    	}

    	if (Locale.getDefault().getLanguage().equals("ja")) {
    		result = I18nUtils.getValue(ontologyItem.label, "ja");
    		//TODO 日本語の定義がない場合（URL等）
    		if (result == null) {
    			result = I18nUtils.getValue(ontologyItem.label);
    		}
    	} else {
    		result = I18nUtils.getValue(ontologyItem.label);
    	}

    	return result;
   	}

    //////////////////////////////////
    // graphActorから移動
    ///////////////////////////////////

	/**
	 * 指定ノードの入力可能属性を取得する（第一階層のみ）
	 *
	 * @param item_id OntologyItem ID(#〜)
	 * @return 属性リスト
	 */
	public List<PLRContentsData.InputtableProperty> getPropertyMenu(String item_id) {
		//logger.trace("◆START");

		OntologyItem item = linkContentsData.getNode(item_id);
		List<PLRContentsData.InputtableProperty> list = new ArrayList<>();
		for (PLRContentsData.InputtableProperty ip : linkContentsData.AllPropertyMenu(item)) {
			if (ip.item.subPropertyOf == null) {

				list.add(ip);
			}
		}

		return list;
	}


	///////////
	// 新設
	///////////

	/*
	public static FileNode getFileNode(String ontologyTitle) {

		// 取得するオントロジー
		//List<String> targetOntologyList = List.of("Base", "Discourse Graph");

		// SystemAccountのルートノード取得
		EntityNode rootNode = null;
		try {
			rootNode = SystemAccount.getPublicRootNode(AppController.getStorage());
			rootNode.syncAndWait();
		} catch (Exception e) {
			e.printStackTrace();
		}

		EntityNode ossNode = null;
		List<com.assemblogue.plr.lib.Node> nodes = rootNode.getProperty("oss");
		for (com.assemblogue.plr.lib.Node node: nodes) {
			if (node.isEntity()) {
				ossNode = node.asEntity();
				break;
			}
		}

		// ossNodeが取得できない場合はnullを返す
		if (ossNode == null) {
			return null;
		}

		//List<FileNode> ontologyNodes = new ArrayList<FileNode>();
		for (com.assemblogue.plr.lib.Node node : ossNode.getProperty("ontology")){
		    if (node.isFile()){
		    	FileNode fn = node.asFile();
		    	String title = fn.getTitle();
		    	System.out.println("FileNode = " + title);

				if (title.endsWith("!")) {
					title = title.substring(0, title.length() - 1);
				}
				if (ontologyTitle.equals(title)) {
					try {
						System.out.println("getFileNode = " + title);
						fn.get(PlrEntry.Origin.NET);
					} catch (Exception e) {
						e.printStackTrace();
					}
					return fn;
				}
		    }
		}
		return null;
	}
	*/

}

/* end of file */