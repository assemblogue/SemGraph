package com.assemblogue.plr.app.generic.semgraph;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class LocalFileIO {

//	private static Logger logger = LogManager.getLogger();

	/** ホームディレクトリ */
	private static final String HOME_DIRECTORY = System.getProperty("user.home");

	/** SemanticEditor 保存用ディレクトリ */
	private static final String SEM_DIRECTORY = ".semEditor";

	/** 保存ファイル名拡張子 */
	private static final String SEM_FILENAME_EXT = ".xml";

	/** XMLタグ ルートノード */
	private static final String XML_NODE_ROOT = "SemanticEditor";
	/** XMLタグ ウィンドウ */
	private static final String XML_NODE_LOCALWINDOW = "LocalWindow";
	/** XMLタグ ウィンドウ属性 ノードID*/
	private static final String XML_ATTR_LOCALWINDOW_ID = "id";
	/** XMLタグ ウィンドウルートX */
	private static final String XML_NODE_WINDOWROOTX = "WindowRootX";
	/** XMLタグ ウィンドウルートY  */
	private static final String XML_NODE_WINDOWROOTY = "WindowRootY";
	/** XMLタグ ウィンドウサイズX */
	private static final String XML_NODE_WINDOWSIZEX = "WindowSizeX";
	/** XMLタグ ウィンドウサイズY  */
	private static final String XML_NODE_WINDOWSIZEY = "WindowSizeY";
	/** XMLタグ ズーム比  */
	private static final String XML_NODE_ZOOM = "Zoom";
	/** XMLタグ グラフサイズX */
	private static final String XML_NODE_GRAPHSIZEX = "GraphSizeX";
	/** XMLタグ グラフサイズY  */
	private static final String XML_NODE_GRAPHSIZEY = "GraphSizeY";
	/** XMLタグ ベースポイントX */
	private static final String XML_NODE_BASEPOINTX = "BasePointX";
	/** XMLタグ ベースポイントY  */
	private static final String XML_NODE_BASEPOINTY = "BasePointY";


	/**
	 *  ローカル保存ディレクトリ名取得
	 * @return
	 */
	private static String getSemLocalDirectory() {
		// SemanticEditor ディレクトリ
		String sem = HOME_DIRECTORY + File.separator + SEM_DIRECTORY;

		return sem;
	}

	/**
	 *  ローカル保存ディレクトリ名チェック
	 * @return
	 */
	private static boolean isSemLocalDirectory() {
		String semDirectory = getSemLocalDirectory();

		File dir = new File(semDirectory);
	    if (dir.isDirectory()) {
	    	return true;
	    } else {
	    	return false;
	    }
	}

	/**
	 *  ローカル保存ディレクトリ作成
	 */
	private static void makeSemDirectory() {
		String semDirectory = getSemLocalDirectory();

		File dir = new File(semDirectory);
		dir.mkdir();
	}

	/**
	 *  ローカル保存ファイル名取得
	 * @return
	 */
	private static String getSemLocalFile(String nodeId) {

		// ノードId
		// ファイル名に先頭の「#」は使えないので、2文字目以降を使用
		if (nodeId.startsWith("#")) {
			nodeId = nodeId.substring(1);
		}

		// SemanticEditor ファイル
		String semFileName = getSemLocalDirectory() + File.separator + nodeId + SEM_FILENAME_EXT;

		return semFileName;
	}

	/**
	 *  ローカル保存ファイル名チェック
	 * @return
	 */
	private static boolean isSemLocalFile(String nodeId) {
		String semFileName = getSemLocalFile(nodeId);

		File file = new File(semFileName);
	    if (file.isFile()) {
	    	return true;
	    } else {
	    	return false;
	    }
	}

	/**
	 *  ウィンドウ情報用XML作成、ファイル保存
	 * @param graphActor
	 * @return
	 */
	public static void writeWindowInfoFile(GraphActor graphActor) {
		//logger.debug("◆START");

		// グラフId
		// 2021/2 AW REP start
		String graphId = graphActor.getGraphData().getGraphNode().getNodeId();
		//String graphId = graphActor.getGraphNode().getNodeId();
		// 2021/2 AW REP end

		// 保存ディレクトリチェック
		if (!isSemLocalDirectory()) {
			// ディレクトリがない場合は作る
			makeSemDirectory();
		}
		try (FileOutputStream fos = new FileOutputStream(new File(getSemLocalFile(graphId)))) {

			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			// root elements
			Document doc = docBuilder.newDocument();
			Element rootElement = doc.createElement(XML_NODE_ROOT);
			doc.appendChild(rootElement);

			// window elements
			Element localWindow = doc.createElement(XML_NODE_LOCALWINDOW);
			rootElement.appendChild(localWindow);

			// set attribute to window element
			Attr attr = doc.createAttribute(XML_ATTR_LOCALWINDOW_ID);
			// グラフノードId
			attr.setValue(graphId);
			localWindow.setAttributeNode(attr);

			//sizeX elements
			Element sizeX = doc.createElement(XML_NODE_WINDOWSIZEX);
			// ウィンドウサイズX
			sizeX.appendChild(doc.createTextNode(String.valueOf(graphActor.getGraphData().getWindowSizeX())));
			localWindow.appendChild(sizeX);

			//sizeY elements
			Element sizeY = doc.createElement(XML_NODE_WINDOWSIZEY);
			// ウィンドウサイズY
			sizeY.appendChild(doc.createTextNode(String.valueOf(graphActor.getGraphData().getWindowSizeY())));
			localWindow.appendChild(sizeY);

			//rootX elements
			Element rootX = doc.createElement(XML_NODE_WINDOWROOTX);
			// ウィンドウルート位置X
			rootX.appendChild(doc.createTextNode(String.valueOf(graphActor.getGraphData().getWindowRootX())));
			localWindow.appendChild(rootX);

			//rootY elements
			Element rootY = doc.createElement(XML_NODE_WINDOWROOTY);
			// ウィンドウルート位置Y
			rootY.appendChild(doc.createTextNode(String.valueOf(graphActor.getGraphData().getWindowRootY())));
			localWindow.appendChild(rootY);

			//zoom elements
			Element zoom = doc.createElement(XML_NODE_ZOOM);
			// ズーム比
			zoom.appendChild(doc.createTextNode(String.valueOf(graphActor.getGraphData().getZoom())));
			localWindow.appendChild(zoom);

			//2020/11 AW ADD strat
			//graphX elements
			Element graphX = doc.createElement(XML_NODE_GRAPHSIZEX);
			// グラフサイズX
			graphX.appendChild(doc.createTextNode(String.valueOf(graphActor.getGraphData().getGraphSizeX())));
			localWindow.appendChild(graphX);

			//graphY elements
			Element graphY = doc.createElement(XML_NODE_GRAPHSIZEY);
			// グラフサイズX
			graphY.appendChild(doc.createTextNode(String.valueOf(graphActor.getGraphData().getGraphSizeY())));
			localWindow.appendChild(graphY);

			//basePointX elements
			Element basePointX = doc.createElement(XML_NODE_BASEPOINTX);
			// ベースポイントX
			basePointX.appendChild(doc.createTextNode(String.valueOf(graphActor.getGraphData().getBasePointX())));
			localWindow.appendChild(basePointX);

			//basePointY elements
			Element basePointY = doc.createElement(XML_NODE_BASEPOINTY);
			// ベースポイントY
			basePointY.appendChild(doc.createTextNode(String.valueOf(graphActor.getGraphData().getBasePointY())));
			localWindow.appendChild(basePointY);
			//2020/11 AW ADD end

			// 保存
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(fos);

			//Output to console for testing
			//StreamResult result = new StreamResult(System.out);

			transformer.transform(source, result);

			//logger.debug("write. windowSizeX={}. windowSizeY={}", String.valueOf(graphActor.getGraphData().getWindowSizeX()), String.valueOf(graphActor.getGraphData().getWindowSizeY()));
			//logger.debug("write. windowRootX={}. windowRootY={}", String.valueOf(graphActor.getGraphData().getWindowRootX()), String.valueOf(graphActor.getGraphData().getWindowRootY()));
			//logger.debug("write. zoom={}", String.valueOf(graphActor.getGraphData().getZoom()));
			//logger.debug("write. graphSizeX={}. graphSizeY={}", String.valueOf(graphActor.getGraphData().getGraphSizeX()), String.valueOf(graphActor.getGraphData().getGraphSizeY()));
			//logger.debug("write. basePointX={}. basePointY={}", String.valueOf(graphActor.getGraphData().getBasePointX()), String.valueOf(graphActor.getGraphData().getBasePointY()));
			//logger.debug("◆END");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}



	/**
	 *  ウィンドウ情報用XMLファイル読み込み
	 * @param graphActor
	 */
	public static void readWindowSizeFile(GraphActor graphActor) {
		//logger.debug("◆START");

		// グラフId
		// 2021/2 AW REP start
		String graphId = graphActor.getGraphData().getGraphNode().getNodeId();
		//String graphId = graphActor.getGraphNode().getNodeId();
		// 2021/2 AW REP end

		// ファイルがない場合、0で戻す
		if (!isSemLocalFile(graphId)) {
			graphActor.getGraphData().setWindowSizeX(0);
        	graphActor.getGraphData().setWindowSizeY(0);

        	//logger.debug("◆END File not found.");
        	return;
		}

		try (FileInputStream fis = new FileInputStream(new File(getSemLocalFile(graphId)))) {

			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fis);

			//optional, but recommended
			doc.getDocumentElement().normalize();

			// ルートノードの確認はしない

			// ローカルウィンドウタグ
			NodeList windowNodeList = doc.getElementsByTagName(XML_NODE_LOCALWINDOW);
			for (int i = 0; i < windowNodeList.getLength(); i++) {

				Node windowNode = windowNodeList.item(i);
				if (windowNode.getNodeType() == Node.ELEMENT_NODE) {

					Element element = (Element) windowNode;
					// Id
					String nodeId = element.getAttribute(XML_ATTR_LOCALWINDOW_ID);
					// IdがgraphIdと同じ場合に情報を取得
					// 2021/2 AW REP start
					if (nodeId.equals(graphActor.getGraphData().getGraphNode().getNodeId())) {
					//if (nodeId.equals(graphActor.getGraphNode().getNodeId())) {
					// 2021/2 AW REP end
						String windowSizeX = element.getElementsByTagName(XML_NODE_WINDOWSIZEX).item(0).getTextContent();
						String windowSizeY = element.getElementsByTagName(XML_NODE_WINDOWSIZEY).item(0).getTextContent();
						String windowRootX = element.getElementsByTagName(XML_NODE_WINDOWROOTX).item(0).getTextContent();
						String windowRootY = element.getElementsByTagName(XML_NODE_WINDOWROOTY).item(0).getTextContent();
						String zoom = element.getElementsByTagName(XML_NODE_ZOOM).item(0).getTextContent();
						//2020/11 AW ADD strat
						String graphSizeX = element.getElementsByTagName(XML_NODE_GRAPHSIZEX).item(0).getTextContent();
						String graphSizeY = element.getElementsByTagName(XML_NODE_GRAPHSIZEY).item(0).getTextContent();
						String basePointX = element.getElementsByTagName(XML_NODE_BASEPOINTX).item(0).getTextContent();
						String basePointY = element.getElementsByTagName(XML_NODE_BASEPOINTY).item(0).getTextContent();
						//2020/11 AW ADD end

						graphActor.getGraphData().setWindowSizeX(Double.parseDouble(windowSizeX));
						graphActor.getGraphData().setWindowSizeY(Double.parseDouble(windowSizeY));
						graphActor.getGraphData().setWindowRootX(Double.parseDouble(windowRootX));
						graphActor.getGraphData().setWindowRootY(Double.parseDouble(windowRootY));
						graphActor.getGraphData().setZoom(Double.parseDouble(zoom));
						//2020/11 AW ADD strat
						graphActor.getGraphData().setGraphSizeX(Double.parseDouble(graphSizeX));
						graphActor.getGraphData().setGraphSizeY(Double.parseDouble(graphSizeY));
						graphActor.getGraphData().setBasePointX(Double.parseDouble(basePointX));
						graphActor.getGraphData().setBasePointY(Double.parseDouble(basePointY));
						//2020/11 AW ADD end

						//logger.debug("read. windowSizeX={}. windowSizeY={}", windowSizeX, windowSizeY);
						//logger.debug("read. windowRootX={}. windowRootY={}", windowRootX, windowRootY);
						//logger.debug("read. zoom={}", zoom);
						//logger.debug("read. graphSizeX={}. graphSizeY={}", graphSizeX, graphSizeY);
						//logger.debug("read. basePointX={}. basePointY={}", basePointX, basePointY);
						//logger.debug("◆END");
						return;
					}
				}
			}

			// XMLファイルに同じノードIDのデータがない場合は0を返す
			graphActor.getGraphData().setWindowSizeX(0);
			graphActor.getGraphData().setWindowSizeY(0);

			//logger.debug("◆END nodeId not found. Set 0,0.");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
