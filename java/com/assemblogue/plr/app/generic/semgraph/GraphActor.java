package com.assemblogue.plr.app.generic.semgraph;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Queue;
import java.util.Stack;

import com.assemblogue.plr.app.generic.semgraph.ui.controller.AppController;
import com.assemblogue.plr.app.lib.FriendInfoUtils;
import com.assemblogue.plr.app.lib.ProfileManager;
import com.assemblogue.plr.contentsdata.PLRContentsData;
import com.assemblogue.plr.contentsdata.ontology.OntologyItem;
import com.assemblogue.plr.lib.EntityNode;
import com.assemblogue.plr.lib.FileNode;
import com.assemblogue.plr.lib.Node;
import com.assemblogue.plr.lib.model.Channel;
import com.assemblogue.plr.lib.model.ProfileItem;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;

/**
 * グラフ操作クラス ルートノード直下のアプリフォルダノードにグラフとしてファイルノードを作成する。
 *Graph operation class Creates a file node as a graph in an app folder node directly under the root node.
 *
 * @author <a href="mailto:kaneko@cri-mw.co.jp">KANEKO, yukinori</a>
 */
public class GraphActor {

	private AppController appController;
	private OssActor ossActor;
	public OssActor getOssActor() {
		return ossActor;
	}

	/** このGraphActorのチャネル */
	/** GraphActor's channel */
	private Channel channel;
	public Channel getChannel() {
		return channel;
	}

	private PlrActor plrAct;

	public Stack<command> commandRecord;
	public boolean undoSwitch;

	// 定数

	// 2021/2 AW ADD start
	//timelinetopnodeのプロパティ名
	//Property name of timelinetopnode
	static final String LINKNODE_NAME = "@link";
	static final String NODE_NAME = "@node";
	static final String PROPERTY_NAME_CNT = "cnt";

	//type;nodeのプロパティ名
	//type;node property name
	static final String PROPERTY_NAME_X = "x";
	static final String PROPERTY_NAME_Y = "y";
	static final String PROPERTY_NAME_NODE = "node";

	//type;linkのプロパティ名
	//Property name for type;link
	static final String PROPERTY_NAME_TYPE = "type";
	static final String PROPERTY_NAME_LINK = "link";
	static final String PROPERTY_NAME_N1 = "n1";
	static final String PROPERTY_NAME_N2 = "n2";

	static final String DATEFORMAT = "yyyy-MM-dd'T'HH:mmZ";
	// 2021/2 AW ADD end

	// グラフデータ
	//Graph data
	private GraphData graphData = new GraphData();


	////////////////////////////////////////////////////////////
	// inner Class (Data)
	////////////////////////////////////////////////////////////

	/**
	 * commandRecoad Class
	 */
	private class command {
		String methodName;
		EntityNode n1;
		EntityNode n2;
		LinkInfo e1;
		String rel;
		String cnt;
		String coords;
		String nodeid;

		command(String methodName, EntityNode n1, EntityNode n2, String rel, String cnt, String coords,String nodeid) {
			this.methodName = methodName;
			this.n1 = n1;
			this.n2 = n2;
			this.rel = rel;
			this.cnt = cnt;
			this.coords = coords;
			this.nodeid= nodeid;
		}

		command(String methodName, LinkInfo newEdge, EntityNode n2, String rel, String cnt, String coords,String nodeid) {
			this.methodName = methodName;
			this.n2 = n2;
			this.rel = rel;
			this.cnt = cnt;
			this.coords = coords;
			this.nodeid= nodeid;
		}
	}

	/**
	 * リレーション（リンク）を表すクラス
	 * Edges class(Tree structure)
	 */
	public class LinkInfo {

		/** リンクの元のノードID */
		private String sourceNodeId;
		/** リンクの先のノードID */
		private String targetNodeId;
		/** リンクのタイプ */
		private String linkType;
		/** リンク情報のEntityNodeのID */
		private String linkEntityNodeId;

		/**
		 * コンストラクタ
		 * @param sourceNodeId
		 * @param targetNodeId
		 * @param linkType
		 * @param linkEntityNodeId
		 */
		public LinkInfo(String sourceNodeId, String targetNodeId, String linkType, String linkEntityNodeId) {
			//logger.trace("◆START");

			this.sourceNodeId = sourceNodeId;
			this.targetNodeId = targetNodeId;
			this.linkType = linkType;
			this.linkEntityNodeId = linkEntityNodeId;
		}

		public String getSourceNodeId() {
			return sourceNodeId;
		}

		public void setSourceNodeId(String sourceNodeId) {
			this.sourceNodeId = sourceNodeId;
		}

		public String getTargetNodeId() {
			return targetNodeId;
		}

		public void setTargetNodeId(String targetNodeId) {
			this.targetNodeId = targetNodeId;
		}

		public String getLinkType() {
			return linkType;
		}

		public void setLinkType(String linkType) {
			this.linkType = linkType;
		}

		public String getLinkEntityNodeId() {
			return linkEntityNodeId;
		}

		public void setLinkEntityNodeId(String linkEntityNodeId) {
			this.linkEntityNodeId = linkEntityNodeId;
		}

	}


	/**
	 * ノード情報クラス
	 * Node information class
	 */
	public class GraphNodeInfo {

		/** グラフノードのノードId
		 * Node Id of the graph node */
		private String graphNodeId;
		/** グラフノードのEntityNode
		 * EntityNode of the graph node */
		private EntityNode graphNodeEntity;

		/** 位置情報のノードId
		 * Node Id for location information */
		private String coordinateNodeId;
		/** 位置情報のEntityNode
		 *  EntityNode for location information*/
		private EntityNode coordinateEntity;

		/** クラス
		 *  class */
		private String classType;
		/** クラスのオントロジーアイテム
		 * Class ontology items */
		private OntologyItem ontologyItem;

		/** ノード表示用の作成者、creator */
		private String displayCreator;
		/** ノード表示用のイメージ */
		private Image image;

		/** 内容(cnt) */
		private List<String> cntList;

		/** 位置座標x
		 * Position coordinate x*/
		private double x;
		/** 位置座標y
		 * Position coordinate y*/
		private double y;

		/**
		 * コンストラクタ
		 * constructor
		 * @param graphNodeEntity
		 * @param coordinateEntity
		 */
		GraphNodeInfo(EntityNode graphNodeEntity, EntityNode coordinateEntity) {
			this.graphNodeEntity = graphNodeEntity;
			this.coordinateEntity = coordinateEntity;

			this.graphNodeId = graphNodeEntity.getNodeId();
			this.coordinateNodeId = coordinateEntity.getNodeId();

			// クラスは複数にはならないと思うので
			this.classType = graphNodeEntity.getType().get(0);
			this.ontologyItem = graphData.contentsData.getNode('#' + classType);

			this.displayCreator = getDisplayName(graphNodeEntity);
			this.image = getDisplayImage(graphNodeEntity);
		}

		public String getCoordinateNodeId() {
			return coordinateNodeId;
		}

		public void setCoordinateNodeId(String coordinateNodeId) {
			this.coordinateNodeId = coordinateNodeId;
		}

		public String getGraphNodeId() {
			return graphNodeId;
		}

		public void setGraphNodeId(String graphNodeId) {
			this.graphNodeId = graphNodeId;
		}

		public List<String> getCntList() {
			return cntList;
		}

		public void setCntList(List<String> cntList) {
			this.cntList = cntList;
		}

		public double getX() {
			return x;
		}

		public void setX(double x) {
			this.x = x;
		}

		public double getY() {
			return y;
		}

		public void setY(double y) {
			this.y = y;
		}

		public EntityNode getGraphNodeEntity() {
			return graphNodeEntity;
		}

		public void setGraphNodeEntity(EntityNode graphNodeEntity) {
			this.graphNodeEntity = graphNodeEntity;
		}

		public EntityNode getCoordinateEntity() {
			return coordinateEntity;
		}

		public void setCoordinateEntity(EntityNode coordinateEntity) {
			this.coordinateEntity = coordinateEntity;
		}

		public String getClassType() {
			return classType;
		}

		public void setClassType(String classType) {
			this.classType = classType;
		}

		public OntologyItem getOntologuItem() {
			return ontologyItem;
		}

		public void setOntologuItem(OntologyItem ontologuItem) {
			this.ontologyItem = ontologuItem;
		}

		public String getDisplayCreator() {
			return displayCreator;
		}

		public void setDisplayCreator(String displayCreator) {
			this.displayCreator = displayCreator;
		}

		public Image getImage() {
			return image;
		}

		public void setImage(Image image) {
			this.image = image;
		}
	}


	/**
	 * グラフデータ（RootLayoutControllerに描くウィンドウ情報、ノード情報、リンク情報をもつ）
	 * Graph data (with window information, node information, and link information drawn on RootLayoutController)
	 */
	public class GraphData {

		/** グラフノード（位置情報、リンク情報、graphSettingを持つノード）
		 * Graph nodes (nodes with location information, link information, and graphSetting)*/
		private EntityNode graphNode = null;
		/** グラフの名前
		 * GraphName */
		private String graphName = null;

		/** 親のグラフのノード
		 * Parent graph node*/
		private EntityNode parentNode = null;

		//ノードリスト
		//node List
		List<GraphNodeInfo> graphNodeInfoList = new ArrayList<GraphNodeInfo>();

		//リンクリスト
		//Link list
		List<LinkInfo> linkList = new ArrayList<LinkInfo>();

		/** 更新時間
		 * Update time*/
		private long updateTime;

		/** グラフサイズ(x)
		 * Graph size (x) */
		private double graphSizeX;

		/** グラフサイズ(y)
		 * Graph size (y) */
		private double graphSizeY;

		/** 表示ウィンドウ起点(x)
		 * Display window starting point (x) */
		private double windowRootX;

		/** 表示ウィンドウ起点(y)
		 * Display window starting point (y) */
		private double windowRootY;

		/** 表示ウィンドウサイズ(x)
		 *  Display window size (x) */
		private double windowSizeX;

		/** 表示ウィンドウサイズ(y)
		 *  Display window size (y)*/
		private double windowSizeY;

		/** ズーム比
		 * Zoom ratio  */
		private double zoom;

		/** ベースポイント(x)
		 *  Base point (x) */
		private double basePointX;

		/** ベースポイント(y)
		 *  Base point (y)*/
		private double basePointY;

		/** PLRContentsData */
		private PLRContentsData contentsData;

		/** リンク用PLRContentsData */
		private PLRContentsData linkContentsData;


		public EntityNode getGraphNode() {
			return graphNode;
		}

		public void setGraphNode(EntityNode graphNode) {
			this.graphNode = graphNode;
		}

		public String getGraphName() {
			return graphName;
		}

		public void setGraphName(String graphName) {
			this.graphName = graphName;
		}

		public List<GraphNodeInfo> getGraphNodeInfoList() {
			return this.graphNodeInfoList;
		}

		public void setGraphNodeInfoList(List<GraphNodeInfo> graphNodeInfoList) {
			this.graphNodeInfoList = graphNodeInfoList;
		}

		public List<LinkInfo> getLinkList() {
			return this.linkList;
		}

		public void setLinkList(List<LinkInfo> linkList) {
			this.linkList = linkList;
		}

		/**
		 * 指定されたNodeIdのノード情報を返す
		 * @param nodeId
		 * @return
		 */
		public GraphNodeInfo getGraphNodeInfo(String nodeId) {
			for (GraphNodeInfo graphNodeInfo: this.graphNodeInfoList) {
				if (graphNodeInfo.getGraphNodeId().equals(nodeId)) {
					return graphNodeInfo;
				}
			}
			return null;
		}

		/**
		 * graphNodeInfoListにメンバ追加
		 * @param graphNodeInfo
		 */
		public void addGraphNodeInfo(GraphNodeInfo graphNodeInfo) {
			this.graphNodeInfoList.add(graphNodeInfo);
		}

		/**
		 * graphNodeInfoListからメンバ削除
		 * @param nodeId
		 */
		public void removeGraphNodeInfo(String nodeId) {

			GraphNodeInfo target = null;
			for (GraphNodeInfo graphNodeInfo: this.graphNodeInfoList) {
				if (graphNodeInfo.getGraphNodeId().equals(nodeId)) {
					target = graphNodeInfo;
					break;
				}
			}
			if (target != null) {
				this.graphNodeInfoList.remove(target);
			}
		}

		/**
		 * リンクリストにメンバ追加
		 * @param link
		 */
		public void addLinkInfo(LinkInfo linkInfo) {
			this.linkList.add(linkInfo);
		}

		/**
		 * リンクリストからメンバ削除
		 * @param link
		 */
		//FIXME
		public void deleteLink(LinkInfo link) {
			if(!(this.linkList.isEmpty())) {
				if(this.linkList.contains(link)) {
					this.linkList.remove(link);
				}
			}
		}

		public long getUpdateTime() {
			return updateTime;
		}

		public void setUpdateTime(long updateTime) {
			this.updateTime = updateTime;
		}

		public double getGraphSizeX() {
			return graphSizeX;
		}

		public void setGraphSizeX(double graphSizeX) {
			this.graphSizeX = graphSizeX;
		}

		public double getGraphSizeY() {
			return graphSizeY;
		}

		public void setGraphSizeY(double graphSizeY) {
			this.graphSizeY = graphSizeY;
		}

		public double getWindowRootX() {
			return windowRootX;
		}

		public void setWindowRootX(double windowRootX) {
			this.windowRootX = windowRootX;
		}

		public double getWindowRootY() {
			return windowRootY;
		}

		public void setWindowRootY(double windowRootY) {
			this.windowRootY = windowRootY;
		}

		public double getWindowSizeX() {
			return windowSizeX;
		}

		public void setWindowSizeX(double windowSizeX) {
			this.windowSizeX = windowSizeX;
		}

		public double getWindowSizeY() {
			return windowSizeY;
		}

		public void setWindowSizeY(double windowSizeY) {
			this.windowSizeY = windowSizeY;
		}

		public double getZoom() {
			return zoom;
		}

		public void setZoom(double zoom) {
			this.zoom = zoom;
		}

		public double getBasePointX() {
			return basePointX;
		}

		public void setBasePointX(double basePointX) {
			this.basePointX = basePointX;
		}

		public double getBasePointY() {
			return basePointY;
		}

		public void setBasePointY(double basePointY) {
			this.basePointY = basePointY;
		}

		public PLRContentsData getContentsData() {
			return contentsData;
		}

		public void setContentsData(PLRContentsData contentsData) {
			this.contentsData = contentsData;
		}

		public PLRContentsData getLinkContentsData() {
			return linkContentsData;
		}

		public void setLinkContentsData(PLRContentsData linkContentsData) {
			this.linkContentsData = linkContentsData;
		}

		public EntityNode getParentNode() {
			return parentNode;
		}

		public void setParentNode(EntityNode parentNode) {
			this.parentNode = parentNode;
		}
	}

	/**
	 * グラフのデータを取得する
	 *  Retrieving data from a graph
	 * @return
	 */
	public GraphData getGraphData() {
		return graphData;
	}

	/**
	 * グラフのデータを設定する
	 *  Setting the data for the graph
	 * @param graphData
	 */
	public void setGraphData(GraphData graphData) {
		this.graphData = graphData;
	}


	/**
	 * コンストラクタ
	 * 空のGraphDataを作成する
	 * Create an empty GraphData
	 */
	public GraphActor(AppController appController) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		// plrActor設定
		this.plrAct = AppController.plrAct;

		this.appController = appController;
		this.ossActor = new OssActor();

		this.undoSwitch = false;
		this.commandRecord = new Stack<command>();
	}

	/**
	 * コンストラクタ
	 * 指定されたグラフのGraphDataを作成する
	 * Creates a GraphData for a given graph.
	 * @param node
	 * @param appController
	 * @param channel
	 */
	public GraphActor(EntityNode node, AppController appController, Channel channel) {
		this(node, null, appController, channel);
	}


	/**
	 * コンストラクタ
	 * 指定されたグラフのGraphDataを作成する
	 * Creates a GraphData for a given graph.
	 * 新規グラフはPLRに該当Nodeも作成する
	 * Create a new graph corresponding to PLR Node
	 * @param node
	 * @param parentNode
	 * @param appController
	 * @param channel
	 */
	public GraphActor(EntityNode node, EntityNode parentNode, AppController appController, Channel channel) {
		this(appController);

		this.channel = channel;

		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		// チャネルに紐づくグラフノードの場合
		//For a graph node associated with a channel
		if (node == null) {
			//logger.debug("新規グラフ");
			// 新規グラフ
			//New graph
			//（コントロールタイムラインノードを作成
			//Create a control timeline node
			this.createNewGraph(channel);

		} else {
			//logger.debug("既存グラフ");
			// 既存グラフ
			//Existing graphs
			// PLRからGraphDataを設定する
			//Setting up GraphData from PLR
			this.openGraph(node, parentNode);
		}

	}

	/**
	 * 新規グラフ生成
	 * Generate new graph
	 * @return
	 */
	public EntityNode createNewGraph(Channel channel) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		// チャネルのルートノードを取得
		//Get the root node of a channel
		EntityNode channelRootNode = channel.getNode();

		// チャネル名取得、とりあえずデフォルト名を取得
		//Get channel name, default name for now
		String channelName = channel.getDefaultName();
		graphData.setGraphName(channelName);

		// チャネルのルートノード配下にGraphのトップノードを作成(PLR)
		//Create Graph's top node under the channel's root node (PLR)
		EntityNode graphTopNode = plrAct.createTopGraphNode(channelRootNode);
		if (graphTopNode == null) {
			return null;
		}

		// graphSetting作成
		//Create graphSetting
		// チャネルのDataSettingから取得する
		//Get from channel DataSetting
		Map<String, String> ossMap = appController.getOssMap();
		boolean ossThreadFlag = appController.isOssThreadFlag();
		List<FileNode> ossList = appController.getOssList();

		List<FileNode> graphSetting = plrAct.getChannelSchemaOntology(channel);

		TinyLog.trace(new Throwable().getStackTrace()[0], false, "channelDataSetting size=" + graphSetting.size());
		for (FileNode fn: graphSetting) {
			TinyLog.trace(new Throwable().getStackTrace()[0], false, "ontology=" + fn.getTitle());
		}

		//TODO
		// Baseと談話関係のFileNode追加
		//Add Base and discourse related FileNode
		//List<FileNode> graphSetting = ossActor.getBasicFileNode(ossList, ossMap, ossThreadFlag, channelDataSetting);
		ossActor.addBasicContentsData(ossList, ossMap, ossThreadFlag, graphSetting);

		// graphSetting作成
		//Create graphSetting
		plrAct.makeGraphSetting(graphTopNode);
		plrAct.setGraphSetting(graphSetting, graphTopNode);

		// 同期
		//Synchronisation
		plrAct.syncSimple(graphTopNode);

		// 作成したノードをGraphDataにセット
		//Set the created node to GraphData
		graphData.setGraphNode(graphTopNode);

		// PLRContentsData作成
		//Create PLRContentsData
		ossActor.makeContentsData(graphSetting, ossMap, ossThreadFlag);

		PLRContentsData contentsData = ossActor.getEventOss();
		PLRContentsData linkContentsData = ossActor.getLinkOss();
		graphData.setContentsData(contentsData);
		graphData.setLinkContentsData(linkContentsData);

		// 更新日時取得
		// Obtained at the time of the update
		long time = Utils.currentEpochtime();
		// 更新日時設定
		//Updated Daytime Settings
		graphData.setUpdateTime(time);

		return graphTopNode;
	}

	/**
	 * PLRからGraphDataにデータを格納する
	 *  Store data from PLR into GraphData
	 * @param node
	 * @param parentNode
	 * @return
	 */
	public EntityNode openGraph(EntityNode node, EntityNode parentNode) {
		return openGraph(node, parentNode, null);
	}

	/**
	 * PLRからGraphDataにデータを格納する
	 * Store data from PLR into GraphData
	 * バックグラウンドタスクから呼ばれた場合
	 * If called by a background task
	 * @param syncTimelineList
	 * @return
	 */
	public EntityNode openGraph(Collection<EntityNode> syncTimelineList) {
		return openGraph(null, null, syncTimelineList);
	}

	/**
	 * PLRからGraphDataにデータを格納する
	 * Store data from PLR into GraphData
	 * @param
	 * @return
	 */
	private EntityNode openGraph(EntityNode node, EntityNode parentNode, Collection<EntityNode> syncTimelineList) {
		TinyLog.debug(new Throwable().getStackTrace()[0], false, "◆START");

		// チャネルのルートノード
		//Root node of channel
		EntityNode channelNode;
		// タイムラインアイテムリスト
		//Timeline item list
		Collection<EntityNode> timelineList;

		// バックグラウンド同期から呼ばれた場合は、同期しない
		//No sync if called from background sync
		if (syncTimelineList == null) {

			if (node == null) {
				return null;
			}

			graphData.setGraphNode(node);
			if (parentNode != null) {
				graphData.setParentNode(parentNode);
			}

			// チャネルのルートノード
			//Root node of channel
			channelNode = node.getTimelineRootNode();

			// タイムラインアイテムリスト取得（同期）
			//Get timeline item list (sync)
			timelineList = plrAct.getTimelineItemsWithSync(channelNode);

		} else {

			// チャネルのルートノード
			//Root node of channel
			channelNode = graphData.getGraphNode().getTimelineRootNode();

			// タイムラインアイテムリスト
			//Timeline item list
			timelineList = syncTimelineList;
		}

		//-------------------------------------------------------------------------
		// これ以降、本メソッド内でnode parentNodeを使用してはいけない！
		//-------------------------------------------------------------------------

		// コンテンツデータ
		//Content data
		List<FileNode> ossList = appController.getOssList();
		Map<String, String> ossMap = appController.getOssMap();
		boolean ossThreadFlag = appController.isOssThreadFlag();

		List<FileNode> graphSetting;
		if (plrAct.isGraphSetting(graphData.getGraphNode())) {
			// graphSetting がある場合
			// graphSettingから選択済みオントロジーを取得
			//If graphSetting is present get selected ontology from graphSetting
			graphSetting = plrAct.getGraphSetting(graphData.getGraphNode());

			TinyLog.trace(new Throwable().getStackTrace()[0], false, "graph Setting exist. count=" + graphSetting.size());

			//TODO
			// 基本と談話関係のオントロジーが無い場合は追加する

		} else {

			// topGraphの場合はチャネルから、ハイパーノードの場合は親グラフから
			// graphSettingの値を引き継ぐ
			//from a channel in the case of a topGraph, or from a parent graph in the case of a hypernode Inherit the value of graphSetting
			//List<FileNode> ontologyList = null;
			if (graphData.getParentNode() == null) {
				graphSetting = plrAct.getChannelSchemaOntology(channel);
				TinyLog.trace(new Throwable().getStackTrace()[0], false, "graph Setting from channel. count=" + graphSetting.size());
			} else {
				graphSetting = plrAct.getGraphSetting(graphData.getParentNode());
				TinyLog.trace(new Throwable().getStackTrace()[0], false, "graph Setting from parent graph. count=" + graphSetting.size());
			}

			//TODO
			// 基本と談話関係のオントロジーが無い場合は追加する
			ossActor.addBasicContentsData(ossList, ossMap, ossThreadFlag, graphSetting);

			// graphSetting作成
			//Create graphSetting
			plrAct.makeGraphSetting(graphData.getGraphNode());
			plrAct.setGraphSetting(graphSetting, graphData.getGraphNode());
		}

		ossActor.makeContentsData(graphSetting, ossMap, ossThreadFlag);

		PLRContentsData contentsData = ossActor.getEventOss();
		PLRContentsData linkContentsData = ossActor.getLinkOss();
		graphData.setContentsData(contentsData);
		graphData.setLinkContentsData(linkContentsData);


		// グラフの名前取得
		// トップグラフの場合はチャネル名
		// ハイパーノードグラフの場合はハイパーノードの内容の1番目から取得
		// Get the name of the graph for a top graph, the channel name and for a hyper-node graph, get the first hyper-node content
		String graphName = null;
		String topGraphId = null;

		//TODO
		Map<String, com.assemblogue.plr.lib.Node> properties = plrAct.listToMap(channelNode);
		if (properties.containsKey(AppProperty.LITERAL_TOP_GRAPH)) {
			com.assemblogue.plr.lib.Node literal = properties.get(AppProperty.LITERAL_TOP_GRAPH);
			topGraphId = literal.asLiteral().getValue().toString();
		}

		String graphId = graphData.getGraphNode().getNodeId();
		if (graphId.equals(topGraphId)) {
			graphName = channel.getDefaultName();
			//graphName =  plrAct.getRootChannelDefaultName();

		} else {
			// 内容の1番目から取得
			//Get from the first of the contents
			String cnt = getLiteral(graphData.getGraphNode(), PROPERTY_NAME_CNT);
			if (cnt == null) {
				// 内容が無い場合、クラス名を入れたいが、
				// ハイパーノードグラフの場合、初期はそのオントロジーが含まれないため取得できない
				// とりあえず、no cntと出力する
				/**If there is no content, we want to include the class name, but in the case of a hypernode graph,
				/we can't get it because it doesn't initially contain its ontology so output has been set as no cnt for now **/
				//String classType = graphData.getGraphNode().getType().get(0);
				//OntologyItem oi = graphData.getContentsData().getNode('#' + classType);
				//graphName = OssActor.getDisplayText(oi);
				graphName = "no cnt.";
			} else {
				graphName = cnt;
			}
		}
		graphData.setGraphName(graphName);

		//TODO
		//Map<String, com.assemblogue.plr.lib.Node> propertiesTest = plrAct.listToMap(node);

		// ノード情報graphNodeInfo作成
		//Create node information graphNodeInfo

		List<GraphNodeInfo> nodeList = new ArrayList<GraphNodeInfo>();

		//FIXME
		TinyLog.debug(new Throwable().getStackTrace()[0], false, "graphId=" + graphId);
		List<Node> aList = returnNodeList(NODE_NAME);
		TinyLog.debug(new Throwable().getStackTrace()[0], false, "coordListSize=" + aList.size());
		for (Node no: aList) {
			EntityNode en = no.asEntity();
			TinyLog.debug(new Throwable().getStackTrace()[0], false, "coordId=" + en.getNodeId());
			List<Node> timelineNodeList = en.getProperty("node");
			String timelineId = (String) timelineNodeList.get(0).asLiteral().getValue();
			TinyLog.debug(new Throwable().getStackTrace()[0], false, "timeLineId=" + timelineId);
		}
		for (EntityNode entityNode : timelineList) {
			TinyLog.debug(new Throwable().getStackTrace()[0], false, "timeline id=" + entityNode.getNodeId());
		}
		//FIXME

		for (Node atNode :returnNodeList(NODE_NAME)) {

			EntityNode coordinateNode = atNode.asEntity();
			String coordinateNodeGraphId = getLiteral(coordinateNode,PROPERTY_NAME_NODE);

			EntityNode graphNode = null;
			for (EntityNode entityNode : timelineList) {
				if (coordinateNodeGraphId.equals(entityNode.getId())) {
					graphNode = entityNode;
					TinyLog.debug(new Throwable().getStackTrace()[0], false, "display timelineItemId=" + entityNode.getNodeId());
					break;
				}
			}

			//FIXME if追加
			if (graphNode != null) {
				GraphNodeInfo tmpNode = new GraphNodeInfo(graphNode, coordinateNode);

				tmpNode.setX(Double.valueOf(getLiteral(coordinateNode, PROPERTY_NAME_X)));
				tmpNode.setY(Double.valueOf(getLiteral(coordinateNode, PROPERTY_NAME_Y)));

				//FIXME
				// cntのリスト対応は、ちゃんとできていない
				String cnt = getLiteral(graphNode,PROPERTY_NAME_CNT);
				if (cnt == null) {
					tmpNode.setCntList(new ArrayList<String>());
				} else {
					tmpNode.setCntList(List.of(cnt));
				}

				// graphNodeがPLRContentsDataに含まれないクラスの場合は
				// 表示対象から外す必要がある
				//// If graphNode is a class that is not included in PLRContentsData it needs to be removed from the display
				String classType = graphNode.getType().get(0);
				OntologyItem oi = graphData.getContentsData().getNode('#' + classType);
				if (oi == null) {
					continue;
				}

				nodeList.add(tmpNode);
			}


		}
		this.graphData.setGraphNodeInfoList(nodeList);


		// リンク情報作成
		//Edge information creation
		List<LinkInfo> linkList = new ArrayList<LinkInfo>();
		for(Node linkNode :returnNodeList(LINKNODE_NAME)) {
			EntityNode entitylinknode = linkNode.asEntity();
			linkList.add(new LinkInfo(getLiteral(entitylinknode,PROPERTY_NAME_N1),getLiteral(entitylinknode,PROPERTY_NAME_N2),
					getLiteral(entitylinknode,PROPERTY_NAME_TYPE),linkNode.getId()));

		}
		this.graphData.setLinkList(linkList);

		// 2021/07 AW start 更新日時は、バックグラウンド同期から呼ばれた場合は更新しない
		if (syncTimelineList == null) {
			// 更新日時取得
			//Obtained at the time of the update
			long time = Utils.currentEpochtime();
			this.graphData.setUpdateTime(time);
		}

		TinyLog.debug(new Throwable().getStackTrace()[0], false, "◆END");

		return graphData.getGraphNode();
	}



	/**
	 * GraphNodeInfoに登録されているEntityNodeのリストを取得する
	 * Get the list of EntityNodes registered with controlNode
	 * @return 実体ノードのリスト
	 *  list of entity nodes
	 */
	public List<EntityNode> getEntityNodeList() {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		if (graphData == null) {
			return null;
		}

		List<EntityNode> nodes = new ArrayList<EntityNode>();
		for (GraphNodeInfo n : graphData.getGraphNodeInfoList()) {
			nodes.add(n.getGraphNodeEntity());
		}

		return nodes;
	}


	/**
	 * 座標を取得
	 * Get coordinates
	 * @param NodeID
	 * @return Queue<Double>
	 */
	public Queue<Double> getNodeCoords(String nodeId) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		// 2021/2 AW ADD start
		//対象node取得
		//Get target node
		GraphNodeInfo node = graphData.getGraphNodeInfo(nodeId);
		Queue<Double> coordsQueue = new LinkedList<Double>();

		//取得できない場合、処理中止
		//f the data cannot be retrieved, the process is aborted.
		if(node == null) {
			coordsQueue.add(0.0);
			coordsQueue.add(0.0);
		} else {
			//x,yの値を格納
			//Stores the values of x and y
			coordsQueue.add(node.getX());
			coordsQueue.add(node.getY());

		}
		return coordsQueue;

		// 2021/2 AW ADD end

		// 2021/2 AW DEL start
		//EntryProperty ep = graphData.nodeMap.get(nodeId);
		//if (ep == null || ep.x_ycoord == null || ep.x_ycoord == "" || ep.x_ycoord == "0_0") {
		//	return "0,0";
		//} else {
		//	return ep.x_ycoord;
		//}
		// 2021/2 AW DEL end

	}

	/**
	 * テキストを取得
	 * @param NodeID
	 * @return
	 */
	/*
	public String getNodeText(String nodeId) {
		//logger.trace("◆START node={}", nodeId);

		// 2021/2 AW ADD start
		//対象IDからタイムラインノードを取得
		GraphNode node = graphData.getNode(nodeId);

		String text = node.getCnt();
		return text == null || text == "" ? "" : text;
		// 2021/2 AW ADD end

		// 2021/2 AW DEL start

		//EntryProperty ep = graphData.nodeMap.get(nodeId);
 		//String text = ep.getText();
		//if (text == null || text == "") {
		//	return "";
		//} else {
		//	return text;
		//}
		// 2021/2 AW DEL end

	}
	*/

	/*
	public String getClassType(String nodeId) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		GraphNodeInfo node = graphData.getNode(nodeId);

		String classTypeName = node.getClassType();
		return classTypeName;
	}
	*/


	public List<String> getCntList(String nodeId) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		GraphNodeInfo node = graphData.getGraphNodeInfo(nodeId);

		List<String> cntList = node.getCntList();
		return cntList;
	}

	public String getDisplayName(String nodeId) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		GraphNodeInfo node = graphData.getGraphNodeInfo(nodeId);

		String displayName = node.getDisplayCreator();
		return displayName;
	}

	public Image getImage(String nodeId) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		GraphNodeInfo node = graphData.getGraphNodeInfo(nodeId);

		Image image = node.getImage();
		return image;
	}

	/**
	 * プロパティ追加・変更アクションメソッド（PLR書き込み）
	 * @param node
	 * @param propertyMap
	 */
	public void addPropertiesByDialog(EntityNode node, Map<String, List<Object>> propertyMap) {

		// プロパティ書き込み
		//Property Entry
		PlrActor.setProperties(node, propertyMap);

		// 2021/07 AW start 本来ここでは対象ノード（タイムラインアイテム）のみを同期する必要がある。
		// 但し、現在の作りでは、ノード追加時もこのメソッドが呼ばれているため
		// ここで位置情報をもつグラフノードも同期する。
		// 同じノードに対する同期が2回走ってしまうのを防ぐため

		// 同期
		//Synchronisation
		//plrAct.syncSimple(node);
		syncWithControlTimeLine(node);
		// 2021/07 AW end

		// cntがある場合は、GraphData上のGraphNodeも書き換え
		//If cnt is present, GraphNode on GraphData is also rewritten
		if (propertyMap.containsKey("cnt")) {

			List<Object> cntList = propertyMap.get("cnt");

			// Stringにする
			List<String> cntStrList = new ArrayList<String>();
			for (Object o: cntList) {
				cntStrList.add(String.valueOf(o));
			}

			GraphNodeInfo dataNode = graphData.getGraphNodeInfo(node.getNodeId());
			dataNode.setCntList(cntStrList);
		}

		// 更新時刻取得設定
		//Write graph data and set update time acquisition
		long time = Utils.currentEpochtime();
		getGraphData().setUpdateTime(time);

		return;
	}

	/**
	 * グラフデータの比較
	 * Comparison of graph data
	 * @param sourceGraph
	 * @param targetGraph
	 * @return
	 */
	/*
	public boolean compareGraphData(GraphData sourceGraph, GraphData targetGraph) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		// 比較元先ノードマップを取得
		//Get source node map
		List<GraphNodeInfo> sourceNodeList = sourceGraph.getGraphNodeInfoList();
		List<GraphNodeInfo> targetNodeList = targetGraph.getGraphNodeInfoList();

		// 全てのノードIDが比較先と一致するかチェック
		// 1. count
		//logger.debug("souceNodeCount={}", sourceNodeList.size());
		//logger.debug("targetNodeCount={}", targetNodeList.size());

		if ((sourceNodeList.size() != targetNodeList.size())) {
			return false;
		}

		// 2. keyの一致チェック
		//Key match check

		for (GraphNodeInfo node : sourceNodeList) {

			String sourceNodeId = node.getCoordinateNodeId();

			boolean equalFlag = false;
			GraphNodeInfo targetNode = null;
			for (GraphNodeInfo n: targetNodeList) {
				String targetNodeId = n.getCoordinateNodeId();
				if (sourceNodeId.equals(targetNodeId)) {
					equalFlag = true;
					targetNode = n;
					break;
				}
			}

			if (!equalFlag) {
				// nodeIdがtargetに存在しない
				//logger.debug("targetNode not Exist.");
				return false;
			}

			else {

				// 3. 位置、テキストの一致チェック
				//node targetnode = targeNodeList.get(targeNodeList.indexOf(node));
				//logger.debug("sourceCoordX={}", node.getX());
				//logger.debug("sourceCoordY={}", node.getY());
				//logger.debug("targetCoordX={}", targetNode.getX());
				//logger.debug("targetCoordY={}", targetNode.getY());

				if (node.getX() != targetNode.getX() || node.getY() != targetNode.getY()) {
					return false;
				}

				// cntのリスト
				//List of cnt
				List<String> sourceCntList = node.getCntList();
				List<String> targetCntList = targetNode.getCntList();

				if (sourceCntList.size() != targetCntList.size()) {
					return false;
				}

				for (int i = 0; i < sourceCntList.size(); ++i) {
					String sourceCnt = sourceCntList.get(i);
					String targetCnt = targetCntList.get(i);
					//logger.debug("sourceText={}", sourceText);
					//logger.debug("targetText ={}", targetText );

					if (sourceCnt == null && targetCnt == null) {

					} else if (sourceCnt != null && targetCnt == null) {
						return false;
					} else if (sourceCnt == null && targetCnt != null) {
						return false;
					} else {
						if (!sourceCnt.equals(targetCnt)) {
							return false;
						}
					}
				}
			}
		}

		// 全てのリンクを比較
		// 全てのリンクが比較先と一致するかチェック
		// 比較元先リンクリストを取得
		//Get the list of links and check if it matches the destination
		List<LinkInfo> sourceEdgesList = sourceGraph.getLinkList();
		List<LinkInfo> targetEdgesList = targetGraph.getLinkList();

		// 1. count
		//logger.debug("sourceEdgesList={}", sourceEdgesList.size());
		//logger.debug("targetEdgesList={}", targetEdgesList.size());

		if (sourceEdgesList.size() != targetEdgesList.size()) {
			return false;
		}

		// 2. リンク元、リンク先、テキスト
		//Link source, link destination, text
		for (LinkInfo sourceEdges: sourceEdgesList) {
			String sourceParentNodeId = sourceEdges.getSourceNodeId();
			String sourceChildNodeId = sourceEdges.getTargetNodeId();

			String sourceLink = sourceEdges.getLinkType();

			//logger.debug("sourceParentNodeId={}", sourceParentNodeId);
			//logger.debug("sourceChildNodeId={}", sourceChildNodeId);
			//logger.debug("sourceLink={}", sourceLink);

			boolean equalFlg = false;
			for (LinkInfo targetEdges: targetEdgesList) {
				String targetParentNodeId = targetEdges.getSourceNodeId();
				String targetChildNodeId = targetEdges.getTargetNodeId();

				String targetLink= targetEdges.getLinkType();

				//logger.debug("targetParentNodeId={}", targetParentNodeId);
				//logger.debug("targetChildNodeId={}", targetChildNodeId);
				//logger.debug("targetLink={}", targetLink);

				if (sourceParentNodeId == null ||
						sourceChildNodeId == null ||
						sourceLink == null) {

					continue;
				}

				if (sourceParentNodeId.equals(targetParentNodeId) &&
						sourceChildNodeId.equals(targetChildNodeId) &&
						sourceLink.equals(targetLink) ) {

					// 値がすべて同じ場合は次のノードへ
					//If all values are the same, go to the next node
					equalFlg = true;
					break;
				}
			}
			if (!equalFlg) {
				// 値がすべて同じものがない場合
				//If no values are all the same
				return false;
			}
		}

		//logger.trace("◆END");
		return true;
	}
	*/


	// 2021/2 AW DEL start
	/**
	 * 座標を変更する（PLR書き込み）
	 * @param nodeId
	 * @param coords
	 */
	//private void changeCoords(String nodeId, double coordsX, double coordsY) {
	//private void changeCoords(String nodeId, String coords) {
	// 2021/2 AW REP end
//		//logger.debug("◆START node={}, coords={}", nodeId, coords);
		// 2021/2 AW ADD start

		//対象node取得
		//Node node  = coordsNode(nodeId);

		//nullにならない想定
		//nullの場合はエラーメッセージ表示にしたい。
//		if(node ==null) {
//			return;
//		}
//		EntityNode entityNode = node.asEntity();
//
//		plrAct.setCoordsValue(entityNode, PROPERTY_NAME_X, "*", coordsX);
//		plrAct.setCoordsValue(entityNode, PROPERTY_NAME_Y, "*", coordsY);
//		syncWithControlTimeLine(entityNode);
//
//		graphData.getNode(nodeId).setX(coordsX) ;
//		graphData.getNode(nodeId).setY(coordsY);
		// 2021/2 AW ADD end

		// 2021/2 AW DEL start

		//EntryProperty ep = graphData.nodeMap.get(nodeId);

		// PLR書き込み
		//plrAct.setLiteral(ep.entryNode, EntryProperty_literal_coords, "*", coords);

		// GraphData書き込み
		//ep.change(EntryProperty_literal_coords, coords);
		//setEntityProperty(nodeId, EntryProperty_literal_coords, coords);
		// 2021/2 AW DEL end

	//}
	// 2021/2 AW DEL end

	// 2020/11 AW DEL start
	/**
	 * グラフサイズを書き込み（PLR書き込み）（アクションメソッド）
	 * @param coords
	 */
	/*
	public void changeGraphSize() {
		//logger.debug("◆START");

		GraphData graphData = getGraphData();
		String size = String.valueOf(graphData.getGraphSizeX()) + ',' + String.valueOf(graphData.getGraphSizeY());

		// PLR書き込み
		plrAct.setLiteral(graphData.getParentNode(),  NAME_GRAPH_SIZE, "*", size);
		// 同期
		plrAct.syncSimple(graphData.getParentNode());

		//logger.debug("write(PLR). graphSizeX={}, graphSizeY={}", graphData.getGraphSizeX(),  graphData.getGraphSizeY());
	}
	*/
	// 2020/11 AW DEL end

	/**
	 * 指定ノードはrootノードとして表示可能かを取得する
	 *
	 * @param node ノードID
	 * @return true:表示可能 false:表示不可
	 */
	/*
	public boolean isVisibleRoot(EntityNode node) {
		if (ctrlNode.entry.containsKey(plrAct.getId(node))) {
			EntryProperty ep = ctrlNode.entry.get(plrAct.getId(node));

			if (ep.visible.equals("true") && ep.root.equals("true")) {
				return true;
			}
		}

		return false;
	}
	*/


	/**
	 * 指定IDのノードを取得する
	 * Get a node with a specified ID
	 *
	 * @param node_id ノードID
	 * @return EntityNode
	 */
	// 2021/2 AW REP start
	public EntityNode getNode(String nodeId) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		// 2021/2 AW ADD start
		// チャネルのルートノードを取得
		//EntityNode rootChannelNode = plrAct.getRootChannelNode();
		/*
		EntityNode rootChannelNode = getGraphData().getControlTimeLineItem().getTimelineRootNode();

		//以下はplractorへ移植予定
		for (EntityNode nodeList : getTimelineItems(rootChannelNode)) {
			if (nodeList.getId().equals(nodeId)) {
				return nodeList;
			}
		}
		*/
		// 2021/2 AW ADD end

		// 2021/2 AW DEL start
		//if (graphData.nodeMap.containsKey(node_id)) {
		//	return graphData.nodeMap.get(node_id).node;
		//}
		//if (ctrlNode.entry.containsKey(node_id)) {
		//	return ctrlNode.entry.get(node_id).node;
		//}
		// 2021/2 AW DEL end

		GraphNodeInfo graphNode = getGraphData().getGraphNodeInfo(nodeId);
		if (graphNode == null) {
			return null;
		} else {
			return graphNode.getGraphNodeEntity();

		}

		//return null;
	}


	/**
	 * PLRContensDataを取得する
	 * Get PLRContentsData
	 *
	 * @return PLRContentsData
	 */
	/*
	public PLRContentsData getOss() {
		//logger.trace("◆START");

		return contentsData;
	}
	*/

	/**
	 * 指定ノードの入力可能属性を取得する（第一階層のみ）
	 *
	 * @param item_id OntologyItem ID(#〜)
	 * @return 属性リスト
	 */
	/*
	public List<PLRContentsData.InputtableProperty> getPropertyMenu(String item_id) {
		//logger.trace("◆START");

		OntologyItem item = contentsData.getNode(item_id);
		List<PLRContentsData.InputtableProperty> list = new ArrayList<>();
		for (PLRContentsData.InputtableProperty ip : contentsData.AllPropertyMenu(item)) {
			if (ip.item.subPropertyOf == null) {

				list.add(ip);
			}
		}

		return list;
	}
	*/

	/**
	 * リンクの追加（PLR書き込み）（アクションメソッド）
	 * Adding a link (PLR writing) (action method)
	 * @param parentNode
	 * @param childNode
	 * @param rel
	 * @param ontItem
	 */
	public void addEdge(EntityNode parentNode, EntityNode childNode, String rel) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		if(undoSwitch) {
			String s="addEdge";
			//FIXME 2021/2 AW DEL start
			//command c = new command(s, newEdge, null, null, null, null, null);
			//commandRecord.push(c);
			// 2021/2 AW DEL end
		}

		// PLR書き込み
		// リンク情報をインナーノードで登録
		//Register the inner node with the node.
		EntityNode newEntity = plrAct.createInnerNode(getGraphData().getGraphNode(), LINKNODE_NAME, PROPERTY_NAME_LINK);
		Map<String, String> literalInputMap = new HashMap<String, String>();
		literalInputMap.put(PROPERTY_NAME_TYPE, rel);
		literalInputMap = setMap_n1_n2(literalInputMap ,parentNode,childNode);
		setLiteral(literalInputMap, newEntity);

		// 同期
		//Synchronisation
		plrAct.syncSimple(getGraphData().getGraphNode());

		// グラフデータ書き込み
		//Write variable
		graphData.addLinkInfo(new LinkInfo(parentNode.getId(), childNode.getId(), rel, newEntity.getId()));

		// 更新時刻取得設定
		//Write graph data and set update time acquisition
		long time = Utils.currentEpochtime();
		getGraphData().setUpdateTime(time);

	}

	/**
	 * リンクの削除（PLR書き込み）（アクションメソッド）
	 * Delete link (PLR write) (action method)
	 * @param parentNode
	 * @param childNode
	 */
	public void deleteEdge(EntityNode parentNode, EntityNode childNode) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		// 2021/2 AW ADD start
		List<Node> nodeList = returnLinknode(parentNode.getId(),childNode.getId());
		//削除
		//Delete
		for(Node node : nodeList) {
			plrAct.removeProperty(graphData.getGraphNode(), node, LINKNODE_NAME);

			//syncWithControlTimeLine(node.asEntity());
		}

		// 同期
		//synchronization
		plrAct.syncSimple(graphData.getGraphNode());

		//変数削除
		//Delete variable
		List<LinkInfo> linkNodeListInGraphData = returnLinknodeInGraphData(plrAct.getId(parentNode),plrAct.getId(parentNode));
		for(LinkInfo edge : linkNodeListInGraphData) {
			graphData.deleteLink(edge);
				}
		// 更新時刻取得設定
		//Update time acquisition setting
		long time = Utils.currentEpochtime();
		getGraphData().setUpdateTime(time);

		// 2021/2 AW ADD end


		// 2021/2 AW DEL start

		//List<NodeInfo<Node>> list = this.list(parentNode.asEntity());
		//List<NodeInfo<Node>> list = getPLRPropertyList(parentNode.asEntity());

		//for (NodeInfo<Node> ni : list) {
		///	if (ni.name.equals("#rel")) {
		//		Map<String, Node> properties = plrAct.listToMap(ni.getNode().asEntity());
		//		if (properties.containsKey(AppProperty.RANGE)
		//				&& (properties.get("range").asEntity().getURI().equals(childNode.getURI()))) {
		//			OntologyItem ontitem = contentsData.getNode("#rel");
		//			this.removeRelationItem(parentNode, ontitem, childNode, ni.node.asEntity());
//
		//		}

			//	if (undoSwitch) {
			//		String s = Thread.currentThread().getStackTrace()[2].getMethodName();
					// commandRecord.add(s+)
			//		command c = new command(s, parentNode, childNode, null, null, null,null);
			//		commandRecord.push(c);
			//	}
			//}

		//}

		// 同期
		//plrAct.syncSimple(graphData.getParentNode());

		//Iterator<Edges> iterator = getGraphData().getEdgesList().iterator();
		//Iterator<Edges> iterator = this.edges.iterator();
		//while (iterator.hasNext()) {
		//	Edges temp = iterator.next();
		//	if (temp.parent_node.getURI().equals(parentNode.getURI())) {
		//		if (temp.child_node != null && temp.child_node.getURI().equals(childNode.getURI())) {
					// 更新時刻取得設定
		//			long time = Utils.currentEpochtime();
		//			getGraphData().setUpdateTime(time);
		//			iterator.remove();
		//			break;
		//		}
		//	}
		//}
		// 2021/2 AW DEL end
	}
	// nodeTpye basically means "#Entity"


	/**
	 * リンクの編集（PLR書き込み）（アクションメソッド）
	 * Editing links (PLR writing) (action method)
	 * @param parentNodeID
	 * @param childNodeID
	 * @param rel
	 */
	public void editEdge(String parentNodeID, String childNodeID, String rel) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		// 2021/2 AW ADD start
		//対象node取得
		//Get target node
		List<Node> nodeList = returnLinknode(parentNodeID,childNodeID);
		//削除プロパティ保持node
		//delete property holding node
		EntityNode controlTimelineitem = graphData.getGraphNode();
		Map<String, String> literalInputMap = new HashMap<String, String>();
		literalInputMap.put(PROPERTY_NAME_TYPE, rel);
		for(Node node :nodeList) {
			setLiteral(literalInputMap, node.asEntity());
		}
		// 2021/2 AW ADD end

		// 2021/2 AW DEL start
		//int n = this.edges.size();
		//EntityNode parentNode = getNode(parentNodeID);
		//EntityNode childNode = getNode(childNodeID);

		// PLR更新
		//List<NodeInfo<Node>> list = getPLRPropertyList(parentNode.asEntity());
		//for (NodeInfo<Node> ni : list) {
		//	if (ni.name.equals("#rel")) {
		//		Map<String, Node> properties = plrAct.listToMap(ni.getNode().asEntity());
		//		if (properties.containsKey(AppProperty.RANGE) && (properties.get("range").asEntity().getURI().equals(childNode.getURI()))) {
		//			this.setRelationLiteral(parentNode, "#rel", ni.node.asEntity(), rel);
		//		}
		//	}
		//}

		// 2021/2 AW DEL end

		// 2021/2 AW REP start

		// 同期
		plrAct.syncSimple(controlTimelineitem);
		//plrAct.syncSimple(graphData.getParentNode());
		// 2021/2 AW REP end

		// 2021/2 AW ADD start
		List<LinkInfo> linkNodeListInGraphData = returnLinknodeInGraphData(parentNodeID,childNodeID);
		for(LinkInfo edge : linkNodeListInGraphData) {
			edge.setLinkType(rel);
		}
		// 2021/2 AW ADD end

		// 2021/2 AW DEL start

		//　グラフデータ更新
		//List<Edges> updateEdgesList = updateEdges(new Edges(parentNode, childNode, rel));
		//getGraphData().setEdgesList(updateEdgesList);
		// 2021/2 AW DEL end

		/*
		int n = this.edges.size();
		EntityNode parentNode = getNode(parentNodeID);
		EntityNode childNode = getNode(childNodeID);

		updateEdges(new Edges(parentNode, childNode, rel));
		List<NodeInfo<Node>> list = this.list(parentNode.asEntity());
		for (NodeInfo<Node> ni : list) {
			if (ni.name.equals("#rel")) {
				Map<String, Node> properties = plrAct.listToMap(ni.getNode().asEntity());
				if (properties.containsKey(AppProperty.RANGE)
						&& (properties.get("range").asEntity().getURI().equals(childNode.getURI()))) {
					this.setRelationLiteral(parentNode, "#rel", ni.node.asEntity(), rel);
				}

			}
		}
		*/

		// 更新時刻取得設定
		//Update time acquisition settings
		long time = Utils.currentEpochtime();
		getGraphData().setUpdateTime(time);

	}

	/**
	 * 矢印方向逆転（PLR書き込み）
	 * Arrow direction reversed (PLR write)
	 * @param parentNode
	 * @param childNode
	 */
	public void reverseEdge(EntityNode parentNode, EntityNode childNode) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		//対象node取得
		//Get target node
		List<Node> linkNodeList = returnLinknode(plrAct.getId(parentNode),plrAct.getId(childNode));
		Map<String, String> literalInputMap = new HashMap<String, String>();
		literalInputMap = setMap_n1_n2(literalInputMap ,childNode,parentNode);
		for(Node node : linkNodeList) {
			setLiteral(literalInputMap, node.asEntity());
			// 同期
			//syncWithControlTimeLine(node.asEntity());
		}
		plrAct.syncSimple(getGraphData().getGraphNode());

		// グラフデータ書き込み
		//Data
		List<LinkInfo> linkNodeListInGraphData = returnLinknodeInGraphData(plrAct.getId(childNode),plrAct.getId(parentNode));
		if(linkNodeListInGraphData.size() != 0) {
			for(LinkInfo edge : linkNodeListInGraphData) {
				edge.setSourceNodeId(plrAct.getId(childNode));
				edge.setTargetNodeId(plrAct.getId(parentNode));
			}
		}

		// 更新時刻取得設定
		//Write graph data and Set update time acquisition
		long time = Utils.currentEpochtime();
		getGraphData().setUpdateTime(time);
	}

	// 2021/2 AW ADD start
	/**
	 * linkがすでに作成されているか、bool値を返す。
	 * 作成されていれば true,作成されていなければ false
	 * Return a bool value if the link has already been created (true if it has been created, false if it is not created)
	 * @param parentNode
	 * @param childNode
	 * @return bool値
	 */
	public boolean addEdgeJudge(EntityNode parentNode, EntityNode childNode) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		boolean retrunbool = continsLinknode(plrAct.getId(parentNode),plrAct.getId(childNode));

		return retrunbool;
	}


	// 2021/2 AW ADD end

	// childNode should be created by 'createInnerNode' method


	/**
	 * ノードの追加（PLR書き込み）（アクションメソッド）
	 * Adding a node (PLR writing) (action method)
	 * @param coordsx
	 * @param coordsy
	 * @param classType
	 * @return
	 */
	public EntityNode addNode(double coordsX, double coordsY, String classType) {
		TinyLog.debug(new Throwable().getStackTrace()[0], false, "◆START");

		// チャネルのルートノードを取得
		//Get the root node of a channel
		EntityNode rootChannelNode = getGraphData().getGraphNode().getTimelineRootNode();

		//タイムラインアイテムを取得
		//Get timeline items
		EntityNode controlTimelineNode = getGraphData().getGraphNode();

		//どちらもかnullである場合は処理中止
		//取得できないことはありえない想定
		///if controlTimelineNode and rootChannelNode is null then abort process
		if (controlTimelineNode == null || rootChannelNode == null) {
			return null;
		}

		//タイムラインノードを作成する。（PLR書き込み）
		//Create a timeline node. (PLR write)
		TinyLog.debug(new Throwable().getStackTrace()[0], false, "timeline item create start.");
		EntityNode newNode = plrAct.createTimelineItem(rootChannelNode, classType);
		//EntityNode nwNode = createGraphInnerNode(nodeType, Boolean.TRUE, coords);
		TinyLog.debug(new Throwable().getStackTrace()[0], false, "timeline item create end.");

		//@ノードでインナーノードを登録する。
		TinyLog.debug(new Throwable().getStackTrace()[0], false, "inner node create start.");
		EntityNode newEntity = plrAct.createInnerNode(controlTimelineNode, NODE_NAME, PROPERTY_NAME_NODE);
		Map<String, String> literalInputMap = new HashMap<String, String>();

		//インナーノードのリテラル、node,x,yを設定
		//Set the literal of the inner node, node,x,y
		literalInputMap.put(PROPERTY_NAME_NODE, plrAct.getId(newNode));
		setLiteral(literalInputMap, newEntity);

		plrAct.setCoordsValue(newEntity, PROPERTY_NAME_X, "*", coordsX);
		plrAct.setCoordsValue(newEntity, PROPERTY_NAME_Y, "*", coordsY);
		TinyLog.debug(new Throwable().getStackTrace()[0], false, "inner node create end.");

		// 同期、ノード本体とノードのグラフノードを同期する
		//Synchronisation, synchronising the node body with the node's graph node

		// 2021/07 AW start
		// 同期を複数回実施するのを防ぐため、後続の処理で同期するよう修正
		//syncWithControlTimeLine(newNode);
		// 2021/07 AW end

		//変数への登録
		//Registering variables
		//GraphNode tmpNode = new GraphNode(newEntity.getId(),newNode.getId());
		GraphNodeInfo tmpNode = new GraphNodeInfo(newNode, newEntity);
		tmpNode.setX(coordsX);
		tmpNode.setY(coordsY);
		tmpNode.setCntList(new ArrayList<String>());
		graphData.addGraphNodeInfo(tmpNode);

		if (undoSwitch) {
			String s = "addNode";
			//FIXME 2021/2 AW DEL start
			//command c = new command(s, nwNode, null, null, null, null,null);
			//commandRecord.push(c);
			// 2021/2 AW DEL end
		}

		// 更新時刻取得設定
		//Write graph data and Set update time acquisition
		long time = Utils.currentEpochtime();
		getGraphData().setUpdateTime(time);

		return newNode;
	}

	/**
	 * ノードの削除（PLR書き込み）（アクションメソッド）
	 * Deleting a node (PLR writing) (action method)
	 * @param node
	 */
	public void deleteNode(EntityNode node) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		String nodeId = node.getNodeId();

		List<Node> nodeList = returnLinknode(node.getId());
		//削除
		//Delete
		for(Node n : nodeList) {
			plrAct.removeProperty(graphData.getGraphNode(), n, LINKNODE_NAME);
			// 同期
			// あとでまとめてやるので不要
			//syncWithControlTimeLine(node.asEntity());
		}

		//変数削除
		//Delete variable
		List<LinkInfo> linkNodeListInGraphData = returnLinknodeInGraphData(plrAct.getId(node),plrAct.getId(node));
		for(LinkInfo edge : linkNodeListInGraphData) {
			graphData.deleteLink(edge);
		}

		//対象node取得
		//Get target node
		Node targetnode = coordsNode(node.getId());

		EntityNode controlTimelineitem = graphData.getGraphNode();
		//削除
		//Delete
		plrAct.removeProperty(controlTimelineitem, targetnode, NODE_NAME);
		// 2021/2 AW ADD end

		if (undoSwitch) {
			String s = Thread.currentThread().getStackTrace()[2].getMethodName();
			// commandRecord.add(s+)
			//FIXME 2021/2 AW DEL start
			//command c = new command(s, node, null, null, null, null,null);
			//commandRecord.push(c);
			// 2021/2 AW DEL end

		}

		// 2021/2 AW DEL start
		// リンクの削除
		//Iterator<Edges> iterator = getGraphData().getEdgesList().iterator();
		//Iterator<Edges> iterator = this.edges.iterator();
		//while (iterator.hasNext()) {
		//	Edges temp = iterator.next();
		//	if (temp.parent_node.getURI().equals(node.getURI())) {
		//		if (temp.child_node == null) {
		//			iterator.remove();
		//			//remove(node);
		//			removeEntityNode(node);
		//			break;
		//		} else {
		//			deleteEdge(node, temp.child_node);
					//remove(node);
		//			removeEntityNode(node);
		///			break;
		//		}
		//	} else if (temp.child_node != null) {
		//		if (temp.child_node.getURI().equals(node.getURI())) {
		//			deleteEdge(temp.parent_node, node);
					// iterator.remove();
					//remove(node);
		//			removeEntityNode(node);
		//			break;
		//		}
		//	}
		//}
		//remove(node);
		//removeEntityNode(node);
		// 2021/2 AW DEL end

		// 同期 位置情報ノードのみ
		// Synchronisation Location node only
		plrAct.syncSimple(controlTimelineitem);

		// graphDataのノード情報を削除
		graphData.removeGraphNodeInfo(nodeId);

		// 更新時刻取得設定
		//Write graph data and Set update time acquisition
		long time = Utils.currentEpochtime();
		getGraphData().setUpdateTime(time);
	}

	// 2021/2 AW DEL start
	/**
	 * ノードのテキストを設定する（PLR書き込み）
	 * @param nodeID
	 * @param cnt
	 */
	// 2021/2 AW REP start
	//private void updateCnt(String nodeId, String cnt) {
	//private void updateCnt(String nodeID, String cnt) {
	// 2021/2 AW REP end
		//logger.debug("◆START");

		// 2021/2 AW ADD start
		//EntityNode timeLineNode = getTimeLimeNode(nodeId);
		//Map<String, String> literalInputMap = new HashMap<String, String>();

		//literalInputMap.put(PROPERTY_NAME_CNT, cnt);
		//setLiteral(literalInputMap, timeLineNode);
		//syncWithControlTimeLine(timeLineNode);

		//node node = graphData.getNode(nodeId);
		//if (node != null) {
		//	graphData.getNode(nodeId).setCnt(cnt);
		//}
		// 2021/2 AW ADD end

		// 2021/2 AW DEL start
		//EntityNode node = getNode(nodeID);
		//plrAct.setLiteral(node, "#cnt", "*", cnt);

		// GraphDataの書き込み
		//setEntityProperty(nodeID, EntryProperty_literal_text, cnt);
		// 2021/2 AW DEL end
	//}
	// 2021/2 AW DEL end

	/**
	 * ノードの座標変更時、アクションメソッド（PLR書き込み）
	 *  Action method (PLR write) when changing coordinates of node.
	 * @param nodeId
	 * @param coordsX
	 * @param coordsY
	 */
	public void updateNodeCoords(String nodeId, double coordsX, double coordsY) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		// 2021/2 AW ADD start

		//対象node取得
		//Get target node
		Node node  = coordsNode(nodeId);

		//nullにならない想定
		//nullの場合はエラーメッセージ表示にしたい。
		if(node ==null) {
			return;
		}
		EntityNode entityNode = node.asEntity();

		plrAct.setCoordsValue(entityNode, PROPERTY_NAME_X, "*", coordsX);
		plrAct.setCoordsValue(entityNode, PROPERTY_NAME_Y, "*", coordsY);
		//syncWithControlTimeLine(entityNode);
		EntityNode controlTimelineitem = graphData.getGraphNode();
		plrAct.syncSimple(controlTimelineitem);

		graphData.getGraphNodeInfo(nodeId).setX(coordsX) ;
		graphData.getGraphNodeInfo(nodeId).setY(coordsY);
		// 2021/2 AW ADD end


		// 2021/2 AW DEL start

		// 座標保存
		//changeCoords(nodeId,coordsX,coordsY);
		//changeCoords(nodeId, coords);
		// 2021/2 AW DEL end

		// 同期
		// 2021/2 AW DEL start
		//EntityNode node = getNode(nodeId);
		//plrAct.syncSimple(node);
		// 2021/2 AW DEL end

		// 更新時刻取得設定
		//Write graph data and Set update time acquisition
		long time = Utils.currentEpochtime();
		getGraphData().setUpdateTime(time);
	}

	/**
	 * ノードのテキスト変更時、アクションメソッド（PLR書き込み）
	 * @param nodeId
	 * @param cnt
	 * @param coords
	 */
	// 2021/2 AW REP start
	/*
	public void updateNodeText(String nodeId, String cnt, double coordsX, double coordsY) {
	//public void updateNodeText(String nodeId, String cnt, String coords) {
	// 2021/2 AW REP end


		//logger.trace("◆START");

		// 2021/2 AW DEL start
		// テキスト保存
		//updateCnt(nodeId, cnt);
		// 2021/2 AW DEL end

		// 2021/2 AW ADD start
		EntityNode timeLineNode = getTimeLimeNode(nodeId);
		Map<String, String> literalInputMap = new HashMap<String, String>();

		literalInputMap.put(PROPERTY_NAME_CNT, cnt);
		setLiteral(literalInputMap, timeLineNode);

		// 同期
		//syncWithControlTimeLine(timeLineNode);
		plrAct.syncSimple(timeLineNode);

		GraphNode node = graphData.getNode(nodeId);
		if (node != null) {
			graphData.getNode(nodeId).setCnt(cnt);
		}
		// 2021/2 AW ADD end

		// 2021/2 AW DEL start
		// 2020/11 AW ADD start
		// 元の座標取得
		//EntryProperty ep = graphData.nodeMap.get(nodeId);
		//String epCoords = ep.getX_ycoord();
		//String[] epCoordsArray = epCoords.split(",");

		// パラメータの座標の高さ取得
		//String[] coordsArray = coords.split(",");

		//String newCoords = epCoordsArray[0] + ',' + epCoordsArray[1] + ',' + coordsArray[2];
		// 2020/11 AW ADD end
		// 2021/2 AW DEL end

		// 座標（ノードの高さ）保存

		// 2021/2 AW DEL start

		// 2020/11 AW REP start
		//changeCoords(nodeId, coordsX,coordsY);
		//changeCoords(nodeId, coords);
		//changeCoords(nodeId, newCoords);
		// 2020/11 AW REP end
		// 同期
		//EntityNode node = getNode(nodeId);
		//plrAct.syncSimple(node);
		// 2021/2 AW ADD start
		//syncWithControlTimeLine(node);
		// 2021/2 AW ADD end

		// 2021/2 AW DEL end

	}
	*/

	// 2021/2 AW DEL start
	/**
	 * グラフサイズ拡張時、アクションメソッド
	 * 全てのノードの座標を指定された値でずらします（加減算します）(PLR書き込み）
	 * @param paramX
	 * @param paramY
	 */
	//public void changeAllCoords(double paramX, double paramY) {
//		//logger.debug("◆START");

	//	Map<String, EntryProperty> nodeMap = getGraphData().getNodeMap();
	//	for (Map.Entry<String, EntryProperty> e: nodeMap.entrySet()) {
	//		String nodeId = e.getKey();
	//		String coords = e.getValue().getX_ycoord();
	//		String[] coordsArray = coords.split(",");
	//		double nodeX = Double.parseDouble(coordsArray[0]);
	//		double nodeY = Double.parseDouble(coordsArray[1]);

	//		nodeX += paramX;
	//		nodeY += paramY;

	//		StringBuilder sb = new StringBuilder()
	//				.append(nodeX)
	//				.append(',')
	//				.append(nodeY);

	//		if (coordsArray.length > 2) {
	//			sb.append(',');
	//			sb.append(coordsArray[2]);
	//		}
			// 保存
	//		changeCoords(nodeId, sb.toString());
	//	}

		// 同期
	//	EntityNode graphNode = getGraphData().getParentNode();
	//	plrAct.syncSimple(graphNode);

	//}
	// 2021/2 AW DEL end


	/**
	 * メニューのUndo
	 */
	public void exc_undo() {
		undoSwitch = false;
		command c = commandRecord.pop();
		switch (c.methodName) {
		case "addNode":
 			deleteNode(c.n1);
			break;
		case "deleteNode":
 			//ctrlNode.undoDelete(c.n1.getNodeId());
			// ノード削除をUndoする
			// Undoは、不可視→可視
			//FIXME 2021/2 AW DEL start
			//EntryProperty ep = graphData.nodeMap.get(c.n1.getNodeId());
			// 2021/2 AW DEL end

			// PLR書き込み
			//FIXME 2021/2 AW DEL start
			//plrAct.setLiteral(ep.entryNode, EntryProperty_literal_visible, "*", "true");
			// 2021/2 AW DEL end

			// 同期
			//FIXME 2021/2 AW DEL start
			//plrAct.syncSimple(ep.entryNode);
			// 2021/2 AW DEL end

			// GraphData書き込み
			//ep.change(EntryProperty_literal_visible, "true");
			//FIXME 2021/2 AW DEL start
			//setEntityProperty(c.n1.getNodeId(), EntryProperty_literal_visible, "true");
			// 2021/2 AW DEL end

			break;
		case "MoveNode":
  			break;
		case "addEdge":
			//FIXME 2021/2 AW DEL start
 			//deleteEdge(c.e1);
			// 2021/2 AW DEL end

		case "deleteEdge":
 			//ctrlNode.undoDelete(c.e1);
		}

		undoSwitch = true;
	}




	// 2021/2 AW ADD start
	/**
	 * タイムラインアイテムとなるノードと、その位置情報をもつノードを同期する。
	 * Synchronize the target node with the node that has @node.
	 * @param node
	 * @throws Exception
	 */
	public void syncWithControlTimeLine(EntityNode node) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		// 対象ノードを同期
		//Synchronise target node
		plrAct.syncSimple(node);

		// 位置情報があるノードを同期
		// Synchronize nodes with location information
		EntityNode controllTimeLineNode = getGraphData().getGraphNode();
		plrAct.syncSimple(controllTimeLineNode);

	}


// ---------------------------------------------------

	/**
	 * リテラルノードから値を取得する
	 * Retrieving a value from a literal node
	 * @param en　対象Node
	 * @param s 対象プロパティ名
	 * @return　String　値
	 */
	private String getLiteral(EntityNode node, String propertyName) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		if(node.getProperty(propertyName).size() !=0) {
			return node.getProperty(propertyName).get(0).asLiteral().getValue() instanceof String ?
						(String) node.getProperty(propertyName).get(0).asLiteral().getValue() :
						String.valueOf(node.getProperty(propertyName).get(0).asLiteral().getValue());
		} else {
			return null;
		}
	}

	/**
	 * controlTimelineitemのプロパティからequalIdに一致するnodeを取得する。
	 * Gets the node that matches equalId from the controlTimelineitem property.
	 * @param parentProperty 親idのプロパティ、@node,@linkが入る想定
	 * @param childProperty　子のnodeのプロパティ名,type:nodeはcnt,type:linkはx,yを想定
	 * @param equalId 取得したいid
	 * @return　node returnNode
	 */
	private List<Node> returnNodeList(String parentProperty) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		return graphData.getGraphNode().getProperty(parentProperty);
	}

	/**
	 * Map変数を対象Nodeに設定する。
	 *  Set the Map variable to the target Node.
	 * @param literalInputMap 対象の変数Map
	 * @param en 対象Node
	 */
	private void setLiteral(Map<String, String> literalInputMap, EntityNode entityNode) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		for (String key : literalInputMap.keySet()) {
			plrAct.setLiteral(entityNode, key, "*", literalInputMap.get(key));
		}
	}

	/**
	 * 座標ノード取得の共通化
	 * Common coordinate node acquisition
	 * @param nodeId
	 * @return
	 */
	private Node coordsNode(String nodeId) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		for (Node node : returnNodeList(NODE_NAME)) {

			EntityNode entityNode = node.asEntity();
			//引数のidと、cntプロパティが一致した場合

			if (equalproperty(entityNode,PROPERTY_NAME_NODE,nodeId)) {
				return node;
			}
		}
		return null;
	}

	/**
	 * 条件式文字列が多いので分離
	 * 現状三ケ所利用
	 * Separate conditional strings as there are many of them
	 * Currently three locations are used
	 * @param en
	 * @param childProperty
	 * @param equalId
	 * @return bool
	 */
	private boolean equalproperty(EntityNode node,String childProperty, String equalId) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		return getLiteral(node, childProperty).equals(equalId);
	}

	/**
	 * linklistに該当情報があるか確認
	 * Check the linklist for the relevant information
	 * @param n1Id　n1ID
	 * @param n2Id　n2ID
	 * @return bool あればtrue
	 */
	private boolean continsLinknode(String n1Id,String n2Id) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		//ローカル変数参照
		List<LinkInfo> node = returnLinknodeInGraphData(n1Id,n2Id);
		//クラウドドライブ参照
		//List<Node> node = returnLinknode(n1Id,n2Id);
		if(node.size() == 0) {
			return false;
		}
		return true;
	}

	/**
	 * graphDataのlinkクラスからedgeクラスを取得する。
	 * Get the edge class from the link class of graphData.
	 * @param n1Id
	 * @param n2Id
	 * @return
	 */
	private List<LinkInfo> returnLinknodeInGraphData(String n1Id,String n2Id) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		List<LinkInfo> returnLinkList = new ArrayList<LinkInfo>();
		for(LinkInfo edge :graphData.getLinkList()) {

			//if ((!(n1Id.equals(n2Id)) && (edge.getParent_node().equals(n1Id) && edge.getChild_node().equals(n2Id))
			//		|| (edge.getParent_node().equals(n2Id) && edge.getChild_node().equals(n1Id)))
			//		||(n1Id.equals(n2Id) && (edge.getParent_node().equals(n1Id) || edge.getChild_node().equals(n2Id)))) {

			if ((!(n1Id.equals(n2Id)) && (edge.getSourceNodeId().equals(n1Id) && edge.getTargetNodeId().equals(n2Id))
					|| (edge.getSourceNodeId().equals(n2Id) && edge.getTargetNodeId().equals(n1Id)))
					||(n1Id.equals(n2Id) && (edge.getSourceNodeId().equals(n1Id) || edge.getTargetNodeId().equals(n2Id)))) {
				returnLinkList.add(edge);
			}
		}
		return returnLinkList;
	}

	/**
	 * @linkプロパティからnodeを取得する。
	 * Retrieves a node from the @link property.
	 *
	 * @param n1Id
	 * @param n2Id
	 * @return　n1,n2プロパティから一致したnodeList
	 */
	private List<Node> returnLinknode(String n1Id,String n2Id) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		List<Node> returnLinkList = new ArrayList<Node>();
		//プロパティをループ
		for (Node n : returnNodeList(LINKNODE_NAME)) {
			EntityNode en = n.asEntity();
			//引数のidと、cntプロパティが一致した場合
			if ((!(n1Id.equals(n2Id)) && (equalproperty(en, PROPERTY_NAME_N1, n1Id) && equalproperty(en, PROPERTY_NAME_N2, n2Id))
					|| (equalproperty(en, PROPERTY_NAME_N1, n2Id) && equalproperty(en, PROPERTY_NAME_N2, n1Id)))
					||(n1Id.equals(n2Id) && (equalproperty(en, PROPERTY_NAME_N1, n1Id) || equalproperty(en, PROPERTY_NAME_N2, n2Id)))) {
				returnLinkList.add(n);
			}
		}
		return returnLinkList;
	}

	/**
	 * @linkプロパティからnodeを取得する。
	 * node削除用でn1,n2どちらかに該当idがあるリストを取得
	 * @param n1id
	 * @return　n1プロパティから一致したnodelist
	 */
	private List<Node> returnLinknode(String n1Id) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		return returnLinknode(n1Id,n1Id);
	}

	/**
	 * string 親id,string　子id,をmapに追加する。
	 * link追加、link逆転で利用する。
	 * @param literalInputMap　格納変数
	 * @param parentNode　親Node
	 * @param childNode 子Node
	 * @return 格納後変数
	 */
	private Map<String, String> setMap_n1_n2(Map<String, String> literalInputMap ,EntityNode parentNode, EntityNode childNode) {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		literalInputMap.put(PROPERTY_NAME_N1, parentNode.getId());
		literalInputMap.put(PROPERTY_NAME_N2, childNode.getId());
		return literalInputMap;
	}

	/**
	 * リンクのリストを取得する
	 * @return
	 */
	public List<LinkInfo> getEdges() {
		TinyLog.trace(new Throwable().getStackTrace()[0], false, "◆START");

		return graphData.getLinkList();
	}

//-------------------------------------------------------------------------------------------

	/**
	 * プロフィールからEntityNodeのcreatorの表示名（名前）を取得し返す
	 * 取得できない場合は、自分の場合は"自分"、自分以外の場合はメールアドレスを返す
	 * @param node
	 * @return
	 */
	private String getDisplayName(EntityNode node) {

		// ログインユーザ
		String loginUser = plrAct.getUserID();

		// 作成者creator
		String creator = (String) PlrActor.getPropertySingleValue(node, "creator");

		// storageType を削除する
		// 最初の文字から初めて見つかる':'の文字までを削除
		String user = creator.substring(creator.indexOf(":") + 1);

		// プロフィール取得(キャッシュ）
		Map<OntologyItem, List<ProfileItem>> ontologyItemListMap = ProfileManager.getCache(AppController.getStorage(), creator, true);
		String profileName = FriendInfoUtils.getName(ontologyItemListMap);

		// ログインユーザと作成者が同じ場合
		if (user.equals(loginUser)) {

			if (profileName != null) {

				return profileName;

			} else {
				// プロフィールを取得できない場合

				//TODO
				// ロケールが日本の場合は"自分"(Personary準拠）
				if (Locale.getDefault().getLanguage().equals("ja")) {
					return "自分";
				} else {
					return "myself";
				}
			}

		} else {
			// ログインユーザと作成者が異なる場合
			if (profileName != null) {

				return profileName;

			} else {
				// プロフィールを取得できない場合
				//TODO
				return user;

			}
		}
	}


	private Image getDisplayImage(EntityNode node) {

		// 作成者creator
		String creator = (String) PlrActor.getPropertySingleValue(node, "creator");
		// プロフィール取得(キャッシュ）
		Map<OntologyItem, List<ProfileItem>> ontologyItemListMap = ProfileManager.getCache(AppController.getStorage(), creator, true);
		BufferedImage bImage = FriendInfoUtils.getPictureThumbnailImage(ontologyItemListMap);
		if (bImage == null) {

			return null;

		} else {

			Image image = SwingFXUtils.toFXImage(bImage, null);
			return image;
		}
	}

	public boolean isMyNode(EntityNode node) {

		// ログインユーザ
		String loginUser = plrAct.getUserID();

		// 作成者creator
		String creator = (String) PlrActor.getPropertySingleValue(node, "creator");

		// storageType を削除する
		// 最初の文字から初めて見つかる':'の文字までを削除
		String user = creator.substring(creator.indexOf(":") + 1);

		// ログインユーザと作成者が同じ場合
		if (user.equals(loginUser)) {
			return true;
		} else {
			return false;
		}
	}


	//-----------------------------------

	/**
	 * コピーペースト用
	 * @param coordsX
	 * @param coordsY
	 * @param parentCopyNode
	 * @return
	 */
	public EntityNode pasteNode(double coordsX, double coordsY,EntityNode parentCopyNode) {

		//対象外プロパティ名リスト
		List<String> ignoreList = List.of("begin","end","creator");

		String copyNodeType = parentCopyNode.getType().get(0);
		Map<String, List<Object>> pastePropertyMap = new HashMap<>();

		// コピー元プロパティ
		Map<String, List<Node>> copyPropertyMap = PlrActor.getProperties(parentCopyNode);

		for (Map.Entry<String, List<Node>> m: copyPropertyMap.entrySet()) {
			if(!ignoreList.contains(m.getKey())) {
				List<Object> tmpList = new ArrayList<>();

				for(Node n : m.getValue()) {
					if(n.isLiteral()) {
						tmpList.add(n.asLiteral().getValue());

					} else if(n.isEntity()) {
						//TODO
						// ph2

					} else if(n.isFile()) {
						//TODO
						// ph3
					}
					pastePropertyMap.put(m.getKey(),tmpList);
				}
			}
		}

		//コピー先ノード作成
		EntityNode newNode = addNode(coordsX, coordsY, copyNodeType);

		// プロパティコピー
		PlrActor.setProperties(newNode, pastePropertyMap);
		plrAct.syncSimple(newNode);

		return newNode;
	}

	/**
	 * GraphSetting消去（デバッグ用）
	 */
	public void removeGraphSetting() {

		EntityNode graphNode = getGraphData().getGraphNode();
		boolean result = graphNode.removeProperty("graphSetting");

		plrAct.syncSimple(graphNode);

		TinyLog.debug(new Throwable().getStackTrace()[0], false, "◆START removeGraphSetting result=" + result);

	}
}
